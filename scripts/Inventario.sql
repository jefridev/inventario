USE [master]
GO
/****** Object:  Database [Inventario]    Script Date: 27/3/2017 1:21:42 p.m. ******/
CREATE DATABASE [Inventario]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Inventario', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\Inventario.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Inventario_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\Inventario_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Inventario] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Inventario].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Inventario] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Inventario] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Inventario] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Inventario] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Inventario] SET ARITHABORT OFF 
GO
ALTER DATABASE [Inventario] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Inventario] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Inventario] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Inventario] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Inventario] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Inventario] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Inventario] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Inventario] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Inventario] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Inventario] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Inventario] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Inventario] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Inventario] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Inventario] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Inventario] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Inventario] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Inventario] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Inventario] SET RECOVERY FULL 
GO
ALTER DATABASE [Inventario] SET  MULTI_USER 
GO
ALTER DATABASE [Inventario] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Inventario] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Inventario] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Inventario] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Inventario] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Inventario', N'ON'
GO
ALTER DATABASE [Inventario] SET QUERY_STORE = OFF
GO
USE [Inventario]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Inventario]
GO
/****** Object:  Table [dbo].[Ajuste]    Script Date: 27/3/2017 1:21:43 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ajuste](
	[IDAjuste] [int] IDENTITY(1,1) NOT NULL,
	[IDEmpresa] [int] NOT NULL,
	[IDAlmacen] [int] NOT NULL,
	[IDTipoAjuste] [int] NOT NULL,
	[NumeroAjuste] [int] NOT NULL,
	[IDUsuario] [int] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_Ajuste] PRIMARY KEY CLUSTERED 
(
	[IDAjuste] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AjusteProducto]    Script Date: 27/3/2017 1:21:44 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AjusteProducto](
	[IDAjuste] [int] NOT NULL,
	[IDProducto] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
 CONSTRAINT [PK_AjusteProducto_1] PRIMARY KEY CLUSTERED 
(
	[IDAjuste] ASC,
	[IDProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Almacen]    Script Date: 27/3/2017 1:21:44 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Almacen](
	[IDAlmacen] [int] IDENTITY(1,1) NOT NULL,
	[IDSucursal] [int] NOT NULL,
	[IDEmpleadoResposable] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
 CONSTRAINT [PK_Almacen] PRIMARY KEY CLUSTERED 
(
	[IDAlmacen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Caja]    Script Date: 27/3/2017 1:21:44 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Caja](
	[IDCaja] [int] IDENTITY(1,1) NOT NULL,
	[IDSucursal] [int] NOT NULL,
	[IDUsuario] [int] NOT NULL,
	[IDTanda] [varchar](3) NOT NULL,
	[FechaInicio] [datetime] NOT NULL,
	[FechaCierre] [datetime] NOT NULL,
	[MontoInicial] [decimal](18, 2) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_Caja] PRIMARY KEY CLUSTERED 
(
	[IDCaja] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CategoriaProducto]    Script Date: 27/3/2017 1:21:44 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoriaProducto](
	[IDCategoriaProducto] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
 CONSTRAINT [PK_CategoriaProducto] PRIMARY KEY CLUSTERED 
(
	[IDCategoriaProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cierre]    Script Date: 27/3/2017 1:21:44 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cierre](
	[IDCierre] [int] IDENTITY(1,1) NOT NULL,
	[IDUsuario] [int] NOT NULL,
	[IDCaja] [int] NOT NULL,
	[IDSucursal] [int] NOT NULL,
	[IDTanda] [varchar](3) NOT NULL,
	[TotalDigitado] [decimal](18, 2) NOT NULL,
	[TotalFacturado] [decimal](18, 2) NOT NULL,
	[Cobrado] [decimal](18, 2) NOT NULL,
	[TotalBruto] [decimal](18, 2) NOT NULL,
	[TotalNeto] [decimal](18, 2) NOT NULL,
	[Sobrante] [decimal](18, 2) NOT NULL,
	[Faltante] [decimal](18, 2) NOT NULL,
	[Desembolso] [decimal](18, 2) NOT NULL,
	[TotalGeneral] [decimal](18, 2) NOT NULL,
	[Tarjeta] [decimal](18, 2) NOT NULL,
	[Cheque] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Cierre] PRIMARY KEY CLUSTERED 
(
	[IDCierre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 27/3/2017 1:21:44 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[IDCliente] [int] IDENTITY(1,1) NOT NULL,
	[IDTipoCliente] [int] NOT NULL,
	[IDEmpresa] [int] NOT NULL,
	[IDTipoIdentificacion] [int] NOT NULL,
	[Identificacion] [varchar](20) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[LimiteCredito] [decimal](18, 2) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[IDCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ClienteCorreoElectronico]    Script Date: 27/3/2017 1:21:44 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClienteCorreoElectronico](
	[IDCliente] [int] NOT NULL,
	[IDCorreoElectronico] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_ClienteCorreoElectronico] PRIMARY KEY CLUSTERED 
(
	[IDCliente] ASC,
	[IDCorreoElectronico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ClienteDireccion]    Script Date: 27/3/2017 1:21:44 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClienteDireccion](
	[IDCliente] [int] NOT NULL,
	[IDDireccion] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_ClienteDireccion] PRIMARY KEY CLUSTERED 
(
	[IDCliente] ASC,
	[IDDireccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ClienteTelefono]    Script Date: 27/3/2017 1:21:44 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClienteTelefono](
	[IDCliente] [int] NOT NULL,
	[IDTelefono] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_ClienteTelefono] PRIMARY KEY CLUSTERED 
(
	[IDCliente] ASC,
	[IDTelefono] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Comprobante]    Script Date: 27/3/2017 1:21:44 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comprobante](
	[IDCompronte] [int] IDENTITY(1,1) NOT NULL,
	[IDEmpresa] [int] NOT NULL,
	[IDTipoComprobante] [varchar](3) NOT NULL,
	[Prefijo] [varchar](20) NOT NULL,
	[Desde] [int] NOT NULL,
	[Hasta] [int] NOT NULL,
	[Proximo] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
 CONSTRAINT [PK_Comprobante] PRIMARY KEY CLUSTERED 
(
	[IDCompronte] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Conduce]    Script Date: 27/3/2017 1:21:44 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Conduce](
	[IDConduce] [int] IDENTITY(1,1) NOT NULL,
	[IDEmpresa] [int] NOT NULL,
	[IDAlmacen] [int] NOT NULL,
	[IDCliente] [int] NOT NULL,
	[IDUsuario] [int] NOT NULL,
	[IDFactura] [int] NULL,
	[Facturado] [bit] NOT NULL,
	[NumeroConduce] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
 CONSTRAINT [PK_Conduce] PRIMARY KEY CLUSTERED 
(
	[IDConduce] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ConduceProducto]    Script Date: 27/3/2017 1:21:44 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConduceProducto](
	[IDConduce] [int] NOT NULL,
	[IDProducto] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
 CONSTRAINT [PK_ConduceProducto] PRIMARY KEY CLUSTERED 
(
	[IDConduce] ASC,
	[IDProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contacto]    Script Date: 27/3/2017 1:21:45 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contacto](
	[IDContacto] [int] NOT NULL,
	[IDTipoContacto] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido] [varchar](50) NOT NULL,
	[Cargo] [varchar](50) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
 CONSTRAINT [PK_Contacto] PRIMARY KEY CLUSTERED 
(
	[IDContacto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ContactoCorreoElectronico]    Script Date: 27/3/2017 1:21:45 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactoCorreoElectronico](
	[IDContacto] [int] NOT NULL,
	[IDCorreoElectronico] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_ContactoCorreoElectronico] PRIMARY KEY CLUSTERED 
(
	[IDContacto] ASC,
	[IDCorreoElectronico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ContactoDireccion]    Script Date: 27/3/2017 1:21:45 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactoDireccion](
	[IDContacto] [int] NOT NULL,
	[IDDireccion] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_ContactoDireccion] PRIMARY KEY CLUSTERED 
(
	[IDContacto] ASC,
	[IDDireccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ContactoTelefono]    Script Date: 27/3/2017 1:21:45 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactoTelefono](
	[IDContacto] [int] NOT NULL,
	[IDTelefono] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_ContactoTelefono] PRIMARY KEY CLUSTERED 
(
	[IDContacto] ASC,
	[IDTelefono] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CorreoElectronico]    Script Date: 27/3/2017 1:21:45 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CorreoElectronico](
	[IDCorreoElectronico] [int] IDENTITY(1,1) NOT NULL,
	[IDTipoCorreoElectronico] [int] NOT NULL,
	[Correo] [varchar](100) NOT NULL,
	[Principal] [bit] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_CorreoElectronico] PRIMARY KEY CLUSTERED 
(
	[IDCorreoElectronico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CuadreCaja]    Script Date: 27/3/2017 1:21:45 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CuadreCaja](
	[IDCuadreCaja] [int] IDENTITY(1,1) NOT NULL,
	[IDEmpresa] [int] NOT NULL,
	[IDSucursal] [int] NOT NULL,
	[IDUsuario] [int] NOT NULL,
	[IDMoneda] [varchar](3) NOT NULL,
	[MontoCalculadoBoucher] [decimal](18, 2) NOT NULL,
	[MontoCalculadoEfectivo] [decimal](18, 2) NOT NULL,
	[MontoCalculadoCheque] [decimal](18, 2) NOT NULL,
	[MontoContadoBoucher] [decimal](18, 2) NOT NULL,
	[MontoContadoEfectivo] [decimal](18, 2) NOT NULL,
	[MontoContadoCheque] [decimal](18, 2) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
 CONSTRAINT [PK_CuadreCaja] PRIMARY KEY CLUSTERED 
(
	[IDCuadreCaja] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Devolucion]    Script Date: 27/3/2017 1:21:45 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Devolucion](
	[IDDevolucion] [int] NOT NULL,
	[IDEmpresa] [int] NOT NULL,
	[IDCliente] [int] NOT NULL,
	[IDFactura] [int] NOT NULL,
	[NumeroDevolucion] [int] NOT NULL,
	[NCF] [varchar](50) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
 CONSTRAINT [PK_Devolucion] PRIMARY KEY CLUSTERED 
(
	[IDDevolucion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DevolucionProducto]    Script Date: 27/3/2017 1:21:45 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DevolucionProducto](
	[IDDevolucion] [int] NOT NULL,
	[IDProducto] [int] NOT NULL,
	[IDAlmacen] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Precio] [decimal](18, 2) NOT NULL,
	[Descuento] [decimal](18, 2) NOT NULL,
	[Impuesto] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_DevolucionProducto] PRIMARY KEY CLUSTERED 
(
	[IDDevolucion] ASC,
	[IDProducto] ASC,
	[IDAlmacen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Direccion]    Script Date: 27/3/2017 1:21:45 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Direccion](
	[IDDireccion] [int] IDENTITY(1,1) NOT NULL,
	[IDTipoDireccion] [int] NOT NULL,
	[DireccionLinea1] [varchar](100) NOT NULL,
	[DireccionLinea2] [varchar](100) NULL,
	[Ciudad] [varchar](60) NOT NULL,
	[CodigoPostal] [varchar](15) NULL,
	[Principal] [bit] NOT NULL,
	[Correspondencia] [bit] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NULL,
 CONSTRAINT [PK__Direccio__2BA9D3845F4B9F88] PRIMARY KEY CLUSTERED 
(
	[IDDireccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Empleado]    Script Date: 27/3/2017 1:21:45 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empleado](
	[IDEmpleado] [int] IDENTITY(1,1) NOT NULL,
	[IDEmpresa] [int] NOT NULL,
	[IDSucursal] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido] [varchar](50) NOT NULL,
	[IDTipoIdentificacion] [int] NOT NULL,
	[Identificacion] [varchar](20) NOT NULL,
	[Cargo] [varchar](50) NOT NULL,
	[FechaIngreso] [datetime] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_Empleado] PRIMARY KEY CLUSTERED 
(
	[IDEmpleado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmpleadoCorreoElectronico]    Script Date: 27/3/2017 1:21:45 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmpleadoCorreoElectronico](
	[IDCorreoElectronico] [int] NOT NULL,
	[IDEmpleado] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_EmpleadoCorreoElectronico] PRIMARY KEY CLUSTERED 
(
	[IDCorreoElectronico] ASC,
	[IDEmpleado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmpleadoDireccion]    Script Date: 27/3/2017 1:21:45 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmpleadoDireccion](
	[IDEmpleado] [int] NOT NULL,
	[IDDireccion] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_EmpleadoDireccion] PRIMARY KEY CLUSTERED 
(
	[IDEmpleado] ASC,
	[IDDireccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmpleadoTelefono]    Script Date: 27/3/2017 1:21:45 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmpleadoTelefono](
	[IDEmpleado] [int] NOT NULL,
	[IDTelefono] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_EmpleadoTelefono] PRIMARY KEY CLUSTERED 
(
	[IDEmpleado] ASC,
	[IDTelefono] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Empresa]    Script Date: 27/3/2017 1:21:45 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empresa](
	[IDEmpresa] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](120) NOT NULL,
	[RNC] [varchar](20) NOT NULL,
	[Descripcion] [varchar](500) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
 CONSTRAINT [PK_Empresa] PRIMARY KEY CLUSTERED 
(
	[IDEmpresa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Entrada]    Script Date: 27/3/2017 1:21:46 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Entrada](
	[IDEntrada] [int] IDENTITY(1,1) NOT NULL,
	[IDEmpresa] [int] NOT NULL,
	[IDAlmacen] [int] NOT NULL,
	[IDSuplidor] [int] NOT NULL,
	[IDUsuario] [int] NOT NULL,
	[NumeroEntrada] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
 CONSTRAINT [PK_Entrada] PRIMARY KEY CLUSTERED 
(
	[IDEntrada] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EntradaProducto]    Script Date: 27/3/2017 1:21:46 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EntradaProducto](
	[IDEntradaProducto] [int] IDENTITY(1,1) NOT NULL,
	[IDEntrada] [int] NOT NULL,
	[IDProducto] [int] NOT NULL,
	[Estante] [varchar](20) NULL,
	[Compartimiento] [varchar](20) NULL,
	[Cantidad] [int] NOT NULL,
	[Costo] [int] NOT NULL,
 CONSTRAINT [PK_EntradaProducto] PRIMARY KEY CLUSTERED 
(
	[IDEntradaProducto] ASC,
	[IDEntrada] ASC,
	[IDProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Factura]    Script Date: 27/3/2017 1:21:46 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Factura](
	[IDFactura] [int] IDENTITY(1,1) NOT NULL,
	[IDEmpresa] [int] NOT NULL,
	[IDSucursal] [int] NOT NULL,
	[IDCliente] [int] NOT NULL,
	[IDUsuario] [int] NOT NULL,
	[IDCaja] [int] NOT NULL,
	[NCF] [varchar](50) NOT NULL,
	[NumeroFactura] [int] NOT NULL,
	[Itbis] [decimal](18, 2) NOT NULL,
	[IDModoPago] [varchar](3) NOT NULL,
	[IDTipoPago] [varchar](3) NOT NULL,
	[MontoRecibido] [decimal](18, 2) NOT NULL,
	[MontoDevuelta] [decimal](18, 2) NOT NULL,
	[MontoTarjeta] [decimal](18, 2) NOT NULL,
	[NumeroAprobacionTarjeta] [varchar](25) NULL,
	[MontoBruto] [decimal](18, 2) NOT NULL,
	[MontoNeto] [decimal](18, 2) NOT NULL,
	[MontoRestante] [decimal](18, 2) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
	[EstatusPago] [varchar](3) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
 CONSTRAINT [PK_Factura] PRIMARY KEY CLUSTERED 
(
	[IDFactura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FacturaProducto]    Script Date: 27/3/2017 1:21:46 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FacturaProducto](
	[IDFactura] [int] NOT NULL,
	[IDProducto] [int] NOT NULL,
	[Precio] [decimal](18, 2) NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Descuentos] [decimal](18, 2) NOT NULL,
	[Itbis] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_FacturaProducto] PRIMARY KEY CLUSTERED 
(
	[IDFactura] ASC,
	[IDProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HistorialCostoProducto]    Script Date: 27/3/2017 1:21:46 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistorialCostoProducto](
	[IDProducto] [int] NOT NULL,
	[FechaInicio] [datetime] NOT NULL,
	[FechaFin] [datetime] NOT NULL,
	[Costo] [decimal](18, 2) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
 CONSTRAINT [PK_HistorialCostoProducto] PRIMARY KEY CLUSTERED 
(
	[IDProducto] ASC,
	[FechaInicio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HistorialPrecioProducto]    Script Date: 27/3/2017 1:21:46 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistorialPrecioProducto](
	[IDProducto] [int] NOT NULL,
	[FechaInicio] [datetime] NOT NULL,
	[FechaFin] [datetime] NOT NULL,
	[Precio] [decimal](18, 2) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
 CONSTRAINT [PK_HistorialPrecioProducto] PRIMARY KEY CLUSTERED 
(
	[IDProducto] ASC,
	[FechaInicio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Log]    Script Date: 27/3/2017 1:21:46 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[IDLog] [int] IDENTITY(1,1) NOT NULL,
	[IDUsuario] [int] NOT NULL,
	[IDObjeto] [int] NOT NULL,
	[IDRol] [int] NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Comentario] [text] NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[IDLog] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ModoPago]    Script Date: 27/3/2017 1:21:46 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModoPago](
	[IDModoPago] [varchar](3) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_ModoPago] PRIMARY KEY CLUSTERED 
(
	[IDModoPago] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Moneda]    Script Date: 27/3/2017 1:21:46 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Moneda](
	[IDMoneda] [varchar](3) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_Moneda] PRIMARY KEY CLUSTERED 
(
	[IDMoneda] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NotaCredito]    Script Date: 27/3/2017 1:21:46 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotaCredito](
	[IDNotaCredito] [int] IDENTITY(1,1) NOT NULL,
	[IDCliente] [int] NOT NULL,
	[IDSucursal] [int] NOT NULL,
	[IDUsuario] [int] NOT NULL,
	[NumeroNotaCredito] [int] NOT NULL,
	[NCF] [varchar](40) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
 CONSTRAINT [PK_NotaCredito] PRIMARY KEY CLUSTERED 
(
	[IDNotaCredito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NotaCreditoFactura]    Script Date: 27/3/2017 1:21:46 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotaCreditoFactura](
	[IDNotaCredito] [int] NOT NULL,
	[IDFactura] [int] NOT NULL,
	[Monto] [decimal](18, 2) NOT NULL,
	[MontoRestante] [decimal](18, 2) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_NotaCreditoFactura] PRIMARY KEY CLUSTERED 
(
	[IDNotaCredito] ASC,
	[IDFactura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Objeto]    Script Date: 27/3/2017 1:21:46 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Objeto](
	[IDObjeto] [int] IDENTITY(1,1) NOT NULL,
	[NombreLogico] [varchar](200) NOT NULL,
	[NombreFisico] [varchar](300) NOT NULL,
	[TipoObjeto] [varchar](10) NOT NULL,
	[IDObjetoRelacionado] [int] NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_Objetos] PRIMARY KEY CLUSTERED 
(
	[IDObjeto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PoliticaSeguridad]    Script Date: 27/3/2017 1:21:46 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PoliticaSeguridad](
	[IDPolitica] [int] IDENTITY(1,1) NOT NULL,
	[LongitudClave] [int] NULL,
	[IncluirLetras] [bit] NOT NULL,
	[IncluirNumeros] [bit] NOT NULL,
	[IncluirCaracteres] [bit] NOT NULL,
	[NumeroIntentos] [int] NULL,
	[TiempoBloqueo] [int] NULL,
	[TiempoTokenCliente] [int] NOT NULL,
	[TiempoTokenWeb] [int] NOT NULL,
	[VigenciaClave] [int] NULL,
	[VigenciaLog] [int] NULL,
 CONSTRAINT [PK_PoliticaSeguridad] PRIMARY KEY CLUSTERED 
(
	[IDPolitica] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Producto]    Script Date: 27/3/2017 1:21:47 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Producto](
	[IDProducto] [int] IDENTITY(1,1) NOT NULL,
	[IDEmpresa] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[CodigoProducto] [varchar](30) NOT NULL,
	[IDTipoProducto] [int] NOT NULL,
	[Color] [varchar](15) NULL,
	[NivelSeguroStock] [smallint] NOT NULL,
	[PuntoReorden] [smallint] NOT NULL,
	[Costo] [decimal](18, 2) NOT NULL,
	[Precio] [decimal](18, 2) NOT NULL,
	[Impuesto] [bit] NOT NULL,
	[Size] [varchar](5) NULL,
	[CodigoUnidadMedidaSize] [varchar](3) NULL,
	[CodigoUnidadMedidaPeso] [varchar](3) NULL,
	[Peso] [decimal](8, 2) NULL,
	[IDSubcategoriaProducto] [int] NULL,
	[FechaInicioVenta] [datetime] NOT NULL,
	[FechaFinVenta] [datetime] NOT NULL,
	[Descontinuado] [bit] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_Producto] PRIMARY KEY CLUSTERED 
(
	[IDProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductoExistencia]    Script Date: 27/3/2017 1:21:47 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductoExistencia](
	[IDProducto] [int] NOT NULL,
	[AlmacenID] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
 CONSTRAINT [PK_ProductoExistencia] PRIMARY KEY CLUSTERED 
(
	[IDProducto] ASC,
	[AlmacenID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Recibo]    Script Date: 27/3/2017 1:21:47 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recibo](
	[IDRecibo] [int] IDENTITY(1,1) NOT NULL,
	[IDCliente] [int] NOT NULL,
	[IDSucursal] [int] NOT NULL,
	[IDCaja] [int] NOT NULL,
	[NumeroRecibo] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
 CONSTRAINT [PK_Recibo] PRIMARY KEY CLUSTERED 
(
	[IDRecibo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReciboFactura]    Script Date: 27/3/2017 1:21:47 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReciboFactura](
	[IDRecibo] [int] NOT NULL,
	[IDFactura] [int] NOT NULL,
	[Monto] [decimal](18, 2) NOT NULL,
	[MontoRestante] [decimal](18, 2) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_ReciboFactura] PRIMARY KEY CLUSTERED 
(
	[IDRecibo] ASC,
	[IDFactura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReciboPago]    Script Date: 27/3/2017 1:21:47 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReciboPago](
	[IDReciboPago] [int] IDENTITY(1,1) NOT NULL,
	[IDRecibo] [int] NOT NULL,
	[IDTipoPago] [varchar](3) NOT NULL,
	[Monto] [decimal](18, 2) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_ReciboPago] PRIMARY KEY CLUSTERED 
(
	[IDReciboPago] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Rol]    Script Date: 27/3/2017 1:21:47 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rol](
	[IDRol] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Descripcion] [varchar](500) NOT NULL,
	[RutaPrincipal] [varchar](100) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_Rol] PRIMARY KEY CLUSTERED 
(
	[IDRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RolObjeto]    Script Date: 27/3/2017 1:21:47 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolObjeto](
	[IDRol] [int] NOT NULL,
	[IDObjeto] [int] NOT NULL,
	[RegistrarLog] [bit] NOT NULL,
 CONSTRAINT [PK_Rol_Objeto] PRIMARY KEY CLUSTERED 
(
	[IDRol] ASC,
	[IDObjeto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SubcategoriaProducto]    Script Date: 27/3/2017 1:21:47 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubcategoriaProducto](
	[IDSubcategoriaProducto] [int] IDENTITY(1,1) NOT NULL,
	[IDCategoriaProducto] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
 CONSTRAINT [PK_SubcategoriaProducto] PRIMARY KEY CLUSTERED 
(
	[IDSubcategoriaProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sucursal]    Script Date: 27/3/2017 1:21:47 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sucursal](
	[IDSucursal] [int] IDENTITY(1,1) NOT NULL,
	[IDEmpresa] [int] NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_Sucursal] PRIMARY KEY CLUSTERED 
(
	[IDSucursal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SucursalContacto]    Script Date: 27/3/2017 1:21:47 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SucursalContacto](
	[IDSucursal] [int] NOT NULL,
	[IDContacto] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_SucursalContacto] PRIMARY KEY CLUSTERED 
(
	[IDSucursal] ASC,
	[IDContacto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SucursalDireccion]    Script Date: 27/3/2017 1:21:47 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SucursalDireccion](
	[IDSucursal] [int] NOT NULL,
	[IDDireccion] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_SucursalDireccion] PRIMARY KEY CLUSTERED 
(
	[IDSucursal] ASC,
	[IDDireccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SucursalTelefono]    Script Date: 27/3/2017 1:21:47 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SucursalTelefono](
	[IDSucursal] [int] NOT NULL,
	[IDTelefono] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_SucursalTelefono] PRIMARY KEY CLUSTERED 
(
	[IDSucursal] ASC,
	[IDTelefono] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Suplidor]    Script Date: 27/3/2017 1:21:47 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Suplidor](
	[IDSuplidor] [int] IDENTITY(1,1) NOT NULL,
	[IDEmpresa] [int] NOT NULL,
	[IDTipoSuplidor] [int] NOT NULL,
	[IDTipoIdentificacion] [int] NOT NULL,
	[Identificacion] [varchar](20) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Comentario] [varchar](200) NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_Suplidor] PRIMARY KEY CLUSTERED 
(
	[IDSuplidor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SuplidorContacto]    Script Date: 27/3/2017 1:21:47 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SuplidorContacto](
	[IDSuplidor] [int] NOT NULL,
	[IDContacto] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_SuplidorContacto] PRIMARY KEY CLUSTERED 
(
	[IDSuplidor] ASC,
	[IDContacto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SuplidorCorreoElectronico]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SuplidorCorreoElectronico](
	[IDSuplidor] [int] NOT NULL,
	[IDCorreoElectronico] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_SuplidorCorreoElectronico] PRIMARY KEY CLUSTERED 
(
	[IDSuplidor] ASC,
	[IDCorreoElectronico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SuplidorDireccion]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SuplidorDireccion](
	[IDSuplidor] [int] NOT NULL,
	[IDDireccion] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_SuplidorDireccion] PRIMARY KEY CLUSTERED 
(
	[IDSuplidor] ASC,
	[IDDireccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SuplidorTelefono]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SuplidorTelefono](
	[IDSuplidor] [int] NOT NULL,
	[IDTelefono] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_SuplidorTelefono] PRIMARY KEY CLUSTERED 
(
	[IDSuplidor] ASC,
	[IDTelefono] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tanda]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tanda](
	[IDTanda] [varchar](3) NOT NULL,
	[Nombre] [varchar](20) NOT NULL,
	[Estatus] [varchar](3) NULL,
 CONSTRAINT [PK_Tanda] PRIMARY KEY CLUSTERED 
(
	[IDTanda] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Telefono]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Telefono](
	[IDTelefono] [int] IDENTITY(1,1) NOT NULL,
	[IDTipoTelefono] [int] NOT NULL,
	[Numero] [varchar](20) NOT NULL,
	[Principal] [bit] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NULL,
 CONSTRAINT [PK_Telefono] PRIMARY KEY CLUSTERED 
(
	[IDTelefono] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoAjuste]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoAjuste](
	[IDTipoAjuste] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_TipoAjuste] PRIMARY KEY CLUSTERED 
(
	[IDTipoAjuste] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoCliente]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoCliente](
	[IDTipoCliente] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
 CONSTRAINT [PK_TipoCliente] PRIMARY KEY CLUSTERED 
(
	[IDTipoCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoComprobante]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoComprobante](
	[IDTipoComprobante] [varchar](3) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TipoComprobante] PRIMARY KEY CLUSTERED 
(
	[IDTipoComprobante] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoContacto]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoContacto](
	[IDTipoContacto] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
 CONSTRAINT [PK_TipoContacto] PRIMARY KEY CLUSTERED 
(
	[IDTipoContacto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoCorreoElectronico]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoCorreoElectronico](
	[IDTipoCorreoElectronico] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_TipoCorreoElectronico] PRIMARY KEY CLUSTERED 
(
	[IDTipoCorreoElectronico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoDireccion]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoDireccion](
	[IDTipoDireccion] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](60) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NULL,
 CONSTRAINT [PK_TipoDireccion] PRIMARY KEY CLUSTERED 
(
	[IDTipoDireccion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoIdentificacion]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoIdentificacion](
	[IDTipoIdentificacion] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_TipoIdentificacion] PRIMARY KEY CLUSTERED 
(
	[IDTipoIdentificacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoPago]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoPago](
	[IDTipoPago] [varchar](3) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_TipoPago] PRIMARY KEY CLUSTERED 
(
	[IDTipoPago] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoProducto]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoProducto](
	[IDTipoProducto] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
 CONSTRAINT [PK_TipoProducto] PRIMARY KEY CLUSTERED 
(
	[IDTipoProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoSuplidor]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoSuplidor](
	[IDTipoSuplidor] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_TipoSuplidor] PRIMARY KEY CLUSTERED 
(
	[IDTipoSuplidor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoTelefono]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoTelefono](
	[IDTipoTelefono] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NULL,
 CONSTRAINT [PK_TipoTelefono] PRIMARY KEY CLUSTERED 
(
	[IDTipoTelefono] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Transferencia]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transferencia](
	[IDTransferencia] [int] IDENTITY(1,1) NOT NULL,
	[IDAlmacenOrigen] [int] NOT NULL,
	[IDAlmacenDestino] [int] NOT NULL,
	[IDUsuarioSolicita] [int] NOT NULL,
	[IDUsuarioRecibe] [int] NOT NULL,
	[FechaSolicita] [datetime] NOT NULL,
	[FechaRecibe] [datetime] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_Transferencia] PRIMARY KEY CLUSTERED 
(
	[IDTransferencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TransferenciaProducto]    Script Date: 27/3/2017 1:21:48 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransferenciaProducto](
	[IDTransferenciaProducto] [int] IDENTITY(1,1) NOT NULL,
	[IDTransferencia] [int] NOT NULL,
	[IDProducto] [int] NOT NULL,
	[CantidadEnvio] [int] NOT NULL,
	[CantidadRecibe] [int] NOT NULL,
	[Comentario] [varchar](500) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_TransferenciaProducto] PRIMARY KEY CLUSTERED 
(
	[IDTransferenciaProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UnidadMedida]    Script Date: 27/3/2017 1:21:49 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnidadMedida](
	[CodigoUnidad] [varchar](3) NOT NULL,
	[Nombre] [varchar](60) NOT NULL,
	[Descripcion] [varchar](200) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
 CONSTRAINT [PK_UnidadMedida] PRIMARY KEY CLUSTERED 
(
	[CodigoUnidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 27/3/2017 1:21:49 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[IDUsuario] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](150) NOT NULL,
	[PasswordHash] [varchar](max) NOT NULL,
	[Salt] [varchar](max) NOT NULL,
	[Nombre] [varchar](100) NULL,
	[Correo] [varchar](150) NULL,
	[Telefono] [varchar](50) NULL,
	[Idioma] [varchar](10) NULL,
	[Intentos] [int] NOT NULL,
	[FechaBloqueo] [date] NULL,
	[Token] [varchar](max) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
	[FechaCambioClave] [datetime] NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[IDUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UsuarioEmpleado]    Script Date: 27/3/2017 1:21:49 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioEmpleado](
	[IDUsuario] [int] NOT NULL,
	[IDEmpleado] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_UsuarioEmpleado] PRIMARY KEY CLUSTERED 
(
	[IDUsuario] ASC,
	[IDEmpleado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UsuarioRol]    Script Date: 27/3/2017 1:21:49 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioRol](
	[IDUsuario] [int] NOT NULL,
	[IDRol] [int] NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_UsuarioRol] PRIMARY KEY CLUSTERED 
(
	[IDUsuario] ASC,
	[IDRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UsuarioToken]    Script Date: 27/3/2017 1:21:49 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioToken](
	[IDUsuarioToken] [int] IDENTITY(1,1) NOT NULL,
	[IDUsuario] [int] NOT NULL,
	[Token] [text] NOT NULL,
	[FechaInicio] [datetime] NOT NULL,
	[FechaExpiracion] [datetime] NOT NULL,
	[FechaSalida] [datetime] NULL,
	[Canal] [varchar](10) NOT NULL,
	[Estatus] [varchar](3) NOT NULL,
 CONSTRAINT [PK_UsuarioToken] PRIMARY KEY CLUSTERED 
(
	[IDUsuarioToken] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Log] ON 

INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (1, 3, 19, 10, CAST(N'2017-03-15T02:12:05.563' AS DateTime), N'Consultar objetos asignados a usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (2, 3, 5, 10, CAST(N'2017-03-15T02:12:06.010' AS DateTime), N'Consultar logs')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (3, 3, 12, 10, CAST(N'2017-03-15T02:12:12.823' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (4, 3, 22, 10, CAST(N'2017-03-15T02:12:14.503' AS DateTime), N'Consultar roles')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (5, 3, 41, 10, CAST(N'2017-03-15T02:12:16.127' AS DateTime), N'Consultar rol por id')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (6, 3, 51, 10, CAST(N'2017-03-15T02:12:17.417' AS DateTime), N'Obtener política del sistema')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (7, 3, 5, 10, CAST(N'2017-03-15T02:12:19.890' AS DateTime), N'Consultar logs')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (8, 3, 12, 10, CAST(N'2017-03-15T02:12:39.993' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (9, 3, 19, 10, CAST(N'2017-03-15T21:05:49.597' AS DateTime), N'Consultar objetos asignados a usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (10, 3, 5, 10, CAST(N'2017-03-15T21:05:52.277' AS DateTime), N'Consultar logs')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (11, 3, 28, 10, CAST(N'2017-03-15T21:05:57.197' AS DateTime), N'Consultar usuarios')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (12, 3, 49, 10, CAST(N'2017-03-15T21:06:00.303' AS DateTime), N'Obtener usuario por id')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (13, 3, 30, 10, CAST(N'2017-03-15T21:06:15.557' AS DateTime), N'Actualizar usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (14, 3, 28, 10, CAST(N'2017-03-15T21:06:17.453' AS DateTime), N'Consultar usuarios')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (15, 3, 19, 10, CAST(N'2017-03-16T14:55:24.400' AS DateTime), N'Consultar objetos asignados a usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (16, 3, 5, 10, CAST(N'2017-03-16T14:55:24.800' AS DateTime), N'Consultar logs')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (17, 3, 19, 10, CAST(N'2017-03-16T19:10:26.410' AS DateTime), N'Consultar objetos asignados a usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (18, 3, 5, 10, CAST(N'2017-03-16T19:10:26.733' AS DateTime), N'Consultar logs')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (19, 3, 28, 10, CAST(N'2017-03-16T19:10:28.710' AS DateTime), N'Consultar usuarios')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (20, 3, 22, 10, CAST(N'2017-03-16T19:10:32.440' AS DateTime), N'Consultar roles')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (21, 3, 28, 10, CAST(N'2017-03-16T19:10:33.313' AS DateTime), N'Consultar usuarios')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (22, 3, 12, 10, CAST(N'2017-03-16T19:10:37.287' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (23, 3, 5, 10, CAST(N'2017-03-16T19:10:38.080' AS DateTime), N'Consultar logs')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (24, 3, 28, 10, CAST(N'2017-03-16T19:10:39.080' AS DateTime), N'Consultar usuarios')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (25, 3, 51, 10, CAST(N'2017-03-16T19:10:40.753' AS DateTime), N'Obtener política del sistema')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (26, 3, 28, 10, CAST(N'2017-03-16T19:10:42.700' AS DateTime), N'Consultar usuarios')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (27, 3, 28, 10, CAST(N'2017-03-16T19:19:44.917' AS DateTime), N'Consultar usuarios')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (28, 3, 19, 10, CAST(N'2017-03-16T19:20:56.677' AS DateTime), N'Consultar objetos asignados a usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (29, 3, 5, 10, CAST(N'2017-03-16T19:20:59.907' AS DateTime), N'Consultar logs')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (30, 3, 5, 10, CAST(N'2017-03-16T19:23:59.557' AS DateTime), N'Consultar logs')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (31, 3, 19, 10, CAST(N'2017-03-16T19:24:22.033' AS DateTime), N'Consultar objetos asignados a usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (32, 3, 5, 10, CAST(N'2017-03-16T19:24:22.183' AS DateTime), N'Consultar logs')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (33, 3, 12, 10, CAST(N'2017-03-16T19:28:31.290' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (34, 3, 20, 10, CAST(N'2017-03-16T19:28:36.707' AS DateTime), N'Consultar tipos de objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (35, 3, 12, 10, CAST(N'2017-03-16T19:28:36.717' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (36, 3, 10, 10, CAST(N'2017-03-16T19:29:01.793' AS DateTime), N'Crear nuevo objeto')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (37, 3, 12, 10, CAST(N'2017-03-16T19:29:03.270' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (38, 3, 22, 10, CAST(N'2017-03-16T19:29:05.443' AS DateTime), N'Consultar roles')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (39, 3, 17, 10, CAST(N'2017-03-16T19:29:06.953' AS DateTime), N'Consultar objetos asignado a un rol')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (40, 3, 41, 10, CAST(N'2017-03-16T19:29:06.957' AS DateTime), N'Consultar rol por id')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (41, 3, 18, 10, CAST(N'2017-03-16T19:29:06.967' AS DateTime), N'Consultar objetos no asignados a rol')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (42, 3, 26, 10, CAST(N'2017-03-16T19:29:08.777' AS DateTime), N'Asignar permisos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (43, 3, 19, 10, CAST(N'2017-03-16T19:29:17.760' AS DateTime), N'Consultar objetos asignados a usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (44, 3, 5, 10, CAST(N'2017-03-16T19:29:17.867' AS DateTime), N'Consultar logs')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (45, 3, 30, 10, CAST(N'2017-03-16T19:29:28.383' AS DateTime), N'Actualizar usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (46, 3, 28, 10, CAST(N'2017-03-16T19:31:26.833' AS DateTime), N'Consultar usuarios')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (47, 3, 19, 10, CAST(N'2017-03-16T19:31:31.820' AS DateTime), N'Consultar objetos asignados a usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (48, 3, 5, 10, CAST(N'2017-03-16T19:31:31.923' AS DateTime), N'Consultar logs')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (49, 3, 19, 10, CAST(N'2017-03-16T19:42:11.080' AS DateTime), N'Consultar objetos asignados a usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (50, 3, 5, 10, CAST(N'2017-03-16T19:42:11.313' AS DateTime), N'Consultar logs')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (51, 3, 12, 10, CAST(N'2017-03-16T19:42:13.520' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (52, 3, 20, 10, CAST(N'2017-03-16T19:42:17.123' AS DateTime), N'Consultar tipos de objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (53, 3, 12, 10, CAST(N'2017-03-16T19:42:17.147' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (54, 3, 12, 10, CAST(N'2017-03-16T19:42:23.047' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (55, 3, 19, 10, CAST(N'2017-03-16T19:59:29.347' AS DateTime), N'Consultar objetos asignados a usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (56, 3, 5, 10, CAST(N'2017-03-16T19:59:29.617' AS DateTime), N'Consultar logs')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (57, 3, 51, 10, CAST(N'2017-03-16T20:01:17.003' AS DateTime), N'Obtener política del sistema')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (58, 3, 28, 10, CAST(N'2017-03-16T20:01:19.077' AS DateTime), N'Consultar usuarios')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (59, 3, 22, 10, CAST(N'2017-03-16T20:01:20.623' AS DateTime), N'Consultar roles')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (60, 3, 5, 10, CAST(N'2017-03-16T20:01:21.447' AS DateTime), N'Consultar logs')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (61, 3, 12, 10, CAST(N'2017-03-16T20:01:22.047' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (62, 3, 19, 10, CAST(N'2017-03-16T20:31:53.987' AS DateTime), N'Consultar objetos asignados a usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (63, 3, 19, 10, CAST(N'2017-03-16T20:32:10.673' AS DateTime), N'Consultar objetos asignados a usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (64, 3, 19, 10, CAST(N'2017-03-16T20:34:04.647' AS DateTime), N'Consultar objetos asignados a usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (65, 3, 19, 10, CAST(N'2017-03-16T20:34:21.393' AS DateTime), N'Consultar objetos asignados a usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (66, 3, 19, 10, CAST(N'2017-03-16T20:37:34.740' AS DateTime), N'Consultar objetos asignados a usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (67, 3, 5, 10, CAST(N'2017-03-16T20:37:37.907' AS DateTime), N'Consultar logs')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (68, 3, 22, 10, CAST(N'2017-03-16T20:37:44.337' AS DateTime), N'Consultar roles')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (69, 3, 41, 10, CAST(N'2017-03-16T20:37:46.443' AS DateTime), N'Consultar rol por id')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (70, 3, 12, 10, CAST(N'2017-03-16T20:37:52.967' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (71, 3, 22, 10, CAST(N'2017-03-16T20:37:57.243' AS DateTime), N'Consultar roles')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (72, 3, 41, 10, CAST(N'2017-03-16T20:37:58.683' AS DateTime), N'Consultar rol por id')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (73, 3, 24, 10, CAST(N'2017-03-16T20:38:02.460' AS DateTime), N'Actualizar rol')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (74, 3, 19, 10, CAST(N'2017-03-16T20:38:11.557' AS DateTime), N'Consultar objetos asignados a usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (75, 3, 12, 10, CAST(N'2017-03-16T20:38:11.717' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (76, 3, 22, 10, CAST(N'2017-03-16T20:38:19.983' AS DateTime), N'Consultar roles')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (77, 3, 41, 10, CAST(N'2017-03-16T20:38:21.163' AS DateTime), N'Consultar rol por id')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (78, 3, 22, 10, CAST(N'2017-03-16T20:40:38.293' AS DateTime), N'Consultar roles')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (79, 3, 12, 10, CAST(N'2017-03-16T20:43:33.513' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (80, 3, 16, 10, CAST(N'2017-03-16T20:43:36.313' AS DateTime), N'Consultar objetos por id')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (81, 3, 20, 10, CAST(N'2017-03-16T20:43:36.313' AS DateTime), N'Consultar tipos de objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (82, 3, 12, 10, CAST(N'2017-03-16T20:43:36.323' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (83, 3, 12, 10, CAST(N'2017-03-16T20:46:49.583' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (84, 3, 16, 10, CAST(N'2017-03-16T20:46:49.583' AS DateTime), N'Consultar objetos por id')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (85, 3, 20, 10, CAST(N'2017-03-16T20:46:49.583' AS DateTime), N'Consultar tipos de objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (86, 3, 19, 10, CAST(N'2017-03-16T20:58:30.307' AS DateTime), N'Consultar objetos asignados a usuario')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (87, 3, 12, 10, CAST(N'2017-03-16T20:58:30.463' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (88, 3, 22, 10, CAST(N'2017-03-16T20:58:32.863' AS DateTime), N'Consultar roles')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (89, 3, 41, 10, CAST(N'2017-03-16T20:58:34.223' AS DateTime), N'Consultar rol por id')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (90, 3, 18, 10, CAST(N'2017-03-16T20:58:34.230' AS DateTime), N'Consultar objetos no asignados a rol')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (91, 3, 17, 10, CAST(N'2017-03-16T20:58:34.233' AS DateTime), N'Consultar objetos asignado a un rol')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (92, 3, 12, 10, CAST(N'2017-03-16T20:58:36.357' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (93, 3, 20, 10, CAST(N'2017-03-16T20:58:38.807' AS DateTime), N'Consultar tipos de objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (94, 3, 16, 10, CAST(N'2017-03-16T20:58:38.807' AS DateTime), N'Consultar objetos por id')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (95, 3, 12, 10, CAST(N'2017-03-16T20:58:38.810' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (96, 3, 12, 10, CAST(N'2017-03-16T21:00:03.780' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (97, 3, 16, 10, CAST(N'2017-03-16T21:00:03.780' AS DateTime), N'Consultar objetos por id')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (98, 3, 20, 10, CAST(N'2017-03-16T21:00:03.780' AS DateTime), N'Consultar tipos de objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (99, 3, 12, 10, CAST(N'2017-03-16T21:00:29.230' AS DateTime), N'Consultar objetos')
GO
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (100, 3, 20, 10, CAST(N'2017-03-16T21:00:29.247' AS DateTime), N'Consultar tipos de objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (101, 3, 16, 10, CAST(N'2017-03-16T21:00:29.320' AS DateTime), N'Consultar objetos por id')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (102, 3, 12, 10, CAST(N'2017-03-16T21:03:19.177' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (103, 3, 16, 10, CAST(N'2017-03-16T21:03:19.177' AS DateTime), N'Consultar objetos por id')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (104, 3, 20, 10, CAST(N'2017-03-16T21:03:19.187' AS DateTime), N'Consultar tipos de objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (105, 3, 12, 10, CAST(N'2017-03-16T21:03:24.903' AS DateTime), N'Consultar objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (106, 3, 16, 10, CAST(N'2017-03-16T21:03:29.150' AS DateTime), N'Consultar objetos por id')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (107, 3, 20, 10, CAST(N'2017-03-16T21:03:29.150' AS DateTime), N'Consultar tipos de objetos')
INSERT [dbo].[Log] ([IDLog], [IDUsuario], [IDObjeto], [IDRol], [Fecha], [Comentario]) VALUES (108, 3, 12, 10, CAST(N'2017-03-16T21:03:29.150' AS DateTime), N'Consultar objetos')
SET IDENTITY_INSERT [dbo].[Log] OFF
SET IDENTITY_INSERT [dbo].[Objeto] ON 

INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (3, N'Módulo de seguridad', N'security', N'Pantalla', NULL, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (4, N'Módulo de logs', N'security/logs', N'Pantalla', 3, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (5, N'Consultar logs', N'security/logs/consultar', N'Pantalla', 4, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (9, N'Módulo de objetos', N'security/objects', N'Pantalla', 3, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (10, N'Crear nuevo objeto', N'security/objects/create', N'Pantalla', 9, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (11, N'Eliminar objeto', N'security/objects/delete', N'Pantalla', 9, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (12, N'Consultar objetos', N'security/objects/consultar', N'Pantalla', 9, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (13, N'Consultar objeto', N'security/objects/consultar-objeto', N'Pantalla', 9, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (14, N'Actualizar objeto', N'security/objects/update', N'Pantalla', 9, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (15, N'Buscar objetos', N'security/objects/buscarObjetos', N'Pantalla', 9, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (16, N'Consultar objetos por id', N'security/objects/consultarPorID', N'Pantalla', 9, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (17, N'Consultar objetos asignado a un rol', N'security/objects/consultarAsignadosRol', N'Pantalla', 9, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (18, N'Consultar objetos no asignados a rol', N'security/objects/consultarNoAsignadosARol', N'Pantalla', 9, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (19, N'Consultar objetos asignados a usuario', N'security/objects/consultarObjetosAsignadosUsuario', N'Pantalla', 9, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (20, N'Consultar tipos de objetos', N'security/objects/consultarTiposObjetos', N'Pantalla', 9, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (21, N'Módulo de roles', N'security/roles', N'Pantalla', NULL, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (22, N'Consultar roles', N'security/roles/consultar', N'Pantalla', 21, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (23, N'Crear nuevo rol', N'security/roles/create', N'Pantalla', 21, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (24, N'Actualizar rol', N'security/roles/update', N'Pantalla', 21, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (25, N'Eliminar rol', N'security/roles/delete', N'Pantalla', 21, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (26, N'Asignar permisos', N'security/roles/asignarPermiso', N'Pantalla', 21, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (27, N'Módulo de  usuario', N'security/users', N'Pantalla', NULL, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (28, N'Consultar usuarios', N'security/users/consultar', N'Pantalla', 27, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (29, N'Crear nuevo usuario', N'security/users/create', N'Pantalla', 27, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (30, N'Actualizar usuario', N'security/users/update', N'Pantalla', 27, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (31, N'Eliminar usuario', N'security/users/delete', N'Pantalla', 27, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (32, N'Regenerar contraseña de usuario', N'security/users/regeneratePassword', N'Pantalla', 27, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (37, N'Consultar log por id', N'security/logs/obtenerLogPorID', N'Pantalla', 4, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (41, N'Consultar rol por id', N'security/roles/obtenerRolPorID', N'Pantalla', 21, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (42, N'Obtener roles asignados por usuario', N'security/roles/asignadosPorUsuario', N'Pantalla', 21, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (43, N'Obtener roles no asignados a usuario', N'security/roles/noAsignadosPorUsuario', N'Pantalla', 21, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (44, N'RemoverPermiso', N'security/roles/removerPermiso', N'Pantalla', 21, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (45, N'Regenerar contraseña de usuario', N'security/users/regeneratePassword', N'Pantalla', 27, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (46, N'Asignar rol a usuario', N'security/users/asignarRol', N'Pantalla', 27, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (48, N'Remover rol a usuario', N'security/users/removerRol', N'Pantalla', 27, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (49, N'Obtener usuario por id', N'security/users/obtenerUsuarioPorID', N'Pantalla', 27, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (50, N'Módulo de políticas', N'security/policies', N'Pantalla', NULL, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (51, N'Obtener política del sistema', N'security/policies/obtenerPolitica', N'Pantalla', 50, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (52, N'Actualizar política del sistema', N'security/policies/update', N'Pantalla', 50, N'ACT')
INSERT [dbo].[Objeto] ([IDObjeto], [NombreLogico], [NombreFisico], [TipoObjeto], [IDObjetoRelacionado], [Estatus]) VALUES (53, N'Cambiar contraseña', N'security/users/cambiarContrasena', N'Pantalla', 27, N'ACT')
SET IDENTITY_INSERT [dbo].[Objeto] OFF
SET IDENTITY_INSERT [dbo].[PoliticaSeguridad] ON 

INSERT [dbo].[PoliticaSeguridad] ([IDPolitica], [LongitudClave], [IncluirLetras], [IncluirNumeros], [IncluirCaracteres], [NumeroIntentos], [TiempoBloqueo], [TiempoTokenCliente], [TiempoTokenWeb], [VigenciaClave], [VigenciaLog]) VALUES (1, 7, 1, 1, 1, 3, 3, 60, 10, 31, 31)
SET IDENTITY_INSERT [dbo].[PoliticaSeguridad] OFF
SET IDENTITY_INSERT [dbo].[Rol] ON 

INSERT [dbo].[Rol] ([IDRol], [Nombre], [Descripcion], [RutaPrincipal], [Estatus]) VALUES (10, N'Administrador de seguridad', N'Encargado de gestionar permisos dentro del sistema. ', N'/security/objects', N'ACT')
SET IDENTITY_INSERT [dbo].[Rol] OFF
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 3, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 4, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 5, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 9, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 10, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 11, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 12, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 13, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 14, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 15, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 16, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 17, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 18, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 19, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 20, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 21, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 22, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 23, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 24, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 25, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 26, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 27, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 28, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 29, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 30, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 31, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 32, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 37, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 41, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 42, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 43, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 44, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 45, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 46, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 48, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 49, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 50, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 51, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 52, 0)
INSERT [dbo].[RolObjeto] ([IDRol], [IDObjeto], [RegistrarLog]) VALUES (10, 53, 0)
SET IDENTITY_INSERT [dbo].[Usuario] ON 

INSERT [dbo].[Usuario] ([IDUsuario], [Username], [PasswordHash], [Salt], [Nombre], [Correo], [Telefono], [Idioma], [Intentos], [FechaBloqueo], [Token], [Estatus], [FechaCambioClave]) VALUES (3, N'Administrador', N'Ay1+ZT5sSl0kSnyGMB7ZKD/xEmv5Ydj7laEp6DMunpM=', N'W3b0K6lNcTjxtltXWQ6hDA==', N'Administrador', N'j.martinez@deadlocksolutions.com', N'809-332-1212', NULL, 0, NULL, N'H2PHLVddQPTV431WaP4aoT1kvUs+nc2KNKw8QF/uL8VvbcdRI89u5zL6nAdFzrkRp1JxuF1kPotodfM9YayDjA==', N'ACT', CAST(N'2017-03-16T20:58:30.250' AS DateTime))
SET IDENTITY_INSERT [dbo].[Usuario] OFF
INSERT [dbo].[UsuarioRol] ([IDUsuario], [IDRol], [Estatus]) VALUES (3, 10, N'ACT')
SET IDENTITY_INSERT [dbo].[UsuarioToken] ON 

INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (32, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYTg0MTA5OWUtMTNiZS00MDc5LTkzOTYtM2ViNmI5MzM2MzY5IiwiaWF0IjoxNDg5MDA1NzE4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMDU3MTcsImV4cCI6MTQ4OTAwNjAxNywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vb2NhbGhvc3Q6MzE3MDMifQ.B8U61Et5FPQ42c7mVQuTYAsXgLGrGJsaZUFJAHrJuPs', CAST(N'2017-03-08T20:42:07.193' AS DateTime), CAST(N'2017-03-08T20:46:57.573' AS DateTime), CAST(N'2017-03-08T20:44:21.120' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (33, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiOTMzZDA4MTUtNGE1OC00MzlhLWFlNjItZDg0NDRjNGVkMzMyIiwiaWF0IjoxNDg5MDA1NzE4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMDU3MTcsImV4cCI6MTQ4OTAwNjAxNywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vb2NhbGhvc3Q6MzE3MDMifQ.VGfpAMrSaoLjDP_XO0G3dd9yOXNdmifxvSfBGVxJp8E', CAST(N'2017-03-08T20:44:20.717' AS DateTime), CAST(N'2017-03-08T20:46:57.573' AS DateTime), CAST(N'2017-03-08T20:45:39.300' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (34, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMWRkNTViZDEtMTQzMC00YmJjLTk0MjItZDQyODJiY2MyMDlmIiwiaWF0IjoxNDg5MDA1NzE4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMDU3MTcsImV4cCI6MTQ4OTAwNjAxNywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vb2NhbGhvc3Q6MzE3MDMifQ.8yyg4gdY4zFoQF-BIU80qFledBlPW7v5cJjJs_dHsIc', CAST(N'2017-03-08T20:45:38.867' AS DateTime), CAST(N'2017-03-08T20:46:57.573' AS DateTime), CAST(N'2017-03-08T21:09:01.497' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (35, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYmMxNGMxMTItYjU1Yi00YmJmLWJkYWQtYTExY2FjZWE2OTE1IiwiaWF0IjoxNDg5MDA3MzIzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMDczMjMsImV4cCI6MTQ4OTAwNzYyMywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vb2NhbGhvc3Q6MzE3MDMifQ.ZKcntdmwtZcnNgf0bYxjAOmgCftSxHaxGHw37geE8xE', CAST(N'2017-03-08T21:09:00.477' AS DateTime), CAST(N'2017-03-08T21:13:43.267' AS DateTime), CAST(N'2017-03-08T21:09:36.637' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (36, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiY2E2OWMzZjktNWM3Zi00ZDk2LWJkYjMtZjZkZTg2YWZhMzIxIiwiaWF0IjoxNDg5MDA3MzIzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMDczMjMsImV4cCI6MTQ4OTAwNzYyMywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vb2NhbGhvc3Q6MzE3MDMifQ.bUXxhaeD4zgBaGWNeHqiI1Dsd_xvMCyRwRLAH7VoM_E', CAST(N'2017-03-08T21:09:36.157' AS DateTime), CAST(N'2017-03-08T21:13:43.267' AS DateTime), CAST(N'2017-03-08T21:15:11.773' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (37, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNGE3NmFiODQtMmYzYS00NGNlLTliZDAtMTJmMzdiYTJjYjBhIiwiaWF0IjoxNDg5MDA3MzIzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMDczMjMsImV4cCI6MTQ4OTAwNzYyMywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vb2NhbGhvc3Q6MzE3MDMifQ.mIA5waZ_2n_4bxeHxFQNOKlL8lAy9fb7B4Hl4C2hda4', CAST(N'2017-03-08T21:15:10.860' AS DateTime), CAST(N'2017-03-08T21:13:43.267' AS DateTime), CAST(N'2017-03-08T21:15:29.827' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (38, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZjVlMjllNTktYjQ5ZC00MzczLWEyNTgtODAzZDE1MzQyNmU2IiwiaWF0IjoxNDg5MDA3MzIzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMDczMjMsImV4cCI6MTQ4OTAwNzYyMywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vb2NhbGhvc3Q6MzE3MDMifQ.UmV0klA_Yy6FHhsWHcyUc8VUnedaPlaZQxm2ZU-juqI', CAST(N'2017-03-08T21:15:29.307' AS DateTime), CAST(N'2017-03-08T21:13:43.267' AS DateTime), CAST(N'2017-03-08T21:51:29.510' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (39, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMjkwNWIxMDAtYzFhYS00MGNiLTgwMWQtMTcyYTNiZmZlM2E0IiwiaWF0IjoxNDg5MDA5ODg1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMDk4ODUsImV4cCI6MTQ4OTAxMDE4NSwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vb2NhbGhvc3Q6MzE3MDMifQ.MY5D1lGWYRUDwSH4yT4SEAIxVVqu77LaeXPN2MsqK48', CAST(N'2017-03-08T21:51:28.273' AS DateTime), CAST(N'2017-03-08T21:56:25.317' AS DateTime), CAST(N'2017-03-09T00:05:39.713' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (40, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYzIyYTc5YTItODAwZC00ZmRkLWEyYTQtYzRkNTM1ZjY5NDk0IiwiaWF0IjoxNDg5MDE3OTM2LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMTc5MzUsImV4cCI6MTQ4OTAxODIzNSwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vb2NhbGhvc3Q6MzE3MDMifQ.UfPNw1sxOYK7LGIB3FrR-OOIASiVpLGvK_eHvhi4OIk', CAST(N'2017-03-09T00:05:38.563' AS DateTime), CAST(N'2017-03-09T00:10:35.660' AS DateTime), CAST(N'2017-03-09T00:41:46.860' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (41, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMmY4ZWI4MTAtN2RhYS00MDczLTg1YjItYTRkN2Y4MDA4ZWU2IiwiaWF0IjoxNDg5MDIwMTAzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMjAxMDIsImV4cCI6MTQ4OTAyMDQwMiwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vb2NhbGhvc3Q6MzE3MDMifQ.gI3H2wWX7l02v9CeGUDkPuTC6k9Cbj3Vp3WGE1-hHoQ', CAST(N'2017-03-09T00:41:45.633' AS DateTime), CAST(N'2017-03-09T00:46:42.893' AS DateTime), CAST(N'2017-03-09T00:43:15.783' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (42, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMjQ2YzAxNzYtYWQxYy00OGEzLTlhN2ItNmUyNmIxNDVlMmI5IiwiaWF0IjoxNDg5MDIwMTAzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMjAxMDIsImV4cCI6MTQ4OTAyMDQwMiwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vb2NhbGhvc3Q6MzE3MDMifQ.Isms43GzrIbS-FDdmEKX2xZmU-FII9nz0ip6XDz_tz0', CAST(N'2017-03-09T00:43:15.177' AS DateTime), CAST(N'2017-03-09T00:46:42.893' AS DateTime), CAST(N'2017-03-09T00:47:18.570' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (43, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMzRlN2JhYmYtYzMyNi00YzFlLTk5NjEtMDg0MGQxNGFhNDMyIiwiaWF0IjoxNDg5MDIwNDM1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMjA0MzQsImV4cCI6MTQ4OTAyMDczNCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vb2NhbGhvc3Q6MzE3MDMifQ.c4awKVJOYBr2iTLYWukAWwvXLvHOO7Us_zWVC8-r6Gw', CAST(N'2017-03-09T00:47:17.243' AS DateTime), CAST(N'2017-03-09T00:52:14.503' AS DateTime), CAST(N'2017-03-09T00:50:46.033' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (44, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMjMxYjcxNjktNGEzZC00ZGU4LWEzZDItYjJmMTY0ZWE5YmUzIiwiaWF0IjoxNDg5MDIwNjQyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMjA2NDIsImV4cCI6MTQ4OTAyMDk0MiwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.SRGOPlEKywe66IQb6sp_qKHvq6nv0OY6hTn9QxoVJ0c', CAST(N'2017-03-09T00:50:44.790' AS DateTime), CAST(N'2017-03-09T00:55:42.020' AS DateTime), CAST(N'2017-03-09T00:52:00.537' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (45, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYTI1NzA5MDQtNWE1OS00MGM5LWI0ZWItYjJiMjZlNWQ3ZmViIiwiaWF0IjoxNDg5MDIwNjQyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMjA2NDIsImV4cCI6MTQ4OTAyMDk0MiwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.SS4m9v2Fs3KoQSQ3iVmjnAnv7XGPMVcWb4LOfukUkgM', CAST(N'2017-03-09T00:51:59.937' AS DateTime), CAST(N'2017-03-09T00:55:42.020' AS DateTime), CAST(N'2017-03-09T00:53:18.480' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (46, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiODkzMThhOTctNzUyZC00MmNkLWI5NzMtOGZhMGYzY2NhYjJjIiwiaWF0IjoxNDg5MDIwNzk0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMjA3OTQsImV4cCI6MTQ4OTAyMTA5NCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMwMDAifQ.TRFMjcP9OEJVyZI1JbH1Kga4V0kOrjZHYRwVgfpTuog', CAST(N'2017-03-09T00:53:17.227' AS DateTime), CAST(N'2017-03-09T00:58:14.110' AS DateTime), CAST(N'2017-03-09T00:58:50.990' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (47, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYzZhNjgzOGYtYmM1Mi00YWQ5LWEzNGMtNDYxMWMyOTRhMDJlIiwiaWF0IjoxNDg5MDIwNzk0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMjA3OTQsImV4cCI6MTQ4OTAyMTA5NCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMwMDAifQ.Wh1iDZENh5KEXHFhB9z-XCm8YwcSNzYNJsCJb1sezrk', CAST(N'2017-03-09T00:58:50.330' AS DateTime), CAST(N'2017-03-09T00:58:14.110' AS DateTime), CAST(N'2017-03-09T01:10:23.580' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (48, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNWQzNTIxNjYtZTMxYy00ZGU0LTg1M2MtMGI4ODRlYjFmNjE0IiwiaWF0IjoxNDg5MDIwNzk0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMjA3OTQsImV4cCI6MTQ4OTAyMTA5NCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMwMDAifQ.2dyIBSbcUuSQbpku8CqCyb63kouv7Vpydk1NxJdUQqo', CAST(N'2017-03-09T01:10:22.907' AS DateTime), CAST(N'2017-03-09T00:58:14.110' AS DateTime), CAST(N'2017-03-09T01:11:54.887' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (49, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiOTJkMjhkMzUtMWM0Ni00YjA0LThiY2YtMzQ0MzUxZDQzZGMyIiwiaWF0IjoxNDg5MDIwNzk0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMjA3OTQsImV4cCI6MTQ4OTAyMTA5NCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMwMDAifQ.Emic_CL7TZ3pNtyQrvJZcVz6oBRdFpqEiEN3HSUrhYo', CAST(N'2017-03-09T01:11:54.227' AS DateTime), CAST(N'2017-03-09T00:58:14.110' AS DateTime), CAST(N'2017-03-09T01:14:06.847' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (50, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYmM0N2Y1ODctYzRiZC00OTE5LWI1ZTItYzhjZWUzZGE3Zjc2IiwiaWF0IjoxNDg5MDIwNzk0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMjA3OTQsImV4cCI6MTQ4OTAyMTA5NCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMwMDAifQ.Q1vA1nES1REWhQEOy5u9isVyHa9i8xR_9CobFf3IghQ', CAST(N'2017-03-09T01:14:06.153' AS DateTime), CAST(N'2017-03-09T00:58:14.110' AS DateTime), CAST(N'2017-03-09T01:16:18.377' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (51, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMTA0ZDA1MDMtYmFiZS00NWY4LTgzMmYtYWM1NjJkODYzOTg1IiwiaWF0IjoxNDg5MDIyMTc0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMjIxNzMsImV4cCI6MTQ4OTAyMjQ3MywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.xkhhopehNW4hggu4tBRjydPIiFphxhwq75ekadB7RHs', CAST(N'2017-03-09T01:16:16.920' AS DateTime), CAST(N'2017-03-09T01:21:13.923' AS DateTime), CAST(N'2017-03-09T01:17:14.803' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (52, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMTA5OTRjMjMtZmUwOC00OTNhLTllMDEtYWQ1NWY5NDE4YmQ4IiwiaWF0IjoxNDg5MDIyMTc0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMjIxNzMsImV4cCI6MTQ4OTAyMjQ3MywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.RwdVXM5vqD381JylQbWw0oJWfxmqEfyJ-5t0DbQIPdI', CAST(N'2017-03-09T01:17:14.070' AS DateTime), CAST(N'2017-03-09T01:21:13.923' AS DateTime), CAST(N'2017-03-09T01:20:19.300' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (53, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNzc4MDA0OWYtMTE0Mi00YTQ0LThlYzUtNDUxNDI0ZDY0NzRhIiwiaWF0IjoxNDg5MDIyMTc0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMjIxNzMsImV4cCI6MTQ4OTAyMjQ3MywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.eVYgv0rhFBxjI9xQnvWpy6hGufNdI8K5S7gbdVuQMhA', CAST(N'2017-03-09T01:20:18.640' AS DateTime), CAST(N'2017-03-09T01:21:13.923' AS DateTime), CAST(N'2017-03-09T01:30:13.123' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (54, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZjc3MmQxNDAtZjhmNS00NDA5LTlhY2MtYTFhZTAyYTlkMmU5IiwiaWF0IjoxNDg5MDIyMTc0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMjIxNzMsImV4cCI6MTQ4OTAyMjQ3MywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.Mxkf2_jIxlLsbjsBSwqpUCErLqaQ3-0UfWLfI3d0xYM', CAST(N'2017-03-09T01:30:12.373' AS DateTime), CAST(N'2017-03-09T01:21:13.923' AS DateTime), CAST(N'2017-03-09T01:31:12.990' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (55, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZjZiYTM1MTgtNDVlNC00MWRiLThmZjctMzdiYjliNWRhOWE3IiwiaWF0IjoxNDg5MDIyMTc0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwMjIxNzMsImV4cCI6MTQ4OTAyMjQ3MywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.zp6huE1UNuy_FOajp7Og8hXNnOGRxvUSw1lLjv29ta8', CAST(N'2017-03-09T01:31:12.220' AS DateTime), CAST(N'2017-03-09T01:21:13.923' AS DateTime), CAST(N'2017-03-09T16:14:15.257' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (56, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMzc0ZTRhYTgtNTU0NS00YzJlLThjMzQtMjFlNmNiM2VmNTdmIiwiaWF0IjoxNDg5MDc2MDU0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwNzYwNTMsImV4cCI6MTQ4OTA3NjM1MywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0._LC6GO5lVqrErzbVwpUSQ18Sa9etlW1MFjH6znyIu1Q', CAST(N'2017-03-09T16:14:15.197' AS DateTime), CAST(N'2017-03-09T16:19:13.830' AS DateTime), CAST(N'2017-03-09T16:20:46.197' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (57, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNDMwNmM2YzYtMGQyMy00ZWYwLWJlNjItMDFmMmYwMjJjOWZmIiwiaWF0IjoxNDg5MDc2MDU0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwNzYwNTMsImV4cCI6MTQ4OTA3NjM1MywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.052uxHcRJWsX-L3hQbb-feaToa2S6Vu8GlXED2eH6d4', CAST(N'2017-03-09T16:20:46.103' AS DateTime), CAST(N'2017-03-09T16:19:13.830' AS DateTime), CAST(N'2017-03-09T16:36:18.113' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (58, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMWE5NjNkYWMtN2IyMi00NjQxLWIxNDAtMmE3NzgwMWU4MTA3IiwiaWF0IjoxNDg5MDc2MDU0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwNzYwNTMsImV4cCI6MTQ4OTA3NjM1MywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.yB0LVq8pVGBC90l1IlUj9jWbfjC91T-D-WrqMaJKMgU', CAST(N'2017-03-09T16:36:17.883' AS DateTime), CAST(N'2017-03-09T16:19:13.830' AS DateTime), CAST(N'2017-03-09T18:08:12.690' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (59, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMjI4OGIzNzUtYzM4ZS00Nzk4LWE0Y2UtNDBmMmJmOWM4OTI4IiwiaWF0IjoxNDg5MDc2MDU0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwNzYwNTMsImV4cCI6MTQ4OTA3NjM1MywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.q1ci7E0uzEapxGbrlTktZL_vLEuUXyEa_dElJ3rnIkA', CAST(N'2017-03-09T18:08:12.397' AS DateTime), CAST(N'2017-03-09T16:19:13.830' AS DateTime), CAST(N'2017-03-09T18:20:02.013' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (60, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYTJjZDJjYWMtMTI1MC00MWUwLWI3MGUtNzEyNDgyYzY5YmIyIiwiaWF0IjoxNDg5MDc2MDU0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwNzYwNTMsImV4cCI6MTQ4OTA3NjM1MywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.Y2323DOeNsyDr0FNOoqb2DsCE9ut9zJ8ige-kBKDGgk', CAST(N'2017-03-09T18:20:01.713' AS DateTime), CAST(N'2017-03-09T16:19:13.830' AS DateTime), CAST(N'2017-03-09T18:37:48.680' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (61, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMjJlZWQ1MGUtYjNiMi00M2EzLWIxZjYtNmZkOGIxNDdhYWE2IiwiaWF0IjoxNDg5MDc2MDU0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkwNzYwNTMsImV4cCI6MTQ4OTA3NjM1MywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.C2jvOVZ8Z8iaFfzNcJADOGhBDtlOkJn_aoJ8blNcVY8', CAST(N'2017-03-09T18:37:48.350' AS DateTime), CAST(N'2017-03-09T16:19:13.830' AS DateTime), CAST(N'2017-03-10T18:03:33.893' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (62, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYzZjYTgzNWItNjUxNC00YmRhLTljOTYtYzYwNDIwNmZkZTA5IiwiaWF0IjoxNDg5MTY5MDEwLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxNjkwMTAsImV4cCI6MTQ4OTE2OTMxMCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.EiMBxgYYTruOVb2K_Zuet1DNQx6Pd4s2QoKJcIdPMI0', CAST(N'2017-03-10T18:03:33.157' AS DateTime), CAST(N'2017-03-10T18:08:30.273' AS DateTime), CAST(N'2017-03-10T18:30:41.143' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (63, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNWE1NzAyMzAtMmU5Zi00OTcyLTg1NGYtZWEwY2QxYTFkMzYxIiwiaWF0IjoxNDg5MTY5MDEwLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxNjkwMTAsImV4cCI6MTQ4OTE2OTMxMCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.jB9c1_p3G5CsxxpAJwuw0CUhzpXkWjk9YROJGjpSUr4', CAST(N'2017-03-10T18:30:40.863' AS DateTime), CAST(N'2017-03-10T18:08:30.273' AS DateTime), CAST(N'2017-03-10T18:32:56.727' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (64, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiOWMwZTI4NzItYTk4MC00Mzk3LThiYTctZThmMzY0NWQwNzU5IiwiaWF0IjoxNDg5MTY5MDEwLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxNjkwMTAsImV4cCI6MTQ4OTE2OTMxMCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.oKcZ82M6SKLZEYphVU70z5CLCtNJo1x4fw4S4ayr2SM', CAST(N'2017-03-10T18:32:56.460' AS DateTime), CAST(N'2017-03-10T18:08:30.273' AS DateTime), CAST(N'2017-03-10T18:35:13.770' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (65, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZTEwMjE5MWEtNWY1OC00YzNhLThiMjktZTdlMjMyMDkyODVhIiwiaWF0IjoxNDg5MTY5MDEwLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxNjkwMTAsImV4cCI6MTQ4OTE2OTMxMCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.M73GRkr6QWDdxgqUpkLtYi3q-s-hakI2-wotUos45h8', CAST(N'2017-03-10T18:35:13.367' AS DateTime), CAST(N'2017-03-10T18:08:30.273' AS DateTime), CAST(N'2017-03-10T20:14:30.343' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (66, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiOWMxNThjNGYtNjFmMC00OTI3LWI2ZTAtNmM0ZDAyZDJhMjdhIiwiaWF0IjoxNDg5MTc2ODU5LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxNzY4NTgsImV4cCI6MTQ4OTE3NzE1OCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.Og0lPYouvn7alOAXBh7x5rQNvEVzhaktihzjGQMHkIg', CAST(N'2017-03-10T20:14:29.200' AS DateTime), CAST(N'2017-03-10T20:19:18.890' AS DateTime), CAST(N'2017-03-10T20:21:04.810' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (67, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNGQyNTA1NmQtZmVjYy00NzBmLThhYmYtOWIyNzZkYjQ2NzdkIiwiaWF0IjoxNDg5MTc3MjYyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxNzcyNjEsImV4cCI6MTQ4OTE3NzU2MSwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.TKWzHUBVYy9xpmYMqZe8Rq2CX_9qw4nvljDjR6CFAvg', CAST(N'2017-03-10T20:21:03.907' AS DateTime), CAST(N'2017-03-10T20:26:01.877' AS DateTime), CAST(N'2017-03-10T20:22:36.960' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (68, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNWYxYjI1ZmEtYmNjNy00YmM3LWJmZDQtZTNlZTJmOTM5ZjJlIiwiaWF0IjoxNDg5MTc3MzU0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxNzczNTMsImV4cCI6MTQ4OTE3NzY1MywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.VGh_F6OK9wGoBfKl62pLEKPNpK52QxmAH-Lg-kRszTo', CAST(N'2017-03-10T20:22:36.053' AS DateTime), CAST(N'2017-03-10T20:27:33.770' AS DateTime), CAST(N'2017-03-10T20:27:24.483' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (69, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZjAyOTY2NGUtNmJjNy00MzNlLWFhY2MtMGVlNjBmZjY4YWUxIiwiaWF0IjoxNDg5MTc3NjQxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxNzc2NDEsImV4cCI6MTQ4OTE3Nzk0MSwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.DSs25gmWW7EPiITjW44vlAgx-ZmLjzdArdySDz_X7CU', CAST(N'2017-03-10T20:27:23.410' AS DateTime), CAST(N'2017-03-10T20:32:21.267' AS DateTime), CAST(N'2017-03-10T20:28:44.737' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (70, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYTMzODg4NzgtNDQ0MS00OGZkLWI4ZjAtMDBlZTBjYTE1NGEwIiwiaWF0IjoxNDg5MTc3NjQxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxNzc2NDEsImV4cCI6MTQ4OTE3Nzk0MSwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzIn0.O69ZDJnbkxnGAD2XDpVJ_k2OZxl-ADBwOxdfzdBdDJE', CAST(N'2017-03-10T20:28:44.253' AS DateTime), CAST(N'2017-03-10T20:32:21.267' AS DateTime), CAST(N'2017-03-10T22:23:26.557' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (71, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiOGIzMmNiYmYtOTExMS00OWU0LTliOTQtOWU0OTNhOTk4OTMzIiwiaWF0IjoxNDg5MTg0NjAyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxODQ2MDIsImV4cCI6MTQ4OTE4NDkwMiwiaXNzIjoiYXNkZjIzZWRzZmFzZGYyMzEyYXNkZjswMTJBMTIzMSIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzE3MDMifQ.chtp08LvUrKKTfujOzRi7S_4WbW6ljQgDA-s19WRm4U', CAST(N'2017-03-10T22:23:25.107' AS DateTime), CAST(N'2017-03-10T22:28:22.377' AS DateTime), CAST(N'2017-03-10T22:27:05.587' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (72, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYzNhNjJiY2UtZWZkNi00ZjViLWI2OWQtNTM5ZjBmYTdhYTljIiwiaWF0IjoxNDg5MTg0NjAyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxODQ2MDIsImV4cCI6MTQ4OTE4NDkwMiwiaXNzIjoiYXNkZjIzZWRzZmFzZGYyMzEyYXNkZjswMTJBMTIzMSIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzE3MDMifQ.YL5zGO3QqGaUmNv1YqKUedptEQhLxZPLpnm8htfgRBA', CAST(N'2017-03-10T22:27:04.967' AS DateTime), CAST(N'2017-03-10T22:28:22.377' AS DateTime), CAST(N'2017-03-10T22:35:40.667' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (73, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMDg3NjBmNTMtNDY2Mi00MzMzLThhMzktNTc5NzgxNjc4ZjFhIiwiaWF0IjoxNDg5MTg1MzIzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxODUzMjMsImV4cCI6MTQ4OTE4NTYyMywiaXNzIjoiYXNkZjIzZWRzZmFzZGYyMzEyYXNkZjswMTJBMTIzMSIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzE3MDMifQ.LL78dg5-O1SotXQtEPPNy4czz1ptAtJsLd3Wf_8CpRg', CAST(N'2017-03-10T22:35:35.403' AS DateTime), CAST(N'2017-03-10T22:40:23.403' AS DateTime), CAST(N'2017-03-10T22:45:46.213' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (74, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZTU3NTllNWUtNWYzZS00Y2U0LTkyY2UtMzk0ODQwYWYzNjJmIiwiaWF0IjoxNDg5MTg1OTQxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxODU5NDEsImV4cCI6MTQ4OTE4NjI0MSwiaXNzIjoiYXNkZjIzZWRzZmFzZGYyMzEyYXNkZjswMTJBMTIzMSIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzAwMCJ9.WY7oIT853vvjDMJ5MVkH2JUlOMvaMwPrYvPGbgvx3dc', CAST(N'2017-03-10T22:45:44.760' AS DateTime), CAST(N'2017-03-10T22:50:41.370' AS DateTime), CAST(N'2017-03-10T22:50:07.270' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (75, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiOGZmMTFhYTItNmQ1ZC00MDdkLTg1ZWEtMjFhNjJkZmMxMmIxIiwiaWF0IjoxNDg5MTg2MjAzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxODYyMDMsImV4cCI6MTQ4OTE4NjUwMywiaXNzIjoiYXNkZjIzZWRzZmFzZGYyMzEyYXNkZjswMTJBMTIzMSIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzE3MDMifQ.mjoc_q2d_sz_qKlrwrAh9Yn6p-GDOU-Y2dGZUXFp1bU', CAST(N'2017-03-10T22:50:05.910' AS DateTime), CAST(N'2017-03-10T22:55:03.043' AS DateTime), CAST(N'2017-03-10T23:01:00.463' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (76, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNzA4M2U4ZWUtNjg2Yy00YmEwLTllZWYtNWM0NDZlNGE0NDI2IiwiaWF0IjoxNDg5MTg2MjAzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxODYyMDMsImV4cCI6MTQ4OTE4NjUwMywiaXNzIjoiYXNkZjIzZWRzZmFzZGYyMzEyYXNkZjswMTJBMTIzMSIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzE3MDMifQ._NZin1kbEIXrms7VRW-biNIjh7bL0LuXIaZebKlqV2s', CAST(N'2017-03-10T23:00:59.777' AS DateTime), CAST(N'2017-03-10T22:55:03.043' AS DateTime), CAST(N'2017-03-11T00:34:49.023' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (77, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMzE1YWE3NjktODdjOS00ZTViLTg5OWUtZTUzYWU4Zjg3MzBhIiwiaWF0IjoxNDg5MTkyNDg0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxOTI0ODQsImV4cCI6MTQ4OTE5Mjc4NCwiaXNzIjoiYXNkZjIzZWRzZmFzZGYyMzEyYXNkZjswMTJBMTIzMSIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzE3MDMvIn0.Dod6UNQlWF2gHctG14wNqVZAebR7dncUH4-YXoYqejg', CAST(N'2017-03-11T00:34:47.457' AS DateTime), CAST(N'2017-03-10T20:39:44.320' AS DateTime), CAST(N'2017-03-11T00:47:09.293' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (78, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNjE5MDBiZGEtMjhhNS00YTI1LTk3YWEtNzY4NThjMDM4MmUxIiwiaWF0IjoxNDg5MTkyNDg0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxOTI0ODQsImV4cCI6MTQ4OTE5Mjc4NCwiaXNzIjoiYXNkZjIzZWRzZmFzZGYyMzEyYXNkZjswMTJBMTIzMSIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzE3MDMvIn0.UUs6c2tibIDjvbL3ySSCRO0Szq2skRYed41C25gTwYo', CAST(N'2017-03-11T00:47:08.660' AS DateTime), CAST(N'2017-03-10T20:39:44.320' AS DateTime), CAST(N'2017-03-11T00:47:32.533' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (79, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYTkxMTMzYjAtNGUyMy00ZTA0LWI0ZTQtODBmYTZlNmE4MzU1IiwiaWF0IjoxNDg5MTkyNDg0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxOTI0ODQsImV4cCI6MTQ4OTE5Mjc4NCwiaXNzIjoiYXNkZjIzZWRzZmFzZGYyMzEyYXNkZjswMTJBMTIzMSIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzE3MDMvIn0.adzu3iKCxD0a7Co12nUuQQThcVsCPSBa0QMrBJEgRX0', CAST(N'2017-03-11T00:47:31.873' AS DateTime), CAST(N'2017-03-10T20:39:44.320' AS DateTime), CAST(N'2017-03-11T00:56:52.337' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (80, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNjE3NmI1MjEtNWRhMi00NmMwLWJmZWMtNmE4MGY0YjFiMjk0IiwiaWF0IjoxNDg5MTkzODA4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxOTM4MDgsImV4cCI6MTQ4OTE5NDEwOCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzLyJ9.u6eK8lx_sy6xgjJuyR0ygiBvHCQR29Ew5E_PMl_EkJs', CAST(N'2017-03-11T00:56:51.013' AS DateTime), CAST(N'2017-03-10T21:01:48.020' AS DateTime), CAST(N'2017-03-11T00:57:31.603' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (81, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiM2Y0OTIxOTAtMzZlNy00ZjVjLTkzOTUtZGMyODFkMzU1NDA2IiwiaWF0IjoxNDg5MTkzODA4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxOTM4MDgsImV4cCI6MTQ4OTE5NDEwOCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzLyJ9.2NWX-_U0zwbn16pJNoLOQMVLHEKBfPTF_tiaGeOoDCI', CAST(N'2017-03-11T00:57:30.993' AS DateTime), CAST(N'2017-03-10T21:01:48.020' AS DateTime), CAST(N'2017-03-11T01:02:34.810' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (82, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMDI5Y2NkM2ItMzI1Ni00MDBlLWJkYzItYjNhOWUzYzc0NDY1IiwiaWF0IjoxNDg5MTk0MTUwLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkxOTQxNTAsImV4cCI6MTQ4OTE5NDQ1MCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzLyJ9.M5_kj44sosNhdqsiu1x0ihu7bV5qwHcvC94GUGcBlF4', CAST(N'2017-03-11T01:02:33.320' AS DateTime), CAST(N'2017-03-10T21:07:30.193' AS DateTime), CAST(N'2017-03-11T14:22:12.633' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (83, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZTIzNTE4ODEtOWU4ZC00ZTE4LTg0MGEtZjU2MzFiMzM5NjgyIiwiaWF0IjoxNDg5MjQyMTMxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkyNDIxMzEsImV4cCI6MTQ4OTI0MjQzMSwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzLyJ9.UZ45FhbmpvrQ4TCjudzipbFIpzs-LvvJ1qqaVX6UPgk', CAST(N'2017-03-11T14:22:12.533' AS DateTime), CAST(N'2017-03-11T10:27:11.473' AS DateTime), CAST(N'2017-03-11T14:32:45.383' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (84, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNmU2NmE5NTktNzk2NS00NTY3LTlkZTItMzNjZWQ1YTRhNzY3IiwiaWF0IjoxNDg5MjQyMTMxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkyNDIxMzEsImV4cCI6MTQ4OTI0MjQzMSwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzLyJ9.wCi2bXp9JN4ZciYjOqvTbni8xGe9i9VO6pcLc_2nSRQ', CAST(N'2017-03-11T14:32:45.327' AS DateTime), CAST(N'2017-03-11T10:27:11.473' AS DateTime), CAST(N'2017-03-11T14:37:03.783' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (85, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYTNmZTYxNzMtODZkNC00OWIyLTk0ZGUtNDk1YTRlOGE2MTMwIiwiaWF0IjoxNDg5MjQyMTMxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkyNDIxMzEsImV4cCI6MTQ4OTI0MjQzMSwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vbG9jYWxob3N0OjMxNzAzLyJ9.FKoDy25VGWCpruMmVsD5vEfKHX0dE0938KJPJ7xsucQ', CAST(N'2017-03-11T14:37:03.720' AS DateTime), CAST(N'2017-03-11T10:27:11.473' AS DateTime), CAST(N'2017-03-11T14:42:35.223' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (86, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNjYyMTQyMjktZDMxNS00NjM3LWI3YWYtZTNiODJkZmQ3NWIzIiwiaWF0IjoxNDg5MjQzMzU0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkyNDMzNTMsImV4cCI6MTQ4OTI0MzY1MywiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDozMTcwMyJ9.OldD2patSfmXYkSpLk_y-cX5ju4FPYKVXp6cg2jnysk', CAST(N'2017-03-11T14:42:34.970' AS DateTime), CAST(N'2017-03-11T10:47:33.677' AS DateTime), CAST(N'2017-03-11T14:43:09.937' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (87, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZDM3ZGJiMDUtM2JlYi00MTAzLTgzODgtYjc0M2QxYWJkZjQ4IiwiaWF0IjoxNDg5MjQzMzU0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkyNDMzNTMsImV4cCI6MTQ4OTI0MzY1MywiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDozMTcwMyJ9.DG6OqjaHn-6Kgn1cSLyQvNMa8JY-l8ZFpl0gsNfHFvU', CAST(N'2017-03-11T14:43:09.830' AS DateTime), CAST(N'2017-03-11T10:47:33.677' AS DateTime), CAST(N'2017-03-11T14:43:53.793' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (88, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMDY3Yzc5YzQtMmQ0Yi00MGE1LWFmNzItN2EzNGMyMzFmZWRiIiwiaWF0IjoxNDg5MjQzNDMyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkyNDM0MzIsImV4cCI6MTQ4OTI0MzczMiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDozMTcwMyJ9.nx0FYNfSL9pxhrcEmqBI996hnviIxxxfhuoZmnywt90', CAST(N'2017-03-11T14:43:53.517' AS DateTime), CAST(N'2017-03-11T10:48:52.173' AS DateTime), CAST(N'2017-03-11T14:49:42.483' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (89, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNzIzNDhmZTQtOTA0Ny00MjQ0LTkxODEtYjg5ZWM4ZDNiNjhjIiwiaWF0IjoxNDg5MjQzNzgxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkyNDM3ODAsImV4cCI6MTQ4OTI0NDA4MCwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDozMTcwMyJ9.fgHT7N4X4BZXaibBgKuFFQ7tz9imo-R8jximVaG477s', CAST(N'2017-03-11T14:49:42.180' AS DateTime), CAST(N'2017-03-11T10:54:40.693' AS DateTime), CAST(N'2017-03-11T14:51:38.373' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (90, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZTUxN2JlMGQtZjM2ZS00MTc4LTk4ZGQtY2QyMDg5MDQ2YWE0IiwiaWF0IjoxNDg5MjQzODk3LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkyNDM4OTYsImV4cCI6MTQ4OTI0NDE5NiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDozMDAwIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDozMTcwMyJ9.gl5ghg7kSLbOawiOzG6y7s-_ea_dtxec_StFT5BXhSs', CAST(N'2017-03-11T14:51:38.063' AS DateTime), CAST(N'2017-03-11T10:56:36.523' AS DateTime), CAST(N'2017-03-11T17:05:52.217' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (91, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiM2RhMmJjNzYtZDAyOC00YTY0LTk2N2UtNzQwMDVlNWQzOTFhIiwiaWF0IjoxNDg5MjUxOTUxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODkyNTE5NTEsImV4cCI6MTQ4OTI1MjI1MSwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.d2LJc_jWiIBZIVzr-2890Z-GzIuXcLs5DrwtDErTRt0', CAST(N'2017-03-11T17:05:52.183' AS DateTime), CAST(N'2017-03-11T13:10:51.180' AS DateTime), CAST(N'2017-03-13T13:34:09.127' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (92, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZDMwYmNmNDUtZWNiZS00NWNjLThjY2UtYzM5NTNhOTQ1NjBhIiwiaWF0IjoxNDg5NDEyMDQ4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTIwNDcsImV4cCI6MTQ4OTQxMjM0NywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.y1BoUHcVOh0TqEuLhZQCMCaM2Nm0HuRlk-_blSHf66E', CAST(N'2017-03-13T13:34:09.093' AS DateTime), CAST(N'2017-03-13T09:39:07.990' AS DateTime), CAST(N'2017-03-13T13:35:30.247' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (93, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZmIwMTE0NDYtMjEzZi00MDlhLWFiNDMtNDdjOTk0MmE0ODI4IiwiaWF0IjoxNDg5NDEyMDQ4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTIwNDcsImV4cCI6MTQ4OTQxMjM0NywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.8g9FNBzxSUlCxNXXMP0gsYM4H6ZPBGSGX4pxF2Ly3mU', CAST(N'2017-03-13T13:35:30.243' AS DateTime), CAST(N'2017-03-13T09:39:07.990' AS DateTime), CAST(N'2017-03-13T13:36:40.787' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (94, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYmE3ZDA3ODEtMzQ3My00NjRkLWEyMzEtNzhhMjA3MDQyZjQ2IiwiaWF0IjoxNDg5NDEyMDQ4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTIwNDcsImV4cCI6MTQ4OTQxMjM0NywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.rPG5WIgXc0I_Wsqy5oDLz4VQKSFvdnbG83J8O7hi3Ps', CAST(N'2017-03-13T13:36:40.787' AS DateTime), CAST(N'2017-03-13T09:39:07.990' AS DateTime), CAST(N'2017-03-13T13:37:01.213' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (95, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZmVlZmQxYzQtNGY2Yi00ZGJhLWEyZTgtOWZhMzA0MTVkYWVhIiwiaWF0IjoxNDg5NDEyMDQ4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTIwNDcsImV4cCI6MTQ4OTQxMjM0NywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.hpdilzp9IW760vKWYrtDshty4k8NWR-us6DH5DhqduY', CAST(N'2017-03-13T13:37:01.213' AS DateTime), CAST(N'2017-03-13T09:39:07.990' AS DateTime), CAST(N'2017-03-13T13:37:19.337' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (96, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNTE0Yjk4MWUtMmI0ZS00MDdiLThhYzQtOTc3ZTQ2ZTIyMjJmIiwiaWF0IjoxNDg5NDEyMDQ4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTIwNDcsImV4cCI6MTQ4OTQxMjM0NywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.RYhC6ahxxuGg_TmwyFB46FadZUZnGhtWAVkTJOuTGC4', CAST(N'2017-03-13T13:37:19.337' AS DateTime), CAST(N'2017-03-13T09:39:07.990' AS DateTime), CAST(N'2017-03-13T13:40:17.207' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (97, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMDVkOTFhMjEtMWMyMS00ZjE5LWJjZjEtYmQ0YTMwZjIyYTBjIiwiaWF0IjoxNDg5NDEyNDE2LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTI0MTYsImV4cCI6MTQ4OTQxMjcxNiwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.BTCce0sNOZH0sx3VeJHaFoYYiUu1X1KljelzM3gkcDw', CAST(N'2017-03-13T13:40:17.183' AS DateTime), CAST(N'2017-03-13T09:45:16.490' AS DateTime), CAST(N'2017-03-13T13:40:45.823' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (98, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYmNjNTU4ZTgtYmVhZC00Mzc4LWI5ZTItYjg3ZTBmZTAyOThkIiwiaWF0IjoxNDg5NDEyNDE2LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTI0MTYsImV4cCI6MTQ4OTQxMjcxNiwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.1H6XLUhGmr-xT5S__rBOyxSduqgKFKjPSArK5OAGW84', CAST(N'2017-03-13T13:40:45.823' AS DateTime), CAST(N'2017-03-13T09:45:16.490' AS DateTime), CAST(N'2017-03-13T13:46:09.423' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (99, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMWMzNzNlMTUtODQwZC00ODUyLWE2YmQtNDkxODI5MjBiMWNlIiwiaWF0IjoxNDg5NDEyNDE2LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTI0MTYsImV4cCI6MTQ4OTQxMjcxNiwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.Ths-h9h6XiBI4skVJfArSbNuOBMyez8rYgCtyQiZCIs', CAST(N'2017-03-13T13:46:09.410' AS DateTime), CAST(N'2017-03-13T09:45:16.490' AS DateTime), CAST(N'2017-03-13T13:46:49.230' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (100, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNDMzOTcxMjYtYjViNi00M2Y4LWIwNWEtZTA3YWZlZGYzNzQ0IiwiaWF0IjoxNDg5NDEyNDE2LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTI0MTYsImV4cCI6MTQ4OTQxMjcxNiwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.wfJuvc4GDDEFzgfkPF5mgXlzD0LSOqT9Wl_PbZJ8VuM', CAST(N'2017-03-13T13:46:49.223' AS DateTime), CAST(N'2017-03-13T09:45:16.490' AS DateTime), CAST(N'2017-03-13T13:49:11.200' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (101, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMjg5ZmY2YzUtYTcwOS00MmMyLTkxN2UtYTc5OTM0YTQ5ZjA0IiwiaWF0IjoxNDg5NDEyNDE2LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTI0MTYsImV4cCI6MTQ4OTQxMjcxNiwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.iv3hwSfNawa9msbDUwWO4U_3-vQW5ZcsMHsOGsrWlmA', CAST(N'2017-03-13T13:49:11.197' AS DateTime), CAST(N'2017-03-13T09:45:16.490' AS DateTime), CAST(N'2017-03-13T13:49:41.620' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (102, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiODliZDE4ODEtZWJlNC00NTM0LWJkYWItZjMwNjE2YTA3MWFkIiwiaWF0IjoxNDg5NDEyNDE2LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTI0MTYsImV4cCI6MTQ4OTQxMjcxNiwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.sg5QMqiLqky6AJUKHZSkrfprQQeoUTZ9WTFuAD3OErY', CAST(N'2017-03-13T13:49:41.620' AS DateTime), CAST(N'2017-03-13T09:45:16.490' AS DateTime), CAST(N'2017-03-13T13:52:17.340' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (103, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMWJlM2Q0ODctYzBiMi00Mzk2LTg5NWQtNmVkM2M5MDdmOGNjIiwiaWF0IjoxNDg5NDEzMTMzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTMxMzMsImV4cCI6MTQ4OTQxMzQzMywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.Fc9WjMLRAeqqqjxjupI0lwLEEKMTzjb7Jh8raSz9T6I', CAST(N'2017-03-13T13:52:17.323' AS DateTime), CAST(N'2017-03-13T09:57:13.360' AS DateTime), CAST(N'2017-03-13T13:53:29.103' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (104, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNTViN2E5OTEtM2QwYS00MDE5LWI3MTYtMjkyMjJkMGZkNzIyIiwiaWF0IjoxNDg5NDEzMTMzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTMxMzMsImV4cCI6MTQ4OTQxMzQzMywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.3uVQuDzXSfq5zqDZ55W5H2nMjndEkJva7cZiRRfFdlc', CAST(N'2017-03-13T13:53:29.103' AS DateTime), CAST(N'2017-03-13T09:57:13.360' AS DateTime), CAST(N'2017-03-13T13:54:00.077' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (105, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZDNiNjZhMTUtNGE4OC00M2E4LTljNmMtNTg3MDI0OTJkOGM3IiwiaWF0IjoxNDg5NDEzMTMzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTMxMzMsImV4cCI6MTQ4OTQxMzQzMywiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.yB6xQqVFOOA1xUKI2s5x3Wfg6FOK9EFSjcMUTBW23Cg', CAST(N'2017-03-13T13:54:00.060' AS DateTime), CAST(N'2017-03-13T09:57:13.360' AS DateTime), CAST(N'2017-03-13T13:54:35.940' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (106, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiODRiNmZiMzAtZjMzNS00NDk3LWJhZTctZjY4NDVkZTIzYWE3IiwiaWF0IjoxNDg5NDEzMjY4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTMyNjgsImV4cCI6MTQ4OTQxMzU2OCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.iGqIXH4JQEhg5xGKDJnW95kLLxhTbg4ikGLWzt_Ixdk', CAST(N'2017-03-13T13:54:35.923' AS DateTime), CAST(N'2017-03-13T09:59:28.170' AS DateTime), CAST(N'2017-03-13T13:55:35.783' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (107, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMjUyNzY4NWUtYzlhMi00MjViLWI0OTQtMjI4ZWQ2MmQ3MTA2IiwiaWF0IjoxNDg5NDEzMzI0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTMzMjQsImV4cCI6MTQ4OTQxMzYyNCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.v-fVGbQiS82Rymey4FCRMIrlDkfaafopVweYjFaPQXQ', CAST(N'2017-03-13T13:55:35.750' AS DateTime), CAST(N'2017-03-13T10:00:24.233' AS DateTime), CAST(N'2017-03-13T13:56:00.993' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (108, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYWM4YWMzMmUtZjg1My00OWZhLWJjNzItMTc0Mzc2NjM1NWRlIiwiaWF0IjoxNDg5NDEzMzI0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTMzMjQsImV4cCI6MTQ4OTQxMzYyNCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.b7as_e-vDDJ-u1W9JydieAYp64bLjX3sHqdu8gYxWNw', CAST(N'2017-03-13T13:56:00.993' AS DateTime), CAST(N'2017-03-13T10:00:24.233' AS DateTime), CAST(N'2017-03-13T14:04:40.300' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (109, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZGJlZmZmZTUtZjFmMi00MWU5LTljNmEtNDdkNjllOTc2MTI1IiwiaWF0IjoxNDg5NDEzMzI0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTMzMjQsImV4cCI6MTQ4OTQxMzYyNCwiaXNzIjoiQXdlc29tZVVzZXIiLCJhdWQiOiJodHRwOi8vamVmcmlkZXYtMDAxLXNpdGUzLmN0ZW1wdXJsLmNvbSJ9.jneZX8b--spVzeVcTRrPJDA7-aG8uDP1uL4puDuuf_w', CAST(N'2017-03-13T14:04:40.300' AS DateTime), CAST(N'2017-03-13T10:00:24.233' AS DateTime), CAST(N'2017-03-13T14:09:39.737' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (110, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMDI3M2IxNDQtNzk1OS00Nzc4LWJmYmUtNGNlZjU4ZTJkMDJiIiwiaWF0IjoxNDg5NDE0MTc1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTQxNzQsImV4cCI6MTQ4OTQxNDQ3NCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.Qp-Zwx38TXIgFCwU9aXqxQqwUltsRgzPrOQmT7NZ0I4', CAST(N'2017-03-13T14:09:39.707' AS DateTime), CAST(N'2017-03-13T10:14:34.920' AS DateTime), CAST(N'2017-03-13T14:10:19.600' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (111, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZDk5ZWYzNzItM2ZjZC00MGU2LTg4ZWItMWNmMzk1NzhkYTRiIiwiaWF0IjoxNDg5NDE0MTc1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTQxNzQsImV4cCI6MTQ4OTQxNDQ3NCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.fgPmyI1o_zQQ1YoK1OIEkEgk0HQXuqVmaJZe6vrY6-k', CAST(N'2017-03-13T14:10:15.843' AS DateTime), CAST(N'2017-03-13T10:14:34.920' AS DateTime), CAST(N'2017-03-13T14:19:20.583' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (112, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiODg4MmQzZDgtMzFlZS00MjUzLWJlY2UtMGE4ZDQ1NWY0MmJkIiwiaWF0IjoxNDg5NDE0MTc1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTQxNzQsImV4cCI6MTQ4OTQxNDQ3NCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.HXJTtXjaSoXKykhqx_VR7REPk8GYAYWHCR4ppupA0Sc', CAST(N'2017-03-13T14:19:20.567' AS DateTime), CAST(N'2017-03-13T10:14:34.920' AS DateTime), CAST(N'2017-03-13T14:20:15.663' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (113, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNWRiZjc2ZjUtM2NlOC00YWNkLWE2OTAtNGVhZTQ2ZTM4NTgxIiwiaWF0IjoxNDg5NDE0MTc1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTQxNzQsImV4cCI6MTQ4OTQxNDQ3NCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.GtcxMVGaJpbJf7MiDbq1KX_LSnkpR2PthKUFG4IrMoo', CAST(N'2017-03-13T14:20:15.663' AS DateTime), CAST(N'2017-03-13T10:14:34.920' AS DateTime), CAST(N'2017-03-13T14:21:47.700' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (114, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYjBjMmYwMmMtNDVlNC00ODM2LTkxNzUtMWZkMGMzMWY3YzM3IiwiaWF0IjoxNDg5NDE0MTc1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTQxNzQsImV4cCI6MTQ4OTQxNDQ3NCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.j0-V6G0JXBNh9fVlUK5f-YYT8Kz7oFQ9DPFu9BTSE3A', CAST(N'2017-03-13T14:21:47.683' AS DateTime), CAST(N'2017-03-13T10:14:34.920' AS DateTime), CAST(N'2017-03-13T14:23:27.220' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (115, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZmU0ZGFkMmMtNWY5YS00MTAwLTg3OTgtNmM2NWU1OGEwMGQzIiwiaWF0IjoxNDg5NDE0MTc1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTQxNzQsImV4cCI6MTQ4OTQxNDQ3NCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.HjHgKg-Lq_JO5s7sjjLNdOenkzfoswObxY0kS_vDw-M', CAST(N'2017-03-13T14:23:27.217' AS DateTime), CAST(N'2017-03-13T10:14:34.920' AS DateTime), CAST(N'2017-03-13T14:25:40.887' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (116, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiODRhMmIzM2MtYjVlMi00ZWJkLWJhMzItYjg2MWJkMjg5MjZlIiwiaWF0IjoxNDg5NDE1MTMyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTUxMzIsImV4cCI6MTQ4OTQxNTQzMiwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.oYXbpJpcTennkqMz9cRWk1eC74Gzs1gMJRwq-MDiSSo', CAST(N'2017-03-13T14:25:40.867' AS DateTime), CAST(N'2017-03-13T10:30:32.467' AS DateTime), CAST(N'2017-03-13T14:26:03.727' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (117, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMWU4Mjk1OWUtNGQzNS00NTYyLTkwNzgtN2RiYTYwYTJmNDAzIiwiaWF0IjoxNDg5NDE1MTMyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTUxMzIsImV4cCI6MTQ4OTQxNTQzMiwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.0ATL8qmb_HaXr5KONNDziC0I3rb3g14fc2k90Jy7-9M', CAST(N'2017-03-13T14:26:03.727' AS DateTime), CAST(N'2017-03-13T10:30:32.467' AS DateTime), CAST(N'2017-03-13T14:26:38.850' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (118, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMjVkYTgzNGYtMTc4Ni00NTg1LTlhYmQtYjE3NzQxN2FhOTE1IiwiaWF0IjoxNDg5NDE1MTMyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTUxMzIsImV4cCI6MTQ4OTQxNTQzMiwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.uPPDiivDnC00zYVuZjOh4UEfJF28BUnoQxgvAXilGys', CAST(N'2017-03-13T14:26:38.850' AS DateTime), CAST(N'2017-03-13T10:30:32.467' AS DateTime), CAST(N'2017-03-13T14:27:39.840' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (119, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYjJiMTZkYjEtYzBhMC00NzYwLTk1ZjktMDU2NWY4NWZjNzBiIiwiaWF0IjoxNDg5NDE1MjU2LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTUyNTUsImV4cCI6MTQ4OTQxNTU1NSwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.0I8M2yubMP8Zkfv-Xh9sG7ezl9UAlygaYel8XzLeQco', CAST(N'2017-03-13T14:27:39.823' AS DateTime), CAST(N'2017-03-13T10:32:35.683' AS DateTime), CAST(N'2017-03-13T14:28:35.730' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (120, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYTM2MDQ2ZTUtNTE4MS00NmIwLWJlYTAtNzQxMjE5ZTJjMTAwIiwiaWF0IjoxNDg5NDE1MzA4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTUzMDgsImV4cCI6MTQ4OTQxNTYwOCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.ErMKa10QU7PysTv3hB62TZPyNEDM5h6KpJxoLO7PjWA', CAST(N'2017-03-13T14:28:35.713' AS DateTime), CAST(N'2017-03-13T10:33:28.180' AS DateTime), CAST(N'2017-03-13T14:29:39.653' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (121, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMDc3MzA4ZDAtNGI3Yi00ZGM1LWJjYjQtZDYzNGQ4OWFiZTA0IiwiaWF0IjoxNDg5NDE1MzA4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTUzMDgsImV4cCI6MTQ4OTQxNTYwOCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.iciIiSoO_yrBdJ8QZ54sAtZpZjzJSZJvRvaGEJopRm4', CAST(N'2017-03-13T14:29:39.653' AS DateTime), CAST(N'2017-03-13T10:33:28.180' AS DateTime), CAST(N'2017-03-13T14:30:18.037' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (122, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZjIxZDFhNzktMTRmYS00YTYyLWFiZWEtNWU4ZTVmZTg5MWI2IiwiaWF0IjoxNDg5NDE1MzA4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTUzMDgsImV4cCI6MTQ4OTQxNTYwOCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.8RxwTr8NQKiw01KVRs4NRGJNO-vh4heMYnoYOZ8tAzQ', CAST(N'2017-03-13T14:30:18.037' AS DateTime), CAST(N'2017-03-13T10:33:28.180' AS DateTime), CAST(N'2017-03-13T14:30:35.580' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (123, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiODNjMmVhMmItMjg3MS00NzQwLTlmYzUtYjYwN2YxZTgyMzc5IiwiaWF0IjoxNDg5NDE1MzA4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTUzMDgsImV4cCI6MTQ4OTQxNTYwOCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.aMuiZO6-dNeQIjlvAhXbRIVkuzDLAppNhc6I9XStBVI', CAST(N'2017-03-13T14:30:35.580' AS DateTime), CAST(N'2017-03-13T10:33:28.180' AS DateTime), CAST(N'2017-03-13T14:33:29.853' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (124, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiODY0YmRjN2YtYWIyMi00OTdkLTkyN2EtZjJmYzkzNjBiMDI5IiwiaWF0IjoxNDg5NDE1NjAzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTU2MDMsImV4cCI6MTQ4OTQxNTkwMywiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.JDYfuYJsS4rhAE_Hs6HdRQTUseqjNRmkBZwgbA3HhtQ', CAST(N'2017-03-13T14:33:29.830' AS DateTime), CAST(N'2017-03-13T10:38:23.403' AS DateTime), CAST(N'2017-03-13T14:41:07.013' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (125, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMTA1NGEyNGMtNzgxMy00YTdlLThiNzMtNDJkNzdhOGE3YWI4IiwiaWF0IjoxNDg5NDE2MDY0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTYwNjQsImV4cCI6MTQ4OTQxNjM2NCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.mu3iqDluV9As_Gzly3I1UgMZU2-GTGZkNtdfPuMK3_s', CAST(N'2017-03-13T14:41:06.990' AS DateTime), CAST(N'2017-03-13T10:46:04.140' AS DateTime), CAST(N'2017-03-13T15:02:43.980' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (126, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNDMwNDUwMTAtYzhjZS00NTJlLTk2MmQtNmNmOTYxZjljNzRjIiwiaWF0IjoxNDg5NDE2MDY0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTYwNjQsImV4cCI6MTQ4OTQxNjM2NCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.HmKVuq4UQuZoTudZTJvZp3FZNiucanxZMZKEf-R2GnQ', CAST(N'2017-03-13T15:02:43.977' AS DateTime), CAST(N'2017-03-13T10:46:04.140' AS DateTime), CAST(N'2017-03-13T15:03:32.217' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (127, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMDlhZTY1NzYtZWQ4MS00NzRkLTk1MWQtMzFhNTdiZmRmZWY4IiwiaWF0IjoxNDg5NDE2MDY0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTYwNjQsImV4cCI6MTQ4OTQxNjM2NCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.repw7K0xA8VrEeG19PF1xlxi-lP7yLUsicqqBtC1FpM', CAST(N'2017-03-13T15:03:32.207' AS DateTime), CAST(N'2017-03-13T10:46:04.140' AS DateTime), CAST(N'2017-03-13T15:05:31.720' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (128, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiN2ZlMDgxODgtNDNkOS00ZTQ2LWE2MWEtZTAxZmI5ZDc5OWRkIiwiaWF0IjoxNDg5NDE2MDY0LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTYwNjQsImV4cCI6MTQ4OTQxNjM2NCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.GukA1KgDmXxdm0b2LXzMPFxP06VfUHCuiVtTa0xsEN8', CAST(N'2017-03-13T15:05:31.717' AS DateTime), CAST(N'2017-03-13T10:46:04.140' AS DateTime), CAST(N'2017-03-13T15:07:06.470' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (129, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNDE3OTlhN2ItMTJkNS00ZjkwLTg2YmQtNTU1NmJiMjFiOTE5IiwiaWF0IjoxNDg5NDE3NjI1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MTc2MjUsImV4cCI6MTQ4OTQxNzkyNSwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.gkA1HxsSNKKDpib4rbxlIYiSpVyqqyWxLNR63fHnaBA', CAST(N'2017-03-13T15:07:06.450' AS DateTime), CAST(N'2017-03-13T11:12:05.417' AS DateTime), CAST(N'2017-03-13T19:02:38.830' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (130, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZDdiZTBkMmMtYmUyMi00NWQ4LTlhOTUtMDczYmFhNzY5NzU0IiwiaWF0IjoxNDg5NDMxNzU4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MzE3NTcsImV4cCI6MTQ4OTQzMjA1NywiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.r96rYOcW5O0kIhxNxGACO5v5WNfRDoAbkDNWnSoNeOc', CAST(N'2017-03-13T19:02:38.793' AS DateTime), CAST(N'2017-03-13T15:07:37.823' AS DateTime), CAST(N'2017-03-13T19:15:32.743' AS DateTime), N'Web', N'INA')
GO
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (131, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNmY3MDgzYjctYTMwNS00YmE2LTk5NTAtOWE1YWQzOWViODgzIiwiaWF0IjoxNDg5NDMxNzU4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MzE3NTcsImV4cCI6MTQ4OTQzMjA1NywiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.L_HG1OT6HNElDi2VUcrAAs_dzwAogrf5OQlb7oXCmt0', CAST(N'2017-03-13T19:15:32.737' AS DateTime), CAST(N'2017-03-13T15:07:37.823' AS DateTime), CAST(N'2017-03-13T19:15:47.863' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (132, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZWI3NWQ2MjctZGQxNS00MDQ4LTgzM2YtOThlNjA0ZjU1MzM1IiwiaWF0IjoxNDg5NDMxNzU4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MzE3NTcsImV4cCI6MTQ4OTQzMjA1NywiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.ws6hsRvbPjGbCtBX88sFC11Zt0rQ4cPw511aYhH0Zps', CAST(N'2017-03-13T19:15:47.860' AS DateTime), CAST(N'2017-03-13T15:07:37.823' AS DateTime), CAST(N'2017-03-13T19:16:19.210' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (133, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMTA2MzAwYWMtNDBiYS00ZTgzLWE2ZWQtYjM2MTRiMDk2MGJhIiwiaWF0IjoxNDg5NDMyNTc4LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0MzI1NzcsImV4cCI6MTQ4OTQzMjg3NywiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.nQKxgSs_z_UOvmfZVHIBENEQhUBV2q8QrPYJLgtEEZg', CAST(N'2017-03-13T19:16:19.180' AS DateTime), CAST(N'2017-03-13T15:21:17.887' AS DateTime), CAST(N'2017-03-13T20:57:16.043' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (134, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYTk2ZTUwNjMtMjY4NC00ODJlLTgwZGItYmY0MjgwOWI4MzU1IiwiaWF0IjoxNDg5NDM4NjM1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0Mzg2MzUsImV4cCI6MTQ4OTQzODkzNSwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.tG7nbRsOEwayLnb0adq0nZdCy_v836CaEiksdw6tq0s', CAST(N'2017-03-13T20:57:16.003' AS DateTime), CAST(N'2017-03-13T17:02:15.013' AS DateTime), CAST(N'2017-03-14T13:30:19.677' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (135, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMGE2OTJlZmEtZjQ1NS00NWVkLTkzNDktN2IyOWIyNWUyZjQ1IiwiaWF0IjoxNDg5NDk4MjE5LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0OTgyMTgsImV4cCI6MTQ4OTQ5ODUxOCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.8e623icw4Wlp-EckwsSG7Ea2-SOQ0RrcM8bNVj8uTB0', CAST(N'2017-03-14T13:30:19.647' AS DateTime), CAST(N'2017-03-14T09:35:18.730' AS DateTime), CAST(N'2017-03-14T14:34:28.490' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (136, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMTI5NjAxMmYtOWY2NC00MTViLTlkMGQtNjM3ZGQzZDBmZjVkIiwiaWF0IjoxNDg5NDk4MjE5LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk0OTgyMTgsImV4cCI6MTQ4OTQ5ODUxOCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.VGu60ucW7L335Kzf_PfTZkcuXQ8PZ883vrIrdm-URBk', CAST(N'2017-03-14T14:34:28.487' AS DateTime), CAST(N'2017-03-14T09:35:18.730' AS DateTime), CAST(N'2017-03-14T14:34:53.247' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (137, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYjg1NmI5YjUtOWQzNy00YjJiLWJhY2QtN2IxY2MwOWFiMDg2IiwiaWF0IjoxNDg5NTAyMDkyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDIwOTIsImV4cCI6MTQ4OTUwMjM5MiwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.y2GU9EsycoOK8CeaWZdlJObqGxd1XkDsafPt0gYF0G4', CAST(N'2017-03-14T14:34:53.223' AS DateTime), CAST(N'2017-03-14T10:39:52.187' AS DateTime), CAST(N'2017-03-14T14:42:56.170' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (138, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNTQ5ZTdhNjctOWE0YS00MzI0LThkNzUtMzMxZDY4OGMwZWYzIiwiaWF0IjoxNDg5NTAyNTc1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDI1NzQsImV4cCI6MTQ4OTUwMjg3NCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.pt5_MkNYVaI_3pGF-pFUV86KhIN9a1-pIUruzVBbmFY', CAST(N'2017-03-14T14:42:56.140' AS DateTime), CAST(N'2017-03-14T10:47:54.877' AS DateTime), CAST(N'2017-03-14T14:48:13.130' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (139, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZGM0OThmYzUtNmQ3NC00MjhjLWEyYTYtYjg4YjY1Zjk2ODlmIiwiaWF0IjoxNDg5NTAyNTc1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDI1NzQsImV4cCI6MTQ4OTUwMjg3NCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.8CEoHs7LfnBYR4o16h1gEj8qgLfSHcdNyTDCNPKiMnQ', CAST(N'2017-03-14T14:48:13.127' AS DateTime), CAST(N'2017-03-14T10:47:54.877' AS DateTime), CAST(N'2017-03-14T14:48:43.820' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (140, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNGQ1OGQzMTctNjQyYy00MmVhLWJmNTItYjFkMmE1NmVhNjg5IiwiaWF0IjoxNDg5NTAyOTIzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDI5MjIsImV4cCI6MTQ4OTUwMzIyMiwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.TGck4zy_MZ62F_ylYmsCn_jx6GtWeVzIoS6g0ZsFUjc', CAST(N'2017-03-14T14:48:43.797' AS DateTime), CAST(N'2017-03-14T10:53:42.713' AS DateTime), CAST(N'2017-03-14T14:56:14.973' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (141, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZDM0ZTIwYWYtNTU2MS00YmI5LThiNmItZDI2YTVmY2VhNmM3IiwiaWF0IjoxNDg5NTAyOTIzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDI5MjIsImV4cCI6MTQ4OTUwMzIyMiwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.O0f-P_xz0Bbtq_VYlrlp5FZ61bs-59ZpA8YQmvIVfvA', CAST(N'2017-03-14T14:56:14.970' AS DateTime), CAST(N'2017-03-14T10:53:42.713' AS DateTime), CAST(N'2017-03-14T14:56:22.480' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (142, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYWFjMzg1OTQtNTdlNy00M2ZiLTkwZDgtODA3ZmU4MGM2MDc2IiwiaWF0IjoxNDg5NTAyOTIzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDI5MjIsImV4cCI6MTQ4OTUwMzIyMiwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.ATchZ9VtQht1H9QPqznmWNd2aFtgCeubKMOoKtvMUlk', CAST(N'2017-03-14T14:56:22.477' AS DateTime), CAST(N'2017-03-14T10:53:42.713' AS DateTime), CAST(N'2017-03-14T14:56:50.593' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (143, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZGMwYTY4ZTUtM2M1ZC00M2NmLThmZTQtZmNjMzRkZTNkZTIyIiwiaWF0IjoxNDg5NTAyOTIzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDI5MjIsImV4cCI6MTQ4OTUwMzIyMiwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.pVNFkRlro5Zeam9c5iJ1KN22240hhzP3yVR1eitKsLM', CAST(N'2017-03-14T14:56:50.593' AS DateTime), CAST(N'2017-03-14T10:53:42.713' AS DateTime), CAST(N'2017-03-14T14:57:11.893' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (144, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZDAyZTAyMGEtYTBjNS00OGUxLWIxYzUtN2IzNzQ0YjU1MDdmIiwiaWF0IjoxNDg5NTAzNDMxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDM0MzAsImV4cCI6MTQ4OTUwMzczMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.d7ATpeiFcgwtUhwqOLR3eR6evvvpbKb8vJLj6qKeGV8', CAST(N'2017-03-14T14:57:11.877' AS DateTime), CAST(N'2017-03-14T11:02:10.817' AS DateTime), CAST(N'2017-03-14T14:58:06.790' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (145, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNDdmMDVjMDMtMWVjNS00M2RkLTk2NWMtYzFiYmYxNzVkNDBjIiwiaWF0IjoxNDg5NTAzNDMxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDM0MzAsImV4cCI6MTQ4OTUwMzczMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.9XdKTmfbNVrgaxDawA2GesRrw7O_t0kXap0_AMHh-Bw', CAST(N'2017-03-14T14:58:06.787' AS DateTime), CAST(N'2017-03-14T11:02:10.817' AS DateTime), CAST(N'2017-03-14T14:58:25.943' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (146, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMjI1M2EzZjMtNDA0Ny00ZWIwLWJlYTUtZDliY2FlMDEyZWI1IiwiaWF0IjoxNDg5NTAzNDMxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDM0MzAsImV4cCI6MTQ4OTUwMzczMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.cnVJXYQxGYPuYgupej5R0ky2APWcmyyoWFiwh-6pkT4', CAST(N'2017-03-14T14:58:25.943' AS DateTime), CAST(N'2017-03-14T11:02:10.817' AS DateTime), CAST(N'2017-03-14T15:02:38.273' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (147, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNDM5ZTM5MmQtZTBlYy00YWRjLWE2YWEtMmI4ODdmZTdhNWUyIiwiaWF0IjoxNDg5NTAzNDMxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDM0MzAsImV4cCI6MTQ4OTUwMzczMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.hMdGg1sqUaWJl2QHl4AiDzBp3hnkHxCX6Y-EWE8ue4I', CAST(N'2017-03-14T15:02:38.270' AS DateTime), CAST(N'2017-03-14T11:02:10.817' AS DateTime), CAST(N'2017-03-14T15:02:40.900' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (148, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNWRkMGRmMjgtNjdkYy00MDdkLWJmNDQtN2Q4ZmNhZWU3NDNmIiwiaWF0IjoxNDg5NTAzNDMxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDM0MzAsImV4cCI6MTQ4OTUwMzczMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.3MZW-QujFJKC8hLZWJ46E-AIsmyP_qGzB0v7mgpSGxg', CAST(N'2017-03-14T15:02:40.900' AS DateTime), CAST(N'2017-03-14T11:02:10.817' AS DateTime), CAST(N'2017-03-14T15:02:43.400' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (149, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMmNlZWU5ZTEtNGMzOC00MmFkLWJlYjMtOGRhNTdkZjI2ZTFkIiwiaWF0IjoxNDg5NTAzNDMxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDM0MzAsImV4cCI6MTQ4OTUwMzczMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.eMrKQ-7S7Mull7uW1qlUsjZTvsBCGfsHV6cZ0xw2zgc', CAST(N'2017-03-14T15:02:43.400' AS DateTime), CAST(N'2017-03-14T11:02:10.817' AS DateTime), CAST(N'2017-03-14T15:14:07.207' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (150, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNTE2NzYzYmItNjQ2MC00NTdiLTllNWUtYjA0Yjg3MmUwODA3IiwiaWF0IjoxNDg5NTAzNDMxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDM0MzAsImV4cCI6MTQ4OTUwMzczMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.g22loR9AeY9aK335BNTFC6TlFVVKZJBI7uWMtzPIn7o', CAST(N'2017-03-14T15:14:07.203' AS DateTime), CAST(N'2017-03-14T11:02:10.817' AS DateTime), CAST(N'2017-03-14T15:14:09.447' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (151, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZDg2ZGQ5MDMtOTNiZS00MzYzLTkxYTItN2QzMDlhNTBjMDk2IiwiaWF0IjoxNDg5NTAzNDMxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDM0MzAsImV4cCI6MTQ4OTUwMzczMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.1SS1TJFdQG2r6cyMzeFlZIl0rv6Fe6y738Uy9vFmLDg', CAST(N'2017-03-14T15:14:09.443' AS DateTime), CAST(N'2017-03-14T11:02:10.817' AS DateTime), CAST(N'2017-03-14T15:14:12.877' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (152, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNjM2YjY0ZjktNTE2ZC00YzI2LWJkNDctNzhiZmRjOTQyNzZmIiwiaWF0IjoxNDg5NTAzNDMxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDM0MzAsImV4cCI6MTQ4OTUwMzczMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.kW6P2sogwRYaYCiThcyEBDfpYR1dHiJxa4jpseU2sKI', CAST(N'2017-03-14T15:14:12.877' AS DateTime), CAST(N'2017-03-14T11:02:10.817' AS DateTime), CAST(N'2017-03-14T15:14:41.493' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (153, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYTU4ZThmNDEtYzkxOC00M2FmLWEzNmUtMDQ0ZGYyM2M5YzQwIiwiaWF0IjoxNDg5NTAzNDMxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDM0MzAsImV4cCI6MTQ4OTUwMzczMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.Cwl00dr0mwseyOvSGNFlcII2ORt5DGn96GckzZBW644', CAST(N'2017-03-14T15:14:41.493' AS DateTime), CAST(N'2017-03-14T11:02:10.817' AS DateTime), CAST(N'2017-03-14T15:15:00.263' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (154, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiN2MxYTkwMzQtYmYwOS00MGZiLTk0NDktNzM3ZWViZTEzMDljIiwiaWF0IjoxNDg5NTAzNDMxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDM0MzAsImV4cCI6MTQ4OTUwMzczMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.bJXVOZt_l8v1ch18wt4UhNDgONrR6dT6eBZFT9hf-_E', CAST(N'2017-03-14T15:15:00.257' AS DateTime), CAST(N'2017-03-14T11:02:10.817' AS DateTime), CAST(N'2017-03-14T15:17:35.687' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (155, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiN2U0YjU5NzEtNzFlOS00OTFlLThmMjEtMWFjOGQ2OGVmOWFmIiwiaWF0IjoxNDg5NTA0NjU1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDQ2NTQsImV4cCI6MTQ4OTUwNDk1NCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.6msO6qvF2FKVQiKBN3MIpxsz4kc_OSKcDeOtD4ubXQ4', CAST(N'2017-03-14T15:17:35.663' AS DateTime), CAST(N'2017-03-14T11:22:34.683' AS DateTime), CAST(N'2017-03-14T15:29:56.847' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (156, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNmFhYWE3NWYtODJjNi00MmZlLTliYjMtMTRkMjllN2Y2YmY4IiwiaWF0IjoxNDg5NTA1Mzk2LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDUzOTUsImV4cCI6MTQ4OTUwNTQwMSwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.AGuey8Xt-mLkDUGp4XehSjUcdzP1_YkJA01PY3WgJGY', CAST(N'2017-03-14T15:29:56.807' AS DateTime), CAST(N'2017-03-14T15:30:01.767' AS DateTime), CAST(N'2017-03-14T15:30:04.807' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (157, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNjA2NzkzNTAtMGY3OC00MDgzLTk3OGYtZmU2N2UxZDgxMGEwIiwiaWF0IjoxNDg5NTA1Mzk2LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDUzOTUsImV4cCI6MTQ4OTUwNTQwMSwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.3JHlgb-l4aSl1j1K_3ARw2P_BeIWPZDV9VIjVOg7IiU', CAST(N'2017-03-14T15:30:04.807' AS DateTime), CAST(N'2017-03-14T15:30:01.767' AS DateTime), CAST(N'2017-03-14T15:32:06.097' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (158, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYjU0YTcyNGQtZjc3MC00Mjk3LWIxYzktNGY1ZWZiOGUxNzk5IiwiaWF0IjoxNDg5NTA1NDUyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDU0NTIsImV4cCI6MTQ4OTUwNTQ1OCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.kfs08AWxP1jCTvEfY1oi7Dt4KruWAT1w0QR4fjmVE-E', CAST(N'2017-03-14T15:30:53.413' AS DateTime), CAST(N'2017-03-14T15:30:58.330' AS DateTime), CAST(N'2017-03-14T15:32:06.097' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (159, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNzQxMzY3YzctOGQ3NS00Y2JjLTkzNTItMjA3MDFlN2EzMzIzIiwiaWF0IjoxNDg5NTA1NDUyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDU0NTIsImV4cCI6MTQ4OTUwNTQ1OCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.lBxBhZLepEMXtud_DJjDLB641EnP9mt9hVw5oFhQYds', CAST(N'2017-03-14T15:31:01.343' AS DateTime), CAST(N'2017-03-14T15:30:58.330' AS DateTime), CAST(N'2017-03-14T15:32:06.097' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (160, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMmM2YTRiNDYtMjVkOC00NDEwLTk3ZjUtMTJlM2RiN2E3NzRiIiwiaWF0IjoxNDg5NTA1NTI1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDU1MjQsImV4cCI6MTQ4OTUwNTUzMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.dz5e4Y6rmCbNkNT4lzc1AAo0jQV5u9BWBhG5ex57KqY', CAST(N'2017-03-14T15:32:06.070' AS DateTime), CAST(N'2017-03-14T15:32:10.990' AS DateTime), CAST(N'2017-03-14T15:32:34.497' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (161, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYTZhYzQ0OWQtNGVkYi00NTBiLWE5MTgtNjRlNWNjNGFlNjQ4IiwiaWF0IjoxNDg5NTA1NTI1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDU1MjQsImV4cCI6MTQ4OTUwNTUzMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.SE449MELDT_Hd8oZVv5c18AcSwrcfpR5YO0-daBaO6Y', CAST(N'2017-03-14T15:32:34.493' AS DateTime), CAST(N'2017-03-14T15:32:10.990' AS DateTime), CAST(N'2017-03-14T15:39:22.023' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (162, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYTM2NTAwZTktNjBkYy00NTE3LWEzZjMtZDNkM2RhY2YzMmMyIiwiaWF0IjoxNDg5NTA1NTI1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDU1MjQsImV4cCI6MTQ4OTUwNTUzMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.4KfSNjoiOLgqOv8LBdnHh2C9dsFhDRDkic3HBUWy1Wg', CAST(N'2017-03-14T15:39:22.020' AS DateTime), CAST(N'2017-03-14T15:32:10.990' AS DateTime), CAST(N'2017-03-14T15:40:45.660' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (163, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYmMzNWExMWMtNGFiMi00ODcwLThmMTgtODU2OWY4MDdlNmQ0IiwiaWF0IjoxNDg5NTA1NTI1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDU1MjQsImV4cCI6MTQ4OTUwNTUzMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.aMAZ04Su6yVNzx16aDFfTyabI71M4Xlx18K20Qaoy7U', CAST(N'2017-03-14T15:40:45.660' AS DateTime), CAST(N'2017-03-14T15:32:10.990' AS DateTime), CAST(N'2017-03-14T15:44:51.847' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (164, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMzYyMWJlZGMtMzMyZC00ODc5LThkYTItY2VjMWJlNWI4MjhiIiwiaWF0IjoxNDg5NTA1NTI1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDU1MjQsImV4cCI6MTQ4OTUwNTUzMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.wYtTOq-OKO0gJxkc9-MMAAPAs3hKh5JohP-RkiKbhvc', CAST(N'2017-03-14T15:44:51.837' AS DateTime), CAST(N'2017-03-14T15:32:10.990' AS DateTime), CAST(N'2017-03-14T15:45:28.263' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (165, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMGNiODVjNmEtZmUxNy00OWI0LThmMmQtYzI0MTEyY2U2NDIzIiwiaWF0IjoxNDg5NTA1NTI1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDU1MjQsImV4cCI6MTQ4OTUwNTUzMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.9OHtNeprdMuUaGxpXqqHiS62Vub-O_gwQBo0cTSs6A8', CAST(N'2017-03-14T15:45:28.263' AS DateTime), CAST(N'2017-03-14T15:32:10.990' AS DateTime), CAST(N'2017-03-14T15:55:46.273' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (166, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMDA1NWMzYmEtYzkxNy00NWYyLWFkMzEtZTQyNWJkY2I3ZmRjIiwiaWF0IjoxNDg5NTA2OTQ1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDY5NDYsImV4cCI6MTQ4OTUwNjk1MiwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.pzGu_as4nHtwprL3mFUSypv-vOnlh6dWosS_qwyUIoM', CAST(N'2017-03-14T15:55:46.243' AS DateTime), CAST(N'2017-03-14T15:55:51.007' AS DateTime), CAST(N'2017-03-14T15:55:59.290' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (167, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNDBkYWE2YmYtY2M4YS00ODI1LTg1ZWYtYWRmYzNlZGEyOWNkIiwiaWF0IjoxNDg5NTA2OTQ1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDY5NTksImV4cCI6MTQ4OTUwNjk2NSwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.7-stttlWy-Wsfd4yl5nICk0kKfJXeKAip6t-6gngk-U', CAST(N'2017-03-14T15:55:59.287' AS DateTime), CAST(N'2017-03-14T15:55:51.007' AS DateTime), CAST(N'2017-03-14T15:56:12.293' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (168, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNzM5NTk5ODktNWViNi00YmM5LTkwZjMtMjhhNWI3NGE5OWExIiwiaWF0IjoxNDg5NTA2OTQ1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MDY5NzIsImV4cCI6MTQ4OTUwNjk3OCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.bMrIRbnT6njT_OwXSp_Hg-2wuW1ZX8Z09KXAj96TRI0', CAST(N'2017-03-14T15:56:12.293' AS DateTime), CAST(N'2017-03-14T15:55:51.007' AS DateTime), CAST(N'2017-03-14T19:50:48.497' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (169, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiOGZkN2U3MDItMGM3Yy00YmYxLTg0YmYtNjdlZjU0NDVlNDFhIiwiaWF0IjoxNDg5NTIxMDQ3LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MjEwNDgsImV4cCI6MTQ4OTUyNDY0OCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.II-JjjHBgcin8_PqyeRNCZ0XN1nlFgDBrJ9UYFaxvmI', CAST(N'2017-03-14T19:50:48.000' AS DateTime), CAST(N'2017-03-14T20:50:48.000' AS DateTime), CAST(N'2017-03-14T19:52:48.027' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (170, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNjJhOGU5YTMtMDA2Zi00NTYwLWJmZDAtYmMwMzgxZTM4YTM1IiwiaWF0IjoxNDg5NTIxMDQ3LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MjExNjgsImV4cCI6MTQ4OTUyNDc2OCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.tR4fqSCB82UKHwSmzHBHiGUVC7aHFYDdlwooWbTU8N4', CAST(N'2017-03-14T19:52:48.000' AS DateTime), CAST(N'2017-03-14T20:52:48.000' AS DateTime), CAST(N'2017-03-14T19:53:04.137' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (171, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiY2Y5ZDc2MjktMTM1Ny00YzY4LWI3YzItNDg2MWU1MmU5Yjg1IiwiaWF0IjoxNDg5NTIxMDQ3LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MjExODQsImV4cCI6MTQ4OTUyNDc4NCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.-2w_FZPXySy--iM7rGN_GmXAPjqt5hsCDyHDETcHZVI', CAST(N'2017-03-14T19:53:04.000' AS DateTime), CAST(N'2017-03-14T20:53:04.000' AS DateTime), CAST(N'2017-03-14T19:54:21.553' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (172, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiOTVkOTBjNzEtNTYxZi00Y2ViLTk0ZmQtNTcwYTY0NzY4OWZiIiwiaWF0IjoxNDg5NTIxMDQ3LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MjEyNjEsImV4cCI6MTQ4OTUyNDg2MSwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.XtlH0r1Q8tf2LjYMiFt1f1fF19j3bVPWIfS57JPi4Do', CAST(N'2017-03-14T19:54:21.000' AS DateTime), CAST(N'2017-03-14T20:54:21.000' AS DateTime), CAST(N'2017-03-14T19:56:17.567' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (173, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZDFkMTRhOWQtMjczMS00ZmUxLWE3MTktOTkyZTU2MDQ3MjY2IiwiaWF0IjoxNDg5NTIxMDQ3LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MjEzODAsImV4cCI6MTQ4OTUyNDk4MCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.Iu7pzFFaZmsbXymTxu4InrJYc2qtPEeL_FuY7uZT42Y', CAST(N'2017-03-14T19:56:20.000' AS DateTime), CAST(N'2017-03-14T20:56:20.000' AS DateTime), CAST(N'2017-03-14T19:59:26.353' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (174, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNjhkMWI0MmUtZDQ4Zi00OGRkLTlhMWMtMzBiYjgyZDgxMWMzIiwiaWF0IjoxNDg5NTIxNTY1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MjE1NjksImV4cCI6MTQ4OTUyNTE2OSwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.mRba6PtGuDKeiPYKwH4BfzcYZkwN8m_Hx0G_GtBQ8Z0', CAST(N'2017-03-14T19:59:29.000' AS DateTime), CAST(N'2017-03-14T20:59:29.000' AS DateTime), CAST(N'2017-03-14T19:59:36.363' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (175, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZDI5ODkxN2EtODMxZC00ZmUwLTg3YzctODEwNjExYTQ3ZmFhIiwiaWF0IjoxNDg5NTIxNTY1LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MjE1ODMsImV4cCI6MTQ4OTUyNTE4MywiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.dFYs-L2zz0vsm75N_wcmYyGjxiLrMHN7LGIVdl5H2s8', CAST(N'2017-03-14T19:59:43.000' AS DateTime), CAST(N'2017-03-14T20:59:43.000' AS DateTime), CAST(N'2017-03-15T00:37:20.417' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (176, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMDViOTk2YzQtNzFlNS00OTUwLTk5NjEtMzZhMjVjN2Y5ODYxIiwiaWF0IjoxNDg5NTM4MjM5LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1MzgyNDAsImV4cCI6MTQ4OTU0MTg0MCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.Qz8GTAailoL94M87dvMuGPK3mtR7VU4yUGTxzXbEfcs', CAST(N'2017-03-15T00:37:20.000' AS DateTime), CAST(N'2017-03-15T01:37:20.000' AS DateTime), CAST(N'2017-03-15T00:57:25.847' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (177, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZDEwYWQ0NTMtMGU2Yi00Y2NiLWI2MjQtZDhmZTU5ZjVkZGE3IiwiaWF0IjoxNDg5NTM4MjM5LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1Mzk2NzYsImV4cCI6MTQ4OTU0MDI4MCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.nmlZQbOyTV-62LqjlrTR07-jeM5aqShIaDzGkg2Fnrs', CAST(N'2017-03-15T01:01:16.000' AS DateTime), CAST(N'2017-03-15T01:11:20.000' AS DateTime), CAST(N'2017-03-15T02:07:44.910' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (178, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiOWY0N2IxMTItOGNmMC00MTFmLWI3NzItYzc4YzdhODZiOGFiIiwiaWF0IjoxNDg5NTQzNjU3LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1NDM2NjQsImV4cCI6MTQ4OTU0NDI2NCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.fKTF4fTp2I2l5SUOXoEQtwWuLObl32oRnuKJRG7W_1Q', CAST(N'2017-03-15T02:07:44.000' AS DateTime), CAST(N'2017-03-15T02:17:44.000' AS DateTime), CAST(N'2017-03-15T02:12:05.130' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (179, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMTg1N2IzMTQtMmMzYi00Y2I1LTk2ZTgtNDc3NzgzY2M4ZjJmIiwiaWF0IjoxNDg5NTQzOTEzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk1NDM5MjUsImV4cCI6MTQ4OTU0NDUyNSwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.ubPc7-egTwxdwkaKVKFBl7jSLFw-X4i1WTKIX0J37oY', CAST(N'2017-03-15T02:12:05.000' AS DateTime), CAST(N'2017-03-15T02:22:05.000' AS DateTime), CAST(N'2017-03-15T02:12:41.300' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (180, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiOWViMGYzYWItN2EyZC00YmVjLWI2M2ItMWRjNjY0OTU1ZWU3IiwiaWF0IjoxNDg5NjExOTE2LCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk2MTE5NDksImV4cCI6MTQ4OTYxMjU0OSwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.ma1AfiUGi5D20_D97xaXM8HjrK6owxQlOb_oQUA3nso', CAST(N'2017-03-15T21:05:49.000' AS DateTime), CAST(N'2017-03-15T21:15:49.000' AS DateTime), CAST(N'2017-03-15T21:06:49.187' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (181, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZDc5YjM3MjctNTVkOS00MDc5LTk1ZWUtYzAzYTA4NTRhZGE0IiwiaWF0IjoxNDg5Njc1NjQwLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk2NzYxMjMsImV4cCI6MTQ4OTY3NjcyMywiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.cegFUcG8Fhi_a7TwXoqc25wULj8D0rI1w5rHxTFxnzE', CAST(N'2017-03-16T14:55:23.000' AS DateTime), CAST(N'2017-03-16T15:05:23.000' AS DateTime), CAST(N'2017-03-16T14:56:56.303' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (182, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYzI4ZTllYTMtYjgyMy00NTM0LWJmZDgtMWRkMGY1NGI4ODIxIiwiaWF0IjoxNDg5NjkxNDIxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk2OTE0MjUsImV4cCI6MTQ4OTY5MjAyNSwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.BAWJ0DrEmTTo8PTAZMcCYsIRaKeoGTU6fUHrdyd27lY', CAST(N'2017-03-16T19:10:25.000' AS DateTime), CAST(N'2017-03-16T19:20:25.000' AS DateTime), CAST(N'2017-03-16T19:20:56.610' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (183, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYjViYTdlMGItYmNkMS00NjExLWJmOTgtNDQ0YTgxM2QxYjc2IiwiaWF0IjoxNDg5NjkxNDIxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk2OTIwNTYsImV4cCI6MTQ4OTY5MjY1NiwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.rOSr5SbGqxBaepCPOZoCGX6v0UPx0pVtydpsU_ngZGA', CAST(N'2017-03-16T19:20:56.000' AS DateTime), CAST(N'2017-03-16T19:30:56.000' AS DateTime), CAST(N'2017-03-16T19:24:13.527' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (184, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYWYyMzExNDItODc2NS00MTU2LWFkMmQtZmNiMmQ1ZTUzZWU5IiwiaWF0IjoxNDg5NjkxNDIxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk2OTIyNjEsImV4cCI6MTQ4OTY5Mjg2MSwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.3sBktWNFMFH1nsHg7xCbbynKcOXXxpqcgXPsBCVlm58', CAST(N'2017-03-16T19:24:21.000' AS DateTime), CAST(N'2017-03-16T19:34:21.000' AS DateTime), CAST(N'2017-03-16T19:29:12.790' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (185, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZWM2NDBmZGYtMTlkMi00MGM1LWI3ODQtYTZiODYyNmEyMDk2IiwiaWF0IjoxNDg5NjkxNDIxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk2OTI1NTcsImV4cCI6MTQ4OTY5MzE1NywiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.rfBT47zh6j8uwTF3nqgA2_1dxQ7puzWcqzTivaxGpyg', CAST(N'2017-03-16T19:29:17.000' AS DateTime), CAST(N'2017-03-16T19:39:17.000' AS DateTime), CAST(N'2017-03-16T19:31:28.683' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (186, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMzk0ZjRiMjUtNWY4NS00OWFhLTlmY2EtOTYxZmNiMTU2ODA3IiwiaWF0IjoxNDg5NjkxNDIxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk2OTI2OTEsImV4cCI6MTQ4OTY5MzI5MSwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.bH-YOcG1tEwPunRs1Eb2EfgDp5pe5Mpq3WI2et-jp9o', CAST(N'2017-03-16T19:31:31.000' AS DateTime), CAST(N'2017-03-16T19:41:31.000' AS DateTime), CAST(N'2017-03-16T19:31:35.160' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (187, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZWVmNjA3OWUtMmEwYS00ZTM3LWI0NWMtNThjN2FhYWRlYmUwIiwiaWF0IjoxNDg5NjkxNDIxLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk2OTMzMzEsImV4cCI6MTQ4OTY5MzkzMSwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.2pj3UK1DoLFnx_4vxe6igsdniOlvwDZpqcwOvhNznMs', CAST(N'2017-03-16T19:42:11.000' AS DateTime), CAST(N'2017-03-16T19:52:11.000' AS DateTime), CAST(N'2017-03-16T19:59:28.923' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (188, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiZjg3NmU3NDMtMmE3ZS00NDUwLWE4ZTQtN2JiYzNhZWY5MTM0IiwiaWF0IjoxNDg5Njk0MzYzLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk2OTQzNjgsImV4cCI6MTQ4OTY5NDk2OCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.OaVhaWSZmmxJHcu6D6ntJd2PtSXb-f3dObmGYg4sKXM', CAST(N'2017-03-16T19:59:28.000' AS DateTime), CAST(N'2017-03-16T20:09:28.000' AS DateTime), CAST(N'2017-03-16T20:31:53.507' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (189, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMjA3YzVlNWEtYzU4Mi00ZmJmLWJiZDUtZTc2ODQ2NzhiNDEzIiwiaWF0IjoxNDg5Njk2MzEyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk2OTYzMTMsImV4cCI6MTQ4OTY5NjkxMywiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.nklU7bU1XJ11AwkEOKikze4j-fl0LjmgSlyPnC-jXAg', CAST(N'2017-03-16T20:31:53.000' AS DateTime), CAST(N'2017-03-16T20:41:53.000' AS DateTime), CAST(N'2017-03-16T20:32:10.550' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (190, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYjhjYmE2NDAtYjVkYy00MzZmLTg1OTItYTA1NTUwY2NlNmQyIiwiaWF0IjoxNDg5Njk2MzEyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk2OTYzMzAsImV4cCI6MTQ4OTY5NjkzMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.N38KoGyF6CDpik64lUpGUWmwCaSp_RkL4MSTglsQYJ0', CAST(N'2017-03-16T20:32:10.000' AS DateTime), CAST(N'2017-03-16T20:42:10.000' AS DateTime), CAST(N'2017-03-16T20:34:04.060' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (191, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiYjI5NTBjOWItNDgyNC00ZmY2LTkxZDMtNTcyZjFlYzEwMDVhIiwiaWF0IjoxNDg5Njk2NDQyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk2OTY0NDMsImV4cCI6MTQ4OTY5NzA0MywiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.cCj6t3swNCQ-MZqkGzBH8DoHs0ASC33C_eQq_AEpwP0', CAST(N'2017-03-16T20:34:03.000' AS DateTime), CAST(N'2017-03-16T20:44:03.000' AS DateTime), CAST(N'2017-03-16T20:34:21.337' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (192, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiMjM4YWUwYzUtYzMzOS00ZDNkLWEzOTQtNjhkMjgxZTFmYmM4IiwiaWF0IjoxNDg5Njk2NDQyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk2OTY0NjEsImV4cCI6MTQ4OTY5NzA2MSwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.SsRF__Hb0zr_wqqQFGyv_kOW-iRWsMwSeWc6YfEWNrk', CAST(N'2017-03-16T20:34:21.000' AS DateTime), CAST(N'2017-03-16T20:44:21.000' AS DateTime), CAST(N'2017-03-16T20:37:34.673' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (193, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNzFjYTgzOTItOTJkZi00ODk0LTk0NzYtODJkNjRlZmVhZjk2IiwiaWF0IjoxNDg5Njk2NDQyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk2OTY2NTQsImV4cCI6MTQ4OTY5NzI1NCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.uJhtsDXiKUVxPVx9q2TeD_Knft_rX1MViRePaFWtOqI', CAST(N'2017-03-16T20:37:34.000' AS DateTime), CAST(N'2017-03-16T20:47:34.000' AS DateTime), CAST(N'2017-03-16T20:38:06.973' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (194, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNjFhMTQ3MzUtZGY5ZS00OWJhLWE0ZjUtOGVlZDI0MTNkMTAxIiwiaWF0IjoxNDg5Njk2NDQyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk2OTY2OTEsImV4cCI6MTQ4OTY5NzI5MSwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.tSUnWoFZ34_O2rR-jCiheSA4kRz2JN1vYUo-O-qKgGM', CAST(N'2017-03-16T20:38:11.000' AS DateTime), CAST(N'2017-03-16T20:48:11.000' AS DateTime), CAST(N'2017-03-16T20:58:30.260' AS DateTime), N'Web', N'INA')
INSERT [dbo].[UsuarioToken] ([IDUsuarioToken], [IDUsuario], [Token], [FechaInicio], [FechaExpiracion], [FechaSalida], [Canal], [Estatus]) VALUES (195, 3, N'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJBZG1pbmlzdHJhZG9yIiwianRpIjoiNWNlNmFmZjktMWNjZi00MzVkLTk5N2ItZTVjYTNmODViYjU1IiwiaWF0IjoxNDg5Njk2NDQyLCJOYW1lIjoiQWRtaW5pc3RyYWRvciIsIlVzZXJJRCI6IjMiLCJuYmYiOjE0ODk2OTc5MTAsImV4cCI6MTQ4OTY5ODUxMCwiaXNzIjoibklTSzItTztqQVNCSz0tQHNkLXNHU0Q4M3ExX2RmZ2hqYXNpeCIsImF1ZCI6Imh0dHA6Ly9qZWZyaWRldi0wMDEtc2l0ZTMuY3RlbXB1cmwuY29tIn0.lGJKjXK7kY3YGvZ6fy03V5kcJb-q-UM1soalX030evM', CAST(N'2017-03-16T20:58:30.000' AS DateTime), CAST(N'2017-03-16T21:08:30.000' AS DateTime), NULL, N'Web', N'ACT')
SET IDENTITY_INSERT [dbo].[UsuarioToken] OFF
ALTER TABLE [dbo].[Ajuste]  WITH CHECK ADD  CONSTRAINT [FK_Ajuste_Almacen] FOREIGN KEY([IDAlmacen])
REFERENCES [dbo].[Almacen] ([IDAlmacen])
GO
ALTER TABLE [dbo].[Ajuste] CHECK CONSTRAINT [FK_Ajuste_Almacen]
GO
ALTER TABLE [dbo].[Ajuste]  WITH CHECK ADD  CONSTRAINT [FK_Ajuste_Empresa] FOREIGN KEY([IDEmpresa])
REFERENCES [dbo].[Empresa] ([IDEmpresa])
GO
ALTER TABLE [dbo].[Ajuste] CHECK CONSTRAINT [FK_Ajuste_Empresa]
GO
ALTER TABLE [dbo].[Ajuste]  WITH CHECK ADD  CONSTRAINT [FK_Ajuste_TipoAjuste] FOREIGN KEY([IDTipoAjuste])
REFERENCES [dbo].[TipoAjuste] ([IDTipoAjuste])
GO
ALTER TABLE [dbo].[Ajuste] CHECK CONSTRAINT [FK_Ajuste_TipoAjuste]
GO
ALTER TABLE [dbo].[Ajuste]  WITH CHECK ADD  CONSTRAINT [FK_Ajuste_Usuario] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Usuario] ([IDUsuario])
GO
ALTER TABLE [dbo].[Ajuste] CHECK CONSTRAINT [FK_Ajuste_Usuario]
GO
ALTER TABLE [dbo].[AjusteProducto]  WITH CHECK ADD  CONSTRAINT [FK_AjusteProducto_Ajuste] FOREIGN KEY([IDAjuste])
REFERENCES [dbo].[Ajuste] ([IDAjuste])
GO
ALTER TABLE [dbo].[AjusteProducto] CHECK CONSTRAINT [FK_AjusteProducto_Ajuste]
GO
ALTER TABLE [dbo].[AjusteProducto]  WITH CHECK ADD  CONSTRAINT [FK_AjusteProducto_Producto] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Producto] ([IDProducto])
GO
ALTER TABLE [dbo].[AjusteProducto] CHECK CONSTRAINT [FK_AjusteProducto_Producto]
GO
ALTER TABLE [dbo].[Almacen]  WITH CHECK ADD  CONSTRAINT [FK_Almacen_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([IDSucursal])
GO
ALTER TABLE [dbo].[Almacen] CHECK CONSTRAINT [FK_Almacen_Sucursal]
GO
ALTER TABLE [dbo].[Caja]  WITH CHECK ADD  CONSTRAINT [FK_Caja_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([IDSucursal])
GO
ALTER TABLE [dbo].[Caja] CHECK CONSTRAINT [FK_Caja_Sucursal]
GO
ALTER TABLE [dbo].[Caja]  WITH CHECK ADD  CONSTRAINT [FK_Caja_Tanda] FOREIGN KEY([IDTanda])
REFERENCES [dbo].[Tanda] ([IDTanda])
GO
ALTER TABLE [dbo].[Caja] CHECK CONSTRAINT [FK_Caja_Tanda]
GO
ALTER TABLE [dbo].[Caja]  WITH CHECK ADD  CONSTRAINT [FK_Caja_Usuario] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Usuario] ([IDUsuario])
GO
ALTER TABLE [dbo].[Caja] CHECK CONSTRAINT [FK_Caja_Usuario]
GO
ALTER TABLE [dbo].[Cierre]  WITH CHECK ADD  CONSTRAINT [FK_Cierre_Caja] FOREIGN KEY([IDCaja])
REFERENCES [dbo].[Caja] ([IDCaja])
GO
ALTER TABLE [dbo].[Cierre] CHECK CONSTRAINT [FK_Cierre_Caja]
GO
ALTER TABLE [dbo].[Cierre]  WITH CHECK ADD  CONSTRAINT [FK_Cierre_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([IDSucursal])
GO
ALTER TABLE [dbo].[Cierre] CHECK CONSTRAINT [FK_Cierre_Sucursal]
GO
ALTER TABLE [dbo].[Cierre]  WITH CHECK ADD  CONSTRAINT [FK_Cierre_Tanda] FOREIGN KEY([IDTanda])
REFERENCES [dbo].[Tanda] ([IDTanda])
GO
ALTER TABLE [dbo].[Cierre] CHECK CONSTRAINT [FK_Cierre_Tanda]
GO
ALTER TABLE [dbo].[Cierre]  WITH CHECK ADD  CONSTRAINT [FK_Cierre_Usuario] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Usuario] ([IDUsuario])
GO
ALTER TABLE [dbo].[Cierre] CHECK CONSTRAINT [FK_Cierre_Usuario]
GO
ALTER TABLE [dbo].[Cliente]  WITH CHECK ADD  CONSTRAINT [FK_Cliente_Empresa] FOREIGN KEY([IDEmpresa])
REFERENCES [dbo].[Empresa] ([IDEmpresa])
GO
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Empresa]
GO
ALTER TABLE [dbo].[Cliente]  WITH CHECK ADD  CONSTRAINT [FK_Cliente_TipoCliente] FOREIGN KEY([IDTipoCliente])
REFERENCES [dbo].[TipoCliente] ([IDTipoCliente])
GO
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_TipoCliente]
GO
ALTER TABLE [dbo].[Cliente]  WITH CHECK ADD  CONSTRAINT [FK_Cliente_TipoIdentificacion] FOREIGN KEY([IDTipoIdentificacion])
REFERENCES [dbo].[TipoIdentificacion] ([IDTipoIdentificacion])
GO
ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_TipoIdentificacion]
GO
ALTER TABLE [dbo].[ClienteCorreoElectronico]  WITH CHECK ADD  CONSTRAINT [FK_ClienteCorreoElectronico_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([IDCliente])
GO
ALTER TABLE [dbo].[ClienteCorreoElectronico] CHECK CONSTRAINT [FK_ClienteCorreoElectronico_Cliente]
GO
ALTER TABLE [dbo].[ClienteCorreoElectronico]  WITH CHECK ADD  CONSTRAINT [FK_ClienteCorreoElectronico_CorreoElectronico] FOREIGN KEY([IDCorreoElectronico])
REFERENCES [dbo].[CorreoElectronico] ([IDCorreoElectronico])
GO
ALTER TABLE [dbo].[ClienteCorreoElectronico] CHECK CONSTRAINT [FK_ClienteCorreoElectronico_CorreoElectronico]
GO
ALTER TABLE [dbo].[ClienteDireccion]  WITH CHECK ADD  CONSTRAINT [FK_ClienteDireccion_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([IDCliente])
GO
ALTER TABLE [dbo].[ClienteDireccion] CHECK CONSTRAINT [FK_ClienteDireccion_Cliente]
GO
ALTER TABLE [dbo].[ClienteDireccion]  WITH CHECK ADD  CONSTRAINT [FK_ClienteDireccion_Direccion] FOREIGN KEY([IDDireccion])
REFERENCES [dbo].[Direccion] ([IDDireccion])
GO
ALTER TABLE [dbo].[ClienteDireccion] CHECK CONSTRAINT [FK_ClienteDireccion_Direccion]
GO
ALTER TABLE [dbo].[ClienteTelefono]  WITH CHECK ADD  CONSTRAINT [FK_ClienteTelefono_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([IDCliente])
GO
ALTER TABLE [dbo].[ClienteTelefono] CHECK CONSTRAINT [FK_ClienteTelefono_Cliente]
GO
ALTER TABLE [dbo].[ClienteTelefono]  WITH CHECK ADD  CONSTRAINT [FK_ClienteTelefono_Telefono] FOREIGN KEY([IDTelefono])
REFERENCES [dbo].[Telefono] ([IDTelefono])
GO
ALTER TABLE [dbo].[ClienteTelefono] CHECK CONSTRAINT [FK_ClienteTelefono_Telefono]
GO
ALTER TABLE [dbo].[Comprobante]  WITH CHECK ADD  CONSTRAINT [FK_Comprobante_Empresa] FOREIGN KEY([IDEmpresa])
REFERENCES [dbo].[Empresa] ([IDEmpresa])
GO
ALTER TABLE [dbo].[Comprobante] CHECK CONSTRAINT [FK_Comprobante_Empresa]
GO
ALTER TABLE [dbo].[Comprobante]  WITH CHECK ADD  CONSTRAINT [FK_Comprobante_TipoComprobante] FOREIGN KEY([IDTipoComprobante])
REFERENCES [dbo].[TipoComprobante] ([IDTipoComprobante])
GO
ALTER TABLE [dbo].[Comprobante] CHECK CONSTRAINT [FK_Comprobante_TipoComprobante]
GO
ALTER TABLE [dbo].[Conduce]  WITH CHECK ADD  CONSTRAINT [FK_Conduce_Almacen] FOREIGN KEY([IDAlmacen])
REFERENCES [dbo].[Almacen] ([IDAlmacen])
GO
ALTER TABLE [dbo].[Conduce] CHECK CONSTRAINT [FK_Conduce_Almacen]
GO
ALTER TABLE [dbo].[Conduce]  WITH CHECK ADD  CONSTRAINT [FK_Conduce_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([IDCliente])
GO
ALTER TABLE [dbo].[Conduce] CHECK CONSTRAINT [FK_Conduce_Cliente]
GO
ALTER TABLE [dbo].[Conduce]  WITH CHECK ADD  CONSTRAINT [FK_Conduce_Empresa] FOREIGN KEY([IDEmpresa])
REFERENCES [dbo].[Empresa] ([IDEmpresa])
GO
ALTER TABLE [dbo].[Conduce] CHECK CONSTRAINT [FK_Conduce_Empresa]
GO
ALTER TABLE [dbo].[Conduce]  WITH CHECK ADD  CONSTRAINT [FK_Conduce_Factura] FOREIGN KEY([IDFactura])
REFERENCES [dbo].[Factura] ([IDFactura])
GO
ALTER TABLE [dbo].[Conduce] CHECK CONSTRAINT [FK_Conduce_Factura]
GO
ALTER TABLE [dbo].[Conduce]  WITH CHECK ADD  CONSTRAINT [FK_Conduce_Usuario] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Usuario] ([IDUsuario])
GO
ALTER TABLE [dbo].[Conduce] CHECK CONSTRAINT [FK_Conduce_Usuario]
GO
ALTER TABLE [dbo].[ConduceProducto]  WITH CHECK ADD  CONSTRAINT [FK_ConduceProducto_Conduce] FOREIGN KEY([IDConduce])
REFERENCES [dbo].[Conduce] ([IDConduce])
GO
ALTER TABLE [dbo].[ConduceProducto] CHECK CONSTRAINT [FK_ConduceProducto_Conduce]
GO
ALTER TABLE [dbo].[ConduceProducto]  WITH CHECK ADD  CONSTRAINT [FK_ConduceProducto_Producto] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Producto] ([IDProducto])
GO
ALTER TABLE [dbo].[ConduceProducto] CHECK CONSTRAINT [FK_ConduceProducto_Producto]
GO
ALTER TABLE [dbo].[Contacto]  WITH CHECK ADD  CONSTRAINT [FK_Contacto_TipoContacto] FOREIGN KEY([IDTipoContacto])
REFERENCES [dbo].[TipoContacto] ([IDTipoContacto])
GO
ALTER TABLE [dbo].[Contacto] CHECK CONSTRAINT [FK_Contacto_TipoContacto]
GO
ALTER TABLE [dbo].[ContactoCorreoElectronico]  WITH CHECK ADD  CONSTRAINT [FK_ContactoCorreoElectronico_Contacto] FOREIGN KEY([IDContacto])
REFERENCES [dbo].[Contacto] ([IDContacto])
GO
ALTER TABLE [dbo].[ContactoCorreoElectronico] CHECK CONSTRAINT [FK_ContactoCorreoElectronico_Contacto]
GO
ALTER TABLE [dbo].[ContactoCorreoElectronico]  WITH CHECK ADD  CONSTRAINT [FK_ContactoCorreoElectronico_CorreoElectronico] FOREIGN KEY([IDCorreoElectronico])
REFERENCES [dbo].[CorreoElectronico] ([IDCorreoElectronico])
GO
ALTER TABLE [dbo].[ContactoCorreoElectronico] CHECK CONSTRAINT [FK_ContactoCorreoElectronico_CorreoElectronico]
GO
ALTER TABLE [dbo].[ContactoDireccion]  WITH CHECK ADD  CONSTRAINT [FK_ContactoDireccion_Contacto] FOREIGN KEY([IDContacto])
REFERENCES [dbo].[Contacto] ([IDContacto])
GO
ALTER TABLE [dbo].[ContactoDireccion] CHECK CONSTRAINT [FK_ContactoDireccion_Contacto]
GO
ALTER TABLE [dbo].[ContactoDireccion]  WITH CHECK ADD  CONSTRAINT [FK_ContactoDireccion_Direccion] FOREIGN KEY([IDDireccion])
REFERENCES [dbo].[Direccion] ([IDDireccion])
GO
ALTER TABLE [dbo].[ContactoDireccion] CHECK CONSTRAINT [FK_ContactoDireccion_Direccion]
GO
ALTER TABLE [dbo].[ContactoTelefono]  WITH CHECK ADD  CONSTRAINT [FK_ContactoTelefono_Contacto] FOREIGN KEY([IDContacto])
REFERENCES [dbo].[Contacto] ([IDContacto])
GO
ALTER TABLE [dbo].[ContactoTelefono] CHECK CONSTRAINT [FK_ContactoTelefono_Contacto]
GO
ALTER TABLE [dbo].[ContactoTelefono]  WITH CHECK ADD  CONSTRAINT [FK_ContactoTelefono_Telefono] FOREIGN KEY([IDContacto])
REFERENCES [dbo].[Telefono] ([IDTelefono])
GO
ALTER TABLE [dbo].[ContactoTelefono] CHECK CONSTRAINT [FK_ContactoTelefono_Telefono]
GO
ALTER TABLE [dbo].[CorreoElectronico]  WITH CHECK ADD  CONSTRAINT [FK_CorreoElectronico_TipoCorreoElectronico] FOREIGN KEY([IDTipoCorreoElectronico])
REFERENCES [dbo].[TipoCorreoElectronico] ([IDTipoCorreoElectronico])
GO
ALTER TABLE [dbo].[CorreoElectronico] CHECK CONSTRAINT [FK_CorreoElectronico_TipoCorreoElectronico]
GO
ALTER TABLE [dbo].[CuadreCaja]  WITH CHECK ADD  CONSTRAINT [FK_CuadreCaja_Empresa] FOREIGN KEY([IDEmpresa])
REFERENCES [dbo].[Empresa] ([IDEmpresa])
GO
ALTER TABLE [dbo].[CuadreCaja] CHECK CONSTRAINT [FK_CuadreCaja_Empresa]
GO
ALTER TABLE [dbo].[CuadreCaja]  WITH CHECK ADD  CONSTRAINT [FK_CuadreCaja_Moneda] FOREIGN KEY([IDMoneda])
REFERENCES [dbo].[Moneda] ([IDMoneda])
GO
ALTER TABLE [dbo].[CuadreCaja] CHECK CONSTRAINT [FK_CuadreCaja_Moneda]
GO
ALTER TABLE [dbo].[CuadreCaja]  WITH CHECK ADD  CONSTRAINT [FK_CuadreCaja_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([IDSucursal])
GO
ALTER TABLE [dbo].[CuadreCaja] CHECK CONSTRAINT [FK_CuadreCaja_Sucursal]
GO
ALTER TABLE [dbo].[CuadreCaja]  WITH CHECK ADD  CONSTRAINT [FK_CuadreCaja_Usuario] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Usuario] ([IDUsuario])
GO
ALTER TABLE [dbo].[CuadreCaja] CHECK CONSTRAINT [FK_CuadreCaja_Usuario]
GO
ALTER TABLE [dbo].[Devolucion]  WITH CHECK ADD  CONSTRAINT [FK_Devolucion_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([IDCliente])
GO
ALTER TABLE [dbo].[Devolucion] CHECK CONSTRAINT [FK_Devolucion_Cliente]
GO
ALTER TABLE [dbo].[Devolucion]  WITH CHECK ADD  CONSTRAINT [FK_Devolucion_Empresa] FOREIGN KEY([IDEmpresa])
REFERENCES [dbo].[Empresa] ([IDEmpresa])
GO
ALTER TABLE [dbo].[Devolucion] CHECK CONSTRAINT [FK_Devolucion_Empresa]
GO
ALTER TABLE [dbo].[Devolucion]  WITH CHECK ADD  CONSTRAINT [FK_Devolucion_Factura] FOREIGN KEY([IDFactura])
REFERENCES [dbo].[Factura] ([IDFactura])
GO
ALTER TABLE [dbo].[Devolucion] CHECK CONSTRAINT [FK_Devolucion_Factura]
GO
ALTER TABLE [dbo].[DevolucionProducto]  WITH CHECK ADD  CONSTRAINT [FK_DevolucionProducto_Almacen] FOREIGN KEY([IDAlmacen])
REFERENCES [dbo].[Almacen] ([IDAlmacen])
GO
ALTER TABLE [dbo].[DevolucionProducto] CHECK CONSTRAINT [FK_DevolucionProducto_Almacen]
GO
ALTER TABLE [dbo].[DevolucionProducto]  WITH CHECK ADD  CONSTRAINT [FK_DevolucionProducto_Devolucion] FOREIGN KEY([IDDevolucion])
REFERENCES [dbo].[Devolucion] ([IDDevolucion])
GO
ALTER TABLE [dbo].[DevolucionProducto] CHECK CONSTRAINT [FK_DevolucionProducto_Devolucion]
GO
ALTER TABLE [dbo].[DevolucionProducto]  WITH CHECK ADD  CONSTRAINT [FK_DevolucionProducto_Producto] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Producto] ([IDProducto])
GO
ALTER TABLE [dbo].[DevolucionProducto] CHECK CONSTRAINT [FK_DevolucionProducto_Producto]
GO
ALTER TABLE [dbo].[Direccion]  WITH CHECK ADD  CONSTRAINT [FK_Direccion_TipoDireccion] FOREIGN KEY([IDTipoDireccion])
REFERENCES [dbo].[TipoDireccion] ([IDTipoDireccion])
GO
ALTER TABLE [dbo].[Direccion] CHECK CONSTRAINT [FK_Direccion_TipoDireccion]
GO
ALTER TABLE [dbo].[Empleado]  WITH CHECK ADD  CONSTRAINT [FK_Empleado_Empresa] FOREIGN KEY([IDEmpresa])
REFERENCES [dbo].[Empresa] ([IDEmpresa])
GO
ALTER TABLE [dbo].[Empleado] CHECK CONSTRAINT [FK_Empleado_Empresa]
GO
ALTER TABLE [dbo].[Empleado]  WITH CHECK ADD  CONSTRAINT [FK_Empleado_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([IDSucursal])
GO
ALTER TABLE [dbo].[Empleado] CHECK CONSTRAINT [FK_Empleado_Sucursal]
GO
ALTER TABLE [dbo].[Empleado]  WITH CHECK ADD  CONSTRAINT [FK_Empleado_TipoIdentificacion] FOREIGN KEY([IDTipoIdentificacion])
REFERENCES [dbo].[TipoIdentificacion] ([IDTipoIdentificacion])
GO
ALTER TABLE [dbo].[Empleado] CHECK CONSTRAINT [FK_Empleado_TipoIdentificacion]
GO
ALTER TABLE [dbo].[EmpleadoCorreoElectronico]  WITH CHECK ADD  CONSTRAINT [FK_EmpleadoCorreoElectronico_CorreoElectronico] FOREIGN KEY([IDCorreoElectronico])
REFERENCES [dbo].[CorreoElectronico] ([IDCorreoElectronico])
GO
ALTER TABLE [dbo].[EmpleadoCorreoElectronico] CHECK CONSTRAINT [FK_EmpleadoCorreoElectronico_CorreoElectronico]
GO
ALTER TABLE [dbo].[EmpleadoCorreoElectronico]  WITH CHECK ADD  CONSTRAINT [FK_EmpleadoCorreoElectronico_Empleado] FOREIGN KEY([IDEmpleado])
REFERENCES [dbo].[Empleado] ([IDEmpleado])
GO
ALTER TABLE [dbo].[EmpleadoCorreoElectronico] CHECK CONSTRAINT [FK_EmpleadoCorreoElectronico_Empleado]
GO
ALTER TABLE [dbo].[EmpleadoDireccion]  WITH CHECK ADD  CONSTRAINT [FK_EmpleadoDireccion_Direccion] FOREIGN KEY([IDDireccion])
REFERENCES [dbo].[Direccion] ([IDDireccion])
GO
ALTER TABLE [dbo].[EmpleadoDireccion] CHECK CONSTRAINT [FK_EmpleadoDireccion_Direccion]
GO
ALTER TABLE [dbo].[EmpleadoDireccion]  WITH CHECK ADD  CONSTRAINT [FK_EmpleadoDireccion_Empleado] FOREIGN KEY([IDEmpleado])
REFERENCES [dbo].[Empleado] ([IDEmpleado])
GO
ALTER TABLE [dbo].[EmpleadoDireccion] CHECK CONSTRAINT [FK_EmpleadoDireccion_Empleado]
GO
ALTER TABLE [dbo].[EmpleadoTelefono]  WITH CHECK ADD  CONSTRAINT [FK_EmpleadoTelefono_Empleado] FOREIGN KEY([IDEmpleado])
REFERENCES [dbo].[Empleado] ([IDEmpleado])
GO
ALTER TABLE [dbo].[EmpleadoTelefono] CHECK CONSTRAINT [FK_EmpleadoTelefono_Empleado]
GO
ALTER TABLE [dbo].[EmpleadoTelefono]  WITH CHECK ADD  CONSTRAINT [FK_EmpleadoTelefono_Telefono] FOREIGN KEY([IDTelefono])
REFERENCES [dbo].[Telefono] ([IDTelefono])
GO
ALTER TABLE [dbo].[EmpleadoTelefono] CHECK CONSTRAINT [FK_EmpleadoTelefono_Telefono]
GO
ALTER TABLE [dbo].[Entrada]  WITH CHECK ADD  CONSTRAINT [FK_Entrada_Almacen] FOREIGN KEY([IDAlmacen])
REFERENCES [dbo].[Almacen] ([IDAlmacen])
GO
ALTER TABLE [dbo].[Entrada] CHECK CONSTRAINT [FK_Entrada_Almacen]
GO
ALTER TABLE [dbo].[Entrada]  WITH CHECK ADD  CONSTRAINT [FK_Entrada_Empresa] FOREIGN KEY([IDEmpresa])
REFERENCES [dbo].[Empresa] ([IDEmpresa])
GO
ALTER TABLE [dbo].[Entrada] CHECK CONSTRAINT [FK_Entrada_Empresa]
GO
ALTER TABLE [dbo].[Entrada]  WITH CHECK ADD  CONSTRAINT [FK_Entrada_Suplidor] FOREIGN KEY([IDSuplidor])
REFERENCES [dbo].[Suplidor] ([IDSuplidor])
GO
ALTER TABLE [dbo].[Entrada] CHECK CONSTRAINT [FK_Entrada_Suplidor]
GO
ALTER TABLE [dbo].[Entrada]  WITH CHECK ADD  CONSTRAINT [FK_Entrada_Usuario] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Usuario] ([IDUsuario])
GO
ALTER TABLE [dbo].[Entrada] CHECK CONSTRAINT [FK_Entrada_Usuario]
GO
ALTER TABLE [dbo].[EntradaProducto]  WITH CHECK ADD  CONSTRAINT [FK_EntradaProducto_Entrada] FOREIGN KEY([IDEntrada])
REFERENCES [dbo].[Entrada] ([IDEntrada])
GO
ALTER TABLE [dbo].[EntradaProducto] CHECK CONSTRAINT [FK_EntradaProducto_Entrada]
GO
ALTER TABLE [dbo].[EntradaProducto]  WITH CHECK ADD  CONSTRAINT [FK_EntradaProducto_Producto] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Producto] ([IDProducto])
GO
ALTER TABLE [dbo].[EntradaProducto] CHECK CONSTRAINT [FK_EntradaProducto_Producto]
GO
ALTER TABLE [dbo].[Factura]  WITH CHECK ADD  CONSTRAINT [FK_Factura_Caja] FOREIGN KEY([IDCaja])
REFERENCES [dbo].[Caja] ([IDCaja])
GO
ALTER TABLE [dbo].[Factura] CHECK CONSTRAINT [FK_Factura_Caja]
GO
ALTER TABLE [dbo].[Factura]  WITH CHECK ADD  CONSTRAINT [FK_Factura_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([IDCliente])
GO
ALTER TABLE [dbo].[Factura] CHECK CONSTRAINT [FK_Factura_Cliente]
GO
ALTER TABLE [dbo].[Factura]  WITH CHECK ADD  CONSTRAINT [FK_Factura_Empresa] FOREIGN KEY([IDEmpresa])
REFERENCES [dbo].[Empresa] ([IDEmpresa])
GO
ALTER TABLE [dbo].[Factura] CHECK CONSTRAINT [FK_Factura_Empresa]
GO
ALTER TABLE [dbo].[Factura]  WITH CHECK ADD  CONSTRAINT [FK_Factura_ModoPago] FOREIGN KEY([IDModoPago])
REFERENCES [dbo].[ModoPago] ([IDModoPago])
GO
ALTER TABLE [dbo].[Factura] CHECK CONSTRAINT [FK_Factura_ModoPago]
GO
ALTER TABLE [dbo].[Factura]  WITH CHECK ADD  CONSTRAINT [FK_Factura_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([IDSucursal])
GO
ALTER TABLE [dbo].[Factura] CHECK CONSTRAINT [FK_Factura_Sucursal]
GO
ALTER TABLE [dbo].[Factura]  WITH CHECK ADD  CONSTRAINT [FK_Factura_TipoPago] FOREIGN KEY([IDTipoPago])
REFERENCES [dbo].[TipoPago] ([IDTipoPago])
GO
ALTER TABLE [dbo].[Factura] CHECK CONSTRAINT [FK_Factura_TipoPago]
GO
ALTER TABLE [dbo].[Factura]  WITH CHECK ADD  CONSTRAINT [FK_Factura_Usuario] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Usuario] ([IDUsuario])
GO
ALTER TABLE [dbo].[Factura] CHECK CONSTRAINT [FK_Factura_Usuario]
GO
ALTER TABLE [dbo].[FacturaProducto]  WITH CHECK ADD  CONSTRAINT [FK_FacturaProducto_Factura] FOREIGN KEY([IDFactura])
REFERENCES [dbo].[Factura] ([IDFactura])
GO
ALTER TABLE [dbo].[FacturaProducto] CHECK CONSTRAINT [FK_FacturaProducto_Factura]
GO
ALTER TABLE [dbo].[FacturaProducto]  WITH CHECK ADD  CONSTRAINT [FK_FacturaProducto_Producto] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Producto] ([IDProducto])
GO
ALTER TABLE [dbo].[FacturaProducto] CHECK CONSTRAINT [FK_FacturaProducto_Producto]
GO
ALTER TABLE [dbo].[HistorialCostoProducto]  WITH CHECK ADD  CONSTRAINT [FK_HistorialCostoProducto_Producto] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Producto] ([IDProducto])
GO
ALTER TABLE [dbo].[HistorialCostoProducto] CHECK CONSTRAINT [FK_HistorialCostoProducto_Producto]
GO
ALTER TABLE [dbo].[HistorialPrecioProducto]  WITH CHECK ADD  CONSTRAINT [FK_HistorialPrecioProducto_Producto] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Producto] ([IDProducto])
GO
ALTER TABLE [dbo].[HistorialPrecioProducto] CHECK CONSTRAINT [FK_HistorialPrecioProducto_Producto]
GO
ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_Objeto] FOREIGN KEY([IDObjeto])
REFERENCES [dbo].[Objeto] ([IDObjeto])
GO
ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_Objeto]
GO
ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_Rol] FOREIGN KEY([IDRol])
REFERENCES [dbo].[Rol] ([IDRol])
GO
ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_Rol]
GO
ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_Usuario] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Usuario] ([IDUsuario])
GO
ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_Usuario]
GO
ALTER TABLE [dbo].[NotaCredito]  WITH CHECK ADD  CONSTRAINT [FK_NotaCredito_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([IDCliente])
GO
ALTER TABLE [dbo].[NotaCredito] CHECK CONSTRAINT [FK_NotaCredito_Cliente]
GO
ALTER TABLE [dbo].[NotaCredito]  WITH CHECK ADD  CONSTRAINT [FK_NotaCredito_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([IDSucursal])
GO
ALTER TABLE [dbo].[NotaCredito] CHECK CONSTRAINT [FK_NotaCredito_Sucursal]
GO
ALTER TABLE [dbo].[NotaCredito]  WITH CHECK ADD  CONSTRAINT [FK_NotaCredito_Usuario] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Usuario] ([IDUsuario])
GO
ALTER TABLE [dbo].[NotaCredito] CHECK CONSTRAINT [FK_NotaCredito_Usuario]
GO
ALTER TABLE [dbo].[Objeto]  WITH CHECK ADD  CONSTRAINT [FK_Objetos_Objetos] FOREIGN KEY([IDObjetoRelacionado])
REFERENCES [dbo].[Objeto] ([IDObjeto])
GO
ALTER TABLE [dbo].[Objeto] CHECK CONSTRAINT [FK_Objetos_Objetos]
GO
ALTER TABLE [dbo].[Producto]  WITH CHECK ADD  CONSTRAINT [FK_Producto_Empresa] FOREIGN KEY([IDEmpresa])
REFERENCES [dbo].[Empresa] ([IDEmpresa])
GO
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_Empresa]
GO
ALTER TABLE [dbo].[Producto]  WITH CHECK ADD  CONSTRAINT [FK_Producto_SubcategoriaProducto] FOREIGN KEY([IDSubcategoriaProducto])
REFERENCES [dbo].[SubcategoriaProducto] ([IDSubcategoriaProducto])
GO
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_SubcategoriaProducto]
GO
ALTER TABLE [dbo].[Producto]  WITH CHECK ADD  CONSTRAINT [FK_Producto_TipoProducto] FOREIGN KEY([IDTipoProducto])
REFERENCES [dbo].[TipoProducto] ([IDTipoProducto])
GO
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_TipoProducto]
GO
ALTER TABLE [dbo].[Producto]  WITH CHECK ADD  CONSTRAINT [FK_Producto_UnidadMedida] FOREIGN KEY([CodigoUnidadMedidaSize])
REFERENCES [dbo].[UnidadMedida] ([CodigoUnidad])
GO
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_UnidadMedida]
GO
ALTER TABLE [dbo].[Producto]  WITH CHECK ADD  CONSTRAINT [FK_Producto_UnidadMedida1] FOREIGN KEY([CodigoUnidadMedidaPeso])
REFERENCES [dbo].[UnidadMedida] ([CodigoUnidad])
GO
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_UnidadMedida1]
GO
ALTER TABLE [dbo].[ProductoExistencia]  WITH CHECK ADD  CONSTRAINT [FK_ProductoExistencia_Almacen] FOREIGN KEY([AlmacenID])
REFERENCES [dbo].[Almacen] ([IDAlmacen])
GO
ALTER TABLE [dbo].[ProductoExistencia] CHECK CONSTRAINT [FK_ProductoExistencia_Almacen]
GO
ALTER TABLE [dbo].[ProductoExistencia]  WITH CHECK ADD  CONSTRAINT [FK_ProductoExistencia_Producto] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Producto] ([IDProducto])
GO
ALTER TABLE [dbo].[ProductoExistencia] CHECK CONSTRAINT [FK_ProductoExistencia_Producto]
GO
ALTER TABLE [dbo].[Recibo]  WITH CHECK ADD  CONSTRAINT [FK_Recibo_Caja] FOREIGN KEY([IDCaja])
REFERENCES [dbo].[Caja] ([IDCaja])
GO
ALTER TABLE [dbo].[Recibo] CHECK CONSTRAINT [FK_Recibo_Caja]
GO
ALTER TABLE [dbo].[Recibo]  WITH CHECK ADD  CONSTRAINT [FK_Recibo_Cliente] FOREIGN KEY([IDCliente])
REFERENCES [dbo].[Cliente] ([IDCliente])
GO
ALTER TABLE [dbo].[Recibo] CHECK CONSTRAINT [FK_Recibo_Cliente]
GO
ALTER TABLE [dbo].[Recibo]  WITH CHECK ADD  CONSTRAINT [FK_Recibo_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([IDSucursal])
GO
ALTER TABLE [dbo].[Recibo] CHECK CONSTRAINT [FK_Recibo_Sucursal]
GO
ALTER TABLE [dbo].[ReciboFactura]  WITH CHECK ADD  CONSTRAINT [FK_ReciboFactura_Factura] FOREIGN KEY([IDFactura])
REFERENCES [dbo].[Factura] ([IDFactura])
GO
ALTER TABLE [dbo].[ReciboFactura] CHECK CONSTRAINT [FK_ReciboFactura_Factura]
GO
ALTER TABLE [dbo].[ReciboFactura]  WITH CHECK ADD  CONSTRAINT [FK_ReciboFactura_Recibo] FOREIGN KEY([IDRecibo])
REFERENCES [dbo].[Recibo] ([IDRecibo])
GO
ALTER TABLE [dbo].[ReciboFactura] CHECK CONSTRAINT [FK_ReciboFactura_Recibo]
GO
ALTER TABLE [dbo].[ReciboPago]  WITH CHECK ADD  CONSTRAINT [FK_ReciboPago_Recibo] FOREIGN KEY([IDRecibo])
REFERENCES [dbo].[Recibo] ([IDRecibo])
GO
ALTER TABLE [dbo].[ReciboPago] CHECK CONSTRAINT [FK_ReciboPago_Recibo]
GO
ALTER TABLE [dbo].[ReciboPago]  WITH CHECK ADD  CONSTRAINT [FK_ReciboPago_TipoPago] FOREIGN KEY([IDTipoPago])
REFERENCES [dbo].[TipoPago] ([IDTipoPago])
GO
ALTER TABLE [dbo].[ReciboPago] CHECK CONSTRAINT [FK_ReciboPago_TipoPago]
GO
ALTER TABLE [dbo].[RolObjeto]  WITH CHECK ADD  CONSTRAINT [FK_Rol_Objeto_Objeto] FOREIGN KEY([IDObjeto])
REFERENCES [dbo].[Objeto] ([IDObjeto])
GO
ALTER TABLE [dbo].[RolObjeto] CHECK CONSTRAINT [FK_Rol_Objeto_Objeto]
GO
ALTER TABLE [dbo].[RolObjeto]  WITH CHECK ADD  CONSTRAINT [FK_RolObjeto_Rol] FOREIGN KEY([IDRol])
REFERENCES [dbo].[Rol] ([IDRol])
GO
ALTER TABLE [dbo].[RolObjeto] CHECK CONSTRAINT [FK_RolObjeto_Rol]
GO
ALTER TABLE [dbo].[SubcategoriaProducto]  WITH CHECK ADD  CONSTRAINT [FK_SubcategoriaProducto_CategoriaProducto] FOREIGN KEY([IDCategoriaProducto])
REFERENCES [dbo].[CategoriaProducto] ([IDCategoriaProducto])
GO
ALTER TABLE [dbo].[SubcategoriaProducto] CHECK CONSTRAINT [FK_SubcategoriaProducto_CategoriaProducto]
GO
ALTER TABLE [dbo].[Sucursal]  WITH CHECK ADD  CONSTRAINT [FK_Sucursal_Empresa] FOREIGN KEY([IDEmpresa])
REFERENCES [dbo].[Empresa] ([IDEmpresa])
GO
ALTER TABLE [dbo].[Sucursal] CHECK CONSTRAINT [FK_Sucursal_Empresa]
GO
ALTER TABLE [dbo].[SucursalContacto]  WITH CHECK ADD  CONSTRAINT [FK_SucursalContacto_Contacto] FOREIGN KEY([IDContacto])
REFERENCES [dbo].[Contacto] ([IDContacto])
GO
ALTER TABLE [dbo].[SucursalContacto] CHECK CONSTRAINT [FK_SucursalContacto_Contacto]
GO
ALTER TABLE [dbo].[SucursalContacto]  WITH CHECK ADD  CONSTRAINT [FK_SucursalContacto_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([IDSucursal])
GO
ALTER TABLE [dbo].[SucursalContacto] CHECK CONSTRAINT [FK_SucursalContacto_Sucursal]
GO
ALTER TABLE [dbo].[SucursalDireccion]  WITH CHECK ADD  CONSTRAINT [FK_SucursalDireccion_Direccion] FOREIGN KEY([IDDireccion])
REFERENCES [dbo].[Direccion] ([IDDireccion])
GO
ALTER TABLE [dbo].[SucursalDireccion] CHECK CONSTRAINT [FK_SucursalDireccion_Direccion]
GO
ALTER TABLE [dbo].[SucursalDireccion]  WITH CHECK ADD  CONSTRAINT [FK_SucursalDireccion_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([IDSucursal])
GO
ALTER TABLE [dbo].[SucursalDireccion] CHECK CONSTRAINT [FK_SucursalDireccion_Sucursal]
GO
ALTER TABLE [dbo].[SucursalTelefono]  WITH CHECK ADD  CONSTRAINT [FK_SucursalTelefono_Sucursal] FOREIGN KEY([IDSucursal])
REFERENCES [dbo].[Sucursal] ([IDSucursal])
GO
ALTER TABLE [dbo].[SucursalTelefono] CHECK CONSTRAINT [FK_SucursalTelefono_Sucursal]
GO
ALTER TABLE [dbo].[SucursalTelefono]  WITH CHECK ADD  CONSTRAINT [FK_SucursalTelefono_Telefono] FOREIGN KEY([IDTelefono])
REFERENCES [dbo].[Telefono] ([IDTelefono])
GO
ALTER TABLE [dbo].[SucursalTelefono] CHECK CONSTRAINT [FK_SucursalTelefono_Telefono]
GO
ALTER TABLE [dbo].[Suplidor]  WITH CHECK ADD  CONSTRAINT [FK_Suplidor_Empresa] FOREIGN KEY([IDEmpresa])
REFERENCES [dbo].[Empresa] ([IDEmpresa])
GO
ALTER TABLE [dbo].[Suplidor] CHECK CONSTRAINT [FK_Suplidor_Empresa]
GO
ALTER TABLE [dbo].[Suplidor]  WITH CHECK ADD  CONSTRAINT [FK_Suplidor_TipoIdentificacion] FOREIGN KEY([IDTipoIdentificacion])
REFERENCES [dbo].[TipoIdentificacion] ([IDTipoIdentificacion])
GO
ALTER TABLE [dbo].[Suplidor] CHECK CONSTRAINT [FK_Suplidor_TipoIdentificacion]
GO
ALTER TABLE [dbo].[Suplidor]  WITH CHECK ADD  CONSTRAINT [FK_Suplidor_TipoSuplidor] FOREIGN KEY([IDTipoSuplidor])
REFERENCES [dbo].[TipoSuplidor] ([IDTipoSuplidor])
GO
ALTER TABLE [dbo].[Suplidor] CHECK CONSTRAINT [FK_Suplidor_TipoSuplidor]
GO
ALTER TABLE [dbo].[SuplidorContacto]  WITH CHECK ADD  CONSTRAINT [FK_SuplidorContacto_Contacto] FOREIGN KEY([IDContacto])
REFERENCES [dbo].[Contacto] ([IDContacto])
GO
ALTER TABLE [dbo].[SuplidorContacto] CHECK CONSTRAINT [FK_SuplidorContacto_Contacto]
GO
ALTER TABLE [dbo].[SuplidorContacto]  WITH CHECK ADD  CONSTRAINT [FK_SuplidorContacto_Suplidor] FOREIGN KEY([IDSuplidor])
REFERENCES [dbo].[Suplidor] ([IDSuplidor])
GO
ALTER TABLE [dbo].[SuplidorContacto] CHECK CONSTRAINT [FK_SuplidorContacto_Suplidor]
GO
ALTER TABLE [dbo].[SuplidorCorreoElectronico]  WITH CHECK ADD  CONSTRAINT [FK_SuplidorCorreoElectronico_CorreoElectronico] FOREIGN KEY([IDCorreoElectronico])
REFERENCES [dbo].[CorreoElectronico] ([IDCorreoElectronico])
GO
ALTER TABLE [dbo].[SuplidorCorreoElectronico] CHECK CONSTRAINT [FK_SuplidorCorreoElectronico_CorreoElectronico]
GO
ALTER TABLE [dbo].[SuplidorCorreoElectronico]  WITH CHECK ADD  CONSTRAINT [FK_SuplidorCorreoElectronico_Suplidor] FOREIGN KEY([IDSuplidor])
REFERENCES [dbo].[Suplidor] ([IDSuplidor])
GO
ALTER TABLE [dbo].[SuplidorCorreoElectronico] CHECK CONSTRAINT [FK_SuplidorCorreoElectronico_Suplidor]
GO
ALTER TABLE [dbo].[SuplidorDireccion]  WITH CHECK ADD  CONSTRAINT [FK_SuplidorDireccion_Direccion] FOREIGN KEY([IDDireccion])
REFERENCES [dbo].[Direccion] ([IDDireccion])
GO
ALTER TABLE [dbo].[SuplidorDireccion] CHECK CONSTRAINT [FK_SuplidorDireccion_Direccion]
GO
ALTER TABLE [dbo].[SuplidorDireccion]  WITH CHECK ADD  CONSTRAINT [FK_SuplidorDireccion_Suplidor] FOREIGN KEY([IDSuplidor])
REFERENCES [dbo].[Suplidor] ([IDSuplidor])
GO
ALTER TABLE [dbo].[SuplidorDireccion] CHECK CONSTRAINT [FK_SuplidorDireccion_Suplidor]
GO
ALTER TABLE [dbo].[SuplidorTelefono]  WITH CHECK ADD  CONSTRAINT [FK_SuplidorTelefono_Suplidor] FOREIGN KEY([IDSuplidor])
REFERENCES [dbo].[Suplidor] ([IDSuplidor])
GO
ALTER TABLE [dbo].[SuplidorTelefono] CHECK CONSTRAINT [FK_SuplidorTelefono_Suplidor]
GO
ALTER TABLE [dbo].[SuplidorTelefono]  WITH CHECK ADD  CONSTRAINT [FK_SuplidorTelefono_Telefono] FOREIGN KEY([IDTelefono])
REFERENCES [dbo].[Telefono] ([IDTelefono])
GO
ALTER TABLE [dbo].[SuplidorTelefono] CHECK CONSTRAINT [FK_SuplidorTelefono_Telefono]
GO
ALTER TABLE [dbo].[Telefono]  WITH CHECK ADD  CONSTRAINT [FK_Telefono_TipoTelefono] FOREIGN KEY([IDTipoTelefono])
REFERENCES [dbo].[TipoTelefono] ([IDTipoTelefono])
GO
ALTER TABLE [dbo].[Telefono] CHECK CONSTRAINT [FK_Telefono_TipoTelefono]
GO
ALTER TABLE [dbo].[Transferencia]  WITH CHECK ADD  CONSTRAINT [FK_Transferencia_AlmacenDestino] FOREIGN KEY([IDAlmacenDestino])
REFERENCES [dbo].[Almacen] ([IDAlmacen])
GO
ALTER TABLE [dbo].[Transferencia] CHECK CONSTRAINT [FK_Transferencia_AlmacenDestino]
GO
ALTER TABLE [dbo].[Transferencia]  WITH CHECK ADD  CONSTRAINT [FK_Transferencia_AlmacenOrigen] FOREIGN KEY([IDAlmacenOrigen])
REFERENCES [dbo].[Almacen] ([IDAlmacen])
GO
ALTER TABLE [dbo].[Transferencia] CHECK CONSTRAINT [FK_Transferencia_AlmacenOrigen]
GO
ALTER TABLE [dbo].[Transferencia]  WITH CHECK ADD  CONSTRAINT [FK_Transferencia_UsuarioRecibe] FOREIGN KEY([IDUsuarioRecibe])
REFERENCES [dbo].[Usuario] ([IDUsuario])
GO
ALTER TABLE [dbo].[Transferencia] CHECK CONSTRAINT [FK_Transferencia_UsuarioRecibe]
GO
ALTER TABLE [dbo].[Transferencia]  WITH CHECK ADD  CONSTRAINT [FK_Transferencia_UsuarioSolicita] FOREIGN KEY([IDUsuarioSolicita])
REFERENCES [dbo].[Usuario] ([IDUsuario])
GO
ALTER TABLE [dbo].[Transferencia] CHECK CONSTRAINT [FK_Transferencia_UsuarioSolicita]
GO
ALTER TABLE [dbo].[TransferenciaProducto]  WITH CHECK ADD  CONSTRAINT [FK_TransferenciaProducto_Producto] FOREIGN KEY([IDProducto])
REFERENCES [dbo].[Producto] ([IDProducto])
GO
ALTER TABLE [dbo].[TransferenciaProducto] CHECK CONSTRAINT [FK_TransferenciaProducto_Producto]
GO
ALTER TABLE [dbo].[TransferenciaProducto]  WITH CHECK ADD  CONSTRAINT [FK_TransferenciaProducto_Transferencia] FOREIGN KEY([IDTransferencia])
REFERENCES [dbo].[Transferencia] ([IDTransferencia])
GO
ALTER TABLE [dbo].[TransferenciaProducto] CHECK CONSTRAINT [FK_TransferenciaProducto_Transferencia]
GO
ALTER TABLE [dbo].[UsuarioEmpleado]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioEmpleado_Empleado] FOREIGN KEY([IDEmpleado])
REFERENCES [dbo].[Empleado] ([IDEmpleado])
GO
ALTER TABLE [dbo].[UsuarioEmpleado] CHECK CONSTRAINT [FK_UsuarioEmpleado_Empleado]
GO
ALTER TABLE [dbo].[UsuarioEmpleado]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioEmpleado_Usuario] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Usuario] ([IDUsuario])
GO
ALTER TABLE [dbo].[UsuarioEmpleado] CHECK CONSTRAINT [FK_UsuarioEmpleado_Usuario]
GO
ALTER TABLE [dbo].[UsuarioRol]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Rol_Rol] FOREIGN KEY([IDRol])
REFERENCES [dbo].[Rol] ([IDRol])
GO
ALTER TABLE [dbo].[UsuarioRol] CHECK CONSTRAINT [FK_Usuario_Rol_Rol]
GO
ALTER TABLE [dbo].[UsuarioRol]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Rol_Usuario] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Usuario] ([IDUsuario])
GO
ALTER TABLE [dbo].[UsuarioRol] CHECK CONSTRAINT [FK_Usuario_Rol_Usuario]
GO
ALTER TABLE [dbo].[UsuarioToken]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioToken_Usuario] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Usuario] ([IDUsuario])
GO
ALTER TABLE [dbo].[UsuarioToken] CHECK CONSTRAINT [FK_UsuarioToken_Usuario]
GO
USE [master]
GO
ALTER DATABASE [Inventario] SET  READ_WRITE 
GO
