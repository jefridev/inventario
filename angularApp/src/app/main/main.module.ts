import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router'

import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';

import { NavigationModule } from "../common/navigation/navigation.module";
import { FooterModule } from "../common/footer/footer.module";
import { TopnavbarModule } from "../common/topnavbar/topnavbar.module";

import { MainComponent } from './main.component';

@NgModule({
    imports: [
        RouterModule,

        SlimLoadingBarModule.forRoot(),

        NavigationModule,
        FooterModule,
        TopnavbarModule
    ],
    declarations: [MainComponent]
})
export class MainModule { }
