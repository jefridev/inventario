export class UrlSettings {

    public static API_ENDPOINT: string = "http://localhost:31703/api";

    //AUTHENTICATION SERVICE.
    public static API_AUTH = `${UrlSettings.API_ENDPOINT}/security/auth`;
    public static API_AUTH_LOGOUT = `${UrlSettings.API_ENDPOINT}/security/auth/Logout`;
    public static API_AUTH_FORGOT = `${UrlSettings.API_ENDPOINT}/security/auth/ForgotPassword`;

    //LOG SERVICE.
    public static API_GetLogByID = `${UrlSettings.API_ENDPOINT}/security/log/ObtenerLogPorId`;
    public static API_SearchLogs = `${UrlSettings.API_ENDPOINT}/security/log/BuscarLogs`;

    //OBJECT SERVICE.
    public static API_CreateObject = `${UrlSettings.API_ENDPOINT}/security/objeto/CrearObjeto`;
    public static API_UpdateObject = `${UrlSettings.API_ENDPOINT}/security/objeto/ActualizarObjeto`;
    public static API_DeleteObject = `${UrlSettings.API_ENDPOINT}/security/objeto/EliminarObjeto`;
    public static API_SearchObjects = `${UrlSettings.API_ENDPOINT}/security/objeto/BuscarObjetos`;
    public static API_SearchActiveObjects = `${UrlSettings.API_ENDPOINT}/security/objeto/BuscarObjetosActivos`;
    public static API_GetActiveObjects = `${UrlSettings.API_ENDPOINT}/security/objeto/ObtenerObjetosActivos`;
    public static API_GetObjectById = `${UrlSettings.API_ENDPOINT}/security/objeto/ObtenerObjetoPorId`;
    public static API_GetMenuOptions = `${UrlSettings.API_ENDPOINT}/security/objeto/ObtenerOpcionesDeMenu`;
    public static API_GetScreenButtons = `${UrlSettings.API_ENDPOINT}/security/objeto/ObtenerBotonesDeBatalla`;
    public static API_GetAssignedObjectsToRol = `${UrlSettings.API_ENDPOINT}/security/objeto/ObtenerObjetosAsignadosRol`;
    public static API_GetNotAssignedObjectsToRol = `${UrlSettings.API_ENDPOINT}/security/objeto/ObtenerObjetosNoAsignadosPorRol`;
    public static API_GetObjectTypes = `${UrlSettings.API_ENDPOINT}/security/objeto/ObtenerTipoObjetos`;
    public static API_GetObjectForUser = `${UrlSettings.API_ENDPOINT}/security/objeto/ObtenerObjetosAsignadosUsuario`;


    //ROLE SERVICE.
    public static API_CreateRole = `${UrlSettings.API_ENDPOINT}/security/rol/CrearRol`;
    public static API_UpdateRole = `${UrlSettings.API_ENDPOINT}/security/rol/ActualizarRol`;
    public static API_DeleteRole = `${UrlSettings.API_ENDPOINT}/security/rol/EliminarRol`;
    public static API_SearchRoles = `${UrlSettings.API_ENDPOINT}/security/rol/BuscarRoles`;
    public static API_GetActiveRoles = `${UrlSettings.API_ENDPOINT}/security/rol/ObtenerRolesActivos`;
    public static API_GetRoleById = `${UrlSettings.API_ENDPOINT}/security/rol/ObtenerRolPorId`;
    public static API_AssignPermission = `${UrlSettings.API_ENDPOINT}/security/rol/AsignarPermiso`;
    public static API_RemovePermission = `${UrlSettings.API_ENDPOINT}/security/rol/RemoverPermiso`;
    public static API_GetAssignedRolesToUser = `${UrlSettings.API_ENDPOINT}/security/rol/ObtenerRolesAsignadosUsuario`;
    public static API_GetNotAssignedRolesToUser = `${UrlSettings.API_ENDPOINT}/security/rol/ObtenerRolesNoAsignadosPorUsuario`;

    //USER SERVICE.
    public static API_CreateUser = `${UrlSettings.API_ENDPOINT}/security/usuario/CrearUsuario`;
    public static API_UpdateUser = `${UrlSettings.API_ENDPOINT}/security/usuario/ActualizarUsuario`;
    public static API_DeleteUser = `${UrlSettings.API_ENDPOINT}/security/usuario/EliminarUsuario`;
    public static API_IsUserAvailable = `${UrlSettings.API_ENDPOINT}/security/usuario/UsuarioDisponible`;
    public static API_IsEmailAvailable = `${UrlSettings.API_ENDPOINT}/security/usuario/CorreoDisponible`;
    public static API_SearchUsers = `${UrlSettings.API_ENDPOINT}/security/usuario/BuscarUsuarios`;
    public static API_GetActiveUsers = `${UrlSettings.API_ENDPOINT}/security/usuario/ObtenerUsuariosActivos`;
    public static API_GetUserById = `${UrlSettings.API_ENDPOINT}/security/usuario/ObtenerUsuarioPorId`;
    public static API_AssignRol = `${UrlSettings.API_ENDPOINT}/security/usuario/AsignarRol`;
    public static API_RemoveRol = `${UrlSettings.API_ENDPOINT}/security/usuario/EliminarRol`;
    public static API_RegeneratePassword = `${UrlSettings.API_ENDPOINT}/security/usuario/RegenerarContrasena`;
    public static API_ChangePasswordWithToken = `${UrlSettings.API_ENDPOINT}/security/usuario/CambiarContrasenaConToken`;
    public static API_ChangePassword= `${UrlSettings.API_ENDPOINT}/security/usuario/CambiarContrasena`;
    public static API_ActivateAccount = `${UrlSettings.API_ENDPOINT}/security/usuario/ActivarCuenta`;
    

    //SECURITY POLICY SERVICE.
    public static API_UpdatePolicy = `${UrlSettings.API_ENDPOINT}/security/politica/ActualizarPolitica`;
    public static API_GetPolicy = `${UrlSettings.API_ENDPOINT}/security/politica/ObtenerPolitica`;
}