export class Result<T> {

    private status: number;
    private message: string;
    private value: T;

    get Status(): number {
        return this.status;
    }
    get Message(): string {
        return this.message;
    }
    get Value(): T{
        return this.value;
    }

    constructor(status: number, message?: string, value?: T) {
        this.status = status;
        this.message = message;
        this.value = value;
    }
}