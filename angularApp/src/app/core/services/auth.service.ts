import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { RequestOptionsArgs, URLSearchParams } from '@angular/http';
import { HttpClientService } from './httpclient.service';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { UrlSettings } from '../settings/urlsettings';
import { Result } from '../models/result.model';
import { HttpStatusCode } from '../enums/httpstatuscode.enum';

import { Observable } from 'rxjs';

@Injectable()
export class AuthService {

    private urlLogout: string = UrlSettings.API_AUTH_LOGOUT;

    constructor(private httpService: HttpClientService,
        private slimLoadingBarService: SlimLoadingBarService,
        private router: Router) { }


    public getName(): string {
        let info = this.getLoggedInfo();
        if (info) {
            return info.name;
        } else { return 'NOT FOUND' };
    }

    public logout(): Observable<string> {

        let info = this.getLoggedInfo();
        if (!info) {
            return Observable.throw({});
        } else { localStorage.removeItem('usuario'); }

        let params = new URLSearchParams();
        params.set('token', info.accessToken);

        let options: RequestOptionsArgs = {
            search: params
        };

        return this.httpService.get(this.urlLogout, options)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return result.Message;
                else
                    throw new Error(result.Message);
            });
    }

    private getLoggedInfo(): any {
        let information = localStorage.getItem('usuario');
        if (information) {
            let usuario = JSON.parse(information);
            return usuario;
        } else return null;
    }
}