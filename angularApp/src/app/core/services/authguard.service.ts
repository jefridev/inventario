import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild {

    constructor(private router: Router) { }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let result = this.isLoggedIn();
        if (result)
            return result;
        else {
            this.router.navigate(['./login']);
        }
    }

    canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let result = this.isLoggedIn();
        if (result)
            return result;
        else {
            this.router.navigate(['./login']);
        }
    }

    private isLoggedIn(): boolean {
        let usuario = localStorage.getItem('usuario');
        if (usuario)
            return true;
        else
            return false;
    }
}