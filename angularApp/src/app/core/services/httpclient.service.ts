import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Http, Response, Headers, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs';

import { Result, HttpStatusCode } from '../';

@Injectable()
export class HttpClientService {

    private requestOptions: RequestOptionsArgs = {
        headers: new Headers([{ 'Content-Type': 'application/json' }])
    }

    constructor(private http: Http,
        private router: Router) {
    }

    private setAuthorizationHeader(): void {
        //Remove previous token.
        let header = this.requestOptions.headers.get('Authorization');
        if(header)
            this.requestOptions.headers.delete('Authorization');

        var loggedUser = localStorage.getItem('usuario');
        if (loggedUser) {
            let usuario: any = JSON.parse(loggedUser);
            let authToken: string = <string>usuario.accessToken;

            let result = this.requestOptions.headers.get('Authorization');
            if (!result)
                this.requestOptions.headers.append("Authorization",`Bearer ${authToken}`);
        }
    }

    public handleResponse<T>(response: Response): Result<T> {
        let jsonResult = (response.ok) ? response.json() : {};
        switch (response.status) {
            case HttpStatusCode.OK:
                return new Result<T>(jsonResult.status, jsonResult.message, <T>jsonResult.value);
            default:
                return new Result<T>(response.status, response.statusText);
        }
    }

    public get(url: string, options?: RequestOptionsArgs): Observable<Result<any>> {
        this.setAuthorizationHeader();
        let mergeOptions: RequestOptionsArgs = {};
        Object.assign(mergeOptions, options, this.requestOptions); //Set values from one to another.
        return this.http.get(url, mergeOptions).flatMap(response => {
            let handledResponse = this.handleResponse(response);
            return Observable.of(handledResponse);
        }).catch(this.catchError(this));
    }

    public post(url: string, data: any, options?: RequestOptionsArgs): Observable<Result<any>> {
        this.setAuthorizationHeader();
        let mergeOptions: RequestOptionsArgs = {};
        Object.assign(mergeOptions, options, this.requestOptions); //Set values from one to another.
        return this.http.post(url, data, mergeOptions).flatMap(response => {
            let handledResponse = this.handleResponse(response);
            return Observable.of(handledResponse);
        }).catch(this.catchError(this));
    }

    private catchError(self: HttpClientService) {
        return (res: Response) => {
            if (res.status == HttpStatusCode.Unauthorized) {
                localStorage.removeItem("usuario");
                this.router.navigate(['/login']);
            }
            let result = this.handleResponse(res);
            return Observable.throw(result);
        }
    }
}