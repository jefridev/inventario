import { Injectable } from '@angular/core';
import { RequestOptionsArgs, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';

import { UrlSettings, HttpClientService, Result, HttpStatusCode } from '../../core';


@Injectable()
export class ForgotService {

    private restorePasswordUrl: string = UrlSettings.API_AUTH_FORGOT;

    constructor(private httpService: HttpClientService) { }

    public restorePassword(username: string): Observable<string> {
        let object = {
            username: username
        };
        return this.httpService.post(this.restorePasswordUrl, object)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return result.Message;
                else
                    throw new Error(result.Message);
            });
    }
}