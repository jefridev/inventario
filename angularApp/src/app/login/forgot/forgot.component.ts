import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ForgotService } from './forgot.service';
import { ToastsManager } from 'ng2-toastr';
@Component({
    selector: 'forgot',
    templateUrl: 'forgot.component.html'
})
export class ForgotComponent implements OnInit {

    username: string = '';

    constructor(private forgotService: ForgotService,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() { }

    onSubmit() {
        this.forgotService.restorePassword(this.username).subscribe((result) => {
            this.toastr.success(result);
        }, (error: Error) => {
            this.toastr.error(error.message);
        }); 
    }
}