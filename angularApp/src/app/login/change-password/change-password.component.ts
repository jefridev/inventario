import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChangePassword } from './change-password.model';
import { ChangePasswordService } from './changePassword.service';
import { ToastsManager } from 'ng2-toastr';

@Component({
    selector: 'change-password',
    templateUrl: 'change-password.component.html'
})
export class ChangePasswordComponent implements OnInit {

    passwordChange: ChangePassword = new ChangePassword({});

    constructor(
        private changePasswordService: ChangePasswordService,
        private route: ActivatedRoute,
        private router: Router,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.passwordChange.token = params["token"];
        });
    }

    onSubmit() {
        if (this.passwordChange.contrasena !== this.passwordChange.confirmacion) {
            let message: string = 'Las contraseñas no coinciden';
            this.toastr.warning(message);
            return;
        }

        this.changePasswordService.changePassword(this.passwordChange).subscribe(result => {
            this.toastr.success(result);
            setTimeout(() => {
                this.router.navigate(['/login']);
            }, 3000);
        }, (error: Error) => {
            this.toastr.error(error.message);
        });
    }
}