export class ChangePassword{
    public token: string;
    public contrasena: string;
    public confirmacion: string;

    constructor(args?: any){
        if(args == null || args == undefined) args = {};
        this.token = args.token || '';
        this.contrasena = args.contrasena || '';
        this.confirmacion = args.confirmacion || '';
    }
}