import { Injectable } from '@angular/core';
import { RequestOptionsArgs, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';

import { UrlSettings, HttpClientService, Result, HttpStatusCode } from '../../core';
import { ChangePassword } from './change-password.model';

@Injectable()
export class ChangePasswordService {

    private changePasswordWithTokenUrl: string = UrlSettings.API_ChangePasswordWithToken;

    constructor(private httpService: HttpClientService) { }

    public changePassword(change: ChangePassword): Observable<string> {
        let object = {
            Token: change.token,
            NewPassword:
             change.contrasena
        };
        return this.httpService.post(this.changePasswordWithTokenUrl, object)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return result.Message;
                else
                    throw new Error(result.Message);
            });
    }
}