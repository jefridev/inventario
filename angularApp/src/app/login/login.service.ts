import { Injectable } from '@angular/core';
import { RequestOptionsArgs, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';

import { UrlSettings, HttpClientService, Result, HttpStatusCode } from '../core';
import { Login } from './login.model';
import { LoginResponse } from './login-response.model';


@Injectable()
export class LoginService {

    private loginUrl: string = UrlSettings.API_AUTH;

    constructor(private httpService: HttpClientService) { }

    public signIn(loginInfo: Login): Observable<LoginResponse> {
        return this.httpService.post(this.loginUrl, loginInfo)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return new LoginResponse(result.Value);
                else
                    throw new Error(result.Message);
            });
    }
}