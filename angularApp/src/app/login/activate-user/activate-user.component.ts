import { Component, OnInit } from '@angular/core';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivateUserService } from './activate-user.service';

@Component({
    selector: 'activate-user',
    template: `
        <body class="gray-bg">
            <ng2-slim-loading-bar [color]="'#18A689'" [height]="'4px'"></ng2-slim-loading-bar>
        </body>
    `
})
export class ActivateUserComponent implements OnInit {

    constructor(private router: Router,
        private route: ActivatedRoute,
        private slimLoadingBarService: SlimLoadingBarService,
        private activateUserService: ActivateUserService) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            let token: string =  encodeURIComponent(params['token']);
            this.slimLoadingBarService.start();
            this.activateUserService.activateAccount(token).subscribe(result => {

            }).add(() => {
                this.slimLoadingBarService.complete();
                this.router.navigate(['/login']);
            });
        });
    }
}