import { Injectable } from '@angular/core';
import { RequestOptionsArgs, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';

import { UrlSettings, HttpClientService, Result, HttpStatusCode } from '../../core';

@Injectable()
export class ActivateUserService {

    private activateAccountUrl: string = UrlSettings.API_ActivateAccount;

    constructor(private httpService: HttpClientService) { }

    public activateAccount(token: string): Observable<string> {
        let params = new URLSearchParams();
        params.set('token', token);
        let options: RequestOptionsArgs = {
            search: params
        };

        return this.httpService.get(this.activateAccountUrl, options)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return result.Message;
                else
                    throw new Error(result.Message);
            });
    }
}