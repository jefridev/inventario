import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Login } from './login.model';
import { LoginService } from './login.service';
import { ObjectService } from '../security/objects/services/object.service';

import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

@Component({
    selector: 'login',
    templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

    isLoading: boolean = true;
    loginInfo: Login = new Login({});
    errorMessage: string = null;

    constructor(private loginService: LoginService,
        private objectService: ObjectService,
        private slimLoadingBarService: SlimLoadingBarService,
        private router: Router) { }

    ngOnInit() {
    }

    onSubmit() {
        this.slimLoadingBarService.start();
        this.loginService.signIn(this.loginInfo).subscribe((resultUsuario) => {

            //Save information.
            localStorage.setItem('usuario', JSON.stringify(resultUsuario));

            this.slimLoadingBarService.start();
            this.objectService.getObjectsByUser(resultUsuario.id).subscribe((resultObject) => {

                //Navigate objects
                localStorage.setItem('objetos', JSON.stringify(resultObject));
                //Navigate to main dashboard.
                this.router.navigate([resultUsuario.mainRoute]);

            }, (error: Error) => this.errorMessage = error.message)
                .add(() => this.slimLoadingBarService.complete());

        }, (error: Error) => {
            this.errorMessage = error.message;
        }).add(() => this.slimLoadingBarService.complete());
    }

    closeAlert() {
        this.errorMessage = null;
    }
}