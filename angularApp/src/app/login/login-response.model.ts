export class LoginResponse {
    id: number;
    name: string;
    username: string;
    email: string;
    accessToken: string;
    mainRoute: string;

    constructor(args: any) {
        if(args == null || args == undefined) args = {};
        this.id = +args.id;
        this.name = args.name;
        this.username = args.username;
        this.accessToken = args.accessToken;
        this.mainRoute = args.mainRoute;
    }
}