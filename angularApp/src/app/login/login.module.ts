import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './login.component';
import { ForgotComponent } from './forgot/forgot.component';
import { ActivateUserComponent } from './activate-user/activate-user.component';
import { LoginService } from './login.service';
import { ForgotService } from './forgot/forgot.service';
import { ChangePasswordService } from './change-password/changePassword.service';
import { ActivateUserService } from './activate-user/activate-user.service';

import { ChangePasswordComponent } from './change-password/change-password.component';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';

@NgModule({
    imports: [
        RouterModule,
        FormsModule,
        CommonModule,

        SharedModule,

        SlimLoadingBarModule.forRoot()
    ],
    declarations: [
        LoginComponent,
        ForgotComponent,
        ChangePasswordComponent,
        ActivateUserComponent
    ],
    providers: [
        LoginService,
        ForgotService,
        ChangePasswordService,
        ActivateUserService
    ]
})
export class LoginModule { }
