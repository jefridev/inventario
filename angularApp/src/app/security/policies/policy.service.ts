import { Injectable } from '@angular/core';
import { RequestOptionsArgs, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';

import { UrlSettings, HttpClientService, Result, HttpStatusCode } from '../../core';
import { Policy } from './policy.model';

@Injectable()
export class PolicyService {

    private updatePolicyUrl: string = UrlSettings.API_UpdatePolicy;
    private getPolicyUrl: string = UrlSettings.API_GetPolicy;

    constructor(private httpService: HttpClientService) {
    }

    public getPolicy(): Observable<Policy> {
        return this.httpService.get(this.getPolicyUrl)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return new Policy(result.Value);
                else
                    throw new Error(result.Message);
            });
    }

    public update(policy: Policy): Observable<string> {
        return this.httpService.post(this.updatePolicyUrl, policy)
            .map((response) => {
                if (response.Status == HttpStatusCode.OK) {
                    return response.Message;
                } else {
                    throw new Error(response.Message);
                }
            });
    }
}