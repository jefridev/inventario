import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PolicyService } from './policy.service';
import { Policy } from './policy.model';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { ToastsManager } from 'ng2-toastr';

@Component({
    selector: 'policy-editor',
    templateUrl: 'policy-editor.component.html'
})
export class PolicyEditorComponent implements OnInit {

    private errorMessage: string;

    title: string = "Formulario actualización de política";
    policy: Policy = new Policy({});


    constructor(private route: ActivatedRoute,
        private policyService: PolicyService,
        private slimLoadingBarService: SlimLoadingBarService,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
    }


    ngOnInit() {
        //Load information.
        this.slimLoadingBarService.start();
        this.policyService.getPolicy().subscribe((result) => {
            this.policy = result;
        }, (error: Error) => {
            this.toastr.error(error.message);
        }).add(() => this.slimLoadingBarService.complete());
    }

    onSubmit() {
        this.slimLoadingBarService.start();
        this.policyService.update(this.policy).subscribe(result => {
            this.toastr.success(result);
        }, (error: Error) => {
            this.toastr.error(error.message);
        }).add(() => this.slimLoadingBarService.complete());
    }
}