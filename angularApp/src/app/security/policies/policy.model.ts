export class Policy {
    idPolitica: number;
    longitudClave: number;
    incluirLetras: boolean;
    incluirNumeros: boolean;
    incluirCaracteres: boolean;
    numeroIntentos: number;
    tiempoBloqueo:number;
    tiempoTokenCliente: number;
    tiempoTokenWeb:number;
    vigenciaClave: number;
    vigenciaLog: number;

    constructor(args?: any){
        if(args == null || args == undefined) args = {};
        this.idPolitica = args.idpolitica;
        this.longitudClave = args.longitudClave;
        this.incluirLetras = args.incluirLetras || false;
        this.incluirNumeros = args.incluirNumeros || false;
        this.incluirCaracteres = args.incluirCaracteres || false;
        this.vigenciaClave = args.vigenciaClave;
        this.vigenciaLog = args.vigenciaLog;
        this.numeroIntentos = args.numeroIntentos;
        this.tiempoBloqueo = args.tiempoBloqueo;
        this.tiempoTokenCliente = args.tiempoTokenCliente;
        this.tiempoTokenWeb = args.tiempoTokenWeb;
    }
}