import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgModule } from '@angular/core';

import { PolicyEditorComponent } from './policy-editor.component';
import { PolicyService } from './policy.service';

import { SharedModule } from '../../shared/shared.module';
import { UiSwitchModule } from 'angular2-ui-switch';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        UiSwitchModule,
        
        SharedModule
    ],
    exports: [PolicyEditorComponent],
    declarations: [PolicyEditorComponent],
    providers: [PolicyService],
})
export class PolicyModule { }
