import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';

import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { ToastsManager } from 'ng2-toastr';

import { User } from '../models/user.model';
import { UserService } from '../services/user.service';


@Component({
    selector: 'user-list',
    templateUrl: 'user-list.component.html'
})
export class UserListComponent implements OnInit {

    private title: string = "Listado de usuarios"
    private searchTerm: string = "";
    private currentPage: number = 0;
    private pageLimit: number = 10;

    private errorMessage: string;

    users: User[] = [];

    constructor(private router: Router,
        private userService: UserService,
        private slimLoadingBarService: SlimLoadingBarService,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.getUsers(this.currentPage, this.pageLimit, this.searchTerm);
    }

    getUsers(currentPage: number, pageLimit?: number, searchTerm?: string): void {
        this.slimLoadingBarService.start();
        this.userService.getUsers(currentPage, pageLimit, searchTerm)
            .subscribe(result => {
                this.users = result;
            },
            (error: Error) => {
                this.errorMessage = error.message;
            }).add(() => this.slimLoadingBarService.complete());
    }

    search() {
        this.currentPage = 0;
        this.getUsers(this.currentPage, this.pageLimit, this.searchTerm);
    }

    next(): void {
        this.currentPage += 1;
        this.getUsers(this.currentPage, this.pageLimit, this.searchTerm);
    }

    previous(): void {
        this.currentPage -= 1;
        this.getUsers(this.currentPage, this.pageLimit, this.searchTerm);
    }

    newUser(): void {
        this.router.navigate(['security/users/crear']);
    }

    assignRoles(user: User): void {
        this.router.navigate(['./security/users/roles', user.idUsuario]);
    }

    editUser(user: User): void {
        this.router.navigate(['./security/users/editar', user.idUsuario]);
    }

    removeUser(user: User): void {
        swal({
            title: "Estás seguro?",
            text: "No serás capaz de deshacer esta acción",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Eliminar",
            closeOnConfirm: true
        }, (isConfirm) => {
            if (isConfirm) {
                this.slimLoadingBarService.start();
                this.userService.removeUser(user).subscribe(result => {
                    this.toastr.success(result);
                },
                    (error: Error) => this.toastr.error(error.message))
                    .add(() => this.slimLoadingBarService.complete());
            }
        });
    }

    regeneratePassword(user: User): void {
        swal({
            title: "Estás seguro?",
            text: "Desea reiniciar la contraseña, No serás capaz de deshacer esta acción",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonColor: "#1ab394",
            confirmButtonText: "Confirmar",
            closeOnConfirm: true
        }, (isConfirm) => {
            if (isConfirm) {
                this.slimLoadingBarService.start();
                this.userService.regeneratePassword(user).subscribe(result => {
                    this.toastr.success(result);
                },
                    (error: Error) => this.toastr.error(error.message))
                    .add(() => this.slimLoadingBarService.complete());
            }
        });
    }

    closeAlert(): void {
        this.errorMessage = null;
    }
}