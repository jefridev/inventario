import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { User } from '../models/user.model';
import { Role } from '../../roles/role.model';
import { UserRoleService } from '../services/user-role.service';
import { UserService } from '../services/user.service';


import { ToastsManager } from 'ng2-toastr/ng2-toastr';

declare var jQuery: any;

@Component({
    selector: 'user-role',
    templateUrl: 'user-role.component.html',
    styles: [`
        .role-container {
            overflow-y: auto;
            max-height:500px;
        }
    `]
})
export class UserRoleComponent implements OnInit {

    private idUsuario: number;

    user: User;
    assignedRoles: Role[];
    rolesNotAssigned: Role[];

    errorMessage: string;

    constructor(private userRoleService: UserRoleService,
        private userService: UserService,
        private slimLoadingBarService: SlimLoadingBarService,
        private toastr: ToastsManager,
        private activatedRoute: ActivatedRoute,
        private vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params) => {
            this.idUsuario = +params['id']; //Converts to number.
        });

        this.slimLoadingBarService.start();
        this.userRoleService.getAssignedRoles(this.idUsuario).subscribe((result) => {
            this.assignedRoles = result;
        },
            (error: Error) => this.errorMessage = error.message)
            .add(() => this.slimLoadingBarService.complete());

        this.slimLoadingBarService.start();
        this.userRoleService.getNotAssignedRoles(this.idUsuario).subscribe((result) => {
            this.rolesNotAssigned = result;
        },
            (error: Error) => this.errorMessage = error.message)
            .add(() => this.slimLoadingBarService.complete());

        this.slimLoadingBarService.start();
        this.userService.getUserById(this.idUsuario).subscribe((result) => {
            this.user = result;
        },
            (error: Error) => this.errorMessage = error.message)
            .add(() => this.slimLoadingBarService.complete());
    }

    closeAlert() {
        this.errorMessage = null;
    }

    filterList(search: string) {
        jQuery('.list-group-item').each(function () {
            if (jQuery(this).text().search(new RegExp(search, "i")) < 0) {
                jQuery(this).hide().removeClass('visible');
            } else {
                jQuery(this).show().addClass('visible');
            }
        });
    }

    removeRole(role: Role) {
        this.slimLoadingBarService.start();
        this.userRoleService.removeRoleToUser(this.idUsuario, role.idRol).subscribe(result => {
            let index = this.assignedRoles.indexOf(role);
            this.assignedRoles.splice(index, 1);
            this.rolesNotAssigned.push(role);

            this.toastr.success(result);
        }, (error: Error) => {
            this.toastr.error(error.message);
        }).add(() => this.slimLoadingBarService.complete());
    }

    assignRole(role: Role) {
        this.slimLoadingBarService.start();
        this.userRoleService.assignRoleToUser(this.idUsuario,role.idRol).subscribe(result => {
            let index = this.rolesNotAssigned.indexOf(role);
            this.rolesNotAssigned.splice(index, 1);
            this.assignedRoles.push(role);

            this.toastr.success(result);
        }, (error: Error) => {
            this.toastr.error(error.message);
        }).add(() => this.slimLoadingBarService.complete());
    }
}