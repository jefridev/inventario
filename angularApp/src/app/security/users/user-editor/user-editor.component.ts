import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';

import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { ToastsManager } from 'ng2-toastr';

@Component({
    selector: 'user-editor',
    templateUrl: 'user-editor.component.html'
})
export class UserEditorComponent implements OnInit {

    private sub: any;
    private errorMessage: string;

    id: number = 0;
    title: string = "Formulario creación de usuario";
    user: User = new User({});


    constructor(private route: ActivatedRoute,
        private userService: UserService,
        private slimLoadingBarService: SlimLoadingBarService,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            if (params['id'] != null) {
                this.id = +params['id']; // (+) converts string 'id' to a number
                this.title = "Formulario edición de usuario";

                //Load information.
                this.slimLoadingBarService.start();
                this.userService.getUserById(this.id).subscribe((result) => {
                    this.user = result;
                }, (error: Error) => {
                    console.log(error);
                    this.errorMessage = error.message;
                }).add(() => this.slimLoadingBarService.complete());
            }
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    setFocusElement(inputName: string) {
        let input: HTMLInputElement = <HTMLInputElement>document.getElementsByName(inputName)[0];
        if (input !== null && input !== undefined)
            input.focus();
    }

    onSubmit() {
        if (this.id !== 0) {
            this.userService.update(this.user).subscribe(result => {
                this.toastr.success(result);
            }, (error: Error) => {
                this.toastr.error(error.message);
            });
        } else {
            this.userService.create(this.user).subscribe(result => {
                this.user = new User({});
                this.setFocusElement('nombre');
                this.toastr.success(result);
            }, (error: Error) => {
                this.toastr.error(error.message);
            });
        }
    }
}