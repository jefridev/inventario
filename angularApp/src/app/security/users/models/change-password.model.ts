export class ChangePassword {
    public oldPassword: string;
    public newPassword: string;
    public confirmPassword: string;

    constructor(args: any) {
        if(args == null || args == undefined) args = {};
        this.oldPassword = args.oldPassword;
        this.newPassword = args.newPassword;
        this.confirmPassword = args.confirmPassword;
    }
}