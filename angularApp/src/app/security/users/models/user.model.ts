export class User {
    idUsuario: number;
    nombre: string;
    username: string;
    correo: string;
    telefono:string;
    estatus: string;

    constructor(args?: any) {
        this.idUsuario = args.idUsuario || 0;
        this.nombre = args.nombre || '';
        this.username = args.username || '';
        this.correo = args.correo || '';
        this.telefono = args.telefono || '';
        this.estatus = args.estatus  || '';
    }
}