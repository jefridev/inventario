import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '../../shared/shared.module';
import { UserListComponent } from './user-list/user-list.component';
import { UserEditorComponent } from './user-editor/user-editor.component';
import { UserRoleComponent } from './user-role/user-role.component';
import { ChangeUserPasswordComponent } from './change-password/change-password.component';

import { UserService } from './services/user.service';
import { UserRoleService } from './services/user-role.service';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        SharedModule
    ],
    exports: [
        UserListComponent,
        UserEditorComponent,
        UserRoleComponent
    ],
    declarations: [
        UserListComponent,
        UserEditorComponent,
        UserRoleComponent,
        ChangeUserPasswordComponent
    ],
    providers: [UserService, UserRoleService],
})
export class UserModule { }
