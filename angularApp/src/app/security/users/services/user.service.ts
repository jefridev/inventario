import { Injectable } from '@angular/core';
import { RequestOptionsArgs, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';

import { UrlSettings, HttpClientService, Result, HttpStatusCode } from '../../../core';
import { User } from '../models/user.model';
import { ChangePassword } from '../models/change-password.model';


@Injectable()
export class UserService {

    private searchUsersUrl: string = UrlSettings.API_SearchUsers;
    private getUserByIdUrl: string = UrlSettings.API_GetUserById;
    private removeUserUrl: string = UrlSettings.API_DeleteUser;
    private updateUserUrl: string = UrlSettings.API_UpdateUser;
    private createUserUrl: string = UrlSettings.API_CreateUser;
    private regeneratePasswordUrl: string = UrlSettings.API_RegeneratePassword;
    private changePasswordUrl: string = UrlSettings.API_ChangePassword;

    constructor(private httpService: HttpClientService) { }

    public getUsers(page: number, limit: number, searchTerm: string): Observable<User[]> {

        let params = new URLSearchParams();
        params.set('pagina', page.toString());
        params.set('limite', limit.toString());
        params.set('busqueda', searchTerm);

        let options: RequestOptionsArgs = {
            search: params
        };

        return this.httpService.get(this.searchUsersUrl, options)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return <User[]>result.Value;
                else if (result.Status == HttpStatusCode.NotFound)
                    return new Array<User>();
                else
                    throw new Error(result.Message);
            });
    }

    public getUserById(idUser: number): Observable<User> {
        let params = new URLSearchParams();
        params.set('idUsuario', idUser.toString());

        let options: RequestOptionsArgs = {
            search: params
        };

        return this.httpService.get(this.getUserByIdUrl, options)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return new User(result.Value);
                else
                    throw new Error(result.Message);
            });
    }

    public removeUser(user: User): Observable<string> {
        return this.httpService.post(this.removeUserUrl, user).map(result => {
            if (result.Status == HttpStatusCode.OK)
                return result.Message;
            else
                throw new Error(result.Message);
        });
    }

    public create(user: User): Observable<string> {
        return this.httpService.post(this.createUserUrl, user)
            .map((response) => {
                if (response.Status == HttpStatusCode.OK) {
                    return response.Message;
                } else {
                    throw new Error(response.Message);
                }
            });
    }

    public update(user: User): Observable<string> {
        return this.httpService.post(this.updateUserUrl, user)
            .map((response) => {
                if (response.Status == HttpStatusCode.OK) {
                    return response.Message;
                } else {
                    throw new Error(response.Message);
                }
            });
    }

    public changePassword(change: ChangePassword): Observable<string> {
        return this.httpService.post(this.changePasswordUrl, change)
            .map((response) => {
                if (response.Status == HttpStatusCode.OK) {
                    return response.Message;
                } else {
                    throw new Error(response.Message);
                }
            });
    }

    public regeneratePassword(user: User): Observable<string> {
        return this.httpService.post(this.regeneratePasswordUrl, user)
            .map((response) => {
                if (response.Status == HttpStatusCode.OK) {
                    return response.Message;
                } else {
                    throw new Error(response.Message);
                }
            });
    }
}