import { Injectable } from '@angular/core';
import { RequestOptionsArgs, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';

import { UrlSettings, HttpClientService, Result, HttpStatusCode } from '../../../core';
import { Role } from '../../roles/role.model';

@Injectable()
export class UserRoleService {

    private assignRoleToUserUrl: string = UrlSettings.API_AssignRol;
    private removeRoleToUserUrl: string = UrlSettings.API_RemoveRol;
    private assignedRolesUserUrl: string = UrlSettings.API_GetAssignedRolesToUser;
    private notAssignedRolesUserUrl: string = UrlSettings.API_GetNotAssignedRolesToUser;

    constructor(private httpService: HttpClientService) { }

    public assignRoleToUser(userID: number, roleID: number): Observable<string> {
        let object = {
            idUsuario: userID,
            idRol: roleID
        };

        return this.httpService.post(this.assignRoleToUserUrl, object)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return result.Message;
                else
                    throw new Error(result.Message);
            })
    }

    public removeRoleToUser(userID: number, roleID: number): Observable<string> {
        
        let object = {
            idUsuario: userID,
            idRol: roleID
        };

        return this.httpService.post(this.removeRoleToUserUrl, object)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return result.Message;
                else
                    throw new Error(result.Message);
            })
    }
    
    public getAssignedRoles(userID: number): Observable<Role[]> {
        let params = new URLSearchParams();
        params.set('idUsuario', userID.toString());

        let options: RequestOptionsArgs = {
            search: params
        };

        return this.httpService.get(this.assignedRolesUserUrl, options)
            .map(result => {
                if (result.Status == HttpStatusCode.OK) {
                    let results: Array<any> = result.Value;
                    return results.map((o) => new Role(o));
                }
                else
                    throw new Error(result.Message);
            });
    }

    public getNotAssignedRoles(userID: number): Observable<Role[]> {
        let params = new URLSearchParams();
        params.set('idUsuario', userID.toString());

        let options: RequestOptionsArgs = {
            search: params
        };

        return this.httpService.get(this.notAssignedRolesUserUrl, options)
            .map(result => {
                if (result.Status == HttpStatusCode.OK) {
                    let results: Array<any> = result.Value;
                    return results.map((o) => new Role(o));
                } else if (result.Status == HttpStatusCode.NotFound) {
                    return new Array<Role>();
                }
                else
                    throw new Error(result.Message);
            });
    }
}