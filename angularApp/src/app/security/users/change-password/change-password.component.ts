import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { ChangePassword } from '../models/change-password.model';

import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { ToastsManager } from 'ng2-toastr';

@Component({
    selector: 'change-password',
    templateUrl: 'change-password.component.html'
})
export class ChangeUserPasswordComponent implements OnInit {

    private errorMessage: string;

    title: string = "Formulario cambio de contraseña";
    passwordChange: ChangePassword = new ChangePassword({});


    constructor(private route: ActivatedRoute,
        private userService: UserService,
        private slimLoadingBarService: SlimLoadingBarService,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
    }

    onSubmit() {
        if (this.passwordChange.newPassword !== this.passwordChange.confirmPassword) {
            let message: string = 'La nueva contraseña y la confirmación no coinciden';
            this.toastr.warning(message);
            return;
        }

        this.slimLoadingBarService.start();
        this.userService.changePassword(this.passwordChange).subscribe(result => {
            this.toastr.success(result);
        }, (error: Error) => {
            this.toastr.error(error.message);
        }).add(() => this.slimLoadingBarService.complete());
    }
}