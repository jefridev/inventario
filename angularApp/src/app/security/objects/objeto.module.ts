import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

import { ObjetoEditorComponent } from './object-editor/object-editor.component';
import { ObjectListComponent } from './object-list/object-list.component';
import { ObjectService } from './services/object.service';

import { SelectModule } from 'ng-select';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,

        SelectModule,
        SharedModule
    ],
    exports: [ObjetoEditorComponent, ObjectListComponent],
    declarations: [ObjetoEditorComponent, ObjectListComponent],
    providers: [ObjectService],
})
export class ObjetoModule { }
