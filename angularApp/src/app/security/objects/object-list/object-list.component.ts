import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { ObjectService } from '../services/object.service';
import { Objeto } from '../objeto.model';

import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { ToastsManager } from 'ng2-toastr';

import * as swal from 'sweetalert';

@Component({
    selector: 'object-list',
    templateUrl: 'object-list.component.html'
})
export class ObjectListComponent implements OnInit {

    private title: string = "Listado de objetos"
    private searchTerm: string = "";
    private currentPage: number = 0;
    private pageLimit: number = 10;

    private errorMessage: string;

    objetos: Objeto[] = [];

    constructor(private router: Router,
        private objectService: ObjectService,
        private slimLoadingBarService: SlimLoadingBarService,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.getObjects(this.currentPage, this.pageLimit, this.searchTerm);
    }

    getObjects(currentPage: number, pageLimit?: number, searchTerm?: string): void {
        this.slimLoadingBarService.start();
        this.objectService.getObjects(currentPage, pageLimit, searchTerm)
            .subscribe(result => {
                this.objetos = result;
            },
            (error: Error) => {
                this.errorMessage = error.message;
            }).add(() => this.slimLoadingBarService.complete());
    }

    search() {
        this.currentPage = 0;
        this.getObjects(this.currentPage, this.pageLimit, this.searchTerm);
    }

    next(): void {
        this.currentPage += 1;
        this.getObjects(this.currentPage, this.pageLimit, this.searchTerm);
    }

    previous(): void {
        this.currentPage -= 1;
        this.getObjects(this.currentPage, this.pageLimit, this.searchTerm);
    }

    newObject(): void {
        this.router.navigate(['security/objects/crear']);
    }

    editObject(objeto: Objeto): void {
        this.router.navigate(['./security/objects/editar', objeto.idObjeto]);
    }

    removeObject(objeto: Objeto): void {
        swal({
            title: "Estás seguro?",
            text: "No serás capaz de deshacer esta acción",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Eliminar",
            closeOnConfirm: true
        }, (isConfirm) => {
            if (isConfirm) {
                this.slimLoadingBarService.start();
                this.objectService.removeObject(objeto).subscribe((result) => {
                    let index: number = this.objetos.indexOf(objeto);
                    this.objetos.splice(index, 1);
                }, (error: Error) => this.toastr.error(error.message))
                    .add(() => this.slimLoadingBarService.complete());
            }
        });
    }

    closeAlert(): void {
        this.errorMessage = null;
    }
}