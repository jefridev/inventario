import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ObjectService } from '../services/object.service';
import { Objeto } from '../objeto.model';
import { Option } from '../../../shared/models/option.model';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { ToastsManager } from 'ng2-toastr';


@Component({
    selector: 'object-editor',
    templateUrl: 'object-editor.component.html'
})
export class ObjetoEditorComponent implements OnInit {

    @ViewChild('selectActiveObjects') ngSelect;
    private sub: any;
    private errorMessage: string;

    id: number = 0;
    title: string = "Formulario creación de objetos";
    objeto: Objeto = new Objeto({});

    objectTypes: Array<string> = new Array<string>();
    activeObjects: Array<Option> = new Array<Option>();


    constructor(private route: ActivatedRoute,
        private objectService: ObjectService,
        private slimLoadingBarService: SlimLoadingBarService,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            if (params['id'] != null) {
                this.id = +params['id']; // (+) converts string 'id' to a number
                this.title = "Formulario edición de rol";

                //Load information.
                this.slimLoadingBarService.start();
                this.objectService.getObjectById(this.id).subscribe((result) => {
                    this.objeto = result;
                }, (error: Error) => {
                    this.toastr.error(error.message);
                }).add(() => this.slimLoadingBarService.complete());
            }
        });

        this.objectService.getObjectTypes().subscribe((result) => {
            this.objectTypes = result;
        }, (error: Error) => {
            this.toastr.error(error.message);
        });

        this.objectService.getActiveObjects().subscribe((result) => {
            this.activeObjects = this.objectService.mapObjectToOption(result);
            let option = this.activeObjects.find(x => x.value == this.objeto.idObjetoRelacionado);
            this.ngSelect.select('1');
        }, (error: Error) => {
            this.toastr.error(error.message);
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    setFocusElement(inputName: string) {
        let input: HTMLInputElement = <HTMLInputElement>document.getElementsByName(inputName)[0];
        if (input !== null && input !== undefined)
            input.focus();
    }

    onSubmit() {
        if (this.id !== 0) {
            this.objectService.update(this.objeto).subscribe(result => {
                this.toastr.success(result);
            }, (error: Error) => {
                this.toastr.error(error.message);
            });
        } else {
            this.objectService.create(this.objeto).subscribe(result => {
                this.toastr.success(result);
                this.objeto = new Objeto({});
                this.setFocusElement('nombre');
            }, (error: Error) => {
                this.toastr.error(error.message);
            });
        }
    }
}