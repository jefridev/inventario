import { Injectable } from '@angular/core';
import { RequestOptionsArgs, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';

import { UrlSettings, HttpClientService, Result, HttpStatusCode } from '../../../core';
import { Objeto } from '../objeto.model';
import { Option } from '../../../shared/models/option.model';

@Injectable()
export class ObjectService {

    private updateObjectUrl: string = UrlSettings.API_UpdateObject;
    private createObjectUrl: string = UrlSettings.API_CreateObject;
    private searchObjectsUrl: string = UrlSettings.API_SearchActiveObjects;
    private activeObjectsUrl: string = UrlSettings.API_GetActiveObjects;
    private objectByIdUrl: string = UrlSettings.API_GetObjectById;
    private removeObjectUrl: string = UrlSettings.API_DeleteObject;
    private getObjectTypesUrl: string = UrlSettings.API_GetObjectTypes;
    private getObjectsUserIDUrl: string = UrlSettings.API_GetObjectForUser;

    constructor(private httpService: HttpClientService) {
    }

    public getObjects(page: number, limit: number, searchTerm: string): Observable<Objeto[]> {

        let params = new URLSearchParams();
        params.set('pagina', page.toString());
        params.set('limite', limit.toString());
        params.set('busqueda', searchTerm);

        let options: RequestOptionsArgs = {
            search: params
        };

        return this.httpService.get(this.searchObjectsUrl, options)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return <Objeto[]>result.Value;
                else if (result.Status == HttpStatusCode.NotFound)
                    return new Array<Objeto>();
                else
                    throw new Error(result.Message);
            });
    }

    public getObjectsByUser(userID: number): Observable<Objeto[]> {
        let params = new URLSearchParams();
        params.set('idUsuario', userID.toString());

        let options: RequestOptionsArgs = {
            search: params
        };
        return this.httpService.get(this.getObjectsUserIDUrl, options)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return <Objeto[]>result.Value;
                else if(result.Status == HttpStatusCode.NotFound)
                    return [];
                else
                    throw new Error(result.Message);
            });
    }

    public getActiveObjects(): Observable<Objeto[]> {
        return this.httpService.get(this.activeObjectsUrl)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return <Objeto[]>result.Value;
                else if (result.Status == HttpStatusCode.NotFound)
                    return [];
                else
                    throw new Error(result.Message);
            });
    }

    public getObjectTypes(): Observable<string[]> {
        return this.httpService.get(this.getObjectTypesUrl)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return <string[]>result.Value;
                else
                    throw new Error(result.Message);
            });
    }

    public getObjectById(idObject: number): Observable<Objeto> {
        let params = new URLSearchParams();
        params.set('idObjeto', idObject.toString());

        let options: RequestOptionsArgs = {
            search: params
        };

        return this.httpService.get(this.objectByIdUrl, options)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return new Objeto(result.Value);
                else
                    throw new Error(result.Message);
            });
    }

    public create(objeto: Objeto): Observable<string> {
        return this.httpService.post(this.createObjectUrl, objeto)
            .map((response) => {
                if (response.Status == HttpStatusCode.OK) {
                    return response.Message;
                } else {
                    throw new Error(response.Message);
                }
            });
    }

    public update(objeto: Objeto): Observable<string> {
        return this.httpService.post(this.updateObjectUrl, objeto)
            .map((response) => {
                if (response.Status == HttpStatusCode.OK) {
                    return response.Message;
                } else {
                    throw new Error(response.Message);
                }
            });
    }

    public removeObject(objeto: Objeto): Observable<string> {
        return this.httpService.post(this.removeObjectUrl, objeto).map(result => {
            if (result.Status == HttpStatusCode.OK)
                return result.Message;
            else
                throw new Error(result.Message);
        });
    }

    public mapObjectToOption(objetos: Objeto[]): Option[] {
        if (objetos == null || objetos == undefined)
            return [];

        return objetos.map(x => new Option({ value: x.idObjeto, label: x.nombreLogico }));
    }
}