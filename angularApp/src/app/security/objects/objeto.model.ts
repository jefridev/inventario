export class Objeto {

    idObjeto: number;
    nombreLogico: string;
    nombreFisico: string;
    tipoObjeto: string;
    idObjetoRelacionado: string;
    selectedObjetoRelacionado: string;
    nombreObjetoRelacionado: string;
    estatus: string;

    constructor(args?: any) {
        if (args == null) args = {};
        this.idObjeto = args.idObjeto || 0;
        this.nombreLogico = args.nombreLogico || '';
        this.nombreFisico = args.nombreFisico || '';
        this.tipoObjeto = args.tipoObjeto || '';
        this.idObjetoRelacionado = args.idobjetoRelacionado;
        this.selectedObjetoRelacionado = "2";
        this.nombreObjetoRelacionado = args.nombreObjetoRelacionado || '';
        this.estatus = args.estatus || '';

        console.log('Argumentos', args);
        console.log('Objeto', this);
        console.log('This is', this.selectedObjetoRelacionado);
    }
}