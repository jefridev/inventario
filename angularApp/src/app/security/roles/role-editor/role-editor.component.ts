import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RoleService } from '../services/role.service';
import { Role } from '../role.model';

import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { ToastsManager } from 'ng2-toastr';


@Component({
    selector: 'role-editor',
    templateUrl: 'role-editor.component.html'
})
export class RoleEditorComponent implements OnInit {

    private sub: any;
    private errorMessage: string;

    id: number = 0;
    title: string = "Formulario creación de rol";
    role: Role = new Role({});


    constructor(private route: ActivatedRoute,
        private objectService: RoleService,
        private slimLoadingBarService: SlimLoadingBarService,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef
    ) {
        this.toastr.setRootViewContainerRef(this.vcr);
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            if (params['id'] != null) {
                this.id = +params['id']; // (+) converts string 'id' to a number
                this.title = "Formulario edición de rol";

                //Load information.
                this.slimLoadingBarService.start();
                this.objectService.getRoleById(this.id).subscribe((result) => {
                    this.role = result;
                }, (error: Error) => {
                    this.errorMessage = error.message;
                }).add(() => this.slimLoadingBarService.complete());
            }
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    setFocusElement(inputName: string) {
        let input: HTMLInputElement = <HTMLInputElement>document.getElementsByName(inputName)[0];
        if (input !== null && input !== undefined)
            input.focus();
    }

    onSubmit() {
        if (this.id !== 0) {
            this.objectService.update(this.role).subscribe(result => {
                this.toastr.success(result);
            }, (error: Error) => {
                this.toastr.error(error.message);
            });
        } else {
            this.objectService.create(this.role).subscribe(result => {
                this.role = new Role({});
                this.setFocusElement('nombre');
                this.toastr.success(result);
            }, (error: Error) => {
                this.toastr.error(error.message);
            });
        }
    }
}