import { Injectable } from '@angular/core';
import { Http, URLSearchParams, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Role } from '../role.model';
import { UrlSettings, HttpClientService, Result, HttpStatusCode } from '../../../core';

@Injectable()
export class RoleService {

    private updateRoleUrl: string = UrlSettings.API_UpdateRole;
    private createRoleUrl: string = UrlSettings.API_CreateRole;
    private searchRolesUrl: string = UrlSettings.API_SearchRoles;
    private roleByIdUrl: string = UrlSettings.API_GetRoleById;
    private removeRoleUrl: string = UrlSettings.API_DeleteRole;

    constructor(private httpService: HttpClientService) {
    }

    public getRoles(page: number, limit: number, searchTerm: string): Observable<Role[]> {

        let params = new URLSearchParams();
        params.set('pagina', page.toString());
        params.set('limite', limit.toString());
        params.set('busqueda', searchTerm);

        let options: RequestOptionsArgs = {
            search: params
        };

        return this.httpService.get(this.searchRolesUrl, options)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return <Role[]>result.Value;
                else if(result.Status == HttpStatusCode.NotFound)
                    return [];
                else
                    throw new Error(result.Message);
            });
    }

    public getRoleById(idRole: number): Observable<Role> {
        let params = new URLSearchParams();
        params.set('idRol', idRole.toString());

        let options: RequestOptionsArgs = {
            search: params
        };

        return this.httpService.get(this.roleByIdUrl, options)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return new Role(result.Value);
                else
                    throw new Error(result.Message);
            });
    }

    public create(role: Role): Observable<string> {
        return this.httpService.post(this.createRoleUrl, role)
            .map((response) => {
                if (response.Status == HttpStatusCode.OK) {
                    return response.Message;
                } else {
                    throw new Error(response.Message);
                }
            });
    }

    public update(role: Role): Observable<string> {
        return this.httpService.post(this.updateRoleUrl, role)
            .map((response) => {
                if (response.Status == HttpStatusCode.OK) {
                    return response.Message;
                } else {
                    throw new Error(response.Message);
                }
            });
    }

    public removeRole(role: Role): Observable<string>{
        return this.httpService.post(this.removeRoleUrl,role).map(result => {
             if(result.Status == HttpStatusCode.OK)
                 return result.Message;
             else
                throw new Error(result.Message);
        });
    }
}