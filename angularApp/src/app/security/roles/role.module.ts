import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '../../shared/shared.module';

import { RoleListComponent } from './role-list/role-list.component';
import { RoleEditorComponent } from './role-editor/role-editor.component';
import { PermissionComponent } from './permission/permission.component';
import { RoleService } from './services/role.service';
import { PermissionService } from './permission/permission.service';
import { HttpClientService } from '../../core'

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,

        SharedModule
    ],
    exports: [
        RoleListComponent,
        RoleEditorComponent,
        PermissionComponent
    ],
    declarations: [
        RoleListComponent,
        RoleEditorComponent,
        PermissionComponent
    ],
    providers: [RoleService, PermissionService, HttpClientService],
})
export class RoleModule { }
