export class Role {
    idRol: number;
    nombre: string;
    descripcion: string;
    rutaPrincipal:string;
    estatus: string;

    constructor(args?: any) {
        this.idRol = args.idRol || 0;
        this.nombre = args.nombre || '';
        this.descripcion = args.descripcion || '';
        this.rutaPrincipal = args.rutaPrincipal || '';
        this.estatus = args.estatus || '';
    }
}