import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { Permission } from './permission.model';
import { PermissionService } from './permission.service';
import { Role } from '../role.model';
import { RoleService } from '../services/role.service';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

declare var jQuery: any;

@Component({
    selector: 'tor-permission',
    templateUrl: 'permission.component.html',
    styles: [`
        .permission-container {
            overflow-y: auto;
            max-height:500px;
        }
    `]
})
export class PermissionComponent implements OnInit {

    private idrol: number;

    role: Role;
    assignedObjects: Permission[];
    objectsNotAssigned: Permission[];

    errorMessage: string;

    constructor(private permissionService: PermissionService,
        private roleService: RoleService,
        private slimLoadingBarService: SlimLoadingBarService,
        private toastr: ToastsManager,
        private activatedRoute: ActivatedRoute,
        private vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params) => {
            this.idrol = +params['id']; //Converts to number.
        });

        this.slimLoadingBarService.start();
        this.permissionService.getAssignedObjects(this.idrol).subscribe((result) => {
            this.assignedObjects = result;
        },
            (error: Error) => this.errorMessage = error.message)
            .add(() => this.slimLoadingBarService.complete());

        this.slimLoadingBarService.start();
        this.permissionService.getNotAssignedObjects(this.idrol).subscribe((result) => {
            this.objectsNotAssigned = result;
        },
            (error: Error) => this.errorMessage = error.message)
            .add(() => this.slimLoadingBarService.complete());

        this.slimLoadingBarService.start();
        this.roleService.getRoleById(this.idrol).subscribe((result) => {
            this.role = result;
        },
            (error: Error) => this.errorMessage = error.message)
            .add(() => this.slimLoadingBarService.complete());
    }

    closeAlert() {
        this.errorMessage = null;
    }

    filterList(search: string) {
        jQuery('.list-group-item').each(function () {
            if (jQuery(this).text().search(new RegExp(search, "i")) < 0) {
                jQuery(this).hide().removeClass('visible');
            } else {
                jQuery(this).show().addClass('visible');
            }
        });
    }

    removePermission(permission: Permission) {
        this.slimLoadingBarService.start();
        this.permissionService.removeObjectToRole(permission.id, this.idrol).subscribe(result => {
            this.objectsNotAssigned.push(permission);
            let index = this.assignedObjects.indexOf(permission);
            this.assignedObjects.splice(index, 1);

            this.toastr.success(result);
        }, (error: Error) => {
            this.toastr.error(error.message);
        }).add(() => this.slimLoadingBarService.complete());
    }

    assignPermission(permission: Permission) {
        this.slimLoadingBarService.start();
        this.permissionService.assignObjectToRole(permission.id, this.idrol).subscribe(result => {
            this.assignedObjects.push(permission);
            let index = this.objectsNotAssigned.indexOf(permission);
            this.objectsNotAssigned.splice(index, 1);

            this.toastr.success(result);
        }, (error: Error) => {
            this.toastr.error(error.message);
        }).add(() => this.slimLoadingBarService.complete());
    }
}