export class Permission {
    id: number;
    name: string;
    type: string;

    constructor(args?: any) {
        if(!args) args = {};
        
        this.id = args.idObjeto || 0;
        this.name = args.nombreLogico || '';
        this.type = args.tipoObjeto || '';
    }
}