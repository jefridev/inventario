import { Injectable } from '@angular/core';
import { RequestOptionsArgs, URLSearchParams } from '@angular/http';

import { Permission } from './permission.model';

import { UrlSettings, HttpClientService, HttpStatusCode } from '../../../core/';
import { Observable } from 'rxjs';

@Injectable()
export class PermissionService {

    private assignedObjectsUrl: string = UrlSettings.API_GetAssignedObjectsToRol;
    private objectsNotAssignedUrl: string = UrlSettings.API_GetNotAssignedObjectsToRol;
    private assignObjectToRoleUrl: string = UrlSettings.API_AssignPermission;
    private removeObjectToRoleUrl: string = UrlSettings.API_RemovePermission;

    constructor(private httpService: HttpClientService) { }

    public getAssignedObjects(idRol: number): Observable<Permission[]> {
        let params = new URLSearchParams();
        params.set('idRol', idRol.toString());

        let options: RequestOptionsArgs = {
            search: params
        };

        return this.httpService.get(this.assignedObjectsUrl, options)
            .map(result => {
                if (result.Status == HttpStatusCode.OK) {
                    let results: Array<any> = result.Value;
                    return results.map((o) => new Permission(o));
                } else if (result.Status == HttpStatusCode.NotFound) {
                    return new Array<Permission>();
                }
                else
                    throw new Error(result.Message);
            });
    }

    public getNotAssignedObjects(idRol: number): Observable<Permission[]> {
        let params = new URLSearchParams();
        params.set('idRol', idRol.toString());

        let options: RequestOptionsArgs = {
            search: params
        };

        return this.httpService.get(this.objectsNotAssignedUrl, options)
            .map(result => {
                if (result.Status == HttpStatusCode.OK) {
                    let results: Array<any> = result.Value;
                    return results.map((o) => new Permission(o));
                } else if (result.Status == HttpStatusCode.NotFound) {
                    return new Array<Permission>();
                }
                else
                    throw new Error(result.Message);
            });
    }

    public assignObjectToRole(idObjeto: number, idRol: number): Observable<string> {
        let object = {
            idObjeto: idObjeto,
            idRol: idRol
        };

        return this.httpService.post(this.assignObjectToRoleUrl, object)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return result.Message;
                else
                    throw new Error(result.Message);
            })
    }

    public removeObjectToRole(idObjeto: number, idRol: number): Observable<string> {

        let object = {
            idObjeto: idObjeto,
            idRol: idRol
        };

        return this.httpService.post(this.removeObjectToRoleUrl, object)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return result.Message;
                else
                    throw new Error(result.Message);
            })
    }
}