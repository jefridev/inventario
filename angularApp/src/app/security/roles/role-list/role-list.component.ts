import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { RoleService } from '../services/role.service';
import { Role } from '../role.model';

import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { ToastsManager } from 'ng2-toastr';

import * as swal from 'sweetalert';

@Component({
    selector: 'role-list',
    templateUrl: 'role-list.component.html'
})
export class RoleListComponent implements OnInit {

    private title: string = "Listado de roles"
    private searchTerm: string = "";
    private currentPage: number = 0;
    private pageLimit: number = 10;

    private errorMessage: string;

    roles: Role[] = [];

    constructor(private router: Router,
        private roleListService: RoleService,
        private slimLoadingBarService: SlimLoadingBarService,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.getRoles(this.currentPage, this.pageLimit, this.searchTerm);
    }

    getRoles(currentPage: number, pageLimit?: number, searchTerm?: string): void {
        this.slimLoadingBarService.start();
        this.roleListService.getRoles(currentPage, pageLimit, searchTerm)
            .subscribe(result => {
                this.roles = result;
            },
            (error: Error) => {
                this.errorMessage = error.message;
            }).add(() => this.slimLoadingBarService.complete());
    }

    search() {
        this.currentPage = 0;
        this.getRoles(this.currentPage, this.pageLimit, this.searchTerm);
    }

    next(): void {
        this.currentPage += 1;
        this.getRoles(this.currentPage, this.pageLimit, this.searchTerm);
    }

    previous(): void {
        this.currentPage -= 1;
        this.getRoles(this.currentPage, this.pageLimit, this.searchTerm);
    }

    assignPermissions(role: Role): void {
        this.router.navigate(['security/roles/permisos', role.idRol]);
    }

    newRole(): void {
        this.router.navigate(['security/roles/crear']);
    }

    editRole(role: Role): void {
        console.log('Editando', role);
        this.router.navigate(['./security/roles/editar', role.idRol]);
    }

    removeRole(role: Role): void {
        swal({
            title: "Estás seguro?",
            text: "No serás capaz de deshacer esta acción",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: 'Cancelar',
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Eliminar",
            closeOnConfirm: true
        }, (isConfirm) => {
            if (isConfirm) {
                this.slimLoadingBarService.start();
                this.roleListService.removeRole(role).subscribe((result) => {
                    let index: number = this.roles.indexOf(role);
                    this.roles.splice(index, 1);
                }, (error: Error) => this.errorMessage = error.message)
                    .add(() => this.slimLoadingBarService.complete());
            }
        });
    }

    closeAlert(): void {
        this.errorMessage = null;
    }
}