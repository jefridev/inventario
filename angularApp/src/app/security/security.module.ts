import { NgModule } from '@angular/core';

import { LogModule } from './logs/log.module';
import { RoleModule } from './roles/role.module';
import { UserModule } from './users/user.module';
import { ObjetoModule } from './objects/objeto.module';
import { PolicyModule } from './policies/policy.module';


@NgModule({
    imports: [
        LogModule,
        RoleModule,
        UserModule,
        ObjetoModule,
        PolicyModule
    ],
    exports: [LogModule, RoleModule, UserModule,PolicyModule]
})
export class SecurityModule { }
