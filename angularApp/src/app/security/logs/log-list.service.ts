import { Injectable } from '@angular/core';
import { Response, URLSearchParams, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs';

import { HttpClientService, UrlSettings, Result, HttpStatusCode } from '../../core';
import { Log } from './log.model';

@Injectable()
export class LogListService {

    private searchLogUrl: string = UrlSettings.API_SearchLogs;

    constructor(private httpService: HttpClientService) {
    }

    public getLogs(page: number, limit: number, searchTerm: string): Observable<Log[]> {

        // Inputs parameters
        let searchParams = new URLSearchParams()
        searchParams.append('pagina', page.toString());
        searchParams.append('limite', limit.toString());
        searchParams.append('busqueda', searchTerm);

        let options: RequestOptionsArgs = {
            search: searchParams
        };

        return this.httpService.get(this.searchLogUrl, options)
            .map(result => {
                if (result.Status == HttpStatusCode.OK)
                    return <Log[]>result.Value;
                else if(result.Status == HttpStatusCode.NotFound)
                    return new Array<Log>();
                else
                    throw new Error(result.Message);
            });
    }

    private mapLogs(args: any) {
        
    }
}