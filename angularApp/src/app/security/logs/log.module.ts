import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { LogListComponent } from './log-list.component';
import { LogListService } from './log-list.service';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    imports: 
    [
        CommonModule, 
        FormsModule,
        SharedModule
    ],
    exports: [LogListComponent],
    declarations: [LogListComponent],
    providers: [LogListService],
})
export class LogModule { }
