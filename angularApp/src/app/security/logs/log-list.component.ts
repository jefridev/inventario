import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { LogListService } from './log-list.service';
import { Log } from './log.model';

import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { ToastsManager } from 'ng2-toastr';

@Component({
    selector: 'log-list',
    templateUrl: 'log-list.component.html'
})
export class LogListComponent implements OnInit {

    private title: string = 'Listado de logs';
    private searchTerm: string = '';
    private currentPage: number = 0;
    private pageLimit: number = 10;

    private errorMessage: string;

    logs: Log[] = [];

    constructor(private logListService: LogListService,
        private slimLoadingBarService: SlimLoadingBarService,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef) {
            this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.getLogs(this.currentPage, this.pageLimit, this.searchTerm);
    }

    getLogs(currentPage: number, pageLimit?: number, searchTerm?: string): void {
        this.slimLoadingBarService.start();
        this.logListService.getLogs(currentPage, pageLimit, searchTerm)
            .subscribe(result => {
                this.logs = result;
            },
            (error: Error) => {
                this.errorMessage = error.message;
            }).add(() => this.slimLoadingBarService.complete());
    }

    search() {
        this.currentPage = 0;
        this.getLogs(this.currentPage, this.pageLimit, this.searchTerm);
    }

    next(): void {
        this.currentPage += 1;
        this.getLogs(this.currentPage, this.pageLimit, this.searchTerm);
    }

    previous(): void {
        this.currentPage -= 1;
        this.getLogs(this.currentPage, this.pageLimit, this.searchTerm);
    }

    closeAlert(): void {
        this.errorMessage = null;
    }
}