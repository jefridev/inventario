export class Log {
    idLog: number;
    usuario: string;
    rol: string;
    objeto: string;
    fecha: string;
    comentario: string;

    constructor(args?: any) {
        this.idLog = args.idLog;
        this.usuario = args.usuario;
        this.rol = args.rol;
        this.objeto = args.objeto;
        this.fecha = args.fecha;
        this.comentario = args.comentario;
    }
}