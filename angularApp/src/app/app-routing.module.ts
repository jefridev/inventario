import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { ForgotComponent } from './login/forgot/forgot.component';
import { ChangePasswordComponent } from './login/change-password/change-password.component';
import { ActivateUserComponent } from './login/activate-user/activate-user.component';

import { NotFoundComponent } from './not-found/not-found.component';

import { MainComponent } from './main/main.component';

import { LogListComponent } from './security/logs/log-list.component';
import { RoleListComponent } from './security/roles/role-list/role-list.component';
import { RoleEditorComponent } from './security/roles/role-editor/role-editor.component';
import { PermissionComponent } from './security/roles/permission/permission.component';
import { UserListComponent } from './security/users/user-list/user-list.component';
import { UserEditorComponent } from './security/users/user-editor/user-editor.component';
import { ChangeUserPasswordComponent } from './security/users/change-password/change-password.component';
import { UserRoleComponent } from './security/users/user-role/user-role.component';
import { ObjetoEditorComponent } from './security/objects/object-editor/object-editor.component';
import { ObjectListComponent } from './security/objects/object-list/object-list.component';
import { PolicyEditorComponent } from './security/policies/policy-editor.component';
import { AuthGuardService } from './core/services/authguard.service';


const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'login/forgot', component: ForgotComponent },
    { path: 'login/changePassword/:token', component: ChangePasswordComponent },
    { path: 'login/activate/:token', component: ActivateUserComponent },
    {
        path: 'security',
        canActivateChild: [AuthGuardService],
        children: [
            {
                path: 'logs',
                component: MainComponent,
                children: [
                    {
                        path: '',
                        component: LogListComponent
                    }
                ]
            },
            {
                path: 'roles',
                component: MainComponent,
                children: [
                    {
                        path: '',
                        component: RoleListComponent
                    },
                    {
                        path: 'permisos/:id',
                        component: PermissionComponent
                    },
                    {
                        path: 'crear',
                        component: RoleEditorComponent
                    },
                    {
                        path: 'editar/:id',
                        component: RoleEditorComponent
                    }
                ]
            },
            {
                path: 'users',
                component: MainComponent,
                children: [
                    {
                        path: '',
                        component: UserListComponent
                    },
                    {
                        path: 'crear',
                        component: UserEditorComponent
                    },
                    {
                        path: 'editar/:id',
                        component: UserEditorComponent
                    },
                    {
                        path: 'roles/:id',
                        component: UserRoleComponent
                    }
                ]
            },
            {
                path: 'objects',
                component: MainComponent,
                children: [
                    {
                        path: '',
                        component: ObjectListComponent
                    },
                    {
                        path: 'crear',
                        component: ObjetoEditorComponent
                    },
                    {
                        path: 'editar/:id',
                        component: ObjetoEditorComponent
                    }
                ]
            },
            {
                path: 'policies',
                component: MainComponent,
                children: [
                    {
                        path: '',
                        component: PolicyEditorComponent
                    }
                ]
            }
        ]
    },
    {
        path: 'cambiarContrasena',
        component: MainComponent,
        canActivate: [AuthGuardService],
        children: [
            {
                path: '',
                component: ChangeUserPasswordComponent
            }
        ]
    },
    { path: '', component: LoginComponent },
    { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
