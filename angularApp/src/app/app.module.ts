import { NgModule } from '@angular/core'
import { RouterModule } from "@angular/router";
import { AppComponent } from "./app";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule } from "@angular/http";
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

// App views
import { SecurityModule } from './security/security.module';
import { LoginModule } from './login/login.module';
import { MainModule } from './main/main.module';
import { NotFoundModule } from './not-found/not-found.module';


import { AuthGuardService } from './core/services/authguard.service'

//Routing modules
import { AppRoutingModule } from './app-routing.module';


// App modules/components
import { NavigationModule } from "./common/navigation/navigation.module";
import { FooterModule } from "./common/footer/footer.module";
import { TopnavbarModule } from "./common/topnavbar/topnavbar.module";

import { ToastModule } from 'ng2-toastr/ng2-toastr';



@NgModule({
    declarations: [AppComponent],
    imports: [

        // Angular modules
        BrowserModule,
        HttpModule,

        // Views
        NotFoundModule,
        SecurityModule,
        MainModule,
        LoginModule,

        // Modules
        NavigationModule,
        FooterModule,
        TopnavbarModule,

        AppRoutingModule,

        ToastModule.forRoot()
    ],
    providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }, AuthGuardService],
    bootstrap: [AppComponent]
})

export class AppModule { }