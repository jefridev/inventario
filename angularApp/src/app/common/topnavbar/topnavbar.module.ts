import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { TopnavbarComponent } from "./topnavbar.component";

import { AuthService } from '../../core/services/auth.service';

@NgModule({
    declarations: [TopnavbarComponent],
    imports: [BrowserModule],
    exports: [TopnavbarComponent],
    providers: [AuthService]
})

export class TopnavbarModule { }