import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { smoothlyMenu } from '../../../app/app.helpers';
import { AuthService } from '../../core/services/auth.service';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

declare var jQuery: any;

@Component({
    selector: 'topnavbar',
    templateUrl: 'topnavbar.template.html'
})
export class TopnavbarComponent {

    constructor(private authService: AuthService,
        private slimLoadingService: SlimLoadingBarService,
        private router: Router) { }

    toggleNavigation(): void {
        jQuery("body").toggleClass("mini-navbar");
        smoothlyMenu();
    }

    logout() {
        this.slimLoadingService.start();
        this.authService.logout().subscribe((result) => {
            this.router.navigate((['/login']));
        }, (error: Error) => {
            this.router.navigate(['/login']);
        }).add(() => this.slimLoadingService.complete());
    }
}