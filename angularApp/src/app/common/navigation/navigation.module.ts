import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { NavigationComponent } from "./navigation.component";

import { AuthService } from '../../core/services/auth.service';
import { SharedModule } from '../../shared/shared.module';
@NgModule({
    declarations: [NavigationComponent],
    imports: [
        BrowserModule,
        RouterModule,

        SharedModule
    ],
    exports: [NavigationComponent],
    providers: [AuthService]
})

export class NavigationModule { }