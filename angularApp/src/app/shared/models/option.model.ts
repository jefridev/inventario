export class Option {
    label: string;
    value: string;

    constructor(args: any) {
        if(args == null) args = {};
        this.label = args.label || '';
        this.value = args.value || '';
    }
}