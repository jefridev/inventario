import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
    selector: 'status-box',
    template: `
        <button [className]='styleButtonClass'>
            <i [className]='styleButtonIcon'></i>
        </button>
    `
})
export class StatusBoxComponent implements OnInit, OnChanges {

    @Input() estatus: string;
    private styleButtonClass: string;
    private styleButtonIcon: string;

    constructor() {
    }

    ngOnInit() {
        this.setClassValue();
    }

    ngOnChanges(){
        this.setClassValue();
    }
    
    setClassValue() {
        switch (this.estatus) {
            case 'ACT':
                this.styleButtonClass = 'btn btn-info btn-xs';
                this.styleButtonIcon = 'fa fa-check';
                break;
            case 'INA':
                this.styleButtonClass = 'btn btn-warning btn-xs';
                this.styleButtonIcon = 'fa fa-info';
                break;
            case 'ELI':
                this.styleButtonClass = 'btn btn-danger btn-xs';
                this.styleButtonIcon = 'fa fa-times';
                break;
            default:
                this.styleButtonClass = 'btn btn-default btn-xs';
                this.styleButtonIcon = 'fa fa-question';
                break;
        }
    }
}