import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms';

import { AlertBoxComponent } from './alert-box/alert-box.component';
import { StatusBoxComponent } from './status-box/status-box.component';
import { AuthorizedDirective } from './directives/authorized.directive';

@NgModule({
    imports: [CommonModule, FormsModule],
    exports: [
        AlertBoxComponent,
        StatusBoxComponent,
        AuthorizedDirective,
    ],
    declarations: [
        AlertBoxComponent,
        StatusBoxComponent,
        AuthorizedDirective
    ]
})
export class SharedModule { }
