import { Directive, Input, OnInit, ElementRef } from '@angular/core';
import { Objeto } from '../../security/objects/objeto.model';

@Directive({
    selector: '[authorized]',
})
export class AuthorizedDirective implements OnInit {

    constructor(private el: ElementRef) { }

    @Input('authorized') objectName: string;

    ngOnInit() {
        if (!this.objetoAsignado())
            this.deshabilitarElemento();
    }

    private deshabilitarElemento(): void {
        this.el.nativeElement.parentNode.removeChild(this.el.nativeElement);
    }

    private objetoAsignado(): boolean {
        let objectsStringify = localStorage.getItem('objetos');
        if (!objectsStringify) return false;

        let objects: any[] = <Objeto[]>JSON.parse(objectsStringify);
        let mappedObjects = objects.map((o) => {
            return new Objeto(o);
        });

        let result = mappedObjects.find(x => x.nombreFisico === this.objectName);
        if (result)
            return true;
        else
            return false;
    }
}