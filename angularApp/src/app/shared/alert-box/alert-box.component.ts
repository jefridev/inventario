import { Component, Input, Output, OnInit, EventEmitter, OnChanges } from '@angular/core';

@Component({
    selector: 'alert-box',
    template: `
        <div [className]='alertStyle' role="alert">
            <button type="button" class='close' aria-label="Close" (click)='notifyClose()'><span aria-hidden="true">&times;</span></button>
            {{message}}
        </div>
    `
})
export class AlertBoxComponent implements OnInit, OnChanges {

    private alertStyle: string;
    @Input() message: string;
    @Input() type: string;
    @Output() close : EventEmitter<void> = new EventEmitter<void>();

    constructor() { }

    ngOnInit() { }

    ngOnChanges() {
        switch (this.type) {
            case 'error':
                this.alertStyle = 'alert alert-danger';
                break;
            case 'warning':
                this.alertStyle = 'alert alert-warning';
                break;
            case 'info':
                this.alertStyle = 'alert alert-info';
                break;
            case 'success':
                this.alertStyle = 'alert alert-success';
                break;
            default:
                this.alertStyle = 'alert alert-danger';
                break;
        }
    }

    notifyClose() {
        this.close.emit();
    }
}