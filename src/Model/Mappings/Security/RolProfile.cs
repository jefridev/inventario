﻿using AutoMapper;
using Model.Converters.Security;
using Model.Models;
using Model.ViewModels.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Mappings.Security
{
    public class RolProfile : Profile
    {
        public RolProfile()
        {
            CreateMap<Rol, RolViewModel>()
                     .ConvertUsing<RolViewModelToRolTypeConverter>();

            CreateMap<RolViewModel, Rol>()
                        .ConvertUsing<RolToRolViewModelTypeConverter>();

            CreateMap<RolObjetoViewModel, RolObjeto>();
        }
    }
}
