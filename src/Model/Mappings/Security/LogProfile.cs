﻿using AutoMapper;
using Model.Converters.Security;
using Model.Models;
using Model.ViewModels.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Mappings.Security
{
    public class LogProfile : Profile
    {
        public LogProfile()
        {
            CreateMap<Log, LogViewModel>()
                 .ConvertUsing<LogToLogViewModelITypeConverter>();
        }
    }
}
