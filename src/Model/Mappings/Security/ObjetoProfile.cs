﻿using AutoMapper;
using Model.Converters.Security;
using Model.Models;
using Model.ViewModels.Objectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Mappings.Security
{
    public class ObjetoProfile : Profile
    {
        public ObjetoProfile()
        {
            CreateMap<Objeto, ObjetoViewModel>()
                   .ConvertUsing<ObjetoToObjetoViewModelTypeConverter>();

            CreateMap<ObjetoViewModel, Objeto>()
                        .ConvertUsing<ObjetoViewModelToObjetoTypeConverter>();
        }
    }
}
