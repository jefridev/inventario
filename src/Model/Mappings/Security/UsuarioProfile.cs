﻿using AutoMapper;
using Model.Converters.Security;
using Model.Models;
using Model.ViewModels.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Mappings.Security
{
    public class UsuarioProfile : Profile
    {
        public UsuarioProfile()
        {
            CreateMap<UsuarioViewModel, Usuario>()
                        .ConvertUsing<UsuarioToUsuarioViewModelTypeConverter>();

            CreateMap<Usuario, UsuarioViewModel>()
                        .ConvertUsing<UsuarioViewModelToUsuarioTypeConverter>();

            CreateMap<UsuarioRolViewModel, UsuarioRol>();
        }
    }
}
