﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.ViewModels.Objectos
{
    public class ObjetoViewModel
    {
        public int IDObjeto { get; set; }
        public string NombreLogico { get; set; }
        public string NombreFisico { get; set; }
        public string TipoObjeto { get; set; }
        public int? IdobjetoRelacionado { get; set; }
        public string NombreObjetoRelacionado { get;set;}
        public string Estatus { get; set; }
    }
}
