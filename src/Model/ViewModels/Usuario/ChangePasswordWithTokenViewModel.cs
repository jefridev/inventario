﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.ViewModels.Usuario
{
    public class ChangePasswordWithTokenViewModel
    {
        public string Token { get; set; }
        public string NewPassword { get; set; }
    }
}
