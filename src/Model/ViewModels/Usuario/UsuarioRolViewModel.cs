﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.ViewModels.Usuario
{
    public class UsuarioRolViewModel
    {
        public int IdUsuario { get; set; }
        public int IdRol { get; set; }
    }
}
