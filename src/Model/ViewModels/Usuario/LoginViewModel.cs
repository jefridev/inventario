﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.ViewModels.Usuario
{
    public class LoginViewModel
    {
        public string Username { get; set; }
        public string Contrasena { get; set; }
        public string Canal { get; set; }
    }
}
