﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.ViewModels.Usuario
{
    public class UsuarioViewModel
    {
        public int? IDUsuario { get; set; }
        public string Nombre { get; set; }
        public string Username { get; set; }
        public string Correo { get; set; }
        public string Contrasena { get; set; }
        public string Telefono { get; set; }
        public string Idioma { get; set; }
        public string Estatus { get; set; }
    }
}
