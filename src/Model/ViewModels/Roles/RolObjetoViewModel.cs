﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.ViewModels.Roles
{
    public class RolObjetoViewModel
    {
        public int IDObjeto { get; set; }
        public int IDRol { get; set; }
    }
}
