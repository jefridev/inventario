﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.ViewModels.Roles
{
    public class RolViewModel
    {
        public int IDRol { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string RutaPrincipal { get; set; }
        public string Estatus { get; set; }
    }
}
