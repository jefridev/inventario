﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.ViewModels.Logs
{
    public class LogViewModel
    {
        public int IDLog { get; set; }
        public string Usuario { get; set; }
        public string Objeto { get; set; }
        public string Rol { get; set; }
        public DateTime Fecha { get; set; }
        public string Comentario { get; set; }
    }
}
