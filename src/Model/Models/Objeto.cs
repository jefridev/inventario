﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Objeto
    {
        public Objeto()
        {
            Log = new HashSet<Log>();
            RolObjeto = new HashSet<RolObjeto>();
        }

        public int Idobjeto { get; set; }
        public string NombreLogico { get; set; }
        public string NombreFisico { get; set; }
        public string TipoObjeto { get; set; }
        public int? IdobjetoRelacionado { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<Log> Log { get; set; }
        public virtual ICollection<RolObjeto> RolObjeto { get; set; }
        public virtual Objeto IdobjetoRelacionadoNavigation { get; set; }
        public virtual ICollection<Objeto> InverseIdobjetoRelacionadoNavigation { get; set; }
    }
}
