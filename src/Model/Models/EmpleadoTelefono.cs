﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class EmpleadoTelefono
    {
        public int Idempleado { get; set; }
        public int Idtelefono { get; set; }
        public string Estatus { get; set; }

        public virtual Empleado IdempleadoNavigation { get; set; }
        public virtual Telefono IdtelefonoNavigation { get; set; }
    }
}
