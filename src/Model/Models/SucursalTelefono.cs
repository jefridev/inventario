﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class SucursalTelefono
    {
        public int Idsucursal { get; set; }
        public int Idtelefono { get; set; }
        public string Estatus { get; set; }

        public virtual Sucursal IdsucursalNavigation { get; set; }
        public virtual Telefono IdtelefonoNavigation { get; set; }
    }
}
