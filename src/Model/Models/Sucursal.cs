﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Sucursal
    {
        public Sucursal()
        {
            Almacen = new HashSet<Almacen>();
            Caja = new HashSet<Caja>();
            Cierre = new HashSet<Cierre>();
            CuadreCaja = new HashSet<CuadreCaja>();
            Empleado = new HashSet<Empleado>();
            Factura = new HashSet<Factura>();
            NotaCredito = new HashSet<NotaCredito>();
            Recibo = new HashSet<Recibo>();
            SucursalContacto = new HashSet<SucursalContacto>();
            SucursalDireccion = new HashSet<SucursalDireccion>();
            SucursalTelefono = new HashSet<SucursalTelefono>();
        }

        public int Idsucursal { get; set; }
        public int Idempresa { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<Almacen> Almacen { get; set; }
        public virtual ICollection<Caja> Caja { get; set; }
        public virtual ICollection<Cierre> Cierre { get; set; }
        public virtual ICollection<CuadreCaja> CuadreCaja { get; set; }
        public virtual ICollection<Empleado> Empleado { get; set; }
        public virtual ICollection<Factura> Factura { get; set; }
        public virtual ICollection<NotaCredito> NotaCredito { get; set; }
        public virtual ICollection<Recibo> Recibo { get; set; }
        public virtual ICollection<SucursalContacto> SucursalContacto { get; set; }
        public virtual ICollection<SucursalDireccion> SucursalDireccion { get; set; }
        public virtual ICollection<SucursalTelefono> SucursalTelefono { get; set; }
        public virtual Empresa IdempresaNavigation { get; set; }
    }
}
