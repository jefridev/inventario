﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Factura
    {
        public Factura()
        {
            Conduce = new HashSet<Conduce>();
            Devolucion = new HashSet<Devolucion>();
            FacturaProducto = new HashSet<FacturaProducto>();
            ReciboFactura = new HashSet<ReciboFactura>();
        }

        public int Idfactura { get; set; }
        public int Idempresa { get; set; }
        public int Idsucursal { get; set; }
        public int Idcliente { get; set; }
        public int Idusuario { get; set; }
        public int Idcaja { get; set; }
        public string Ncf { get; set; }
        public int NumeroFactura { get; set; }
        public decimal Itbis { get; set; }
        public string IdmodoPago { get; set; }
        public string IdtipoPago { get; set; }
        public decimal MontoRecibido { get; set; }
        public decimal MontoDevuelta { get; set; }
        public decimal MontoTarjeta { get; set; }
        public string NumeroAprobacionTarjeta { get; set; }
        public decimal MontoBruto { get; set; }
        public decimal MontoNeto { get; set; }
        public decimal MontoRestante { get; set; }
        public string Estatus { get; set; }
        public string EstatusPago { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }

        public virtual ICollection<Conduce> Conduce { get; set; }
        public virtual ICollection<Devolucion> Devolucion { get; set; }
        public virtual ICollection<FacturaProducto> FacturaProducto { get; set; }
        public virtual ICollection<ReciboFactura> ReciboFactura { get; set; }
        public virtual Caja IdcajaNavigation { get; set; }
        public virtual Cliente IdclienteNavigation { get; set; }
        public virtual Empresa IdempresaNavigation { get; set; }
        public virtual ModoPago IdmodoPagoNavigation { get; set; }
        public virtual Sucursal IdsucursalNavigation { get; set; }
        public virtual TipoPago IdtipoPagoNavigation { get; set; }
        public virtual Usuario IdusuarioNavigation { get; set; }
    }
}
