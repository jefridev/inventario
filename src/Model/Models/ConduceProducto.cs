﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class ConduceProducto
    {
        public int Idconduce { get; set; }
        public int Idproducto { get; set; }
        public int Cantidad { get; set; }

        public virtual Conduce IdconduceNavigation { get; set; }
        public virtual Producto IdproductoNavigation { get; set; }
    }
}
