﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class CuadreCaja
    {
        public int IdcuadreCaja { get; set; }
        public int Idempresa { get; set; }
        public int Idsucursal { get; set; }
        public int Idusuario { get; set; }
        public string Idmoneda { get; set; }
        public decimal MontoCalculadoBoucher { get; set; }
        public decimal MontoCalculadoEfectivo { get; set; }
        public decimal MontoCalculadoCheque { get; set; }
        public decimal MontoContadoBoucher { get; set; }
        public decimal MontoContadoEfectivo { get; set; }
        public decimal MontoContadoCheque { get; set; }
        public string Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }

        public virtual Empresa IdempresaNavigation { get; set; }
        public virtual Moneda IdmonedaNavigation { get; set; }
        public virtual Sucursal IdsucursalNavigation { get; set; }
        public virtual Usuario IdusuarioNavigation { get; set; }
    }
}
