﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Cierre
    {
        public int Idcierre { get; set; }
        public int Idusuario { get; set; }
        public int Idcaja { get; set; }
        public int Idsucursal { get; set; }
        public string Idtanda { get; set; }
        public decimal TotalDigitado { get; set; }
        public decimal TotalFacturado { get; set; }
        public decimal Cobrado { get; set; }
        public decimal TotalBruto { get; set; }
        public decimal TotalNeto { get; set; }
        public decimal Sobrante { get; set; }
        public decimal Faltante { get; set; }
        public decimal Desembolso { get; set; }
        public decimal TotalGeneral { get; set; }
        public decimal Tarjeta { get; set; }
        public decimal Cheque { get; set; }

        public virtual Caja IdcajaNavigation { get; set; }
        public virtual Sucursal IdsucursalNavigation { get; set; }
        public virtual Tanda IdtandaNavigation { get; set; }
        public virtual Usuario IdusuarioNavigation { get; set; }
    }
}
