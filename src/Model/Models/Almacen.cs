﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Almacen
    {
        public Almacen()
        {
            Ajuste = new HashSet<Ajuste>();
            Conduce = new HashSet<Conduce>();
            DevolucionProducto = new HashSet<DevolucionProducto>();
            Entrada = new HashSet<Entrada>();
            ProductoExistencia = new HashSet<ProductoExistencia>();
            TransferenciaIdalmacenDestinoNavigation = new HashSet<Transferencia>();
            TransferenciaIdalmacenOrigenNavigation = new HashSet<Transferencia>();
        }

        public int Idalmacen { get; set; }
        public int Idsucursal { get; set; }
        public int IdempleadoResposable { get; set; }
        public string Nombre { get; set; }
        public string Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }

        public virtual ICollection<Ajuste> Ajuste { get; set; }
        public virtual ICollection<Conduce> Conduce { get; set; }
        public virtual ICollection<DevolucionProducto> DevolucionProducto { get; set; }
        public virtual ICollection<Entrada> Entrada { get; set; }
        public virtual ICollection<ProductoExistencia> ProductoExistencia { get; set; }
        public virtual ICollection<Transferencia> TransferenciaIdalmacenDestinoNavigation { get; set; }
        public virtual ICollection<Transferencia> TransferenciaIdalmacenOrigenNavigation { get; set; }
        public virtual Sucursal IdsucursalNavigation { get; set; }
    }
}
