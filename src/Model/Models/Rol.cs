﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Rol
    {
        public Rol()
        {
            Log = new HashSet<Log>();
            RolObjeto = new HashSet<RolObjeto>();
            UsuarioRol = new HashSet<UsuarioRol>();
        }

        public int Idrol { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string RutaPrincipal { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<Log> Log { get; set; }
        public virtual ICollection<RolObjeto> RolObjeto { get; set; }
        public virtual ICollection<UsuarioRol> UsuarioRol { get; set; }
    }
}
