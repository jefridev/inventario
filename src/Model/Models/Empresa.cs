﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Empresa
    {
        public Empresa()
        {
            Ajuste = new HashSet<Ajuste>();
            Cliente = new HashSet<Cliente>();
            Comprobante = new HashSet<Comprobante>();
            Conduce = new HashSet<Conduce>();
            CuadreCaja = new HashSet<CuadreCaja>();
            Devolucion = new HashSet<Devolucion>();
            Empleado = new HashSet<Empleado>();
            Entrada = new HashSet<Entrada>();
            Factura = new HashSet<Factura>();
            Producto = new HashSet<Producto>();
            Sucursal = new HashSet<Sucursal>();
            Suplidor = new HashSet<Suplidor>();
        }

        public int Idempresa { get; set; }
        public string Nombre { get; set; }
        public string Rnc { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }

        public virtual ICollection<Ajuste> Ajuste { get; set; }
        public virtual ICollection<Cliente> Cliente { get; set; }
        public virtual ICollection<Comprobante> Comprobante { get; set; }
        public virtual ICollection<Conduce> Conduce { get; set; }
        public virtual ICollection<CuadreCaja> CuadreCaja { get; set; }
        public virtual ICollection<Devolucion> Devolucion { get; set; }
        public virtual ICollection<Empleado> Empleado { get; set; }
        public virtual ICollection<Entrada> Entrada { get; set; }
        public virtual ICollection<Factura> Factura { get; set; }
        public virtual ICollection<Producto> Producto { get; set; }
        public virtual ICollection<Sucursal> Sucursal { get; set; }
        public virtual ICollection<Suplidor> Suplidor { get; set; }
    }
}
