﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Conduce
    {
        public Conduce()
        {
            ConduceProducto = new HashSet<ConduceProducto>();
        }

        public int Idconduce { get; set; }
        public int Idempresa { get; set; }
        public int Idalmacen { get; set; }
        public int Idcliente { get; set; }
        public int Idusuario { get; set; }
        public int? Idfactura { get; set; }
        public bool Facturado { get; set; }
        public int NumeroConduce { get; set; }
        public string Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }

        public virtual ICollection<ConduceProducto> ConduceProducto { get; set; }
        public virtual Almacen IdalmacenNavigation { get; set; }
        public virtual Cliente IdclienteNavigation { get; set; }
        public virtual Empresa IdempresaNavigation { get; set; }
        public virtual Factura IdfacturaNavigation { get; set; }
        public virtual Usuario IdusuarioNavigation { get; set; }
    }
}
