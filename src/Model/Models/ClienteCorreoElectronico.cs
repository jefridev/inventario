﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class ClienteCorreoElectronico
    {
        public int Idcliente { get; set; }
        public int IdcorreoElectronico { get; set; }
        public string Estatus { get; set; }

        public virtual Cliente IdclienteNavigation { get; set; }
        public virtual CorreoElectronico IdcorreoElectronicoNavigation { get; set; }
    }
}
