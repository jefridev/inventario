﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class EmpleadoCorreoElectronico
    {
        public int IdcorreoElectronico { get; set; }
        public int Idempleado { get; set; }
        public string Estatus { get; set; }

        public virtual CorreoElectronico IdcorreoElectronicoNavigation { get; set; }
        public virtual Empleado IdempleadoNavigation { get; set; }
    }
}
