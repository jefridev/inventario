﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Log
    {
        public int Idlog { get; set; }
        public int Idusuario { get; set; }
        public int Idobjeto { get; set; }
        public int Idrol { get; set; }
        public DateTime Fecha { get; set; }
        public string Comentario { get; set; }

        public virtual Objeto IdobjetoNavigation { get; set; }
        public virtual Rol IdrolNavigation { get; set; }
        public virtual Usuario IdusuarioNavigation { get; set; }
    }
}
