﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class UnidadMedida
    {
        public UnidadMedida()
        {
            ProductoCodigoUnidadMedidaPesoNavigation = new HashSet<Producto>();
            ProductoCodigoUnidadMedidaSizeNavigation = new HashSet<Producto>();
        }

        public string CodigoUnidad { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }

        public virtual ICollection<Producto> ProductoCodigoUnidadMedidaPesoNavigation { get; set; }
        public virtual ICollection<Producto> ProductoCodigoUnidadMedidaSizeNavigation { get; set; }
    }
}
