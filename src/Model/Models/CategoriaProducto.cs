﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class CategoriaProducto
    {
        public CategoriaProducto()
        {
            SubcategoriaProducto = new HashSet<SubcategoriaProducto>();
        }

        public int IdcategoriaProducto { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }

        public virtual ICollection<SubcategoriaProducto> SubcategoriaProducto { get; set; }
    }
}
