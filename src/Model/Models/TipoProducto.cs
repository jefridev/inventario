﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class TipoProducto
    {
        public TipoProducto()
        {
            Producto = new HashSet<Producto>();
        }

        public int IdtipoProducto { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaCreacion { get; set; }

        public virtual ICollection<Producto> Producto { get; set; }
    }
}
