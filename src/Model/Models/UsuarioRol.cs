﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class UsuarioRol
    {
        public int Idusuario { get; set; }
        public int Idrol { get; set; }
        public string Estatus { get; set; }

        public virtual Rol IdrolNavigation { get; set; }
        public virtual Usuario IdusuarioNavigation { get; set; }
    }
}
