﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class TipoCorreoElectronico
    {
        public TipoCorreoElectronico()
        {
            CorreoElectronico = new HashSet<CorreoElectronico>();
        }

        public int IdtipoCorreoElectronico { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<CorreoElectronico> CorreoElectronico { get; set; }
    }
}
