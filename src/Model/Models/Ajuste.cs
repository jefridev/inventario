﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Ajuste
    {
        public Ajuste()
        {
            AjusteProducto = new HashSet<AjusteProducto>();
        }

        public int Idajuste { get; set; }
        public int Idempresa { get; set; }
        public int Idalmacen { get; set; }
        public int IdtipoAjuste { get; set; }
        public int NumeroAjuste { get; set; }
        public int Idusuario { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<AjusteProducto> AjusteProducto { get; set; }
        public virtual Almacen IdalmacenNavigation { get; set; }
        public virtual Empresa IdempresaNavigation { get; set; }
        public virtual TipoAjuste IdtipoAjusteNavigation { get; set; }
        public virtual Usuario IdusuarioNavigation { get; set; }
    }
}
