﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Devolucion
    {
        public Devolucion()
        {
            DevolucionProducto = new HashSet<DevolucionProducto>();
        }

        public int Iddevolucion { get; set; }
        public int Idempresa { get; set; }
        public int Idcliente { get; set; }
        public int Idfactura { get; set; }
        public int NumeroDevolucion { get; set; }
        public string Ncf { get; set; }
        public string Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }

        public virtual ICollection<DevolucionProducto> DevolucionProducto { get; set; }
        public virtual Cliente IdclienteNavigation { get; set; }
        public virtual Empresa IdempresaNavigation { get; set; }
        public virtual Factura IdfacturaNavigation { get; set; }
    }
}
