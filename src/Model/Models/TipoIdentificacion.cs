﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class TipoIdentificacion
    {
        public TipoIdentificacion()
        {
            Cliente = new HashSet<Cliente>();
            Empleado = new HashSet<Empleado>();
            Suplidor = new HashSet<Suplidor>();
        }

        public int IdtipoIdentificacion { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<Cliente> Cliente { get; set; }
        public virtual ICollection<Empleado> Empleado { get; set; }
        public virtual ICollection<Suplidor> Suplidor { get; set; }
    }
}
