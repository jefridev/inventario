﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class ContactoTelefono
    {
        public int Idcontacto { get; set; }
        public int Idtelefono { get; set; }
        public string Estatus { get; set; }

        public virtual Contacto IdcontactoNavigation { get; set; }
        public virtual Telefono Idcontacto1 { get; set; }
    }
}
