﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class CorreoElectronico
    {
        public CorreoElectronico()
        {
            ClienteCorreoElectronico = new HashSet<ClienteCorreoElectronico>();
            ContactoCorreoElectronico = new HashSet<ContactoCorreoElectronico>();
            EmpleadoCorreoElectronico = new HashSet<EmpleadoCorreoElectronico>();
            SuplidorCorreoElectronico = new HashSet<SuplidorCorreoElectronico>();
        }

        public int IdcorreoElectronico { get; set; }
        public int IdtipoCorreoElectronico { get; set; }
        public string Correo { get; set; }
        public bool Principal { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<ClienteCorreoElectronico> ClienteCorreoElectronico { get; set; }
        public virtual ICollection<ContactoCorreoElectronico> ContactoCorreoElectronico { get; set; }
        public virtual ICollection<EmpleadoCorreoElectronico> EmpleadoCorreoElectronico { get; set; }
        public virtual ICollection<SuplidorCorreoElectronico> SuplidorCorreoElectronico { get; set; }
        public virtual TipoCorreoElectronico IdtipoCorreoElectronicoNavigation { get; set; }
    }
}
