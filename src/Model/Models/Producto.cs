﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Producto
    {
        public Producto()
        {
            AjusteProducto = new HashSet<AjusteProducto>();
            ConduceProducto = new HashSet<ConduceProducto>();
            DevolucionProducto = new HashSet<DevolucionProducto>();
            EntradaProducto = new HashSet<EntradaProducto>();
            FacturaProducto = new HashSet<FacturaProducto>();
            HistorialCostoProducto = new HashSet<HistorialCostoProducto>();
            HistorialPrecioProducto = new HashSet<HistorialPrecioProducto>();
            ProductoExistencia = new HashSet<ProductoExistencia>();
            TransferenciaProducto = new HashSet<TransferenciaProducto>();
        }

        public int Idproducto { get; set; }
        public int Idempresa { get; set; }
        public string Nombre { get; set; }
        public string CodigoProducto { get; set; }
        public int IdtipoProducto { get; set; }
        public string Color { get; set; }
        public short NivelSeguroStock { get; set; }
        public short PuntoReorden { get; set; }
        public decimal Costo { get; set; }
        public decimal Precio { get; set; }
        public bool Impuesto { get; set; }
        public string Size { get; set; }
        public string CodigoUnidadMedidaSize { get; set; }
        public string CodigoUnidadMedidaPeso { get; set; }
        public decimal? Peso { get; set; }
        public int? IdsubcategoriaProducto { get; set; }
        public DateTime FechaInicioVenta { get; set; }
        public DateTime FechaFinVenta { get; set; }
        public bool Descontinuado { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<AjusteProducto> AjusteProducto { get; set; }
        public virtual ICollection<ConduceProducto> ConduceProducto { get; set; }
        public virtual ICollection<DevolucionProducto> DevolucionProducto { get; set; }
        public virtual ICollection<EntradaProducto> EntradaProducto { get; set; }
        public virtual ICollection<FacturaProducto> FacturaProducto { get; set; }
        public virtual ICollection<HistorialCostoProducto> HistorialCostoProducto { get; set; }
        public virtual ICollection<HistorialPrecioProducto> HistorialPrecioProducto { get; set; }
        public virtual ICollection<ProductoExistencia> ProductoExistencia { get; set; }
        public virtual ICollection<TransferenciaProducto> TransferenciaProducto { get; set; }
        public virtual UnidadMedida CodigoUnidadMedidaPesoNavigation { get; set; }
        public virtual UnidadMedida CodigoUnidadMedidaSizeNavigation { get; set; }
        public virtual Empresa IdempresaNavigation { get; set; }
        public virtual SubcategoriaProducto IdsubcategoriaProductoNavigation { get; set; }
        public virtual TipoProducto IdtipoProductoNavigation { get; set; }
    }
}
