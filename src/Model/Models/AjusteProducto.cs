﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class AjusteProducto
    {
        public int Idajuste { get; set; }
        public int Idproducto { get; set; }
        public int Cantidad { get; set; }

        public virtual Ajuste IdajusteNavigation { get; set; }
        public virtual Producto IdproductoNavigation { get; set; }
    }
}
