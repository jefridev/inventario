﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class EntradaProducto
    {
        public int IdentradaProducto { get; set; }
        public int Identrada { get; set; }
        public int Idproducto { get; set; }
        public string Estante { get; set; }
        public string Compartimiento { get; set; }
        public int Cantidad { get; set; }
        public int Costo { get; set; }

        public virtual Entrada IdentradaNavigation { get; set; }
        public virtual Producto IdproductoNavigation { get; set; }
    }
}
