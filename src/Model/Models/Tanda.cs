﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Tanda
    {
        public Tanda()
        {
            Caja = new HashSet<Caja>();
            Cierre = new HashSet<Cierre>();
        }

        public string Idtanda { get; set; }
        public string Nombre { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<Caja> Caja { get; set; }
        public virtual ICollection<Cierre> Cierre { get; set; }
    }
}
