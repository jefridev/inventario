﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class TipoSuplidor
    {
        public TipoSuplidor()
        {
            Suplidor = new HashSet<Suplidor>();
        }

        public int IdtipoSuplidor { get; set; }
        public string Nombre { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<Suplidor> Suplidor { get; set; }
    }
}
