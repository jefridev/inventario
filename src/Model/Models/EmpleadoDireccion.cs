﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class EmpleadoDireccion
    {
        public int Idempleado { get; set; }
        public int Iddireccion { get; set; }
        public string Estatus { get; set; }

        public virtual Direccion IddireccionNavigation { get; set; }
        public virtual Empleado IdempleadoNavigation { get; set; }
    }
}
