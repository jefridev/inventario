﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class DevolucionProducto
    {
        public int Iddevolucion { get; set; }
        public int Idproducto { get; set; }
        public int Idalmacen { get; set; }
        public int Cantidad { get; set; }
        public decimal Precio { get; set; }
        public decimal Descuento { get; set; }
        public decimal Impuesto { get; set; }

        public virtual Almacen IdalmacenNavigation { get; set; }
        public virtual Devolucion IddevolucionNavigation { get; set; }
        public virtual Producto IdproductoNavigation { get; set; }
    }
}
