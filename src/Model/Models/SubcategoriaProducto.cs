﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class SubcategoriaProducto
    {
        public SubcategoriaProducto()
        {
            Producto = new HashSet<Producto>();
        }

        public int IdsubcategoriaProducto { get; set; }
        public int IdcategoriaProducto { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }

        public virtual ICollection<Producto> Producto { get; set; }
        public virtual CategoriaProducto IdcategoriaProductoNavigation { get; set; }
    }
}
