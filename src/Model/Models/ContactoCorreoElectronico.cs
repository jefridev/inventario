﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class ContactoCorreoElectronico
    {
        public int Idcontacto { get; set; }
        public int IdcorreoElectronico { get; set; }
        public string Estatus { get; set; }

        public virtual Contacto IdcontactoNavigation { get; set; }
        public virtual CorreoElectronico IdcorreoElectronicoNavigation { get; set; }
    }
}
