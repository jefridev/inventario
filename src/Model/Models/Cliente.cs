﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Cliente
    {
        public Cliente()
        {
            ClienteCorreoElectronico = new HashSet<ClienteCorreoElectronico>();
            ClienteDireccion = new HashSet<ClienteDireccion>();
            ClienteTelefono = new HashSet<ClienteTelefono>();
            Conduce = new HashSet<Conduce>();
            Devolucion = new HashSet<Devolucion>();
            Factura = new HashSet<Factura>();
            NotaCredito = new HashSet<NotaCredito>();
            Recibo = new HashSet<Recibo>();
        }

        public int Idcliente { get; set; }
        public int IdtipoCliente { get; set; }
        public int Idempresa { get; set; }
        public int IdtipoIdentificacion { get; set; }
        public string Identificacion { get; set; }
        public string Nombre { get; set; }
        public decimal LimiteCredito { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<ClienteCorreoElectronico> ClienteCorreoElectronico { get; set; }
        public virtual ICollection<ClienteDireccion> ClienteDireccion { get; set; }
        public virtual ICollection<ClienteTelefono> ClienteTelefono { get; set; }
        public virtual ICollection<Conduce> Conduce { get; set; }
        public virtual ICollection<Devolucion> Devolucion { get; set; }
        public virtual ICollection<Factura> Factura { get; set; }
        public virtual ICollection<NotaCredito> NotaCredito { get; set; }
        public virtual ICollection<Recibo> Recibo { get; set; }
        public virtual Empresa IdempresaNavigation { get; set; }
        public virtual TipoCliente IdtipoClienteNavigation { get; set; }
        public virtual TipoIdentificacion IdtipoIdentificacionNavigation { get; set; }
    }
}
