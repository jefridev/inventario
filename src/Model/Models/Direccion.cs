﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Direccion
    {
        public Direccion()
        {
            ClienteDireccion = new HashSet<ClienteDireccion>();
            ContactoDireccion = new HashSet<ContactoDireccion>();
            EmpleadoDireccion = new HashSet<EmpleadoDireccion>();
            SucursalDireccion = new HashSet<SucursalDireccion>();
            SuplidorDireccion = new HashSet<SuplidorDireccion>();
        }

        public int Iddireccion { get; set; }
        public int IdtipoDireccion { get; set; }
        public string DireccionLinea1 { get; set; }
        public string DireccionLinea2 { get; set; }
        public string Ciudad { get; set; }
        public string CodigoPostal { get; set; }
        public bool Principal { get; set; }
        public bool Correspondencia { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public virtual ICollection<ClienteDireccion> ClienteDireccion { get; set; }
        public virtual ICollection<ContactoDireccion> ContactoDireccion { get; set; }
        public virtual ICollection<EmpleadoDireccion> EmpleadoDireccion { get; set; }
        public virtual ICollection<SucursalDireccion> SucursalDireccion { get; set; }
        public virtual ICollection<SuplidorDireccion> SuplidorDireccion { get; set; }
        public virtual TipoDireccion IdtipoDireccionNavigation { get; set; }
    }
}
