﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class SuplidorCorreoElectronico
    {
        public int Idsuplidor { get; set; }
        public int IdcorreoElectronico { get; set; }
        public string Estatus { get; set; }

        public virtual CorreoElectronico IdcorreoElectronicoNavigation { get; set; }
        public virtual Suplidor IdsuplidorNavigation { get; set; }
    }
}
