﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class NotaCreditoFactura
    {
        public int IdnotaCredito { get; set; }
        public int Idfactura { get; set; }
        public decimal Monto { get; set; }
        public decimal MontoRestante { get; set; }
        public string Estatus { get; set; }
    }
}
