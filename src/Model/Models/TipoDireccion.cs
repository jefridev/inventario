﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class TipoDireccion
    {
        public TipoDireccion()
        {
            Direccion = new HashSet<Direccion>();
        }

        public int IdtipoDireccion { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public virtual ICollection<Direccion> Direccion { get; set; }
    }
}
