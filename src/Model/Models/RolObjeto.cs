﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class RolObjeto
    {
        public int Idrol { get; set; }
        public int Idobjeto { get; set; }
        public bool RegistrarLog { get; set; }

        public virtual Objeto IdobjetoNavigation { get; set; }
        public virtual Rol IdrolNavigation { get; set; }
    }
}
