﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Telefono
    {
        public Telefono()
        {
            ClienteTelefono = new HashSet<ClienteTelefono>();
            ContactoTelefono = new HashSet<ContactoTelefono>();
            EmpleadoTelefono = new HashSet<EmpleadoTelefono>();
            SucursalTelefono = new HashSet<SucursalTelefono>();
            SuplidorTelefono = new HashSet<SuplidorTelefono>();
        }

        public int Idtelefono { get; set; }
        public int IdtipoTelefono { get; set; }
        public string Numero { get; set; }
        public bool Principal { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public virtual ICollection<ClienteTelefono> ClienteTelefono { get; set; }
        public virtual ICollection<ContactoTelefono> ContactoTelefono { get; set; }
        public virtual ICollection<EmpleadoTelefono> EmpleadoTelefono { get; set; }
        public virtual ICollection<SucursalTelefono> SucursalTelefono { get; set; }
        public virtual ICollection<SuplidorTelefono> SuplidorTelefono { get; set; }
        public virtual TipoTelefono IdtipoTelefonoNavigation { get; set; }
    }
}
