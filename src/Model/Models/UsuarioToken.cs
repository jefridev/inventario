﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class UsuarioToken
    {
        public int IdusuarioToken { get; set; }
        public int Idusuario { get; set; }
        public string Token { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaExpiracion { get; set; }
        public DateTime? FechaSalida { get; set; }
        public string Canal { get; set; }
        public string Estatus { get; set; }

        public virtual Usuario IdusuarioNavigation { get; set; }
    }
}
