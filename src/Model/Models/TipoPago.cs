﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class TipoPago
    {
        public TipoPago()
        {
            Factura = new HashSet<Factura>();
            ReciboPago = new HashSet<ReciboPago>();
        }

        public string IdtipoPago { get; set; }
        public string Nombre { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<Factura> Factura { get; set; }
        public virtual ICollection<ReciboPago> ReciboPago { get; set; }
    }
}
