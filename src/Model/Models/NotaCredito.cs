﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class NotaCredito
    {
        public int IdnotaCredito { get; set; }
        public int Idcliente { get; set; }
        public int Idsucursal { get; set; }
        public int Idusuario { get; set; }
        public int NumeroNotaCredito { get; set; }
        public string Ncf { get; set; }
        public string Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }

        public virtual Cliente IdclienteNavigation { get; set; }
        public virtual Sucursal IdsucursalNavigation { get; set; }
        public virtual Usuario IdusuarioNavigation { get; set; }
    }
}
