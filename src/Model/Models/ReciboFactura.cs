﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class ReciboFactura
    {
        public int Idrecibo { get; set; }
        public int Idfactura { get; set; }
        public decimal Monto { get; set; }
        public decimal MontoRestante { get; set; }
        public string Estatus { get; set; }

        public virtual Factura IdfacturaNavigation { get; set; }
        public virtual Recibo IdreciboNavigation { get; set; }
    }
}
