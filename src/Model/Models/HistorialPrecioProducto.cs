﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class HistorialPrecioProducto
    {
        public int Idproducto { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public decimal Precio { get; set; }
        public DateTime FechaCreacion { get; set; }

        public virtual Producto IdproductoNavigation { get; set; }
    }
}
