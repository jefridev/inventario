﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class UsuarioEmpleado
    {
        public int Idusuario { get; set; }
        public int Idempleado { get; set; }
        public string Estatus { get; set; }

        public virtual Empleado IdempleadoNavigation { get; set; }
        public virtual Usuario IdusuarioNavigation { get; set; }
    }
}
