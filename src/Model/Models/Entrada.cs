﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Entrada
    {
        public Entrada()
        {
            EntradaProducto = new HashSet<EntradaProducto>();
        }

        public int Identrada { get; set; }
        public int Idempresa { get; set; }
        public int Idalmacen { get; set; }
        public int Idsuplidor { get; set; }
        public int Idusuario { get; set; }
        public int NumeroEntrada { get; set; }
        public string Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }

        public virtual ICollection<EntradaProducto> EntradaProducto { get; set; }
        public virtual Almacen IdalmacenNavigation { get; set; }
        public virtual Empresa IdempresaNavigation { get; set; }
        public virtual Suplidor IdsuplidorNavigation { get; set; }
        public virtual Usuario IdusuarioNavigation { get; set; }
    }
}
