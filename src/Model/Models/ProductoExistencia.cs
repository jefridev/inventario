﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class ProductoExistencia
    {
        public int Idproducto { get; set; }
        public int AlmacenId { get; set; }
        public int Cantidad { get; set; }
        public DateTime FechaModificacion { get; set; }

        public virtual Almacen Almacen { get; set; }
        public virtual Producto IdproductoNavigation { get; set; }
    }
}
