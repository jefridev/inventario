﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Comprobante
    {
        public int Idcompronte { get; set; }
        public int Idempresa { get; set; }
        public string IdtipoComprobante { get; set; }
        public string Prefijo { get; set; }
        public int Desde { get; set; }
        public int Hasta { get; set; }
        public int Proximo { get; set; }
        public string Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }

        public virtual Empresa IdempresaNavigation { get; set; }
        public virtual TipoComprobante IdtipoComprobanteNavigation { get; set; }
    }
}
