﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Suplidor
    {
        public Suplidor()
        {
            Entrada = new HashSet<Entrada>();
            SuplidorContacto = new HashSet<SuplidorContacto>();
            SuplidorCorreoElectronico = new HashSet<SuplidorCorreoElectronico>();
            SuplidorDireccion = new HashSet<SuplidorDireccion>();
            SuplidorTelefono = new HashSet<SuplidorTelefono>();
        }

        public int Idsuplidor { get; set; }
        public int Idempresa { get; set; }
        public int IdtipoSuplidor { get; set; }
        public int IdtipoIdentificacion { get; set; }
        public string Identificacion { get; set; }
        public string Nombre { get; set; }
        public string Comentario { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<Entrada> Entrada { get; set; }
        public virtual ICollection<SuplidorContacto> SuplidorContacto { get; set; }
        public virtual ICollection<SuplidorCorreoElectronico> SuplidorCorreoElectronico { get; set; }
        public virtual ICollection<SuplidorDireccion> SuplidorDireccion { get; set; }
        public virtual ICollection<SuplidorTelefono> SuplidorTelefono { get; set; }
        public virtual Empresa IdempresaNavigation { get; set; }
        public virtual TipoIdentificacion IdtipoIdentificacionNavigation { get; set; }
        public virtual TipoSuplidor IdtipoSuplidorNavigation { get; set; }
    }
}
