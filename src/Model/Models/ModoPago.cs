﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class ModoPago
    {
        public ModoPago()
        {
            Factura = new HashSet<Factura>();
        }

        public string IdmodoPago { get; set; }
        public string Nombre { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<Factura> Factura { get; set; }
    }
}
