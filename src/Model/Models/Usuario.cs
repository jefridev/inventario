﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Usuario
    {
        public Usuario()
        {
            Ajuste = new HashSet<Ajuste>();
            Caja = new HashSet<Caja>();
            Cierre = new HashSet<Cierre>();
            Conduce = new HashSet<Conduce>();
            CuadreCaja = new HashSet<CuadreCaja>();
            Entrada = new HashSet<Entrada>();
            Factura = new HashSet<Factura>();
            Log = new HashSet<Log>();
            NotaCredito = new HashSet<NotaCredito>();
            TransferenciaIdusuarioRecibeNavigation = new HashSet<Transferencia>();
            TransferenciaIdusuarioSolicitaNavigation = new HashSet<Transferencia>();
            UsuarioEmpleado = new HashSet<UsuarioEmpleado>();
            UsuarioRol = new HashSet<UsuarioRol>();
            UsuarioToken = new HashSet<UsuarioToken>();
        }

        public int Idusuario { get; set; }
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public string Salt { get; set; }
        public string Nombre { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public string Idioma { get; set; }
        public int Intentos { get; set; }
        public DateTime? FechaBloqueo { get; set; }
        public string Token { get; set; }
        public string Estatus { get; set; }
        public DateTime? FechaCambioClave { get; set; }

        public virtual ICollection<Ajuste> Ajuste { get; set; }
        public virtual ICollection<Caja> Caja { get; set; }
        public virtual ICollection<Cierre> Cierre { get; set; }
        public virtual ICollection<Conduce> Conduce { get; set; }
        public virtual ICollection<CuadreCaja> CuadreCaja { get; set; }
        public virtual ICollection<Entrada> Entrada { get; set; }
        public virtual ICollection<Factura> Factura { get; set; }
        public virtual ICollection<Log> Log { get; set; }
        public virtual ICollection<NotaCredito> NotaCredito { get; set; }
        public virtual ICollection<Transferencia> TransferenciaIdusuarioRecibeNavigation { get; set; }
        public virtual ICollection<Transferencia> TransferenciaIdusuarioSolicitaNavigation { get; set; }
        public virtual ICollection<UsuarioEmpleado> UsuarioEmpleado { get; set; }
        public virtual ICollection<UsuarioRol> UsuarioRol { get; set; }
        public virtual ICollection<UsuarioToken> UsuarioToken { get; set; }
    }
}
