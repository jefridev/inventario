﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class ContactoDireccion
    {
        public int Idcontacto { get; set; }
        public int Iddireccion { get; set; }
        public string Estatus { get; set; }

        public virtual Contacto IdcontactoNavigation { get; set; }
        public virtual Direccion IddireccionNavigation { get; set; }
    }
}
