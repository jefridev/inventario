﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class TipoTelefono
    {
        public TipoTelefono()
        {
            Telefono = new HashSet<Telefono>();
        }

        public int IdtipoTelefono { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public virtual ICollection<Telefono> Telefono { get; set; }
    }
}
