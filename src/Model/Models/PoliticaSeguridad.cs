﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class PoliticaSeguridad
    {
        public int Idpolitica { get; set; }
        public int? LongitudClave { get; set; }
        public bool IncluirLetras { get; set; }
        public bool IncluirNumeros { get; set; }
        public bool IncluirCaracteres { get; set; }
        public int? NumeroIntentos { get; set; }
        public int? TiempoBloqueo { get; set; }
        public int TiempoTokenCliente { get; set; }
        public int TiempoTokenWeb { get; set; }
        public int? VigenciaClave { get; set; }
        public int? VigenciaLog { get; set; }
    }
}
