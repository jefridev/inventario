﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class ReciboPago
    {
        public int IdreciboPago { get; set; }
        public int Idrecibo { get; set; }
        public string IdtipoPago { get; set; }
        public decimal Monto { get; set; }
        public string Estatus { get; set; }

        public virtual Recibo IdreciboNavigation { get; set; }
        public virtual TipoPago IdtipoPagoNavigation { get; set; }
    }
}
