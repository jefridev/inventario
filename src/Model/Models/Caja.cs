﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Caja
    {
        public Caja()
        {
            Cierre = new HashSet<Cierre>();
            Factura = new HashSet<Factura>();
            Recibo = new HashSet<Recibo>();
        }

        public int Idcaja { get; set; }
        public int Idsucursal { get; set; }
        public int Idusuario { get; set; }
        public string Idtanda { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaCierre { get; set; }
        public decimal MontoInicial { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<Cierre> Cierre { get; set; }
        public virtual ICollection<Factura> Factura { get; set; }
        public virtual ICollection<Recibo> Recibo { get; set; }
        public virtual Sucursal IdsucursalNavigation { get; set; }
        public virtual Tanda IdtandaNavigation { get; set; }
        public virtual Usuario IdusuarioNavigation { get; set; }
    }
}
