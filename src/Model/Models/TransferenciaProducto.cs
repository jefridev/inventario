﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class TransferenciaProducto
    {
        public int IdtransferenciaProducto { get; set; }
        public int Idtransferencia { get; set; }
        public int Idproducto { get; set; }
        public int CantidadEnvio { get; set; }
        public int CantidadRecibe { get; set; }
        public string Comentario { get; set; }
        public string Estatus { get; set; }

        public virtual Producto IdproductoNavigation { get; set; }
        public virtual Transferencia IdtransferenciaNavigation { get; set; }
    }
}
