﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Moneda
    {
        public Moneda()
        {
            CuadreCaja = new HashSet<CuadreCaja>();
        }

        public string Idmoneda { get; set; }
        public string Nombre { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<CuadreCaja> CuadreCaja { get; set; }
    }
}
