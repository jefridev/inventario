﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Transferencia
    {
        public Transferencia()
        {
            TransferenciaProducto = new HashSet<TransferenciaProducto>();
        }

        public int Idtransferencia { get; set; }
        public int IdalmacenOrigen { get; set; }
        public int IdalmacenDestino { get; set; }
        public int IdusuarioSolicita { get; set; }
        public int IdusuarioRecibe { get; set; }
        public DateTime FechaSolicita { get; set; }
        public DateTime FechaRecibe { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<TransferenciaProducto> TransferenciaProducto { get; set; }
        public virtual Almacen IdalmacenDestinoNavigation { get; set; }
        public virtual Almacen IdalmacenOrigenNavigation { get; set; }
        public virtual Usuario IdusuarioRecibeNavigation { get; set; }
        public virtual Usuario IdusuarioSolicitaNavigation { get; set; }
    }
}
