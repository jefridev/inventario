﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class SuplidorContacto
    {
        public int Idsuplidor { get; set; }
        public int Idcontacto { get; set; }
        public string Estatus { get; set; }

        public virtual Contacto IdcontactoNavigation { get; set; }
        public virtual Suplidor IdsuplidorNavigation { get; set; }
    }
}
