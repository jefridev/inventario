﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class ClienteDireccion
    {
        public int Idcliente { get; set; }
        public int Iddireccion { get; set; }
        public string Estatus { get; set; }

        public virtual Cliente IdclienteNavigation { get; set; }
        public virtual Direccion IddireccionNavigation { get; set; }
    }
}
