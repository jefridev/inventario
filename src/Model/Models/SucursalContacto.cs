﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class SucursalContacto
    {
        public int Idsucursal { get; set; }
        public int Idcontacto { get; set; }
        public string Estatus { get; set; }

        public virtual Contacto IdcontactoNavigation { get; set; }
        public virtual Sucursal IdsucursalNavigation { get; set; }
    }
}
