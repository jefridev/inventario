﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Empleado
    {
        public Empleado()
        {
            EmpleadoCorreoElectronico = new HashSet<EmpleadoCorreoElectronico>();
            EmpleadoDireccion = new HashSet<EmpleadoDireccion>();
            EmpleadoTelefono = new HashSet<EmpleadoTelefono>();
            UsuarioEmpleado = new HashSet<UsuarioEmpleado>();
        }

        public int Idempleado { get; set; }
        public int Idempresa { get; set; }
        public int Idsucursal { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int IdtipoIdentificacion { get; set; }
        public string Identificacion { get; set; }
        public string Cargo { get; set; }
        public DateTime FechaIngreso { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<EmpleadoCorreoElectronico> EmpleadoCorreoElectronico { get; set; }
        public virtual ICollection<EmpleadoDireccion> EmpleadoDireccion { get; set; }
        public virtual ICollection<EmpleadoTelefono> EmpleadoTelefono { get; set; }
        public virtual ICollection<UsuarioEmpleado> UsuarioEmpleado { get; set; }
        public virtual Empresa IdempresaNavigation { get; set; }
        public virtual Sucursal IdsucursalNavigation { get; set; }
        public virtual TipoIdentificacion IdtipoIdentificacionNavigation { get; set; }
    }
}
