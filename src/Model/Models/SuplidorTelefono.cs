﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class SuplidorTelefono
    {
        public int Idsuplidor { get; set; }
        public int Idtelefono { get; set; }
        public string Estatus { get; set; }

        public virtual Suplidor IdsuplidorNavigation { get; set; }
        public virtual Telefono IdtelefonoNavigation { get; set; }
    }
}
