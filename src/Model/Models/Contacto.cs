﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Contacto
    {
        public Contacto()
        {
            ContactoCorreoElectronico = new HashSet<ContactoCorreoElectronico>();
            ContactoDireccion = new HashSet<ContactoDireccion>();
            ContactoTelefono = new HashSet<ContactoTelefono>();
            SucursalContacto = new HashSet<SucursalContacto>();
            SuplidorContacto = new HashSet<SuplidorContacto>();
        }

        public int Idcontacto { get; set; }
        public int IdtipoContacto { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Cargo { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }

        public virtual ICollection<ContactoCorreoElectronico> ContactoCorreoElectronico { get; set; }
        public virtual ICollection<ContactoDireccion> ContactoDireccion { get; set; }
        public virtual ICollection<ContactoTelefono> ContactoTelefono { get; set; }
        public virtual ICollection<SucursalContacto> SucursalContacto { get; set; }
        public virtual ICollection<SuplidorContacto> SuplidorContacto { get; set; }
        public virtual TipoContacto IdtipoContactoNavigation { get; set; }
    }
}
