﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class Recibo
    {
        public Recibo()
        {
            ReciboFactura = new HashSet<ReciboFactura>();
            ReciboPago = new HashSet<ReciboPago>();
        }

        public int Idrecibo { get; set; }
        public int Idcliente { get; set; }
        public int Idsucursal { get; set; }
        public int Idcaja { get; set; }
        public int NumeroRecibo { get; set; }
        public string Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }

        public virtual ICollection<ReciboFactura> ReciboFactura { get; set; }
        public virtual ICollection<ReciboPago> ReciboPago { get; set; }
        public virtual Caja IdcajaNavigation { get; set; }
        public virtual Cliente IdclienteNavigation { get; set; }
        public virtual Sucursal IdsucursalNavigation { get; set; }
    }
}
