﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class SucursalDireccion
    {
        public int Idsucursal { get; set; }
        public int Iddireccion { get; set; }
        public string Estatus { get; set; }

        public virtual Direccion IddireccionNavigation { get; set; }
        public virtual Sucursal IdsucursalNavigation { get; set; }
    }
}
