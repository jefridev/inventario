﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class TipoAjuste
    {
        public TipoAjuste()
        {
            Ajuste = new HashSet<Ajuste>();
        }

        public int IdtipoAjuste { get; set; }
        public string Descripcion { get; set; }
        public string Estatus { get; set; }

        public virtual ICollection<Ajuste> Ajuste { get; set; }
    }
}
