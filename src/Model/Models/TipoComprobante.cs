﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class TipoComprobante
    {
        public TipoComprobante()
        {
            Comprobante = new HashSet<Comprobante>();
        }

        public string IdtipoComprobante { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<Comprobante> Comprobante { get; set; }
    }
}
