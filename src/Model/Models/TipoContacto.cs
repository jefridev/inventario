﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class TipoContacto
    {
        public TipoContacto()
        {
            Contacto = new HashSet<Contacto>();
        }

        public int IdtipoContacto { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaCreacion { get; set; }

        public virtual ICollection<Contacto> Contacto { get; set; }
    }
}
