﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class FacturaProducto
    {
        public int Idfactura { get; set; }
        public int Idproducto { get; set; }
        public decimal Precio { get; set; }
        public int Cantidad { get; set; }
        public decimal Descuentos { get; set; }
        public decimal Itbis { get; set; }

        public virtual Factura IdfacturaNavigation { get; set; }
        public virtual Producto IdproductoNavigation { get; set; }
    }
}
