﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class ClienteTelefono
    {
        public int Idcliente { get; set; }
        public int Idtelefono { get; set; }
        public string Estatus { get; set; }

        public virtual Cliente IdclienteNavigation { get; set; }
        public virtual Telefono IdtelefonoNavigation { get; set; }
    }
}
