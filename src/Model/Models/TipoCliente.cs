﻿using System;
using System.Collections.Generic;

namespace Model.Models
{
    public partial class TipoCliente
    {
        public TipoCliente()
        {
            Cliente = new HashSet<Cliente>();
        }

        public int IdtipoCliente { get; set; }
        public string Nombre { get; set; }
        public string Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }

        public virtual ICollection<Cliente> Cliente { get; set; }
    }
}
