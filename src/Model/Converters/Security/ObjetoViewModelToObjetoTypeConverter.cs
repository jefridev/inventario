﻿using AutoMapper;
using Model.Models;
using Model.ViewModels.Objectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Converters.Security
{
    public class ObjetoViewModelToObjetoTypeConverter : ITypeConverter<ObjetoViewModel, Objeto>
    {
        public Objeto Convert(ObjetoViewModel source, Objeto destination, ResolutionContext context)
        {
            if (source == null)
                return null;

            return new Objeto()
            {
                Idobjeto = source.IDObjeto,
                NombreFisico = source.NombreFisico,
                NombreLogico = source.NombreLogico,
                TipoObjeto = source.TipoObjeto,
                IdobjetoRelacionado = source.IdobjetoRelacionado,
                Estatus = source.Estatus
            };
        }
    }
}
