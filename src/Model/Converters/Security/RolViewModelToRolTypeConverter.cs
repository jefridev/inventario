﻿using AutoMapper;
using Model.Models;
using Model.ViewModels.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Converters.Security
{
    public class RolViewModelToRolTypeConverter : ITypeConverter<Rol, RolViewModel>
    {
        public RolViewModel Convert(Rol source, RolViewModel destination, ResolutionContext context)
        {
            if (source == null)
                return null;

            return new RolViewModel() {
                IDRol = source.Idrol,
                Nombre = source.Nombre,
                Descripcion = source.Descripcion,
                RutaPrincipal = source.RutaPrincipal,
                Estatus = source.Estatus
            };
        }
    }
}
