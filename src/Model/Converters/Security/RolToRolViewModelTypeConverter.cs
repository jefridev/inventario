﻿using AutoMapper;
using Model.Models;
using Model.ViewModels.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Converters.Security
{
    public class RolToRolViewModelTypeConverter : ITypeConverter<RolViewModel, Rol>
    {
        public Rol Convert(RolViewModel source, Rol destination, ResolutionContext context)
        {
            if (source == null)
                return null;

            return new Rol() {
                Idrol = source.IDRol,
                Nombre = source.Nombre,
                Descripcion = source.Descripcion,
                RutaPrincipal = source.RutaPrincipal,
                Estatus = source.Estatus
            };
        }
    }
}
