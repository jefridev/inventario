﻿using AutoMapper;
using Model.Models;
using Model.ViewModels.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Converters.Security
{
    public class LogToLogViewModelITypeConverter : ITypeConverter<Log, LogViewModel>
    {
        public LogViewModel Convert(Log source, LogViewModel destination, ResolutionContext context)
        {
            if (source == null)
                return null;

            return new LogViewModel()
            {
                IDLog = source.Idlog,
                Usuario = source.IdusuarioNavigation?.Username,
                Objeto = source.IdobjetoNavigation?.NombreLogico,
                Rol = source.IdrolNavigation?.Nombre,
                Comentario = source.Comentario,
                Fecha = source.Fecha
            };
        }
    }
}
