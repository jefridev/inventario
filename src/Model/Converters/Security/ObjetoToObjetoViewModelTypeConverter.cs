﻿using AutoMapper;
using Model.Models;
using Model.ViewModels.Objectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Converters.Security
{
    public class ObjetoToObjetoViewModelTypeConverter : ITypeConverter<Objeto, ObjetoViewModel>
    {
        public ObjetoViewModel Convert(Objeto source, ObjetoViewModel destination, ResolutionContext context)
        {
            if (source == null)
                return null;

            return new ObjetoViewModel()
            {
                IDObjeto = source.Idobjeto,
                NombreFisico = source.NombreFisico,
                NombreLogico = source.NombreLogico,
                TipoObjeto = source.TipoObjeto,
                IdobjetoRelacionado = source.IdobjetoRelacionado,
                NombreObjetoRelacionado = source?.IdobjetoRelacionadoNavigation?.NombreLogico,
                Estatus = source.Estatus
            };
        }
    }
}
