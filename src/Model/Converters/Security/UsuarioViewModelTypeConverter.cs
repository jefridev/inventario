﻿using AutoMapper;
using Model.Models;
using Model.ViewModels.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Converters.Security
{
    public class UsuarioToUsuarioViewModelTypeConverter : ITypeConverter<UsuarioViewModel, Usuario>
    {
        public Usuario Convert(UsuarioViewModel source, Usuario destination, ResolutionContext context)
        {
            if (source == null)
                return null;

            return new Usuario()
            {
                Idusuario = (source.IDUsuario == null) ? 0 : source.IDUsuario.Value,
                Nombre = source.Nombre,
                Username = source.Username,
                Correo = source.Correo,
                Telefono = source.Telefono,
                Idioma = source.Idioma,
                Estatus = source.Estatus
            };
        }
    }

    public class UsuarioViewModelToUsuarioTypeConverter : ITypeConverter<Usuario, UsuarioViewModel>
    {
        public UsuarioViewModel Convert(Usuario source, UsuarioViewModel destination, ResolutionContext context)
        {
            if (source == null)
                return null;

            return new UsuarioViewModel()
            {
                IDUsuario = source.Idusuario,
                Nombre = source.Nombre,
                Username = source.Username,
                Correo = source.Correo,
                Telefono = source.Telefono,
                Idioma = source.Idioma,
                Estatus = source.Estatus
            };
        }
    }
}
