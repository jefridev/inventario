﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.OptionModels
{
    public class SiteOptions
    {
        public string Name { get; set; }
        public string AppUrl { get; set; }
        public string ServiceUrl { get; set; }
    }
}
