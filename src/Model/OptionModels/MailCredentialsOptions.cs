﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.OptionModels
{
    public class MailCredentialsOptions
    {
        public string SmtpServer { get; set; }
        public int Port { get; set; }
        public bool Ssl { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
