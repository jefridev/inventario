﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using AutoMapper;
using Model.Mappings.Security;
using Data.Infrastructure.Interfaces;
using Data.Infrastructure.Implementation;
using Service.Contracts.Security;
using Service.Implementation.Security;
using Data.Context;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.Swagger.Model;
using Microsoft.Extensions.PlatformAbstractions;
using System.IO;
using Model.OptionModels;
using System.Net;
using Service.AuthorizationRequirements;
using Microsoft.AspNetCore.Authorization;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Service.Contracts.App;
using Service.Implementation.App;

namespace Inventario
{
    public class Startup
    {
        private readonly SymmetricSecurityKey _signingKey;
        public static IConfigurationRoot Configuration { get; set; }

        public Startup(IHostingEnvironment env)
        {

            Configuration = new ConfigurationBuilder()
                            .SetBasePath(env.ContentRootPath)
                            .AddJsonFile("appsettings.json")
                            .Build();

            string SecretKey = Configuration["SecretKey"];
            _signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SecretKey));
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            string connection = Configuration.GetConnectionString("LocalConnection");
            services.AddDbContext<InventarioContext>(options => options.UseSqlServer(connection));

           
            services.AddOptions();
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]{
                    new CultureInfo("es-Es")
                };
                options.DefaultRequestCulture = new RequestCulture("es-Es");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });

            services.AddCors();
            services.AddMvc();
            services.AddAutoMapper(options =>
            {
                options.AddProfile(new UsuarioProfile());
                options.AddProfile(new RolProfile());
                options.AddProfile(new ObjetoProfile());
                options.AddProfile(new LogProfile());
            });
            services.AddSwaggerGen();
            services.ConfigureSwaggerGen(options =>
            {
                options.SingleApiVersion(new Info()
                {
                    Version = "v1",
                    Title = "Servicios sistema de inventario y ventas",
                    Description = "Conjunto de servicios utilizados para aplicación de control de inventario y ventas",
                    TermsOfService = "No puede ser utilizado sin concentimiento explicito."
                });

                string basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlResultadosDoc = Path.Combine(basePath, Configuration["Documentation:FileName"]);
                options.IncludeXmlComments(xmlResultadosDoc);
            });

            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));
            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
            });

            var siteOptions = Configuration.GetSection(nameof(SiteOptions));
            services.Configure<SiteOptions>(options => {
                options.Name = siteOptions[nameof(SiteOptions.Name)];
                options.AppUrl = siteOptions[nameof(SiteOptions.AppUrl)];
                options.ServiceUrl = siteOptions[nameof(SiteOptions.ServiceUrl)];
            });

            var mailCrendentialsOptions = Configuration.GetSection(nameof(MailCredentialsOptions));
            services.Configure<MailCredentialsOptions>(options =>
            {
                options.Email = mailCrendentialsOptions[nameof(MailCredentialsOptions.Email)];
                options.Password = mailCrendentialsOptions[nameof(MailCredentialsOptions.Password)];
                options.Port = int.Parse(mailCrendentialsOptions[nameof(MailCredentialsOptions.Port)]);
                options.Ssl = (mailCrendentialsOptions[nameof(MailCredentialsOptions.Ssl)] == "true") ? true : false;
                options.SmtpServer = mailCrendentialsOptions[nameof(MailCredentialsOptions.SmtpServer)];
            });

            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IEncryptionService, EncryptionService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IObjectService, ObjectService>();
            services.AddTransient<ILogService, LogService>();
            services.AddTransient<ISecurityPolicyService, SecurityPolicyService>();

            services.AddTransient<IMailService, MailService>();

            var sp = services.BuildServiceProvider();
            var objectService = sp.GetService<IObjectService>();

            services.AddAuthorization(options =>
            {
                var resultService = objectService.GetAllScreenTypeObjects();
                if (resultService.Status == HttpStatusCode.OK)
                {
                    foreach (var objeto in resultService.Value)
                    {
                        options.AddPolicy(objeto.NombreFisico,
                                          policy => policy.AddRequirements(new HasRoleUrlRequirement(objeto.NombreFisico),
                                                                           new HasActiveTokenRequirement()));
                    }
                }
            });

            services.AddTransient<IAuthorizationHandler, HasRoleUrlHandler>();
            services.AddTransient<IAuthorizationHandler, HasActiveTokenHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddFile(Configuration["Logging:LogPath"]);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],

                ValidateAudience = true,
                ValidAudience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingKey,

                RequireExpirationTime = true,
                ValidateLifetime = true,

                ClockSkew = TimeSpan.Zero
            };

            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                TokenValidationParameters = tokenValidationParameters
            });


            app.UseRequestLocalization();
            app.UseStaticFiles();
            app.UseCors(options =>
            {
                options.AllowAnyHeader();
                options.AllowAnyMethod();
                options.AllowAnyOrigin();
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute("areaRoute", "{area:exists}/{controller}/{action}/{id?}");
                routes.MapRoute(name: "default",
                                template: "{controller=Home}/{action=Index}/{id?}");
            });
            app.UseSwagger();
            app.UseSwaggerUi();
        }
    }
}
