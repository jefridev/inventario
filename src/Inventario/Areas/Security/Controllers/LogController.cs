﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Service.Contracts.Security;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Inventario.Areas.Security.Controllers
{
    /// <summary>
    ///  Utilizado para la gestión funciones realacionadas a los logs.
    /// </summary>
    [Area("security")]
    [Route("api/security/log")]
    public class LogController : Controller
    {
        private readonly ILogService LogService;

        /// <summary>
        ///  Constructor usando Dependency Injection.
        /// </summary>
        /// <param name="logService"></param>
        public LogController(ILogService logService)
        {
            LogService = logService;
        }

        /// <summary>
        ///  Busca el log especificado por ID.
        /// </summary>
        /// <param name="idLog"></param>
        /// <response code="200">Operación ejecuta y contiene el objeto</response>
        /// <response code="404">No se encontraro el objeto</response>
        /// <response code="500">Error interno</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpGet]
        [Authorize(Policy = "security/logs/obtenerLogPorID")]
        [Route("ObtenerLogPorId")]
        public IActionResult ObtenerLogPorId([FromQuery]int idLog)
        {
            var result = LogService.GetLogById(idLog);
            return Ok(result);
        }

        /// <summary>
        ///  Busca los logs registrados en el sistema.
        /// </summary>
        /// <param name="pagina">Página que se desea buscar</param>
        /// <param name="limite">Limite de páginas requeridas</param>
        /// <param name="busqueda">Consulta a realizar</param>
        /// <response code="200">Operación ejecuta y contiene resultados</response>
        /// <response code="404">No se encontraron datos</response>
        /// <response code="500">Error interno</response>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Policy = "security/logs/consultar")]
        [Route("BuscarLogs")]
        public IActionResult BuscarObjetos([FromQuery]int pagina, [FromQuery]int limite, [FromQuery] string busqueda)
        {
            var result = LogService.FindLogs(pagina, limite, busqueda);
            return Ok(result);
        }
    }
}
