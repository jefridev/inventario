﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Service.Contracts.Security;
using Model.ViewModels.Roles;
using Model.Models;
using Data.Status;
using Microsoft.AspNetCore.Authorization;

namespace Inventario.Areas.Security.Controllers
{
    /// <summary>
    ///  Utilizado para la gestión funciones realacionadas a los roles.
    /// </summary>
    [Area("security")]
    [Route("api/security/rol")]
    public class RolController : Controller
    {
        private readonly IMapper Mapper;
        private readonly IRoleService RoleService;

        /// <summary>
        /// Constructor que utiliza el inyector de dependencias.
        /// </summary>
        /// <param name="roleService"></param>
        /// <param name="mapper"></param>
        public RolController(IRoleService roleService,
                             IMapper mapper)
        {
            RoleService = roleService;
            Mapper = mapper;
        }


        /// <summary>
        /// Crea un nuevo rol en el sistema
        /// </summary>
        /// <param name="rol">Rol que se desea registrar</param>
        /// <response code="400">Parametros enviados incorrectos</response>
        /// <returns>Result con estatus y descripción.</returns>
        [HttpPost]
        [Authorize(Policy = "security/roles/create")]
        [Route("CrearRol")]
        public IActionResult CrearRol([FromBody]RolViewModel rol)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);


            Rol role = Mapper.Map<RolViewModel, Rol>(rol);
            role.Estatus = RecordStatus.Activo;

            var result = RoleService.CreateRole(role);
            return Ok(result);
        }

        /// <summary>
        ///  Permite la actualización de la información del rol.
        /// </summary>
        /// <param name="rol">Rol que se desea actualizar</param>
        /// <response code="200">Se ha actualizado la entrada correctamente</response>
        /// <response code="304">No se ha modificado, debido a algún evento</response>
        /// <response code="500">Error interno al tratar de realizar la actualización</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpPost]
        [Authorize(Policy = "security/roles/update")]
        [Route("ActualizarRol")]
        public IActionResult ActualizarRol([FromBody]RolViewModel rol)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Rol role = Mapper.Map<RolViewModel, Rol>(rol);
            var result = RoleService.UpdateRole(role);
            return Ok(result);
        }

        /// <summary>
        ///  Eliminar un rol del sistema.
        /// </summary>
        /// <param name="rol">rol que se desea eliminar</param>
        /// <response code="200">Eliminado correctamente</response>
        /// <response code="500">Error al eliminar el rol</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpPost]
        [Authorize(Policy = "security/roles/delete")]
        [Route("EliminarRol")]
        public IActionResult EliminarRol([FromBody]RolViewModel rol)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Rol role = Mapper.Map<RolViewModel, Rol>(rol);
            var result = RoleService.DeleteRole(role);
            return Ok(result);
        }

        /// <summary>
        ///  Busca los roles registrados en el sistema.
        /// </summary>
        /// <param name="pagina">Página que se desea buscar</param>
        /// <param name="limite">Limite de páginas requeridas</param>
        /// <param name="busqueda">Consulta a realizar</param>
        /// <response code="200">Operación ejecuta y contiene resultados</response>
        /// <response code="404">No se encontraron datos</response>
        /// <response code="500">Error interno</response>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Policy = "security/roles/consultar")]
        [Route("BuscarRoles")]
        public IActionResult BuscarRoles([FromQuery]int pagina, [FromQuery]int limite, [FromQuery] string busqueda)
        {
            var result = RoleService.FindRoles(pagina, limite, busqueda);
            return Ok(result);
        }

        /// <summary>
        ///  Busca los roles registrados en el sistema.
        /// </summary>
        /// <response code="200">Operación ejecuta y contiene resultados</response>
        /// <response code="404">No se encontraron datos</response>
        /// <response code="500">Error interno</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpGet]
        [Authorize(Policy = "security/roles/consultar")]
        [Route("ObtenerRolesActivos")]
        public IActionResult ObtenerRolesActivos()
        {
            var result = RoleService.GetAllActiveRoles();
            return Ok(result);
        }

        /// <summary>
        ///  Busca el rol especificado por ID.
        /// </summary>
        /// <response code="200">Operación ejecuta y contiene el rol</response>
        /// <response code="404">No se encontraro el rol</response>
        /// <response code="500">Error interno</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpGet]
        [Authorize(Policy = "security/roles/obtenerRolPorID")]
        [Route("ObtenerRolPorId")]
        public IActionResult ObtenerRolPorId([FromQuery]int idRol)
        {
            var result = RoleService.GetRoleById(idRol);
            return Ok(result);
        }

        /// <summary>
        /// Buscar roles asignados a un usuario
        /// </summary>
        /// <param name="idUsuario">Identificador del usuario</param>
        /// <response code="200">Encontró información y todo es devuelto</response>
        /// <response code="404">No encontró información</response>
        /// <response code="500">Error interno</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpGet]
        [Authorize(Policy = "security/roles/asignadosPorUsuario")]
        [Route("ObtenerRolesAsignadosUsuario")]
        public IActionResult ObtenerRolesAsignadosUsuario([FromQuery]int idUsuario)
        {
            var result = RoleService.GetAllRolesAssignedToUser(idUsuario);
            return Ok(result);
        }

        /// <summary>
        /// Buscar roles no asignados a un usuario
        /// </summary>
        /// <param name="idUsuario">Identificador del usuario</param>
        /// <response code="200">Encontró información y todo es devuelto</response>
        /// <response code="404">No encontró información</response>
        /// <response code="500">Error interno</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpGet]
        [Authorize(Policy = "security/roles/noAsignadosPorUsuario")]
        [Route("ObtenerRolesNoAsignadosPorUsuario")]
        public IActionResult ObtenerObjetosNoAsignadosRol([FromQuery]int idUsuario)
        {
            var result = RoleService.GetAllRolesNotAssignedToUser(idUsuario);
            return Ok(result);
        }

        /// <summary>
        ///  Asigna un objeto a un rol especificado.
        /// </summary>
        /// <param name="rolObjeto"></param>
        /// <response code="201">Rol asignado</response>
        /// <response code="500">Error interno</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpPost]
        [Authorize(Policy = "security/roles/asignarPermiso")]
        [Route("AsignarPermiso")]
        public IActionResult AsignarPermiso([FromBody]RolObjetoViewModel rolObjeto)
        {
            var roleObject = Mapper.Map<RolObjetoViewModel, RolObjeto>(rolObjeto);
            var result = RoleService.AssignPermission(roleObject);
            return Ok(result);
        }

        /// <summary>
        /// Elimina el permiso al objeto si este existe.
        /// </summary>
        /// <param name="rolObjeto">rol que se desea eliminar</param>
        /// <response code="200">Eliminado correctamente</response>
        /// <response code="500">Error al intentar eliminar</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpPost]
        [Authorize(Policy = "security/roles/removerPermiso")]
        [Route("RemoverPermiso")]
        public IActionResult RemoverPermiso([FromBody]RolObjetoViewModel rolObjeto)
        {
            var roleObject = Mapper.Map<RolObjetoViewModel, RolObjeto>(rolObjeto);
            var result = RoleService.RemovePermission(roleObject);
            return Ok(result);
        }
    }
}
