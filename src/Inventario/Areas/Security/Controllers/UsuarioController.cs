﻿using AutoMapper;
using Data.Status;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Model.Models;
using Model.OptionModels;
using Model.ViewModels.Usuario;
using Service.Contracts.Security;
using Microsoft.Extensions.PlatformAbstractions;
using System.Security.Claims;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Inventario.Areas.Security.Controllers
{
    /// <summary>
    ///  Utilizado para la gestión funciones realacionadas a los usuarios.
    /// </summary>
    [Area("security")]
    [Route("api/security/usuario")]
    public class UsuarioController : Controller
    {
        private readonly IMapper Mapper;
        private readonly IUserService UserService;
        private readonly IEncryptionService EncryptionService;

        /// <summary>
        /// Constructor que utiliza el inyector de dependencias.
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="encryptionService"></param>
        /// <param name="mapper"></param>
        public UsuarioController(IUserService userService,
                                 IEncryptionService encryptionService,
                                 IMapper mapper)
        {
            UserService = userService;
            EncryptionService = encryptionService;
            Mapper = mapper;
        }


        /// <summary>
        /// Crea un nuevo usuario en el sistema
        /// </summary>
        /// <param name="usuario">Usuario que se desea registrar</param>
        /// <response code="400">Parametros enviados incorrectos</response>
        /// <returns>Result con estatus y descripción.</returns>
        [HttpPost]
        [Authorize(Policy = "security/users/create")]
        [Route("CrearUsuario")]
        public IActionResult CrearUsuario([FromBody]UsuarioViewModel usuario)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);


            Usuario user = Mapper.Map<UsuarioViewModel, Usuario>(usuario);
            user.Estatus = RecordStatus.Pendiente;

            var result = UserService.CreateUser(user);
            return Ok(result);
        }

        /// <summary>
        ///  Permite la actualización de la información del usuario.
        /// </summary>
        /// <param name="usuario">Usuario que se desea actualizar</param>
        /// <response code="200">Se ha actualizado la entrada correctamente</response>
        /// <response code="304">No se ha modificado, debido a algún evento</response>
        /// <response code="500">Error interno al tratar de realizar la actualización</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpPost]
        [Authorize(Policy = "security/users/update")]
        [Route("ActualizarUsuario")]
        public IActionResult ActualizarUsuario([FromBody]UsuarioViewModel usuario)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Usuario user = Mapper.Map<UsuarioViewModel, Usuario>(usuario);
            var result = UserService.UpdateUser(user);
            return Ok(result);
        }
    

        /// <summary>
        /// Regenera la contraseña de un usuario en particular.
        /// </summary>
        /// <param name="usuario"></param>
        /// <response code="200">Contraseña generada correctamente</response>
        /// <response code="500">Error regenerando la contraseña</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpPost]
        [Authorize(Policy = "security/users/regeneratePassword")]
        [Route("RegenerarContrasena")]
        public IActionResult RegenerarContrasena([FromBody]UsuarioViewModel usuario)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Usuario user = Mapper.Map<UsuarioViewModel, Usuario>(usuario);
            var result = UserService.RegeneratePassword(user);
            return Ok(result);
        }


        /// <summary>
        /// Cambiar contraseña utilizando el token del usuario más su nuevo password.
        /// </summary>
        /// <response code="200">Si todo es procesado en forma correcta</response>
        /// <response code="500">Error interno al intentar procesar la petición</response>
        /// <param name="change"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("CambiarContrasenaConToken")]
        public IActionResult CambiarContrasenaConToken([FromBody]ChangePasswordWithTokenViewModel change)
        {
            var result = UserService.UpdatePassword(change.NewPassword, change.Token);
            return Ok(result);
        }


        /// <summary>
        /// Cambiar contraseña a petición de un usuario que se encuentra logueado en el sistema
        /// y desea realizar una actualización.
        /// </summary>
        /// <response code="200">Si todo es procesado en forma correcta</response>
        /// <response code="500">Error interno al intentar procesar la petición</response>
        /// <param name="change"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Policy = "security/users/cambiarContrasena")]
        [Route("CambiarContrasena")]
        public IActionResult CambiarContrasena([FromBody]ChangePasswordViewModel change)
        {
            var value =  User.FindFirst("UserID").Value;
            int userID = int.Parse(value);
            var result = UserService.UpdatePassword(userID, change.OldPassword, change.NewPassword);
            return Ok(result);
        }


        /// <summary>
        /// Activar una cuenta que se encuentra en estado pendiente.
        /// </summary>
        /// <response code="200">Si la operación fue realizada correctamente</response>
        /// <response code="500">Si ocurre un error interno al intentar activar</response>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("ActivarCuenta")]
        public IActionResult ActivarCuenta([FromQuery]string token)
        {
            var result = UserService.ActivateAccount(token);
            return Ok(result);
        }


        /// <summary>
        ///  Eliminar un usuario del sistema.
        /// </summary>
        /// <param name="usuario">Usuario que se desea eliminar</param>
        /// <response code="200">Eliminado correctamente</response>
        /// <response code="500">Error al eliminar el usuario</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpPost]
        [Authorize(Policy = "security/users/delete")]
        [Route("EliminarUsuario")]
        public IActionResult EliminarUsuario([FromBody]UsuarioViewModel usuario)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Usuario user = Mapper.Map<UsuarioViewModel, Usuario>(usuario);
            var result = UserService.DeleteUser(user);
            return Ok(result);
        }

        /// <summary>
        /// Verifica si el nombre de usuario está disponible o no.
        /// </summary>
        /// <param name="nombreUsuario">Nombre de usuario que se desea verificar</param>
        /// <response code="302">No está disponible</response>
        /// <response code="404">Está disponible</response>
        /// <response code="500">Ocurrió un error al intentar consultar</response>
        /// <returns></returns>
        [HttpGet]
        [Route("UsuarioDisponible")]
        public IActionResult UsuarioDisponible([FromQuery]string nombreUsuario)
        {
            var usuario = new Usuario() { Username = nombreUsuario };
            var result = UserService.IsUsernameAvailable(usuario);
            return Ok(result);
        }

        /// <summary>
        /// Verifica si el correo está disponible o no.
        /// </summary>
        /// <param name="correo">Correo electronico que se desea verificar</param>
        /// <response code="302">No está disponible</response>
        /// <response code="404">Está disponible</response>
        /// <response code="500">Ocurrió un error al intentar consultar</response>
        /// <returns>Result con el estatus y descripción</returns>
        [HttpGet]
        [Route("CorreoDisponible")]
        public IActionResult CorreoDisponible([FromQuery]string correo)
        {
            var usuario = new Usuario() { Correo = correo };
            var result = UserService.IsUsernameAvailable(usuario);
            return Ok(result);
        }

        /// <summary>
        ///  Busca los usuarios registrados en el sistema.
        /// </summary>
        /// <param name="pagina">Página que se desea buscar</param>
        /// <param name="limite">Limite de páginas requeridas</param>
        /// <param name="busqueda">Consulta a realizar</param>
        /// <response code="200">Operación ejecuta y contiene resultados</response>
        /// <response code="404">No se encontraron datos</response>
        /// <response code="500">Error interno</response>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Policy = "security/users/consultar")]
        [Route("BuscarUsuarios")]
        public IActionResult BuscarUsuarios([FromQuery]int pagina, [FromQuery]int limite, [FromQuery] string busqueda)
        {
            var result = UserService.FindUsers(pagina, limite, busqueda);
            return Ok(result);
        }

        /// <summary>
        ///  Busca los usuarios registrados en el sistema.
        /// </summary>
        /// <response code="200">Operación ejecuta y contiene resultados</response>
        /// <response code="404">No se encontraron datos</response>
        /// <response code="500">Error interno</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpGet]
        [Authorize(Policy = "security/users/consultar")]
        [Route("ObtenerUsuariosActivos")]
        public IActionResult ObtenerUsuariosActivos()
        {
            var result = UserService.GetAllActiveUsers();
            return Ok(result);
        }

        /// <summary>
        ///  Busca el usuario especificado por ID.
        /// </summary>
        /// <response code="200">Operación ejecuta y contiene el usuario</response>
        /// <response code="404">No se encontraro el usuario</response>
        /// <response code="500">Error interno</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpGet]
        [Authorize(Policy ="security/users/obtenerUsuarioPorID")]
        [Route("ObtenerUsuarioPorId")]
        public IActionResult ObtenerUsuarioPorId([FromQuery]int idUsuario)
        {
            var result = UserService.GetUserById(idUsuario);
            return Ok(result);
        }

        /// <summary>
        ///  Asigna un rol a un usuario especificado.
        /// </summary>
        /// <param name="usuarioRol"></param>
        /// <response code="201">Rol asignado</response>
        /// <response code="500">Error interno</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpPost]
        [Authorize(Policy = "security/users/asignarRol")]
        [Route("AsignarRol")]
        public IActionResult AsignarRol([FromBody]UsuarioRolViewModel usuarioRol)
        {
            var userRole = Mapper.Map<UsuarioRolViewModel, UsuarioRol>(usuarioRol);
            var result = UserService.AssignRole(userRole);
            return Ok(result);
        }

        /// <summary>
        /// Elimina el rol asignado a un usuario si este existe.
        /// </summary>
        /// <param name="usuarioRol">UsuarioRol que se desea eliminar</param>
        /// <response code="200">Eliminado correctamente</response>
        /// <response code="500">Error al intentar eliminar</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpPost]
        [Authorize(Policy = "security/users/removerRol")]
        [Route("EliminarRol")]
        public IActionResult EliminarRol([FromBody]UsuarioRolViewModel usuarioRol)
        {
            var userRole = Mapper.Map<UsuarioRolViewModel, UsuarioRol>(usuarioRol);
            var result = UserService.RemoveRole(userRole);
            return Ok(result);
        }
    }
}
