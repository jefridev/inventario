﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Model.ViewModels.Usuario;
using System.Security.Claims;
using Microsoft.Extensions.Options;
using Model.OptionModels;
using System.IdentityModel.Tokens.Jwt;
using Service.Contracts.Security;
using System.Security.Principal;
using Model.Models;
using Data.Infrastructure.Response;
using System.Net;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Inventario.Areas.Security.Controllers
{
    /// <summary>
    /// Encargado de gestionar los inicios de sesión en la aplicación.
    /// </summary>
    [Area("security")]
    [Route("api/security/auth")]
    public class AuthController : Controller
    {
        private readonly IUserService UserService;
        
        /// <summary>
        ///  Constructor con inyección de dependencias.
        /// </summary>
        /// <param name="userService"></param>
        public AuthController(IUserService userService)
        {
            UserService = userService;
        }

        /// <summary>
        /// Proporciona la información correspondiente.
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Get([FromBody]LoginViewModel usuario)
        {
            var result = await UserService.Login(usuario); 
            return Ok(result);
        }


        /// <summary>
        /// Cerrar sesión del token especificado.
        /// </summary>
        /// <param name="token">Token de la sesion actual</param>
        /// <returns></returns>
        [HttpGet]
        [Route("Logout")]
        public IActionResult Logout([FromQuery]string token)
        {
            var result = UserService.CloseToken(token);
            return Ok(result);
        }

        /// <summary>
        /// Recuperar una contraseña olvidada
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ForgotPassword")]
        public IActionResult ForgotPassword([FromBody]LoginViewModel user)
        {
            var result = UserService.ForgotPassword(user.Username);
            return Ok(result);
        }
    }
}
