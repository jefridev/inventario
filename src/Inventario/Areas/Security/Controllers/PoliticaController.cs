﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service.Contracts.Security;
using Microsoft.AspNetCore.Authorization;
using Model.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Inventario.Areas.Security.Controllers
{
    /// <summary>
    ///  Utilizado para la gestión funciones realacionadas a los usuarios.
    /// </summary>
    [Area("security")]
    [Route("api/security/politica")]
    public class PoliticaController : Controller
    {
        private readonly ISecurityPolicyService SecurityPolicyService;

        /// <summary>
        /// Constructor que maneja inyección de dependencias.
        /// </summary>
        /// <param name="securityPolicyService"></param>
        public PoliticaController(ISecurityPolicyService securityPolicyService)
        {
            SecurityPolicyService = securityPolicyService;
        }


        /// <summary>
        /// Retorna la política actual que contiene el sistema.
        /// </summary>
        /// <response code="200">Se ha actualizado correctamente</response>
        /// <response code="304">No se ha modificado el registro</response>
        /// <response code="500">Error interno al intentar actualizar</response>
        /// <returns>Result con política del sistema</returns>
        [HttpGet]
        [Authorize(Policy = "security/policies/obtenerPolitica")]
        [Route("ObtenerPolitica")]
        public IActionResult ObtenerPolitica()
        {
            var result = SecurityPolicyService.GetPolicy();
            return Ok(result);
        }

        /// <summary>
        /// Actualiza la información que contiene una política.
        /// </summary>
        /// <response code="200">Se ha actualizado correctamente</response>
        /// <response code="304">No se ha modificado el registro</response>
        /// <response code="500">Error interno al intentar actualizar</response>
        /// <param name="politica">Política a ser actualizada</param>
        /// <returns>Result con el estatus que corresponde al proceso.</returns>
        [HttpPost]
        [Authorize(Policy = "security/policies/update")]
        [Route("ActualizarPolitica")]
        public IActionResult ActualizarPolitica([FromBody]PoliticaSeguridad politica)
        {
            var result = SecurityPolicyService.UpdatePolicy(politica);
            return Ok(result);
        }
    }
}
