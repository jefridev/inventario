﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Service.Contracts.Security;
using Model.Models;
using Model.ViewModels.Objectos;
using Data.Status;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Inventario.Areas.Security.Controllers
{
    /// <summary>
    ///  Utilizado para la gestión funciones realacionadas a los objetos.
    /// </summary>
    [Area("security")]
    [Route("api/security/objeto")]
    public class ObjetoController : Controller
    {
        private readonly IMapper Mapper;
        private readonly IObjectService ObjectService;

        /// <summary>
        /// Constructor que utiliza el inyector de dependencias.
        /// </summary>
        /// <param name="objectService"></param>
        /// <param name="mapper"></param>
        public ObjetoController(IObjectService objectService,
                             IMapper mapper)
        {
            ObjectService = objectService;
            Mapper = mapper;
        }


        /// <summary>
        /// Crea un nuevo objeto en el sistema
        /// </summary>
        /// <param name="objeto">Objeto que se desea registrar</param>
        /// <response code="400">Parametros enviados incorrectos</response>
        /// <returns>Result con estatus y descripción.</returns>
        [HttpPost]
        [Authorize(Policy = "security/objects/create")]
        [Route("CrearObjeto")]
        public IActionResult CrearObjeto([FromBody]ObjetoViewModel objeto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);


            Objeto objectConverted = Mapper.Map<ObjetoViewModel, Objeto>(objeto);
            objectConverted.Estatus = RecordStatus.Activo;

            var result = ObjectService.CreateObject(objectConverted);
            return Ok(result);
        }

        /// <summary>
        ///  Permite la actualización de la información del objeto.
        /// </summary>
        /// <param name="objeto">Objeto que se desea actualizar</param>
        /// <response code="200">Se ha actualizado la entrada correctamente</response>
        /// <response code="304">No se ha modificado, debido a algún evento</response>
        /// <response code="500">Error interno al tratar de realizar la actualización</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpPost]
        [Authorize(Policy = "security/objects/update")]
        [Route("ActualizarObjeto")]
        public IActionResult ActualizarObjeto([FromBody]ObjetoViewModel objeto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Objeto objectConverted = Mapper.Map<ObjetoViewModel, Objeto>(objeto);
            var result = ObjectService.UpdateObject(objectConverted);
            return Ok(result);
        }

        /// <summary>
        ///  Eliminar un objeto del sistema.
        /// </summary>
        /// <param name="objeto">objeto que se desea eliminar</param>
        /// <response code="200">Eliminado correctamente</response>
        /// <response code="500">Error al eliminar el objeto</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpPost]
        //[Authorize(Policy = "security/objects/delete")]
        [Route("EliminarObjeto")]
        public IActionResult EliminarObjeto([FromBody]ObjetoViewModel objeto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Objeto objectConverted = Mapper.Map<ObjetoViewModel, Objeto>(objeto);
            var result = ObjectService.DeleteObject(objectConverted);
            return Ok(result);
        }

        /// <summary>
        ///  Busca los objetos registrados en el sistema.
        /// </summary>
        /// <param name="pagina">Página que se desea buscar</param>
        /// <param name="limite">Limite de páginas requeridas</param>
        /// <param name="busqueda">Consulta a realizar</param>
        /// <response code="200">Operación ejecuta y contiene resultados</response>
        /// <response code="404">No se encontraron datos</response>
        /// <response code="500">Error interno</response>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Policy = "security/objects/consultar")]
        [Route("BuscarObjetos")]
        public IActionResult BuscarObjetos([FromQuery]int pagina, [FromQuery]int limite, [FromQuery] string busqueda)
        {
            var result = ObjectService.FindObjects(pagina, limite, busqueda);
            return Ok(result);
        }

        /// <summary>
        ///  Busca los objetos registrados en el sistema.
        /// </summary>
        /// <param name="pagina">Página que se desea buscar</param>
        /// <param name="limite">Limite de páginas requeridas</param>
        /// <param name="busqueda">Consulta a realizar</param>
        /// <response code="200">Operación ejecuta y contiene resultados</response>
        /// <response code="404">No se encontraron datos</response>
        /// <response code="500">Error interno</response>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Policy = "security/objects/consultar")]
        [Route("BuscarObjetosActivos")]
        public IActionResult BuscarObjetosActivos([FromQuery]int pagina, [FromQuery]int limite, [FromQuery] string busqueda)
        {
            var result = ObjectService.FindActiveObjects(pagina, limite, busqueda);
            return Ok(result);
        }

        /// <summary>
        ///  Busca los objetos registrados en el sistema.
        /// </summary>
        /// <response code="200">Operación ejecuta y contiene resultados</response>
        /// <response code="404">No se encontraron datos</response>
        /// <response code="500">Error interno</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpGet]
        [Authorize(Policy = "security/objects/consultar")]
        [Route("ObtenerObjetosActivos")]
        public IActionResult ObtenerObjetosActivos()
        {
            var result = ObjectService.GetAllActiveObjects();
            return Ok(result);
        }

        /// <summary>
        ///  Busca el objeto especificado por ID.
        /// </summary>
        /// <response code="200">Operación ejecuta y contiene el objeto</response>
        /// <response code="404">No se encontraro el objeto</response>
        /// <response code="500">Error interno</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpGet]
        [Authorize(Policy = "security/objects/consultarPorID")]
        [Route("ObtenerObjetoPorId")]
        public IActionResult ObtenerObjetoPorId([FromQuery]int idObjeto)
        {
            var result = ObjectService.GetObjectById(idObjeto);
            return Ok(result);
        }


        /// <summary>
        /// Buscar objetos asignados a un rol
        /// </summary>
        /// <param name="idrol">Identificador del rol</param>
        /// <response code="200">Encontró información y todo es devuelto</response>
        /// <response code="404">No encontró información</response>
        /// <response code="500">Error interno</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpGet]
        [Authorize(Policy = "security/objects/consultarAsignadosRol")]
        [Route("ObtenerObjetosAsignadosRol")]
        public IActionResult ObtenerObjetosAsignadosRol([FromQuery]int idrol)
        {
            var result = ObjectService.GetAllAObjectsAssignedToRole(idrol);
            return Ok(result);
        }

        /// <summary>
        /// Buscar objetos no asignados a un rol
        /// </summary>
        /// <param name="idrol">Identificador del rol</param>
        /// <response code="200">Encontró información y todo es devuelto</response>
        /// <response code="404">No encontró información</response>
        /// <response code="500">Error interno</response>
        /// <returns>Result con estatus y descripción</returns>
        [HttpGet]
        [Authorize(Policy = "security/objects/consultarNoAsignadosARol")]
        [Route("ObtenerObjetosNoAsignadosPorRol")]
        public IActionResult ObtenerObjetosNoAsignadosRol([FromQuery]int idrol)
        {
            var result = ObjectService.GetAllObjectsNotAssignedToRole(idrol);
            return Ok(result);
        }



        /// <summary>
        /// Obtener todos los objetos asignados a un usuario en base a los roles que este tiene.
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Policy = "security/objects/consultarObjetosAsignadosUsuario",ActiveAuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("ObtenerObjetosAsignadosUsuario")]
        public IActionResult ObtenerObjetosAsignadosUsuario([FromQuery]int idUsuario)
        {
            var result = ObjectService.GetObjectsAssignedToUser(idUsuario);
            return Ok(result);
        }


        /// <summary>
        /// Retorna el listado  de tipos de objetos utilizados al momento de registrar uno nuevo.
        /// </summary>
        /// <returns>Retorna la lista de objetos</returns>
        [HttpGet]
        [Authorize(Policy = "security/objects/consultarTiposObjetos")]
        [Route("ObtenerTipoObjetos")]
        public IActionResult ObtenerTipoObjetos()
        {
            var result = ObjectService.GetObjectTypes();
            return Ok(result);
        }
    }
}
