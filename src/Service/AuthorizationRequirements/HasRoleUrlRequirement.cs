﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Service.AuthorizationRequirements
{
    public class HasRoleUrlRequirement : IAuthorizationRequirement
    {
        public HasRoleUrlRequirement(string url)
        {
            Url = url;
        }

        public string Url { get; set; }
    }
}
