﻿using Microsoft.AspNetCore.Authorization;
using Service.Contracts.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.AuthorizationRequirements
{
    public class HasActiveTokenHandler : AuthorizationHandler<HasActiveTokenRequirement>
    {
        private readonly IUserService UserService;

        public HasActiveTokenHandler(IUserService userService)
        {
            UserService = userService;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, HasActiveTokenRequirement requirement)
        {
            //Validar información básica para obtener la información.
            if (context.User == null)
                return Task.CompletedTask;

            if (!context.User.HasClaim(x => x.Type == "UserID"))
                return Task.CompletedTask;

            int userID = context.User.Claims.Where(x => x.Type == "UserID")
                                     .Select(x => Convert.ToInt32(x.Value))
                                     .SingleOrDefault();


            var result = UserService.HasUserActiveToken(userID);
            if (result.Value)
                context.Succeed(requirement);

            return Task.CompletedTask;
        }
    }
}
