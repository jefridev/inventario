﻿using Microsoft.AspNetCore.Authorization;
using Service.Contracts.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Service.AuthorizationRequirements
{
    public class HasRoleUrlHandler : AuthorizationHandler<HasRoleUrlRequirement>
    {
        private readonly IObjectService ObjectService;
        private readonly ILogService LogService;

        public HasRoleUrlHandler(IObjectService objectService,
                                 ILogService logService)
        {
            ObjectService = objectService;
            LogService = logService;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, HasRoleUrlRequirement requirement)
        {
            //Validar información básica para obtener la información.
            if (context.User == null)
                return Task.CompletedTask;
     
            if (!context.User.HasClaim(x => x.Type == "UserID"))
                return Task.CompletedTask;

            int userID = context.User.Claims.Where(x => x.Type == "UserID")
                                     .Select(x => Convert.ToInt32(x.Value))
                                     .SingleOrDefault();


     
            bool result = ObjectService.HasUserUrlObject(userID, requirement.Url);
            var resultLog = LogService.CreateLog(userID, requirement.Url);
            if (result)
                context.Succeed(requirement);

            return Task.CompletedTask;
        }
    }
}
