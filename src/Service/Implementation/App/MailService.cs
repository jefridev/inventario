﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Service.Contracts.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Infrastructure.Response;
using MimeKit;
using System.Net;
using Model.OptionModels;

namespace Service.Implementation.App
{
    public class MailService : IMailService
    {
        public MailCredentialsOptions CredentialOptions { get; set; }

        public MailService()
        {
        }

        public Result Send(string[] emails, string subject, string message, bool isHtml)
        {
            Result result;
            try
            {
                if (CredentialOptions == null)
                    throw new Exception("Crendentials options must be set to use this method.");
                       
                if (emails == null)
                    throw new ArgumentNullException(nameof(emails));

                MimeMessage mailMessage = new MimeMessage();
                mailMessage.From.Add(new MailboxAddress("", CredentialOptions.Email));
                foreach (var email in emails)
                    mailMessage.To.Add(new MailboxAddress("", email));

                mailMessage.Subject = subject;

                if (isHtml)
                {
                    var bodyBuilder = new BodyBuilder();
                    bodyBuilder.HtmlBody = message;
                    mailMessage.Body = bodyBuilder.ToMessageBody();
                }
                else
                {
                    mailMessage.Body = new TextPart("plain") { Text = message };
                }

                using (var client = new SmtpClient())
                {
                    client.Connect(CredentialOptions.SmtpServer, CredentialOptions.Port,CredentialOptions.Ssl);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(CredentialOptions.Email, CredentialOptions.Password);
                    client.Send(mailMessage);
                    client.Disconnect(true);
                }

                result = new Result(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }
    }
}
