﻿using Data.Infrastructure.Interfaces;
using Service.Contracts.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Infrastructure.Response;
using Model.Models;
using System.Net;
using Model.ViewModels.Objectos;
using AutoMapper;
using Data.Status;
using Data.Types;
using Microsoft.Extensions.Localization;

namespace Service.Implementation.Security
{
    public class ObjectService : IObjectService
    {
        private readonly IUnitOfWork UnitOfWork;
        private readonly IStringLocalizer<ObjectService> Localizer;

        public ObjectService(IUnitOfWork unitOfWork,
                             IStringLocalizer<ObjectService> localizer)
        {
            UnitOfWork = unitOfWork;
            Localizer = localizer;
        }

        public Result<IEnumerable<Objeto>> GetAllScreenTypeObjects()
        {
            Result<IEnumerable<Objeto>> result;
            try
            {
                var objetos = UnitOfWork.Objects.GetAllScreenTypeObjects();
                result = new Result<IEnumerable<Objeto>>(HttpStatusCode.OK) { Value = objetos };
            }
            catch (Exception ex)
            {
                result = new Result<IEnumerable<Objeto>>(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public bool HasUserUrlObject(int userID, string url)
        {
            var objeto = UnitOfWork.Objects.GetObjectByUserRoleUrl(userID, url);
            if (objeto == null)
                return false;

            return true;
        }

        public Result CreateObject(Objeto objectParam)
        {
            Result result;
            try
            {
                //Checks if Objetoe is null returns error.
                if (objectParam == null)
                    throw new ArgumentNullException(nameof(objectParam));

                UnitOfWork.Objects.Add(objectParam);
                UnitOfWork.Complete();

                result = new Result(HttpStatusCode.OK, Localizer["ObjetoCreado"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result UpdateObject(Objeto objectParam)
        {
            Result result;
            try
            {
                UnitOfWork.Objects.Update(objectParam);
                int returnValue = UnitOfWork.Complete();

                if (returnValue > 0)
                    result = new Result(HttpStatusCode.OK, Localizer["ObjetoActualizado"]);
                else
                    result = new Result(HttpStatusCode.NotModified, Localizer["NoModificado"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result DeleteObject(Objeto objectParam)
        {
            Result result;
            try
            {
                UnitOfWork.Objects.Remove(objectParam);
                UnitOfWork.Complete();

                result = new Result(HttpStatusCode.OK, Localizer["ObjetoEliminado"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError,Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result UpdateStatus(Objeto objectParam, string recordStatus)
        {
            Result result;
            try
            {
                objectParam.Estatus = recordStatus;
                UnitOfWork.Objects.Update(objectParam);
                var resultValue = UnitOfWork.Complete();

                if (resultValue > 0)
                    result = new Result(HttpStatusCode.OK, Localizer["ObjetoActualizado"]);
                else
                    result = new Result(HttpStatusCode.NotModified, Localizer["NoModificado"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result<List<ObjetoViewModel>> GetAllActiveObjects()
        {
            Result<List<ObjetoViewModel>> result;
            try
            {
                var value = UnitOfWork.Objects.FindAll(x => x.Estatus == RecordStatus.Activo).ToList();
                if (value.Count == 0)
                    result = new Result<List<ObjetoViewModel>>(HttpStatusCode.NotFound);
                else
                {
                    var convertedValue = value.Select(x => Mapper.Map<Objeto, ObjetoViewModel>(x)).ToList();
                    result = new Result<List<ObjetoViewModel>>(HttpStatusCode.OK) { Value = convertedValue };
                }
            }
            catch (Exception ex)
            {
                result = new Result<List<ObjetoViewModel>>(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result<List<ObjetoViewModel>> FindObjects(int page, int limit, string searchTerm)
        {
            Result<List<ObjetoViewModel>> result;
            try
            {
                var value = UnitOfWork.Objects.FindPagedObjects(page, limit, searchTerm);
                if (value.Count() == 0)
                    result = new Result<List<ObjetoViewModel>>(HttpStatusCode.NotFound);
                else
                {
                    var convertedValue = value.Select(x => Mapper.Map<Objeto, ObjetoViewModel>(x)).ToList();
                    result = new Result<List<ObjetoViewModel>>(HttpStatusCode.OK) { Value = convertedValue };
                }
            }
            catch (Exception ex)
            {
                result = new Result<List<ObjetoViewModel>>(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result<List<ObjetoViewModel>> FindActiveObjects(int page, int limit, string searchTerm)
        {
            Result<List<ObjetoViewModel>> result;
            try
            {
                var value = UnitOfWork.Objects.FindPagedActiveObjects(page, limit, searchTerm);
                if (value.Count() == 0)
                    result = new Result<List<ObjetoViewModel>>(HttpStatusCode.NotFound);
                else
                {
                    var convertedValue = value.Select(x => Mapper.Map<Objeto, ObjetoViewModel>(x)).ToList();
                    result = new Result<List<ObjetoViewModel>>(HttpStatusCode.OK) { Value = convertedValue };
                }
            }
            catch (Exception ex)
            {
                result = new Result<List<ObjetoViewModel>>(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result<ObjetoViewModel> GetObjectById(int objectID)
        {
            Result<ObjetoViewModel> result;
            try
            {
                var objectParam = UnitOfWork.Objects.GetById(objectID);
                if (objectParam == null)
                    result = new Result<ObjetoViewModel>(HttpStatusCode.NotFound);
                else
                {
                    var objectConverted = Mapper.Map<Objeto, ObjetoViewModel>(objectParam);
                    result = new Result<ObjetoViewModel>(HttpStatusCode.OK) { Value = objectConverted };
                }
            }
            catch (Exception ex)
            {
                result = new Result<ObjetoViewModel>(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result<List<string>> GetButtonByScreenAndUserID(int userID, string screenName)
        {
            Result<List<string>> result;
            try
            {
                var value = UnitOfWork.Objects.GetButtonByScreenAndUserID(userID, screenName);
                if (value == null)
                    result = new Result<List<string>>(HttpStatusCode.NotFound);
                else
                    result = new Result<List<string>>(HttpStatusCode.OK) { Value = value.ToList() };
            }
            catch (Exception ex)
            {
                result = new Result<List<string>>(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result<List<string>> GetMenuOptionsByUser(int userID)
        {
            Result<List<string>> result;
            try
            {
                var value = UnitOfWork.Objects.GetMenuOptionsByUser(userID);
                if (value == null)
                    result = new Result<List<string>>(HttpStatusCode.NotFound);
                else
                    result = new Result<List<string>>(HttpStatusCode.OK) { Value = value.ToList() };
            }
            catch (Exception ex)
            {
                result = new Result<List<string>>(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result<List<ObjetoViewModel>> GetAllAObjectsAssignedToRole(int roleID)
        {
            Result<List<ObjetoViewModel>> result;
            try
            {
                var value = UnitOfWork.ObjectsRole.GetAllObjectsAssignedToRole(roleID);
                if (value == null)
                    result = new Result<List<ObjetoViewModel>>(HttpStatusCode.NotFound);
                else
                {
                    var objectsConveted = value.Select(x => Mapper.Map<Objeto, ObjetoViewModel>(x.IdobjetoNavigation)).ToList();
                    result = new Result<List<ObjetoViewModel>>(HttpStatusCode.OK) { Value = objectsConveted };
                }
            }
            catch (Exception ex)
            {
                result = new Result<List<ObjetoViewModel>>(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result<List<ObjetoViewModel>> GetAllObjectsNotAssignedToRole(int roleID)
        {
            Result<List<ObjetoViewModel>> result;

            var activeObjectsResult = this.GetAllActiveObjects();
            var assignedObjectsResult = this.GetAllAObjectsAssignedToRole(roleID);

            if (activeObjectsResult.Status == HttpStatusCode.NotFound)
                return new Result<List<ObjetoViewModel>>(HttpStatusCode.NotFound) { Value = assignedObjectsResult.Value };

            if (activeObjectsResult.Status == HttpStatusCode.OK &&
               assignedObjectsResult.Status == HttpStatusCode.OK)
            {
                var filteredObjects = activeObjectsResult.Value.Where(x => !assignedObjectsResult.Value.Any(s => s.IDObjeto == x.IDObjeto)).ToList();
                if (filteredObjects.Count == 0)
                    result = new Result<List<ObjetoViewModel>>(HttpStatusCode.NotFound);
                else
                    result = new Result<List<ObjetoViewModel>>(HttpStatusCode.OK) { Value = filteredObjects };
            }
            else
            {
                result = new Result<List<ObjetoViewModel>>(HttpStatusCode.InternalServerError);
            }
            return result;
        }

        public Result<List<string>> GetObjectTypes()
        {
            var objectTypes = new List<string>(new string[] { ObjectType.Button, ObjectType.Screen, ObjectType.Report });
            return new Result<List<string>>(HttpStatusCode.OK) { Value = objectTypes };
        }

        public Result<List<ObjetoViewModel>> GetObjectsAssignedToUser(int userID)
        {
            Result<List<ObjetoViewModel>> result;
            try
            {
                var value = UnitOfWork.Objects.GetObjectsAssignedToUser(userID);
                if (value == null)
                    result = new Result<List<ObjetoViewModel>>(HttpStatusCode.NotFound);
                else
                {
                    var objectsConveted = value.Select(x => Mapper.Map<Objeto, ObjetoViewModel>(x)).ToList();
                    result = new Result<List<ObjetoViewModel>>(HttpStatusCode.OK) { Value = objectsConveted };
                }
            }
            catch (Exception ex)
            {
                result = new Result<List<ObjetoViewModel>>(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }
    }
}
