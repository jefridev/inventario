﻿using Data.Infrastructure.Implementation;
using Data.Infrastructure.Interfaces;
using Data.Infrastructure.Response;
using Data.Repositories.Contracts.Security;
using Data.Status;
using Model.Models;
using Service.Contracts.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Model.ViewModels.Usuario;
using AutoMapper;
using Microsoft.Extensions.Localization;
using Service.Contracts.App;
using Model.OptionModels;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Security.Claims;
using System.Security.Principal;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.WebUtilities;

namespace Service.Implementation.Security
{
    public class UserService : IUserService
    {
        private readonly IMapper Mapper;
        private readonly IUnitOfWork UnitOfWork;
        private readonly ISecurityPolicyService SecurityPolicyService;
        private readonly IRoleService RoleService;
        private readonly IEncryptionService EncryptionService;
        private readonly IHostingEnvironment Environment;
        private readonly IMailService MailService;
        private readonly IStringLocalizer<UserService> Localizer;
        private readonly MailCredentialsOptions MailOptions;
        private readonly JwtIssuerOptions JwtOptions;
        private readonly SiteOptions SiteOptions;

        public UserService(IOptions<MailCredentialsOptions> mailOptions,
                           IOptions<JwtIssuerOptions> jwtOptions,
                           IOptions<SiteOptions> siteOptions,
                           IUnitOfWork unitOfWork,
                           IMapper mapper,
                           ISecurityPolicyService securityPolicyService,
                           IRoleService roleService,
                           IEncryptionService encryptionService,
                           IHostingEnvironment environment,
                           IMailService mailService,
                           IStringLocalizer<UserService> localizer)
        {
            MailOptions = mailOptions.Value;
            JwtOptions = jwtOptions.Value;
            SiteOptions = siteOptions.Value;
            UnitOfWork = unitOfWork;
            SecurityPolicyService = securityPolicyService;
            RoleService = roleService;
            Mapper = mapper;
            EncryptionService = encryptionService;
            Environment = environment;
            MailService = mailService;
            Localizer = localizer;
        }


        public async Task<Result<JwtUserViewModel>> Login(LoginViewModel usuario)
        {
            Result<JwtUserViewModel> result;
            try
            {
                var identity = await this.GetClaimsIdentity(usuario);
                if (identity != null)
                {
                    //Information claims
                    var claims = new[]
                    {
                            new Claim(JwtRegisteredClaimNames.Sub, usuario.Username),
                            new Claim(JwtRegisteredClaimNames.Jti, await JwtOptions.JtiGenerator()),
                            new Claim(JwtRegisteredClaimNames.Iat,
                                      ToUnixEpochDate(JwtOptions.IssuedAt).ToString(),
                                      ClaimValueTypes.Integer64),
                            identity.FindFirst("Name"),
                            identity.FindFirst("UserID")
                     };

                    //Time valid for session.
                    var policy = SecurityPolicyService.GetPolicy();
                    if (policy.Status != HttpStatusCode.OK)
                        throw new Exception(policy.Message);

                    var user = this.GetUserByUsername(usuario.Username);
                    if (user.Status != HttpStatusCode.OK)
                        throw new Exception(user.Message);

                    //Check if user is already blocked.
                    var userValue = user.Value;
                    if (userValue.FechaBloqueo.HasValue)
                    {
                        if (DateTime.UtcNow >= userValue.FechaBloqueo.Value)
                        {
                            userValue.FechaBloqueo = null;
                            userValue.Intentos = 0;
                            this.UpdateUser(userValue);
                        }
                        else
                        {
                            return new Result<JwtUserViewModel>(HttpStatusCode.BadRequest, Localizer["UsuarioBloqueado"]);
                        };
                    }

                    if (policy.Value.NumeroIntentos.HasValue)
                    {
                        //Check if user is blocked.
                        if (userValue.Intentos >= policy.Value.NumeroIntentos)
                        {
                            userValue.FechaBloqueo = DateTime.UtcNow.AddMinutes(policy.Value.TiempoBloqueo.Value);
                            this.UpdateUser(userValue);

                            return new Result<JwtUserViewModel>(HttpStatusCode.BadRequest, Localizer["UsuarioBloqueado"]);
                        }
                    }

                    //Reset values for userValue.
                    userValue.FechaBloqueo = null;
                    userValue.Intentos = 0;
                    this.UpdateUser(userValue);

                    //Set time from policy.
                    int expirationTime = 0;
                    switch (usuario.Canal)
                    {
                        case "Web":
                            expirationTime = policy.Value.TiempoTokenWeb;
                            break;
                        case "Client":
                            expirationTime = policy.Value.TiempoTokenCliente;
                            break;
                        default:
                            expirationTime = 15;
                            break;
                    }

                    DateTime notBefore = DateTime.UtcNow;
                    DateTime expiresIn = DateTime.UtcNow.AddMinutes(expirationTime);


                    // Create the JWT security token and encode it.
                    var jwt = new JwtSecurityToken(
                        issuer: JwtOptions.Issuer,
                        audience: JwtOptions.Audience,
                        claims: claims,
                        notBefore: notBefore,
                        expires: expiresIn,
                        signingCredentials: JwtOptions.SigningCredentials);

                    var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
                    var roleResult = RoleService.GetAllRolesAssignedToUser(userValue.Idusuario);
                    string mainRoute = "/";
                    if (roleResult.Status == HttpStatusCode.OK)
                    {
                        var mainRole = roleResult.Value?.SingleOrDefault();
                        if (mainRole != null)
                            mainRoute = mainRole.RutaPrincipal;
                    }

                    var responseValue = new JwtUserViewModel()
                    {
                        Id = int.Parse(identity.FindFirst("UserID").Value),
                        Name = identity.FindFirst("Name").Value,
                        Username = identity.FindFirst("Username").Value,
                        Email = identity.FindFirst("Correo").Value,
                        AccessToken = encodedJwt,
                        MainRoute = mainRoute
                    };


                    var usuarioToken = new UsuarioToken()
                    {
                        Idusuario = responseValue.Id,
                        FechaInicio = jwt.ValidFrom,
                        FechaExpiracion = jwt.ValidTo,
                        Token = responseValue.AccessToken,
                        Canal = usuario.Canal
                    };

                    this.CloseUserTokens(responseValue.Id, usuario.Canal);
                    this.CreateTokenToUser(usuarioToken);

                    result = new Result<JwtUserViewModel>(HttpStatusCode.OK) { Value = responseValue };
                }
                else
                {
                    result = new Result<JwtUserViewModel>(HttpStatusCode.BadRequest, Localizer["CredencialesInvalidas"]);
                }
            }
            catch (Exception ex)
            {
                result = new Result<JwtUserViewModel>(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result IsEmailAvailable(Usuario usuario)
        {
            Result result;
            try
            {
                var userResult = UnitOfWork.Users.Find(x => x.Correo == usuario.Correo);
                if (userResult != null)
                    result = new Result(HttpStatusCode.Found);
                else
                    result = new Result(HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result IsUsernameAvailable(Usuario usuario)
        {
            Result result;
            try
            {
                var userResult = UnitOfWork.Users.Find(x => x.Username == usuario.Username);
                if (userResult != null)
                    result = new Result(HttpStatusCode.Found);
                else
                    result = new Result(HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result CreateUser(Usuario usuario)
        {
            Result result;
            try
            {
                //Checks if user is null returns error.
                if (usuario == null)
                    throw new ArgumentNullException(nameof(usuario));

                usuario.Salt = EncryptionService.GenerateSalt();
                string randomPassword = this.GenerateRandomPassword();
                usuario.PasswordHash = EncryptionService.EncryptPassword(randomPassword, usuario.Salt);

                //Send email with generated password to user.
                string subjectMessage = Localizer["SubjectContrasenaGenerada"];
                string messageBody = string.Format(Localizer["ContrasenaGeneradaBody"], usuario.Nombre, randomPassword);

                string alertTemplatePath = $"{Environment.WebRootPath}/email_templates/alert.html";
                string templateString = File.ReadAllText(alertTemplatePath);
                templateString = templateString.Replace("{0}", subjectMessage);
                templateString = templateString.Replace("{1}", messageBody);

                MailService.CredentialOptions = MailOptions;
                var resultMail = MailService.Send(new string[] { usuario.Correo }, subjectMessage, templateString, true);
                if (resultMail.Status != HttpStatusCode.OK)
                    return resultMail;

                usuario.Intentos = 0;
                UnitOfWork.Users.Add(usuario);
                UnitOfWork.Complete();

                result = new Result(HttpStatusCode.OK, Localizer["UsuarioCreado"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result UpdateUser(Usuario usuario)
        {
            Result result;
            try
            {
                var user = UnitOfWork.Users.GetById(usuario.Idusuario);
                user.Nombre = usuario.Nombre;
                user.Username = usuario.Username;
                user.Correo = usuario.Correo;
                user.Telefono = usuario.Telefono;
                user.Idioma = usuario.Idioma;
                user.Intentos = usuario.Intentos;
                user.FechaBloqueo = usuario.FechaBloqueo;
                user.FechaCambioClave = usuario.FechaCambioClave;

                if (!string.IsNullOrWhiteSpace(usuario.PasswordHash))
                {
                    user.PasswordHash = usuario.PasswordHash;
                    user.Salt = usuario.Salt;
                    user.FechaCambioClave = DateTime.UtcNow;
                }

                UnitOfWork.Users.Update(user);
                int returnValue = UnitOfWork.Complete();

                if (returnValue > 0)
                    result = new Result(HttpStatusCode.OK, Localizer["UsuarioActualizado"]);
                else
                    result = new Result(HttpStatusCode.NotModified, Localizer["NoModificado"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result DeleteUser(Usuario usuario)
        {
            Result result;
            try
            {
                var operationResult = this.UpdateStatus(usuario, RecordStatus.Eliminado);
                if (operationResult.Status == HttpStatusCode.OK)
                    result = new Result(HttpStatusCode.OK, Localizer["UsuarioEliminado"]);
                else
                    throw new Exception(operationResult.Message);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result UpdateStatus(Usuario usuario, string recordStatus)
        {
            Result result;
            try
            {
                var user = UnitOfWork.Users.GetById(usuario.Idusuario);
                user.Estatus = recordStatus;

                UnitOfWork.Users.Update(user);
                var resultValue = UnitOfWork.Complete();

                if (resultValue > 0)
                    result = new Result(HttpStatusCode.OK, Localizer["UsuarioActualizado"]);
                else
                    result = new Result(HttpStatusCode.NotModified, Localizer["NoModificado"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result AssignRole(UsuarioRol usuarioRol)
        {
            Result result;
            try
            {
                if (usuarioRol == null)
                    throw new ArgumentException(nameof(usuarioRol));

                usuarioRol.Estatus = RecordStatus.Activo;
                UnitOfWork.UserRoles.Add(usuarioRol);
                UnitOfWork.Complete();
                result = new Result(HttpStatusCode.OK, Localizer["RolAsignado"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result RemoveRole(UsuarioRol usuarioRol)
        {
            Result result;
            try
            {
                if (usuarioRol == null)
                    throw new ArgumentNullException(nameof(usuarioRol));

                UnitOfWork.UserRoles.Remove(usuarioRol);
                UnitOfWork.Complete();

                result = new Result(HttpStatusCode.OK, Localizer["RolRemovido"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result<Usuario> GetUserByUsername(string username)
        {
            Result<Usuario> result;
            try
            {
                var user = UnitOfWork.Users.Find(x => x.Username == username);
                if (user == null)
                    result = new Result<Usuario>(HttpStatusCode.NotFound, Localizer["NoEncontrado"]);
                else
                    result = new Result<Usuario>(HttpStatusCode.OK) { Value = user };
            }
            catch (Exception ex)
            {
                result = new Result<Usuario>(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result<List<UsuarioViewModel>> GetAllActiveUsers()
        {
            Result<List<UsuarioViewModel>> result;
            try
            {
                var value = UnitOfWork.Users.FindAll(x => x.Estatus == RecordStatus.Activo).ToList();
                if (value.Count == 0)
                    result = new Result<List<UsuarioViewModel>>(HttpStatusCode.NotFound);
                else
                {
                    var convertedValue = value.Select(x => Mapper.Map<Usuario, UsuarioViewModel>(x)).ToList();
                    result = new Result<List<UsuarioViewModel>>(HttpStatusCode.OK, Localizer["MensajePrueba"]) { Value = convertedValue };
                }
            }
            catch (Exception ex)
            {
                result = new Result<List<UsuarioViewModel>>(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result<List<UsuarioViewModel>> FindUsers(int page, int limit, string searchTerm)
        {
            Result<List<UsuarioViewModel>> result;
            try
            {
                var value = UnitOfWork.Users.FindPagedUsers(page, limit, searchTerm);
                if (value.Count() == 0)
                    result = new Result<List<UsuarioViewModel>>(HttpStatusCode.NotFound);
                else
                {
                    var convertedValue = value.Select(x => Mapper.Map<Usuario, UsuarioViewModel>(x)).ToList();
                    result = new Result<List<UsuarioViewModel>>(HttpStatusCode.OK) { Value = convertedValue };
                }
            }
            catch (Exception ex)
            {
                result = new Result<List<UsuarioViewModel>>(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result<UsuarioViewModel> GetUserById(int userID)
        {
            Result<UsuarioViewModel> result;
            try
            {
                var user = UnitOfWork.Users.GetById(userID);
                if (user == null)
                    result = new Result<UsuarioViewModel>(HttpStatusCode.NotFound, Localizer["NoEncontrado"]);
                else
                {
                    var userConverted = Mapper.Map<Usuario, UsuarioViewModel>(user);
                    result = new Result<UsuarioViewModel>(HttpStatusCode.OK) { Value = userConverted };
                }
            }
            catch (Exception ex)
            {
                result = new Result<UsuarioViewModel>(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result CreateTokenToUser(UsuarioToken userToken)
        {
            Result result;
            try
            {
                userToken.Estatus = RecordStatus.Activo;
                UnitOfWork.UserTokens.Add(userToken);
                UnitOfWork.Complete();
                result = new Result(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result CloseUserTokens(int userID, string canal)
        {
            Result result;
            try
            {
                var openTokens = UnitOfWork.UserTokens.FindAll(x => x.Idusuario == userID && x.Canal == canal && x.Estatus == RecordStatus.Activo).ToList();
                if (openTokens == null)
                    result = new Result(HttpStatusCode.NotFound);
                if (openTokens.Count == 0)
                    result = new Result(HttpStatusCode.NotFound);

                foreach (var openToken in openTokens)
                {
                    openToken.FechaSalida = DateTime.UtcNow;
                    openToken.Estatus = RecordStatus.Inactivo;
                }
                UnitOfWork.UserTokens.UpdateRange(openTokens);
                UnitOfWork.Complete();

                result = new Result(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result<bool> HasUserActiveToken(int userID)
        {
            Result<bool> result;
            try
            {
                var value = UnitOfWork.UserTokens.Find(x => x.Idusuario == userID && x.Estatus == RecordStatus.Activo);
                if (value == null)
                    result = new Result<bool>(HttpStatusCode.OK) { Value = false };
                else
                    result = new Result<bool>(HttpStatusCode.OK) { Value = true };

            }
            catch (Exception ex)
            {
                result = new Result<bool>(HttpStatusCode.InternalServerError, ex.Message) { Value = false };
            }
            return result;
        }

        public Result CloseToken(string token)
        {
            Result result;
            try
            {
                var openToken = UnitOfWork.UserTokens.FindAll(x => x.Estatus == RecordStatus.Activo && x.Token.Contains(token)).SingleOrDefault();
                if (openToken == null)
                    result = new Result(HttpStatusCode.NotFound);

                openToken.FechaSalida = DateTime.UtcNow;
                openToken.Estatus = RecordStatus.Inactivo;
                UnitOfWork.UserTokens.Update(openToken);
                UnitOfWork.Complete();

                result = new Result(HttpStatusCode.OK);

            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public string GenerateRandomPassword()
        {
            return Guid.NewGuid().ToString().Substring(0, 8);
        }

        public Result RegeneratePassword(Usuario usuario)
        {
            try
            {
                //Set values.
                usuario.Salt = EncryptionService.GenerateSalt();
                string randomPassword = this.GenerateRandomPassword();
                usuario.PasswordHash = EncryptionService.EncryptPassword(randomPassword, usuario.Salt);

                //Send email with generated password to user.
                string subjectMessage = Localizer["SubjectContrasenaGenerada"];
                string messageBody = string.Format(Localizer["ContrasenaGeneradaBody"], usuario.Nombre, randomPassword);

                string alertTemplatePath = $"{Environment.WebRootPath}/email_templates/alert.html";
                string templateString = File.ReadAllText(alertTemplatePath);
                templateString = templateString.Replace("{0}", subjectMessage);
                templateString = templateString.Replace("{1}", messageBody);

                MailService.CredentialOptions = MailOptions;
                var resultMail = MailService.Send(new string[] { usuario.Correo }, subjectMessage, templateString, true);
                if (resultMail.Status != HttpStatusCode.OK)
                    return resultMail;

                var user = UnitOfWork.Users.GetById(usuario.Idusuario);
                if (user != null)
                {
                    user.Salt = usuario.Salt;
                    user.PasswordHash = usuario.PasswordHash;
                    int returnValue = UnitOfWork.Complete();

                    if (returnValue > 0)
                        return new Result(HttpStatusCode.OK, Localizer["ContrasenaGenerada"]);
                    else
                        return new Result(HttpStatusCode.NotModified, Localizer["ContrasenaNoGenerada"]);
                }
                else
                    throw new Exception(Localizer["ErrorInterno"]);
            }
            catch (Exception ex)
            {
                return new Result(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        public Result ForgotPassword(string username)
        {
            Result result;
            try
            {
                //
                var resultUser = this.GetUserByUsername(username);
                if (resultUser.Status == HttpStatusCode.OK)
                {
                    //Send email with generated password to user.
                    string urlApp = $"{SiteOptions.AppUrl}login/changePassword/{resultUser.Value.Token}";
                    string subjectMessage = Localizer["SubjectRecuperarContrasena"];
                    string messageBody = string.Format(Localizer["RecuperarContrasenaBody"], username, urlApp, urlApp);

                    string alertTemplatePath = $"{Environment.WebRootPath}/email_templates/alert.html";
                    string templateString = File.ReadAllText(alertTemplatePath);
                    templateString = templateString.Replace("{0}", subjectMessage);
                    templateString = templateString.Replace("{1}", messageBody);

                    MailService.CredentialOptions = MailOptions;
                    var resultMail = MailService.Send(new string[] { resultUser.Value.Correo }, subjectMessage, messageBody, true);
                    result = new Result(HttpStatusCode.OK, Localizer["RecuperarContrasenaOk"]);
                }
                else result = new Result(HttpStatusCode.OK, Localizer["RecuperarContrasenaOk"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result ActivateAccount(string token)
        {
            Result result;
            try
            {
                token = WebUtility.UrlDecode(token);
                var usuario = UnitOfWork.Users.FindAll(x => x.Token == token && x.Estatus == RecordStatus.Pendiente).SingleOrDefault();
                if (usuario != null)
                {
                    usuario.Estatus = RecordStatus.Activo;
                    usuario.Token = EncryptionService.GenerateActivationToken();
                    var resultUpdate = this.UpdateUser(usuario);
                    if (resultUpdate.Status == HttpStatusCode.OK)
                        result = new Result(HttpStatusCode.OK, Localizer["UsuarioActivado"]);
                    else
                        result = new Result(HttpStatusCode.NotModified, Localizer["UsuarioNoActivado"]);
                }
                else
                {
                    result = new Result(HttpStatusCode.NotModified, Localizer["UsuarioNoActivado"]);
                }
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result UpdatePassword(string password, string token)
        {
            Result result;
            try
            {
                var usuario = UnitOfWork.Users.FindAll(x => x.Token == token).SingleOrDefault();
                if (usuario != null)
                {

                    usuario.Salt = EncryptionService.GenerateSalt();
                    usuario.PasswordHash = EncryptionService.EncryptPassword(password, usuario.Salt);
                    usuario.Token = EncryptionService.GenerateActivationToken();

                    var resultUpdate = this.UpdateUser(usuario);
                    if (resultUpdate.Status == HttpStatusCode.OK)
                        result = new Result(HttpStatusCode.OK, Localizer["ContrasenaActualizada"]);
                    else
                        result = new Result(HttpStatusCode.NotModified, Localizer["ContrasenaNoActualizada"]);
                }
                else
                {
                    result = new Result(HttpStatusCode.NotModified, Localizer["ContrasenaNoActualizada"]);
                }
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result UpdatePassword(int userID, string oldPassword, string newPassword)
        {
            Result result;
            try
            {
                var usuario = UnitOfWork.Users.GetById(userID);
                var encryptedOldPassword = EncryptionService.EncryptPassword(oldPassword, usuario.Salt);
                if (usuario.PasswordHash != encryptedOldPassword)
                    result = new Result(HttpStatusCode.NotModified, Localizer["ContrasenaNoActualizada"]);
                else
                {
                    usuario.Salt = EncryptionService.GenerateSalt();
                    usuario.PasswordHash = EncryptionService.EncryptPassword(newPassword, usuario.Salt);
                    usuario.Token = EncryptionService.GenerateActivationToken();

                    var resultUpdate = this.UpdateUser(usuario);
                    if (resultUpdate.Status == HttpStatusCode.OK)
                        result = new Result(HttpStatusCode.OK, Localizer["ContrasenaActualizada"]);
                    else
                        result = new Result(HttpStatusCode.NotModified, Localizer["ContrasenaNoActualizada"]);

                }
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        /// <summary>
        ///  Convierte fecha a UnixEpochDate.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private long ToUnixEpochDate(DateTime date)
                         => (long)Math.Round((date.ToUniversalTime() -
                                              new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero))
                                             .TotalSeconds);

        /// <summary>
        ///  Obtiene los claims generado para la sesión generada al usuario.
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        private Task<ClaimsIdentity> GetClaimsIdentity(LoginViewModel usuario)
        {
            var user = this.GetUserByUsername(usuario.Username).Value;
            if (user != null)
            {
                var passwordHash = EncryptionService.EncryptPassword(usuario.Contrasena, user.Salt);
                if (user.Username == usuario.Username &&
                    user.PasswordHash == passwordHash)
                {
                    return Task.FromResult(new ClaimsIdentity(
                             new GenericIdentity(passwordHash, "Token"),
                             new[]
                             {
                                    new Claim("Name",user.Nombre),
                                    new Claim("Username", user.Username),
                                    new Claim("Correo",user.Correo),
                                    new Claim("UserID",user.Idusuario.ToString())
                             }));
                }
                else
                {
                    user.Intentos += 1;
                    this.UpdateUser(user);
                }
            }

            // Credentials are invalid, or account doesn't exist
            return Task.FromResult<ClaimsIdentity>(null);
        }
    }
}
