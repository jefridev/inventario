﻿using Service.Contracts.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Infrastructure.Response;
using Model.Models;
using Model.ViewModels.Roles;
using Data.Infrastructure.Interfaces;
using AutoMapper;
using System.Net;
using Data.Status;
using Microsoft.Extensions.Localization;

namespace Service.Implementation.Security
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork UnitOfWork;
        private readonly IMapper Mapper;
        private readonly IStringLocalizer<RoleService> Localizer;


        public RoleService(IUnitOfWork unitOfWork,
                IMapper mapper,
                IStringLocalizer<RoleService> localizer)
        {
            UnitOfWork = unitOfWork;
            Mapper = mapper;
            Localizer = localizer;
        }

        public Result CreateRole(Rol role)
        {
            Result result;
            try
            {
                //Checks if role is null returns error.
                if (role == null)
                    throw new ArgumentNullException(nameof(role));

                UnitOfWork.Roles.Add(role);
                UnitOfWork.Complete();

                result = new Result(HttpStatusCode.OK, Localizer["RolCreado"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result UpdateRole(Rol role)
        {
            Result result;
            try
            {
                UnitOfWork.Roles.Update(role);
                int returnValue = UnitOfWork.Complete();

                if (returnValue > 0)
                    result = new Result(HttpStatusCode.OK, Localizer["RolActualizado"]);
                else
                    result = new Result(HttpStatusCode.NotModified, Localizer["NoModificado"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result DeleteRole(Rol role)
        {
            Result result;
            try
            {

                var operationResult = this.UpdateStatus(role, RecordStatus.Eliminado);
                if (operationResult.Status == HttpStatusCode.OK)
                    result = new Result(HttpStatusCode.OK, Localizer["RolEliminado"]);
                else
                    throw new Exception(operationResult.Message);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result UpdateStatus(Rol role, string recordStatus)
        {
            Result result;
            try
            {
                role.Estatus = recordStatus;
                UnitOfWork.Roles.Update(role);
                var resultValue = UnitOfWork.Complete();

                if (resultValue > 0)
                    result = new Result(HttpStatusCode.OK, Localizer["RolActualizado"]);
                else
                    result = new Result(HttpStatusCode.NotModified, Localizer["NoModificado"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result<List<RolViewModel>> GetAllActiveRoles()
        {
            Result<List<RolViewModel>> result;
            try
            {
                var value = UnitOfWork.Roles.FindAll(x => x.Estatus == RecordStatus.Activo).ToList();
                if (value.Count == 0)
                    result = new Result<List<RolViewModel>>(HttpStatusCode.NotFound, Localizer["NoEncontrado"]);
                else
                {
                    var convertedValue = value.Select(x => Mapper.Map<Rol, RolViewModel>(x)).ToList();
                    result = new Result<List<RolViewModel>>(HttpStatusCode.OK) { Value = convertedValue };
                }
            }
            catch (Exception ex)
            {
                result = new Result<List<RolViewModel>>(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result<List<RolViewModel>> FindRoles(int page, int limit, string searchTerm)
        {
            Result<List<RolViewModel>> result;
            try
            {
                var value = UnitOfWork.Roles.FindPagedRoles(page, limit, searchTerm);
                if (value.Count() == 0)
                    result = new Result<List<RolViewModel>>(HttpStatusCode.NotFound);
                else
                {
                    var convertedValue = value.Select(x => Mapper.Map<Rol, RolViewModel>(x)).ToList();
                    result = new Result<List<RolViewModel>>(HttpStatusCode.OK) { Value = convertedValue };
                }
            }
            catch (Exception ex)
            {
                result = new Result<List<RolViewModel>>(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result<RolViewModel> GetRoleById(int roleID)
        {
            Result<RolViewModel> result;
            try
            {
                var role = UnitOfWork.Roles.GetById(roleID);
                if (role == null)
                    result = new Result<RolViewModel>(HttpStatusCode.NotFound, Localizer["NoEncontrado"]);
                else
                {
                    var roleConverted = Mapper.Map<Rol, RolViewModel>(role);
                    result = new Result<RolViewModel>(HttpStatusCode.OK) { Value = roleConverted };
                }
            }
            catch (Exception ex)
            {
                result = new Result<RolViewModel>(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result AssignPermission(RolObjeto roleOject)
        {
            Result result;
            try
            {
                UnitOfWork.ObjectsRole.Add(roleOject);
                UnitOfWork.Complete();

                result = new Result(HttpStatusCode.OK, Localizer["PermisoAsignado"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result RemovePermission(RolObjeto roleObject)
        {
            Result result;
            try
            {
                if (roleObject == null)
                    throw new ArgumentNullException(nameof(roleObject));

                UnitOfWork.ObjectsRole.Remove(roleObject);
                UnitOfWork.Complete();
                result = new Result(HttpStatusCode.OK, Localizer["PermisoRemovido"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result<List<RolViewModel>> GetAllRolesAssignedToUser(int userID)
        {
            Result<List<RolViewModel>> result;
            try
            {
                var value = UnitOfWork.UserRoles.GetAllRolesAssignedToUser(userID);
                if (value == null)
                    result = new Result<List<RolViewModel>>(HttpStatusCode.NotFound);
                else
                {
                    var objectsConveted = value.Select(x => Mapper.Map<Rol, RolViewModel>(x.IdrolNavigation)).ToList();
                    result = new Result<List<RolViewModel>>(HttpStatusCode.OK) { Value = objectsConveted };
                }
            }
            catch (Exception ex)
            {
                result = new Result<List<RolViewModel>>(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result<List<RolViewModel>> GetAllRolesNotAssignedToUser(int userID)
        {
            Result<List<RolViewModel>> result;

            var activeRolesResult = this.GetAllActiveRoles();
            var assignedRolesResult = this.GetAllRolesAssignedToUser(userID);

            if (assignedRolesResult.Status == HttpStatusCode.NotFound)
                result = new Result<List<RolViewModel>>(HttpStatusCode.OK) { Value = assignedRolesResult.Value };

            if (activeRolesResult.Status == HttpStatusCode.OK &&
               assignedRolesResult.Status == HttpStatusCode.OK)
            {
                var filteredRoles = activeRolesResult.Value.Where(x => !assignedRolesResult.Value.Any(s => s.IDRol == x.IDRol)).ToList();
                if (filteredRoles.Count == 0)
                    result = new Result<List<RolViewModel>>(HttpStatusCode.NotFound);
                else
                    result = new Result<List<RolViewModel>>(HttpStatusCode.OK) { Value = filteredRoles };
            }
            else
            {
                result = new Result<List<RolViewModel>>(HttpStatusCode.InternalServerError);
            }
            return result;
        }
    }
}
