﻿using Service.Contracts.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Infrastructure.Response;
using Model.Models;
using Data.Infrastructure.Interfaces;
using Microsoft.Extensions.Localization;
using System.Net;
using System.Text.RegularExpressions;

namespace Service.Implementation.Security
{
    public class SecurityPolicyService : ISecurityPolicyService
    {
        private readonly IUnitOfWork UnitOfWork;
        private readonly IStringLocalizer<SecurityPolicyService> Localizer;

        public SecurityPolicyService(IUnitOfWork unitOfWork,
                                     IStringLocalizer<SecurityPolicyService> localizer)
        {
            UnitOfWork = unitOfWork;
            Localizer = localizer;
        }

        public Result<PoliticaSeguridad> GetPolicy()
        {
            Result<PoliticaSeguridad> result;
            try
            {
                var value = UnitOfWork.SecurityPolicies.GetAll().FirstOrDefault();
                if (value != null)
                    result = new Result<PoliticaSeguridad>(HttpStatusCode.OK) { Value = value };
                else
                    result = new Result<PoliticaSeguridad>(HttpStatusCode.NotFound, Localizer["NoEncontrado"]);
            }
            catch (Exception ex)
            {
                result = new Result<PoliticaSeguridad>(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result UpdatePolicy(PoliticaSeguridad policy)
        {
            Result result;
            try
            {
                UnitOfWork.SecurityPolicies.Update(policy);
                int returnValue = UnitOfWork.Complete();

                if (returnValue > 0)
                    result = new Result(HttpStatusCode.OK, Localizer["PoliticaActualizada"]);
                else
                    result = new Result(HttpStatusCode.NotModified, Localizer["NoModificado"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result<bool> ValidatePasswordPolicy(string password)
        {
            try
            {
                var policyResult = this.GetPolicy();
                if (policyResult.Status == HttpStatusCode.OK)
                {
                    Regex patronLetras = new Regex("[a-zA-Z]");
                    Regex patronCaracteres = new Regex("[@_.!()-]");
                    Regex patronNumeros = new Regex("[0-9]");
                    if (password.Length < policyResult.Value.LongitudClave)
                    {
                        string errorLongitud = string.Format(Localizer["LongitudInvalida"], policyResult.Value.LongitudClave);
                        return new Result<bool>(HttpStatusCode.OK, errorLongitud) { Value = false };
                    }
                    if (policyResult.Value.IncluirCaracteres)
                    {
                        if (!patronCaracteres.IsMatch(password))
                        {
                            return new Result<bool>(HttpStatusCode.OK, Localizer["CarecteresEspecialesInvalido"]) { Value = false };
                        }
                    }
                    if (policyResult.Value.IncluirNumeros)
                    {
                        if (!patronNumeros.IsMatch(password))
                        {
                            return new Result<bool>(HttpStatusCode.OK,Localizer["ContieneNumerosInvalido"]) { Value = false };
                        }
                    }
                    if (policyResult.Value.IncluirLetras)
                    {
                        if (!patronLetras.IsMatch(password))
                        {
                            return new Result<bool>(HttpStatusCode.OK,Localizer["ContieneLetrasInvalido"]) { Value = false };
                        }
                    }
                    return new Result<bool>(HttpStatusCode.OK) { Value = true };
                }
                else
                {
                    return new Result<bool>(HttpStatusCode.OK) { Value = false };
                }
            }
            catch (Exception ex)
            {
                return new Result<bool>(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
        }
    }
}
