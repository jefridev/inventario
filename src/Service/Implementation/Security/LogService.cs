﻿using Service.Contracts.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Infrastructure.Response;
using Model.Models;
using Model.ViewModels.Logs;
using Data.Infrastructure.Interfaces;
using System.Net;
using AutoMapper;
using Microsoft.Extensions.Localization;

namespace Service.Implementation.Security
{
    public class LogService : ILogService
    {
        private readonly IUnitOfWork UnitOfWork;
        private readonly IStringLocalizer<LogService> Localizer;

        public LogService(IUnitOfWork unitOfWork,
                          IStringLocalizer<LogService> localizer)
        {
            UnitOfWork = unitOfWork;
            Localizer = localizer;
        }

        public Result CreateLog(Log log)
        {
            Result result;
            try
            {
                UnitOfWork.Logs.Add(log);
                UnitOfWork.Complete();
                result = new Result(HttpStatusCode.OK, Localizer["LogRegistrado"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result CreateLog(int userID, string objectName)
        {
            Result result;
            try
            {
                var user = UnitOfWork.Users.GetById(userID);
                var objectValue = UnitOfWork.Objects.Find(x => x.NombreFisico == objectName);
                var roleUser = UnitOfWork.UserRoles.FindAll(x => x.Idusuario == userID).SingleOrDefault();
                if (user != null && objectValue != null && roleUser != null)
                {
                    var log = new Log()
                    {
                        Idusuario = user.Idusuario,
                        Idobjeto = objectValue.Idobjeto,
                        Idrol = roleUser.Idrol,
                        Comentario = objectValue.NombreLogico,
                        Fecha = DateTime.UtcNow
                    };

                    UnitOfWork.Logs.Add(log);
                    UnitOfWork.Complete();
                }

                result = new Result(HttpStatusCode.OK, Localizer["LogRegistrado"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result CleanLog(DateTime from, DateTime to)
        {
            Result result;
            try
            {
                var value = UnitOfWork.Logs.FindAll(x => x.Fecha >= from && x.Fecha <= to).ToList();
                if (value == null)
                    result = new Result(HttpStatusCode.NotFound, Localizer["LogsNoEncontrados"]);

                if (value.Count == 0)
                    result = new Result(HttpStatusCode.NotFound, Localizer["LogsNoEncontrados"]);

                UnitOfWork.Logs.RemoveRange(value);
                UnitOfWork.Complete();

                result = new Result(HttpStatusCode.OK, Localizer["LogsRemovidos"]);
            }
            catch (Exception ex)
            {
                result = new Result(HttpStatusCode.InternalServerError, ex.Message);
            }
            return result;
        }

        public Result<List<LogViewModel>> FindLogs(int page, int limit, string searchTerm)
        {
            Result<List<LogViewModel>> result;
            try
            {
                var value = UnitOfWork.Logs.FindPagedLogs(page, limit, searchTerm);
                if (value.Count() == 0)
                    result = new Result<List<LogViewModel>>(HttpStatusCode.NotFound, Localizer["LogsNoEncontrados"]);
                else
                {
                    var convertedValue = value.Select(x => Mapper.Map<Log, LogViewModel>(x)).ToList();
                    result = new Result<List<LogViewModel>>(HttpStatusCode.OK) { Value = convertedValue };
                }
            }
            catch (Exception ex)
            {
                result = new Result<List<LogViewModel>>(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }

        public Result<LogViewModel> GetLogById(int logID)
        {
            Result<LogViewModel> result;
            try
            {
                var objectParam = UnitOfWork.Logs.GetById(logID);
                if (objectParam == null)
                    result = new Result<LogViewModel>(HttpStatusCode.NotFound, Localizer["LogsNoEncotrados"]);
                else
                {
                    var objectConverted = Mapper.Map<Log, LogViewModel>(objectParam);
                    result = new Result<LogViewModel>(HttpStatusCode.OK) { Value = objectConverted };
                }
            }
            catch (Exception ex)
            {
                result = new Result<LogViewModel>(HttpStatusCode.InternalServerError, Localizer["ErrorInterno"]);
            }
            return result;
        }
    }
}
