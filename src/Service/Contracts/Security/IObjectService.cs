﻿using Data.Infrastructure.Response;
using Model.Models;
using Model.ViewModels.Objectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Contracts.Security
{
    public interface IObjectService
    {
        /// <summary>
        /// Retorna si el usuario tiene permiso a una Url especificada.
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="url"></param>
        /// <returns>Verdadero o falso</returns>
        bool HasUserUrlObject(int userID, string url);

        /// <summary>
        /// Retorna una lista de objetos de tipo pantalla.
        /// </summary>
        /// <returns></returns>
        Result<IEnumerable<Objeto>> GetAllScreenTypeObjects();


        /// <summary>
        /// Crea un nuevo objeto en el sistema si cumple con los requerimientos.
        /// </summary>
        /// <param name="objectParam">objeto que se desea crear</param>
        /// <returns>Result con un estatus code Created si fue realizado o un InternalServiceError en otro caso</returns>
        Result CreateObject(Objeto objectParam);

        /// <summary>
        ///  Actualiza el objeto que se solicita
        /// </summary>
        /// <param name="objectParam">objeto que se desea actualizar</param>
        /// <returns>Result con un estatus Ok, si fue actualizado. NotModified si no se actualizó, InternalServerError si ocurrió un error en la operación</returns>
        Result UpdateObject(Objeto objectParam);

        /// <summary>
        /// Actualiza el estatus de un objeto por el especificado.
        /// </summary>
        /// <param name="objectParam">rol al que se le desea actualizar el estado</param>
        /// <param name="recordStatus">Estado del registor esto proviene de la clase estática RecordStatus</param>
        /// <returns>Result, Ok si fue actualizado, NotModified si no se actualizó, InternalServer en caso de que ocurra un error.</returns>
        Result UpdateStatus(Objeto objectParam, string recordStatus);

        /// <summary>
        /// Elimina un rol por completo del sistema
        /// </summary>
        /// <param name="objectParam">rol que se desea eliminar</param>
        /// <returns>Result Ok si fue eliminado, Internal Server Error en caso contrario.</returns>
        Result DeleteObject(Objeto objectParam);

        /// <summary>
        /// Obtiene el rol por el id  especificado.
        /// </summary>
        /// <param name="objectIDID">ID del rol que se desea consultar</param>
        /// <returns>Result con el rol consultado.</returns>
        Result<ObjetoViewModel> GetObjectById(int objectID);

        /// <summary>
        /// Obtener todos los objetos que se encuentren en estatus activo.
        /// </summary>
        /// <returns>Result con la lista de objetos</returns>
        Result<List<ObjetoViewModel>> GetAllActiveObjects();


        /// <summary>
        /// Obtener todos los objetos asignados a un rol.
        /// </summary>
        /// <param name="roleID">Identificador del rol</param>
        /// <returns>Listado de objetos asignados</returns>
        Result<List<ObjetoViewModel>> GetAllAObjectsAssignedToRole(int roleID);
    
        /// <summary>
        /// Obtener todos los objetos no asignados a un rol en particular.
        /// </summary>
        /// <param name="roleID">Identificador del rol</param>
        /// <returns>Listado objetos no asignados</returns>
        Result<List<ObjetoViewModel>> GetAllObjectsNotAssignedToRole(int roleID);

        /// <summary>
        /// Retorna la lista de tipos de objetos utilizados en el sistema.
        /// </summary>
        /// <returns></returns>
        Result<List<string>> GetObjectTypes();

        /// <summary>
        /// Obtiene los objetos con paginación y utilizando el término de búsqueda.
        /// </summary>
        /// <param name="page">Página a utilizar</param>
        /// <param name="limit">Limite de registros (Default=10)</param>
        /// <param name="searchTerm">Texto a consultar</param>
        /// <returns>Result con la lista de objetos obtenidos</returns>
        Result<List<ObjetoViewModel>> FindObjects(int page, int limit, string searchTerm);

        /// <summary>
        /// Obtiene los objetos con paginación y utilizando el término de búsqueda.
        /// </summary>
        /// <param name="page">Página a utilizar</param>
        /// <param name="limit">Limite de registros (Default=10)</param>
        /// <param name="searchTerm">Texto a consultar</param>
        /// <returns>Result con la lista de objetos obtenidos</returns>
        Result<List<ObjetoViewModel>> FindActiveObjects(int page, int limit, string searchTerm);

        /// <summary>
        /// Obtiene todos los botones que pertenecen a una determinada pantalla.
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="screenName"></param>
        /// <returns></returns>
        Result<List<string>> GetButtonByScreenAndUserID(int userID, string screenName);

        /// <summary>
        /// Obtiene todos los objetos asignados a cada uno de los de los usuarios
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        Result<List<ObjetoViewModel>> GetObjectsAssignedToUser(int userID);

        /// <summary>
        ///  Obtiene las opciones disponible de un usuario
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        Result<List<string>> GetMenuOptionsByUser(int userID);
    }
}
