﻿using Data.Infrastructure.Response;
using Model.Models;
using Model.ViewModels.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Contracts.Security
{
    public interface ILogService
    {
        /// <summary>
        /// Crear un nuevo registro en log.
        /// </summary>
        /// <param name="log">Log que se se desea incluir</param>
        /// <returns></returns>
        Result CreateLog(Log log);


        /// <summary>
        /// Registra un nuevo log basado en en el usuario y el objeto utilizado.
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="objectName"></param>
        /// <returns></returns>
        Result CreateLog(int userID, string objectName);

        /// <summary>
        /// Obtiene el log por el ID especificado.
        /// </summary>
        /// <param name="logID"></param>
        /// <returns></returns>
        Result<LogViewModel> GetLogById(int logID);

        /// <summary>
        /// Obtiene la lista de logs por página
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="searchTearm"></param>
        /// <returns></returns>
        Result<List<LogViewModel>> FindLogs(int page, int limit, string searchTerm);

        /// <summary>
        /// Limpia el log desde la fecha especificadas.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        Result CleanLog(DateTime from, DateTime to);
    }
}
