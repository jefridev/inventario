﻿using Data.Infrastructure.Response;
using Model.Models;
using Model.ViewModels.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Contracts.Security
{
    public interface IUserService
    {
        /// <summary>
        /// Realiza el proceso de inicio de sesión. 
        /// </summary>
        /// <param name="usuario">Información de usuario</param>
        /// <returns>Result información de usuario logueado</returns>
        Task<Result<JwtUserViewModel>> Login(LoginViewModel usuario);

        /// <summary>
        ///  Retorna si el usuario está disponible basado en correo electrónico.
        /// </summary>
        /// <param name="usuario">Usuario que se desea comprobar</param>
        /// <returns>Verdadero o falso</returns>
        Result IsEmailAvailable(Usuario usuario);

        /// <summary>
        /// Retorna si el usuario está disponible basado en el nombre utilizado.
        /// </summary>
        /// <param name="usuario">Usuario que se desea comprobar</param>
        /// <returns>Verdadero o falso</returns>
        Result IsUsernameAvailable(Usuario usuario);

        /// <summary>
        /// Crea un nuevo usuario en el sistema si cumple con los requerimientos.
        /// </summary>
        /// <param name="usuario">Usuario que se desea crear</param>
        /// <returns>Result con un estatus code Created si fue realizado o un InternalServiceError en otro caso</returns>
        Result CreateUser(Usuario usuario);

        /// <summary>
        ///  Actualiza el usuario que se solicita
        /// </summary>
        /// <param name="usuario">Usuario que se desea actualizar</param>
        /// <returns>Result con un estatus Ok, si fue actualizado. NotModified si no se actualizó, InternalServerError si ocurrió un error en la operación</returns>
        Result UpdateUser(Usuario usuario);

        /// <summary>
        ///  Actualiza la contraseña del usuario de forma aleatorio y notifica a su correo electrónico.
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns>Result con estatus Ok, si fue generada correctamente.</returns>
        Result RegeneratePassword(Usuario usuario);

        /// <summary>
        /// Actualiza el estatus de un usuario por el especificado.
        /// </summary>
        /// <param name="usuario">Usuario al que se le desea actualizar el estado</param>
        /// <param name="recordStatus">Estado del registor esto proviene de la clase estática RecordStatus</param>
        /// <returns>Result, Ok si fue actualizado, NotModified si no se actualizó, InternalServer en caso de que ocurra un error.</returns>
        Result UpdateStatus(Usuario usuario, string recordStatus);

        /// <summary>
        /// Elimina un usuario por completo del sistema
        /// </summary>
        /// <param name="user">Usuario que se desea eliminar</param>
        /// <returns>Result Ok si fue eliminado, Internal Server Error en caso contrario.</returns>
        Result DeleteUser(Usuario user);

        /// <summary>
        /// Asigna un rol al usuario designado.
        /// </summary>
        /// <param name="usuarioRol">Usuario que se desea utiliza y su respectivo rol</param>
        /// <returns>Result</returns>
        Result AssignRole(UsuarioRol usuarioRol);

        /// <summary>
        /// Remover un usuario rol
        /// </summary>
        /// <param name="usuarioRol">Usuario / Role</param>
        /// <returns>Result</returns>
        Result RemoveRole(UsuarioRol usuarioRol);

        /// <summary>
        ///  Retorna el usuario utilizando su nombre de usuario
        /// </summary>
        /// <param name="username"></param>
        /// <returns>Result con el usuario</returns>
        Result<Usuario> GetUserByUsername(string username);

        /// <summary>
        /// Obtiene el usuario por el id usuario especificado.
        /// </summary>
        /// <param name="userID">ID del usuario que se desea consultar</param>
        /// <returns>Result con el usuario consultado.</returns>
        Result<UsuarioViewModel> GetUserById(int userID);

        /// <summary>
        /// Obtener todos los usuarios que se encuentren en estatus activo.
        /// </summary>
        /// <returns>Result con la lista de usuarios</returns>
        Result<List<UsuarioViewModel>> GetAllActiveUsers();

        /// <summary>
        /// Obtiene los usuarios con paginación y utilizando el término de búsqueda.
        /// </summary>
        /// <param name="page">Página a utilizar</param>
        /// <param name="limit">Limite de registros (Default=10)</param>
        /// <param name="searchTerm">Texto a consultar</param>
        /// <returns>Result con la lista de usuarios obtenidos</returns>
        Result<List<UsuarioViewModel>> FindUsers(int page, int limit, string searchTerm);

        /// <summary>
        /// Almacena el token utilizado por el usuario.
        /// </summary>
        /// <param name="userToker"></param>
        /// <returns></returns>
        Result CreateTokenToUser(UsuarioToken userToken);

        /// <summary>
        ///  Retorna si el usuario tiene un token activo o no.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        Result<bool> HasUserActiveToken(int userID);

        /// <summary>
        ///  Cierra un token en particular
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Result CloseToken(string token);

        /// <summary>
        /// Cierra todas las posibles sesiones utilizadas por un usuario particular.
        /// </summary>
        /// <param name="userID">ID del usuario</param>
        /// <param name="canal">Web o Cliente móvil</param>
        /// <returns></returns>
        Result CloseUserTokens(int userID, string canal);

        /// <summary>
        ///  Activa la cuenta y re-asigna un nuevo token al usuario activado,
        ///  este cambia su estatus de pendiente a Activo.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Result ActivateAccount(string token);

        /// <summary>
        ///  Cambiar contraseña se le envia un enlace con un link para cambiar la contraseña.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        Result ForgotPassword(string username);

        /// <summary>
        ///  Actualiza la contraseña si el token utilizado es válido.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        Result UpdatePassword(string password, string token);


        /// <summary>
        /// Actualizar la contraseña del usuario si la contraseña anterior y la actual coinciden.
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        Result UpdatePassword(int userID, string oldPassword, string newPassword);

        /// <summary>
        ///  Este método retorna un string para fines de crear una contraseña aleatoria.
        /// </summary>
        /// <returns></returns>
        string GenerateRandomPassword();
    }
}
