﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Contracts.Security
{
    /// <summary>
    ///  EncryptionService for security.
    /// </summary>
    public interface IEncryptionService
    {
        /// <summary>
        /// Creates a random salt
        /// </summary>
        /// <returns></returns>
        string GenerateSalt();

        /// <summary>
        /// Generates a random token to be used for token activation and password change.
        /// </summary>
        /// <returns></returns>
        string GenerateActivationToken();

        /// <summary>
        /// Generates a Hashed Password.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        string EncryptPassword(string password, string salt);
    }
}
