﻿using Data.Infrastructure.Response;
using Model.Models;
using Model.ViewModels.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Contracts.Security
{
    public interface IRoleService
    {

        /// <summary>
        /// Crea un nuevo rol en el sistema si cumple con los requerimientos.
        /// </summary>
        /// <param name="role">rol que se desea crear</param>
        /// <returns>Result con un estatus code Created si fue realizado o un InternalServiceError en otro caso</returns>
        Result CreateRole(Rol role);

        /// <summary>
        ///  Actualiza el rol que se solicita
        /// </summary>
        /// <param name="role">rol que se desea actualizar</param>
        /// <returns>Result con un estatus Ok, si fue actualizado. NotModified si no se actualizó, InternalServerError si ocurrió un error en la operación</returns>
        Result UpdateRole(Rol role);

        /// <summary>
        /// Actualiza el estatus de un rol por el especificado.
        /// </summary>
        /// <param name="role">rol al que se le desea actualizar el estado</param>
        /// <param name="recordStatus">Estado del registor esto proviene de la clase estática RecordStatus</param>
        /// <returns>Result, Ok si fue actualizado, NotModified si no se actualizó, InternalServer en caso de que ocurra un error.</returns>
        Result UpdateStatus(Rol role, string recordStatus);

        /// <summary>
        /// Elimina un rol por completo del sistema
        /// </summary>
        /// <param name="role">rol que se desea eliminar</param>
        /// <returns>Result Ok si fue eliminado, Internal Server Error en caso contrario.</returns>
        Result DeleteRole(Rol role);

        /// <summary>
        /// Asigna  un permiso al  rol designado.
        /// </summary>
        /// <param name="roleOject">Objeto que se desea utiliza y su respectivo rol</param>
        /// <returns>Result</returns>
        Result AssignPermission(RolObjeto roleOject);

        /// <summary>
        /// Remover un usuario rol
        /// </summary>
        /// <param name="roleObject">Objeto / role</param>
        /// <returns>Result</returns>
        Result RemovePermission(RolObjeto roleObject);

        /// <summary>
        /// Obtiene el rol por el id  especificado.
        /// </summary>
        /// <param name="roleID">ID del rol que se desea consultar</param>
        /// <returns>Result con el rol consultado.</returns>
        Result<RolViewModel> GetRoleById(int roleID);

        /// <summary>
        /// Obtener todos los roles que se encuentren en estatus activo.
        /// </summary>
        /// <returns>Result con la lista de roles</returns>
        Result<List<RolViewModel>> GetAllActiveRoles();

        /// <summary>
        /// Obtiene los roles con paginación y utilizando el término de búsqueda.
        /// </summary>
        /// <param name="page">Página a utilizar</param>
        /// <param name="limit">Limite de registros (Default=10)</param>
        /// <param name="searchTerm">Texto a consultar</param>
        /// <returns>Result con la lista de roles obtenidos</returns>
        Result<List<RolViewModel>> FindRoles(int page, int limit, string searchTerm);

        /// <summary>
        ///  Obtiene la lista de roles asignados a un usuario
        /// </summary>
        /// <param name="userID">Identificador del usuario</param>
        /// <returns>Result con lista de usuarios rol</returns>
        Result<List<RolViewModel>> GetAllRolesAssignedToUser(int userID);

        /// <summary>
        ///  Obtiene la lista de roles no asignados a un usuario
        /// </summary>
        /// <param name="userID">Identificador del usuario</param>
        /// <returns>Result con lista de usuarios rol</returns>
        Result<List<RolViewModel>> GetAllRolesNotAssignedToUser(int userID);

    }
}
