﻿using Data.Infrastructure.Response;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Contracts.Security
{
    public interface ISecurityPolicyService 
    {
        /// <summary>
        /// Retorna la política de seguridad utilizada actualmente por el sistema.
        /// </summary>
        /// <returns>Resultado con el objeto de la política de seguridad</returns>
        Result<PoliticaSeguridad> GetPolicy();

        /// <summary>
        /// Actualiza algún valor dentro de la política de seguridad el sistema.
        /// </summary>
        /// <param name="policy"></param>
        /// <returns>Resultado con su respectiva respuesta al ejecutar la operación</returns>
        Result UpdatePolicy(PoliticaSeguridad policy);


        /// <summary>
        /// Verifica si la contraseña cumple con las políticas asociadas al usuario.
        /// </summary>
        /// <param name="password">Contraseña escogida por el usuario.</param>
        /// <returns>Result con un boolean correspondiente a la validación.</returns>
        Result<bool> ValidatePasswordPolicy(string password);
    }
}
