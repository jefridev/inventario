﻿using Data.Infrastructure.Response;
using Model.OptionModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service.Contracts.App
{
    public interface IMailService
    {
        /// <summary>
        /// Crendentials and information used to authenticate an account and configure
        /// Smtp service.
        /// </summary>
        MailCredentialsOptions CredentialOptions { get; set; }

        /// <summary>
        /// Enviar correo electrónico a varios individuos al mismo tiempo.
        /// </summary>
        /// <param name="emails"></param>
        /// <param name="subject"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        Result Send(string[] emails, string subject,string message, bool isHtml);
    }
}
