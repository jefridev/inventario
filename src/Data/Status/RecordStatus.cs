﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Status
{
    public class RecordStatus
    {
        public readonly static string Activo = "ACT";
        public readonly static string Inactivo = "INA";
        public readonly static string Eliminado = "ELI";
        public readonly static string Pendiente = "PEN";
    }
}
