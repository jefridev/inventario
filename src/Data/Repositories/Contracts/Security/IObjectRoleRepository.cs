﻿using Data.Infrastructure.Interfaces;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories.Contracts.Security
{
    public interface IObjectRoleRepository : IRepository<RolObjeto>
    {
        /// <summary>
        ///  Obtiene todos los objetos que pertecen a un rol en particular
        /// </summary>
        /// <param name="roleID">Identificador del rol a consultar</param>
        /// <returns>Listado de objecto / rol</returns>
        IEnumerable<RolObjeto> GetAllObjectsAssignedToRole(int roleID);
    }
}
