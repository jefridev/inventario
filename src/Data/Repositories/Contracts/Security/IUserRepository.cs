﻿using Data.Infrastructure.Interfaces;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories.Contracts.Security
{
    public interface IUserRepository : IRepository<Usuario>
    {
        IEnumerable<Usuario> FindPagedUsers(int page, int limit, string searchTerm);
    }
}
