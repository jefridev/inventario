﻿using Data.Infrastructure.Interfaces;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories.Contracts.Security
{
    public interface IUserRoleRepository : IRepository<UsuarioRol>
    {
        /// <summary>
        ///  Obtiene todos los roles que pertecen a un usuario en particular
        /// </summary>
        /// <param name="userID">Identificador del usuario a consultar</param>
        /// <returns>Listado de usuario / rol</returns>
        IEnumerable<UsuarioRol> GetAllRolesAssignedToUser(int userID);
    }
}
