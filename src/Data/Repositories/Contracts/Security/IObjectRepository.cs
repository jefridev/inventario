﻿using Data.Infrastructure.Interfaces;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories.Contracts.Security
{
    public interface IObjectRepository : IRepository<Objeto>
    {
        /// <summary>
        /// Retorna el objeto si el usuario tiene permiso a la url especificada.
        /// </summary>
        /// <returns></returns>
        Objeto GetObjectByUserRoleUrl(int userID, string url);

        /// <summary>
        /// Obtiene todos los objetos de tipo pantalla.
        /// </summary>
        /// <returns></returns>
        IEnumerable<Objeto> GetAllScreenTypeObjects();

        /// <summary>
        ///  Obtiene información paginada de los objetos
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="searchTerm"></param>
        /// <returns></returns>
        IEnumerable<Objeto> FindPagedObjects(int page, int limit, string searchTerm);

        /// <summary>
        /// Obtiene todos los objetos paginados que se encuentran activos.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="searchTerm"></param>
        /// <returns></returns>
        IEnumerable<Objeto> FindPagedActiveObjects(int page, int limit, string searchTerm);

        /// <summary>
        /// Obtiene todos los botones que pertenecen a una determinada pantalla.
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="screenName"></param>
        /// <returns></returns>
        IEnumerable<string> GetButtonByScreenAndUserID(int userID, string screenName);

        /// <summary>
        /// Obtiene todos los objetos asignados a cada uno de los de los usuarios
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        IEnumerable<Objeto> GetObjectsAssignedToUser(int userID);

        /// <summary>
        ///  Get all objects available for user.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        IEnumerable<string> GetMenuOptionsByUser(int userID);
    }
}
