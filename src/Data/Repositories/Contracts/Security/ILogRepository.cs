﻿using Data.Infrastructure.Interfaces;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories.Contracts.Security
{
    public interface ILogRepository : IRepository<Log>
    {
        IEnumerable<Log> FindPagedLogs(int page, int limit, string searchTerm);
    }
}
