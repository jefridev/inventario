﻿using Data.Context;
using Data.Infrastructure.Implementation;
using Data.Repositories.Contracts.Security;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories.Implementation.Security
{
    public class UserRepository : Repository<Usuario>, IUserRepository
    {
        private readonly InventarioContext _context;

        public UserRepository(InventarioContext context)
            : base(context)
        {
            _context = context;
        }

        public IEnumerable<Usuario> FindPagedUsers(int page, int limit, string searchTerm)
        {
            if (limit == 0)
                limit = 10; //Default value if limit is not greater than 0.

            var begin = page * limit;
            return Context.Usuario
                              .Where(x => (x.Nombre.StartsWith(searchTerm) || string.IsNullOrEmpty(searchTerm)) ||
                                          (x.Correo.StartsWith(searchTerm) || string.IsNullOrEmpty(searchTerm)) ||
                                          (x.Telefono.StartsWith(searchTerm) || string.IsNullOrEmpty(searchTerm)))
                              .OrderBy(x => x.Idusuario)
                              .Skip(begin)
                              .Take(limit)
                              .ToList() ?? new List<Usuario>();
        }
    }
}
