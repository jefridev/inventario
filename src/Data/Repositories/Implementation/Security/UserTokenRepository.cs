﻿using Data.Context;
using Data.Infrastructure.Implementation;
using Data.Repositories.Contracts.Security;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories.Implementation.Security
{
    public class UserTokenRepository : Repository<UsuarioToken>, IUserTokenRepository
    {
        public UserTokenRepository(InventarioContext context)
            : base(context)
        {
        }
    }
}
