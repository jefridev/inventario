﻿using Data.Context;
using Data.Infrastructure.Implementation;
using Data.Repositories.Contracts.Security;
using Data.Status;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories.Implementation.Security
{
    public class ObjectRoleRepository : Repository<RolObjeto>, IObjectRoleRepository
    {
        public ObjectRoleRepository(InventarioContext context)
            : base(context)
        {
        }

        public IEnumerable<RolObjeto> GetAllObjectsAssignedToRole(int roleID)
        {
            var objects = (from ro in Context.RolObjeto
                           join r in Context.Rol on ro.Idrol equals r.Idrol
                           join o in Context.Objeto on ro.Idobjeto equals o.Idobjeto
                           where ro.Idrol == roleID &&
                                 o.Estatus == RecordStatus.Activo
                           select new RolObjeto{
                               Idobjeto = o.Idobjeto,
                               IdobjetoNavigation = o,
                               Idrol = r.Idrol,
                               IdrolNavigation = r,
                               RegistrarLog = ro.RegistrarLog
                           });

            return objects;
        }
    }
}
