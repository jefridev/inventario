﻿using Data.Context;
using Data.Infrastructure.Implementation;
using Data.Repositories.Contracts.Security;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories.Implementation.Security
{
    public class SecurityPolicyRepository : Repository<PoliticaSeguridad>, ISecurityPolicyRepository
    {
        public SecurityPolicyRepository(InventarioContext context)
            : base(context) { }
    }
}
