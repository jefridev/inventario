﻿using Data.Context;
using Data.Infrastructure.Implementation;
using Data.Repositories.Contracts.Security;
using Data.Status;
using Data.Types;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories.Implementation.Security
{
    public class ObjectRepository : Repository<Objeto>, IObjectRepository
    {
        public ObjectRepository(InventarioContext context)
            : base(context)
        {
        }

        public Objeto GetObjectByUserRoleUrl(int userID, string url)
        {
            var objeto = (from u in Context.Usuario
                          join ur in Context.UsuarioRol on u.Idusuario equals ur.Idusuario
                          join ro in Context.RolObjeto on ur.Idrol equals ro.Idrol
                          join o in Context.Objeto on ro.Idobjeto equals o.Idobjeto
                          join r in Context.Rol on ur.Idrol equals r.Idrol
                          where u.Idusuario == userID && o.NombreFisico == url
                                && o.Estatus == RecordStatus.Activo
                                && r.Estatus == RecordStatus.Activo
                                && ur.Estatus == RecordStatus.Activo
                          select o).SingleOrDefault();

            return objeto;
        }

        public IEnumerable<Objeto> GetAllScreenTypeObjects()
        {
            var objetos = (from o in Context.Objeto
                           where o.Estatus == RecordStatus.Activo
                           select o).ToList();

            return objetos;
        }

        public IEnumerable<Objeto> FindPagedObjects(int page, int limit, string searchTerm)
        {
            if (limit == 0)
                limit = 10; //Default value if limit is not greater than 0.

            var begin = page * limit;

            var resultados = (from o in Context.Objeto
                              join or in Context.Objeto on o.IdobjetoRelacionado equals or.Idobjeto into objetoRelacionado
                              from result in objetoRelacionado.DefaultIfEmpty()
                              where (o.NombreFisico.StartsWith(searchTerm) || string.IsNullOrEmpty(searchTerm)) ||
                                    (o.NombreLogico.StartsWith(searchTerm) || string.IsNullOrEmpty(searchTerm))
                              select new Objeto
                              {
                                  Idobjeto = o.Idobjeto,
                                  NombreLogico = o.NombreLogico,
                                  NombreFisico = o.NombreFisico,
                                  TipoObjeto = o.TipoObjeto,
                                  IdobjetoRelacionado = o.IdobjetoRelacionado,
                                  IdobjetoRelacionadoNavigation = (result != null) ? result : null,
                                  Estatus = o.Estatus
                              }).OrderBy(x => x.Idobjeto)
                                .Skip(begin).Take(limit).ToList() ?? new List<Objeto>();

            return resultados;
        }

        public IEnumerable<string> GetButtonByScreenAndUserID(int userID, string screenName)
        {
            Objeto objeto = Context.Objeto
                                   .Where(x => x.NombreFisico == screenName &&
                                               x.Estatus == RecordStatus.Activo)
                                   .SingleOrDefault();

            if (objeto != null)
            {
                IEnumerable<string> objects = (from u in Context.Usuario
                                               join ur in Context.UsuarioRol on u.Idusuario equals ur.Idusuario
                                               join ro in Context.RolObjeto on ur.Idrol equals ro.Idrol
                                               join o in Context.Objeto on ro.Idobjeto equals o.Idobjeto
                                               join r in Context.Rol on ur.Idrol equals r.Idrol
                                               where u.Idusuario == userID && o.IdobjetoRelacionado == objeto.Idobjeto && o.Estatus == RecordStatus.Activo && r.Estatus == RecordStatus.Activo
                                               select o.NombreFisico).Distinct().ToList();
                return objects;
            }
            else
                return null;
        }

        public IEnumerable<string> GetMenuOptionsByUser(int userID)
        {
            IEnumerable<string> objects = (from u in Context.Usuario
                                           join ur in Context.UsuarioRol on u.Idusuario equals ur.Idusuario
                                           join ro in Context.RolObjeto on ur.Idrol equals ro.Idrol
                                           join o in Context.Objeto on ro.Idobjeto equals o.Idobjeto
                                           join r in Context.Rol on ur.Idrol equals r.Idrol
                                           where u.Idusuario == userID && o.TipoObjeto == ObjectType.Button && o.Estatus == RecordStatus.Activo && r.Estatus == RecordStatus.Activo && ur.Estatus == RecordStatus.Activo
                                           select o.NombreFisico
                       ).ToList();

            return objects;
        }

        public IEnumerable<Objeto> FindPagedActiveObjects(int page, int limit, string searchTerm)
        {
            var pagedList = this.FindPagedObjects(page, limit, searchTerm);
            return pagedList.Where(x => x.Estatus == RecordStatus.Activo);
        }

        public IEnumerable<Objeto> GetObjectsAssignedToUser(int userID)
        {
            var objetos = (from u in Context.Usuario
                           join ur in Context.UsuarioRol on u.Idusuario equals ur.Idusuario
                           join r in Context.Rol on ur.Idrol equals r.Idrol
                           join ro in Context.RolObjeto on r.Idrol equals ro.Idrol
                           join o in Context.Objeto on ro.Idobjeto equals o.Idobjeto
                           where o.Estatus == RecordStatus.Activo &&
                                 r.Estatus == RecordStatus.Activo &&
                                 u.Idusuario == userID
                           select o).GroupBy(g => g.NombreFisico).SelectMany(x => x).ToList() ?? new List<Objeto>();

            return objetos;
        }
    }
}
