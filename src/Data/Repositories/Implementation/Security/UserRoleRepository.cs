﻿using Data.Context;
using Data.Infrastructure.Implementation;
using Data.Repositories.Contracts.Security;
using Data.Status;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories.Implementation.Security
{
    public class UserRoleRepository : Repository<UsuarioRol>, IUserRoleRepository
    {
        public UserRoleRepository(InventarioContext context)
            : base(context)
        {
        }

        public IEnumerable<UsuarioRol> GetAllRolesAssignedToUser(int userID)
        {
            var userRoles = (from ur in Context.UsuarioRol
                             join r in Context.Rol on ur.Idrol equals r.Idrol
                             join u in Context.Usuario on ur.Idusuario equals u.Idusuario
                             where ur.Idusuario == userID &&
                                   u.Estatus == RecordStatus.Activo
                             select new UsuarioRol
                             {
                                 Estatus = ur.Estatus,
                                 IdusuarioNavigation = u,
                                 Idrol = r.Idrol,
                                 IdrolNavigation = r,
                                 Idusuario = u.Idusuario
                             });

            return userRoles;
        }
    }
}
