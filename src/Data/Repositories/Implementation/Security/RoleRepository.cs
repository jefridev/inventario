﻿using Data.Context;
using Data.Infrastructure.Implementation;
using Data.Repositories.Contracts.Security;
using Data.Status;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories.Implementation.Security
{
    public class RoleRepository : Repository<Rol>, IRoleRepository
    {
        public RoleRepository(InventarioContext context)
            : base(context)
        {
        }

        public IEnumerable<Rol> FindPagedRoles(int page, int limit, string searchTerm)
        {
            if (limit == 0)
                limit = 10; //Default value if limit is not greater than 0.

            var begin = page * limit;
            return Context.Rol
                              .Where(x => ((x.Nombre.StartsWith(searchTerm) || string.IsNullOrEmpty(searchTerm)) ||
                                          (x.Descripcion.StartsWith(searchTerm) || string.IsNullOrEmpty(searchTerm))) &&
                                           x.Estatus == RecordStatus.Activo)
                              .OrderBy(x => x.Idrol)
                              .Skip(begin)
                              .Take(limit)
                              .ToList() ?? new List<Rol>();
        }
    }
}
