﻿using Data.Context;
using Data.Infrastructure.Implementation;
using Data.Repositories.Contracts.Security;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Repositories.Implementation.Security
{
    public class LogRepository : Repository<Log>, ILogRepository
    {
        public LogRepository(InventarioContext context)
            : base(context)
        {
        }

        public IEnumerable<Log> FindPagedLogs(int page, int limit, string searchTerm)
        {
            if (limit == 0)
                limit = 10; //Default value if limit is not greater than 0.

            var begin = page * limit;
            return (from l in Context.Log
                    join o in Context.Objeto on l.Idobjeto equals o.Idobjeto
                    join r in Context.Rol on l.Idrol equals r.Idrol
                    join u in Context.Usuario on l.Idusuario equals u.Idusuario
                    where (l.Comentario.StartsWith(searchTerm) || string.IsNullOrEmpty(searchTerm)) ||
                          (u.Username.StartsWith(searchTerm) || string.IsNullOrEmpty(searchTerm)) ||
                          (r.Nombre.StartsWith(searchTerm) || string.IsNullOrEmpty(searchTerm))
                    orderby l.Idlog
                    select new Log()
                    {
                        Idlog = l.Idlog,
                        Idusuario = l.Idusuario,
                        Idrol = l.Idrol,
                        Idobjeto = l.Idobjeto,
                        Comentario = l.Comentario,
                        Fecha = l.Fecha,
                        IdobjetoNavigation = o,
                        IdrolNavigation = r,
                        IdusuarioNavigation = u
                    }).Skip(begin)
                      .Take(limit)
                      .ToList() ?? new List<Log>();
        }
    }
}
