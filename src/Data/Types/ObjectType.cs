﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Types
{
    public class ObjectType
    {
        public static string Screen = "Pantalla";
        public static string Button = "Botón";
        public static string Report = "Reporte";
    }
}
