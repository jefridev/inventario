﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Data.Infrastructure.Response
{
    public class Result<T>
    {
        private HttpStatusCode _status;
        private string _message;

        public HttpStatusCode Status
        {
            get { return _status; }
        }

        public int StatusInt
        {
            get { return (int)_status; }
            private set { }
        }

        public string Message
        {
            get { return _message; }
            private set { }
        }

        public T Value { get; set; }

        public Result(HttpStatusCode status)
        {
            if (status == HttpStatusCode.Unauthorized)
                _message = "Unauthorized access. Login required";

            _status = status;
        }

        public Result(HttpStatusCode status, string message)
        {
            _status = status;
            _message = message;
        }
    }

    public class Result
    {
        private HttpStatusCode _status;
        private string _message;

        public HttpStatusCode Status
        {
            get { return _status; }
        }

        public int StatusInt
        {
            get { return (int)_status; }
            private set { }
        }

        public string Message
        {
            get { return _message; }
            private set { }
        }

        public Result(HttpStatusCode status)
        {
            if (status == HttpStatusCode.Unauthorized)
                _message = "Unauthorized access. Login required";

            _status = status;
        }

        public Result(HttpStatusCode status, string message)
        {
            _status = status;
            _message = message;
        }
    }
}
