﻿using Data.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Repositories.Contracts.Security;
using Data.Context;
using Data.Repositories.Implementation.Security;

namespace Data.Infrastructure.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly InventarioContext _context;

        public UnitOfWork(InventarioContext context)
        {
            _context = context;
            Logs = new LogRepository(context);
            Users = new UserRepository(context);
            Objects = new ObjectRepository(context);
            ObjectsRole = new ObjectRoleRepository(context);
            Roles = new RoleRepository(context);
            SecurityPolicies = new SecurityPolicyRepository(context);
            UserRoles = new UserRoleRepository(context);
            UserTokens = new UserTokenRepository(context);
        }

        public ILogRepository Logs { get; private set; }

        public IObjectRepository Objects { get; private set; }

        public IObjectRoleRepository ObjectsRole { get; private set; }

        public IRoleRepository Roles { get; private set; }

        public ISecurityPolicyRepository SecurityPolicies { get; private set; }

        public IUserRoleRepository UserRoles { get; private set; }

        public IUserRepository Users { get; private set; }
  
        public IUserTokenRepository UserTokens { get; private set; }


        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
