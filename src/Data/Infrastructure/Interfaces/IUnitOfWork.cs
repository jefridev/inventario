﻿using Data.Repositories.Contracts.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Infrastructure.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ILogRepository Logs { get; }
        IUserRepository Users { get; }
        IUserRoleRepository UserRoles { get; }
        IUserTokenRepository UserTokens { get; }
        ISecurityPolicyRepository SecurityPolicies { get; } 
        IObjectRepository Objects { get; }
        IObjectRoleRepository ObjectsRole { get; }
        IRoleRepository Roles { get; }

        int Complete();
    }
}
