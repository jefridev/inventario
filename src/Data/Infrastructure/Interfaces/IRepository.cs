﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Data.Infrastructure.Interfaces
{
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// Marks an entity as new.
        /// </summary>
        void Add(T entity);

        /// <summary>
        /// Add range of entities
        /// </summary>
        /// <param name="entities"></param>
        void AddRange(IEnumerable<T> entities);

        /// <summary>
        /// Marks an entity as modified
        /// </summary>
        void Update(T entity);

        void UpdateRange(IEnumerable<T> entities);

        /// <summary>
        /// Marks an entity to be removed
        /// </summary>
        void Remove(T entity);

        /// <summary>
        /// Remove a list of entities
        /// </summary>
        /// <param name="entities"></param>
        void RemoveRange(IEnumerable<T> entities);

        /// <summary>
        /// Get an entity by int id
        /// </summary>
        T GetById(int id);

        /// <summary>
        /// Get an entity using delegate
        /// </summary>
        /// <param name="where"></param>
        T Find(Expression<Func<T, bool>> where);

        /// <summary>
        /// Get all entity that meets where criteria
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        IEnumerable<T> FindAll(Expression<Func<T, bool>> where);

        /// <summary>
        /// Gets all entities of type T
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetAll();
    }
}
