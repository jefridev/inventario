﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Model.Models;

namespace Data.Context
{
    public partial class InventarioContext : DbContext
    {
        public virtual DbSet<Ajuste> Ajuste { get; set; }
        public virtual DbSet<AjusteProducto> AjusteProducto { get; set; }
        public virtual DbSet<Almacen> Almacen { get; set; }
        public virtual DbSet<Caja> Caja { get; set; }
        public virtual DbSet<CategoriaProducto> CategoriaProducto { get; set; }
        public virtual DbSet<Cierre> Cierre { get; set; }
        public virtual DbSet<Cliente> Cliente { get; set; }
        public virtual DbSet<ClienteCorreoElectronico> ClienteCorreoElectronico { get; set; }
        public virtual DbSet<ClienteDireccion> ClienteDireccion { get; set; }
        public virtual DbSet<ClienteTelefono> ClienteTelefono { get; set; }
        public virtual DbSet<Comprobante> Comprobante { get; set; }
        public virtual DbSet<Conduce> Conduce { get; set; }
        public virtual DbSet<ConduceProducto> ConduceProducto { get; set; }
        public virtual DbSet<Contacto> Contacto { get; set; }
        public virtual DbSet<ContactoCorreoElectronico> ContactoCorreoElectronico { get; set; }
        public virtual DbSet<ContactoDireccion> ContactoDireccion { get; set; }
        public virtual DbSet<ContactoTelefono> ContactoTelefono { get; set; }
        public virtual DbSet<CorreoElectronico> CorreoElectronico { get; set; }
        public virtual DbSet<CuadreCaja> CuadreCaja { get; set; }
        public virtual DbSet<Devolucion> Devolucion { get; set; }
        public virtual DbSet<DevolucionProducto> DevolucionProducto { get; set; }
        public virtual DbSet<Direccion> Direccion { get; set; }
        public virtual DbSet<Empleado> Empleado { get; set; }
        public virtual DbSet<EmpleadoCorreoElectronico> EmpleadoCorreoElectronico { get; set; }
        public virtual DbSet<EmpleadoDireccion> EmpleadoDireccion { get; set; }
        public virtual DbSet<EmpleadoTelefono> EmpleadoTelefono { get; set; }
        public virtual DbSet<Empresa> Empresa { get; set; }
        public virtual DbSet<Entrada> Entrada { get; set; }
        public virtual DbSet<EntradaProducto> EntradaProducto { get; set; }
        public virtual DbSet<Factura> Factura { get; set; }
        public virtual DbSet<FacturaProducto> FacturaProducto { get; set; }
        public virtual DbSet<HistorialCostoProducto> HistorialCostoProducto { get; set; }
        public virtual DbSet<HistorialPrecioProducto> HistorialPrecioProducto { get; set; }
        public virtual DbSet<Log> Log { get; set; }
        public virtual DbSet<ModoPago> ModoPago { get; set; }
        public virtual DbSet<Moneda> Moneda { get; set; }
        public virtual DbSet<NotaCredito> NotaCredito { get; set; }
        public virtual DbSet<NotaCreditoFactura> NotaCreditoFactura { get; set; }
        public virtual DbSet<Objeto> Objeto { get; set; }
        public virtual DbSet<PoliticaSeguridad> PoliticaSeguridad { get; set; }
        public virtual DbSet<Producto> Producto { get; set; }
        public virtual DbSet<ProductoExistencia> ProductoExistencia { get; set; }
        public virtual DbSet<Recibo> Recibo { get; set; }
        public virtual DbSet<ReciboFactura> ReciboFactura { get; set; }
        public virtual DbSet<ReciboPago> ReciboPago { get; set; }
        public virtual DbSet<Rol> Rol { get; set; }
        public virtual DbSet<RolObjeto> RolObjeto { get; set; }
        public virtual DbSet<SubcategoriaProducto> SubcategoriaProducto { get; set; }
        public virtual DbSet<Sucursal> Sucursal { get; set; }
        public virtual DbSet<SucursalContacto> SucursalContacto { get; set; }
        public virtual DbSet<SucursalDireccion> SucursalDireccion { get; set; }
        public virtual DbSet<SucursalTelefono> SucursalTelefono { get; set; }
        public virtual DbSet<Suplidor> Suplidor { get; set; }
        public virtual DbSet<SuplidorContacto> SuplidorContacto { get; set; }
        public virtual DbSet<SuplidorCorreoElectronico> SuplidorCorreoElectronico { get; set; }
        public virtual DbSet<SuplidorDireccion> SuplidorDireccion { get; set; }
        public virtual DbSet<SuplidorTelefono> SuplidorTelefono { get; set; }
        public virtual DbSet<Tanda> Tanda { get; set; }
        public virtual DbSet<Telefono> Telefono { get; set; }
        public virtual DbSet<TipoAjuste> TipoAjuste { get; set; }
        public virtual DbSet<TipoCliente> TipoCliente { get; set; }
        public virtual DbSet<TipoComprobante> TipoComprobante { get; set; }
        public virtual DbSet<TipoContacto> TipoContacto { get; set; }
        public virtual DbSet<TipoCorreoElectronico> TipoCorreoElectronico { get; set; }
        public virtual DbSet<TipoDireccion> TipoDireccion { get; set; }
        public virtual DbSet<TipoIdentificacion> TipoIdentificacion { get; set; }
        public virtual DbSet<TipoPago> TipoPago { get; set; }
        public virtual DbSet<TipoProducto> TipoProducto { get; set; }
        public virtual DbSet<TipoSuplidor> TipoSuplidor { get; set; }
        public virtual DbSet<TipoTelefono> TipoTelefono { get; set; }
        public virtual DbSet<Transferencia> Transferencia { get; set; }
        public virtual DbSet<TransferenciaProducto> TransferenciaProducto { get; set; }
        public virtual DbSet<UnidadMedida> UnidadMedida { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<UsuarioEmpleado> UsuarioEmpleado { get; set; }
        public virtual DbSet<UsuarioRol> UsuarioRol { get; set; }
        public virtual DbSet<UsuarioToken> UsuarioToken { get; set; }

        public InventarioContext(DbContextOptions<InventarioContext> options)
                  : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ajuste>(entity =>
            {
                entity.HasKey(e => e.Idajuste)
                    .HasName("PK_Ajuste");

                entity.Property(e => e.Idajuste).HasColumnName("IDAjuste");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.Idalmacen).HasColumnName("IDAlmacen");

                entity.Property(e => e.Idempresa).HasColumnName("IDEmpresa");

                entity.Property(e => e.IdtipoAjuste).HasColumnName("IDTipoAjuste");

                entity.Property(e => e.Idusuario).HasColumnName("IDUsuario");

                entity.HasOne(d => d.IdalmacenNavigation)
                    .WithMany(p => p.Ajuste)
                    .HasForeignKey(d => d.Idalmacen)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Ajuste_Almacen");

                entity.HasOne(d => d.IdempresaNavigation)
                    .WithMany(p => p.Ajuste)
                    .HasForeignKey(d => d.Idempresa)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Ajuste_Empresa");

                entity.HasOne(d => d.IdtipoAjusteNavigation)
                    .WithMany(p => p.Ajuste)
                    .HasForeignKey(d => d.IdtipoAjuste)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Ajuste_TipoAjuste");

                entity.HasOne(d => d.IdusuarioNavigation)
                    .WithMany(p => p.Ajuste)
                    .HasForeignKey(d => d.Idusuario)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Ajuste_Usuario");
            });

            modelBuilder.Entity<AjusteProducto>(entity =>
            {
                entity.HasKey(e => new { e.Idajuste, e.Idproducto })
                    .HasName("PK_AjusteProducto_1");

                entity.Property(e => e.Idajuste).HasColumnName("IDAjuste");

                entity.Property(e => e.Idproducto).HasColumnName("IDProducto");

                entity.HasOne(d => d.IdajusteNavigation)
                    .WithMany(p => p.AjusteProducto)
                    .HasForeignKey(d => d.Idajuste)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_AjusteProducto_Ajuste");

                entity.HasOne(d => d.IdproductoNavigation)
                    .WithMany(p => p.AjusteProducto)
                    .HasForeignKey(d => d.Idproducto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_AjusteProducto_Producto");
            });

            modelBuilder.Entity<Almacen>(entity =>
            {
                entity.HasKey(e => e.Idalmacen)
                    .HasName("PK_Almacen");

                entity.Property(e => e.Idalmacen).HasColumnName("IDAlmacen");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.IdempleadoResposable).HasColumnName("IDEmpleadoResposable");

                entity.Property(e => e.Idsucursal).HasColumnName("IDSucursal");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.HasOne(d => d.IdsucursalNavigation)
                    .WithMany(p => p.Almacen)
                    .HasForeignKey(d => d.Idsucursal)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Almacen_Sucursal");
            });

            modelBuilder.Entity<Caja>(entity =>
            {
                entity.HasKey(e => e.Idcaja)
                    .HasName("PK_Caja");

                entity.Property(e => e.Idcaja).HasColumnName("IDCaja");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaCierre).HasColumnType("datetime");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaInicio).HasColumnType("datetime");

                entity.Property(e => e.Idsucursal).HasColumnName("IDSucursal");

                entity.Property(e => e.Idtanda)
                    .IsRequired()
                    .HasColumnName("IDTanda")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Idusuario).HasColumnName("IDUsuario");

                entity.Property(e => e.MontoInicial).HasColumnType("decimal");

                entity.HasOne(d => d.IdsucursalNavigation)
                    .WithMany(p => p.Caja)
                    .HasForeignKey(d => d.Idsucursal)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Caja_Sucursal");

                entity.HasOne(d => d.IdtandaNavigation)
                    .WithMany(p => p.Caja)
                    .HasForeignKey(d => d.Idtanda)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Caja_Tanda");

                entity.HasOne(d => d.IdusuarioNavigation)
                    .WithMany(p => p.Caja)
                    .HasForeignKey(d => d.Idusuario)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Caja_Usuario");
            });

            modelBuilder.Entity<CategoriaProducto>(entity =>
            {
                entity.HasKey(e => e.IdcategoriaProducto)
                    .HasName("PK_CategoriaProducto");

                entity.Property(e => e.IdcategoriaProducto).HasColumnName("IDCategoriaProducto");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Cierre>(entity =>
            {
                entity.HasKey(e => e.Idcierre)
                    .HasName("PK_Cierre");

                entity.Property(e => e.Idcierre).HasColumnName("IDCierre");

                entity.Property(e => e.Cheque).HasColumnType("decimal");

                entity.Property(e => e.Cobrado).HasColumnType("decimal");

                entity.Property(e => e.Desembolso).HasColumnType("decimal");

                entity.Property(e => e.Faltante).HasColumnType("decimal");

                entity.Property(e => e.Idcaja).HasColumnName("IDCaja");

                entity.Property(e => e.Idsucursal).HasColumnName("IDSucursal");

                entity.Property(e => e.Idtanda)
                    .IsRequired()
                    .HasColumnName("IDTanda")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Idusuario).HasColumnName("IDUsuario");

                entity.Property(e => e.Sobrante).HasColumnType("decimal");

                entity.Property(e => e.Tarjeta).HasColumnType("decimal");

                entity.Property(e => e.TotalBruto).HasColumnType("decimal");

                entity.Property(e => e.TotalDigitado).HasColumnType("decimal");

                entity.Property(e => e.TotalFacturado).HasColumnType("decimal");

                entity.Property(e => e.TotalGeneral).HasColumnType("decimal");

                entity.Property(e => e.TotalNeto).HasColumnType("decimal");

                entity.HasOne(d => d.IdcajaNavigation)
                    .WithMany(p => p.Cierre)
                    .HasForeignKey(d => d.Idcaja)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Cierre_Caja");

                entity.HasOne(d => d.IdsucursalNavigation)
                    .WithMany(p => p.Cierre)
                    .HasForeignKey(d => d.Idsucursal)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Cierre_Sucursal");

                entity.HasOne(d => d.IdtandaNavigation)
                    .WithMany(p => p.Cierre)
                    .HasForeignKey(d => d.Idtanda)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Cierre_Tanda");

                entity.HasOne(d => d.IdusuarioNavigation)
                    .WithMany(p => p.Cierre)
                    .HasForeignKey(d => d.Idusuario)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Cierre_Usuario");
            });

            modelBuilder.Entity<Cliente>(entity =>
            {
                entity.HasKey(e => e.Idcliente)
                    .HasName("PK_Cliente");

                entity.Property(e => e.Idcliente).HasColumnName("IDCliente");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Idempresa).HasColumnName("IDEmpresa");

                entity.Property(e => e.Identificacion)
                    .IsRequired()
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.IdtipoCliente).HasColumnName("IDTipoCliente");

                entity.Property(e => e.IdtipoIdentificacion).HasColumnName("IDTipoIdentificacion");

                entity.Property(e => e.LimiteCredito).HasColumnType("decimal");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                entity.HasOne(d => d.IdempresaNavigation)
                    .WithMany(p => p.Cliente)
                    .HasForeignKey(d => d.Idempresa)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Cliente_Empresa");

                entity.HasOne(d => d.IdtipoClienteNavigation)
                    .WithMany(p => p.Cliente)
                    .HasForeignKey(d => d.IdtipoCliente)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Cliente_TipoCliente");

                entity.HasOne(d => d.IdtipoIdentificacionNavigation)
                    .WithMany(p => p.Cliente)
                    .HasForeignKey(d => d.IdtipoIdentificacion)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Cliente_TipoIdentificacion");
            });

            modelBuilder.Entity<ClienteCorreoElectronico>(entity =>
            {
                entity.HasKey(e => new { e.Idcliente, e.IdcorreoElectronico })
                    .HasName("PK_ClienteCorreoElectronico");

                entity.Property(e => e.Idcliente).HasColumnName("IDCliente");

                entity.Property(e => e.IdcorreoElectronico).HasColumnName("IDCorreoElectronico");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IdclienteNavigation)
                    .WithMany(p => p.ClienteCorreoElectronico)
                    .HasForeignKey(d => d.Idcliente)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ClienteCorreoElectronico_Cliente");

                entity.HasOne(d => d.IdcorreoElectronicoNavigation)
                    .WithMany(p => p.ClienteCorreoElectronico)
                    .HasForeignKey(d => d.IdcorreoElectronico)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ClienteCorreoElectronico_CorreoElectronico");
            });

            modelBuilder.Entity<ClienteDireccion>(entity =>
            {
                entity.HasKey(e => new { e.Idcliente, e.Iddireccion })
                    .HasName("PK_ClienteDireccion");

                entity.Property(e => e.Idcliente).HasColumnName("IDCliente");

                entity.Property(e => e.Iddireccion).HasColumnName("IDDireccion");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IdclienteNavigation)
                    .WithMany(p => p.ClienteDireccion)
                    .HasForeignKey(d => d.Idcliente)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ClienteDireccion_Cliente");

                entity.HasOne(d => d.IddireccionNavigation)
                    .WithMany(p => p.ClienteDireccion)
                    .HasForeignKey(d => d.Iddireccion)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ClienteDireccion_Direccion");
            });

            modelBuilder.Entity<ClienteTelefono>(entity =>
            {
                entity.HasKey(e => new { e.Idcliente, e.Idtelefono })
                    .HasName("PK_ClienteTelefono");

                entity.Property(e => e.Idcliente).HasColumnName("IDCliente");

                entity.Property(e => e.Idtelefono).HasColumnName("IDTelefono");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IdclienteNavigation)
                    .WithMany(p => p.ClienteTelefono)
                    .HasForeignKey(d => d.Idcliente)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ClienteTelefono_Cliente");

                entity.HasOne(d => d.IdtelefonoNavigation)
                    .WithMany(p => p.ClienteTelefono)
                    .HasForeignKey(d => d.Idtelefono)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ClienteTelefono_Telefono");
            });

            modelBuilder.Entity<Comprobante>(entity =>
            {
                entity.HasKey(e => e.Idcompronte)
                    .HasName("PK_Comprobante");

                entity.Property(e => e.Idcompronte).HasColumnName("IDCompronte");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.Idempresa).HasColumnName("IDEmpresa");

                entity.Property(e => e.IdtipoComprobante)
                    .IsRequired()
                    .HasColumnName("IDTipoComprobante")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Prefijo)
                    .IsRequired()
                    .HasColumnType("varchar(20)");

                entity.HasOne(d => d.IdempresaNavigation)
                    .WithMany(p => p.Comprobante)
                    .HasForeignKey(d => d.Idempresa)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Comprobante_Empresa");

                entity.HasOne(d => d.IdtipoComprobanteNavigation)
                    .WithMany(p => p.Comprobante)
                    .HasForeignKey(d => d.IdtipoComprobante)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Comprobante_TipoComprobante");
            });

            modelBuilder.Entity<Conduce>(entity =>
            {
                entity.HasKey(e => e.Idconduce)
                    .HasName("PK_Conduce");

                entity.Property(e => e.Idconduce).HasColumnName("IDConduce");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.Idalmacen).HasColumnName("IDAlmacen");

                entity.Property(e => e.Idcliente).HasColumnName("IDCliente");

                entity.Property(e => e.Idempresa).HasColumnName("IDEmpresa");

                entity.Property(e => e.Idfactura).HasColumnName("IDFactura");

                entity.Property(e => e.Idusuario).HasColumnName("IDUsuario");

                entity.HasOne(d => d.IdalmacenNavigation)
                    .WithMany(p => p.Conduce)
                    .HasForeignKey(d => d.Idalmacen)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Conduce_Almacen");

                entity.HasOne(d => d.IdclienteNavigation)
                    .WithMany(p => p.Conduce)
                    .HasForeignKey(d => d.Idcliente)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Conduce_Cliente");

                entity.HasOne(d => d.IdempresaNavigation)
                    .WithMany(p => p.Conduce)
                    .HasForeignKey(d => d.Idempresa)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Conduce_Empresa");

                entity.HasOne(d => d.IdfacturaNavigation)
                    .WithMany(p => p.Conduce)
                    .HasForeignKey(d => d.Idfactura)
                    .HasConstraintName("FK_Conduce_Factura");

                entity.HasOne(d => d.IdusuarioNavigation)
                    .WithMany(p => p.Conduce)
                    .HasForeignKey(d => d.Idusuario)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Conduce_Usuario");
            });

            modelBuilder.Entity<ConduceProducto>(entity =>
            {
                entity.HasKey(e => new { e.Idconduce, e.Idproducto })
                    .HasName("PK_ConduceProducto");

                entity.Property(e => e.Idconduce).HasColumnName("IDConduce");

                entity.Property(e => e.Idproducto).HasColumnName("IDProducto");

                entity.HasOne(d => d.IdconduceNavigation)
                    .WithMany(p => p.ConduceProducto)
                    .HasForeignKey(d => d.Idconduce)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ConduceProducto_Conduce");

                entity.HasOne(d => d.IdproductoNavigation)
                    .WithMany(p => p.ConduceProducto)
                    .HasForeignKey(d => d.Idproducto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ConduceProducto_Producto");
            });

            modelBuilder.Entity<Contacto>(entity =>
            {
                entity.HasKey(e => e.Idcontacto)
                    .HasName("PK_Contacto");

                entity.Property(e => e.Idcontacto)
                    .HasColumnName("IDContacto")
                    .ValueGeneratedNever();

                entity.Property(e => e.Apellido)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Cargo)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.IdtipoContacto).HasColumnName("IDTipoContacto");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.HasOne(d => d.IdtipoContactoNavigation)
                    .WithMany(p => p.Contacto)
                    .HasForeignKey(d => d.IdtipoContacto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Contacto_TipoContacto");
            });

            modelBuilder.Entity<ContactoCorreoElectronico>(entity =>
            {
                entity.HasKey(e => new { e.Idcontacto, e.IdcorreoElectronico })
                    .HasName("PK_ContactoCorreoElectronico");

                entity.Property(e => e.Idcontacto).HasColumnName("IDContacto");

                entity.Property(e => e.IdcorreoElectronico).HasColumnName("IDCorreoElectronico");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IdcontactoNavigation)
                    .WithMany(p => p.ContactoCorreoElectronico)
                    .HasForeignKey(d => d.Idcontacto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ContactoCorreoElectronico_Contacto");

                entity.HasOne(d => d.IdcorreoElectronicoNavigation)
                    .WithMany(p => p.ContactoCorreoElectronico)
                    .HasForeignKey(d => d.IdcorreoElectronico)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ContactoCorreoElectronico_CorreoElectronico");
            });

            modelBuilder.Entity<ContactoDireccion>(entity =>
            {
                entity.HasKey(e => new { e.Idcontacto, e.Iddireccion })
                    .HasName("PK_ContactoDireccion");

                entity.Property(e => e.Idcontacto).HasColumnName("IDContacto");

                entity.Property(e => e.Iddireccion).HasColumnName("IDDireccion");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IdcontactoNavigation)
                    .WithMany(p => p.ContactoDireccion)
                    .HasForeignKey(d => d.Idcontacto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ContactoDireccion_Contacto");

                entity.HasOne(d => d.IddireccionNavigation)
                    .WithMany(p => p.ContactoDireccion)
                    .HasForeignKey(d => d.Iddireccion)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ContactoDireccion_Direccion");
            });

            modelBuilder.Entity<ContactoTelefono>(entity =>
            {
                entity.HasKey(e => new { e.Idcontacto, e.Idtelefono })
                    .HasName("PK_ContactoTelefono");

                entity.Property(e => e.Idcontacto).HasColumnName("IDContacto");

                entity.Property(e => e.Idtelefono).HasColumnName("IDTelefono");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IdcontactoNavigation)
                    .WithMany(p => p.ContactoTelefono)
                    .HasForeignKey(d => d.Idcontacto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ContactoTelefono_Contacto");

                entity.HasOne(d => d.Idcontacto1)
                    .WithMany(p => p.ContactoTelefono)
                    .HasForeignKey(d => d.Idcontacto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ContactoTelefono_Telefono");
            });

            modelBuilder.Entity<CorreoElectronico>(entity =>
            {
                entity.HasKey(e => e.IdcorreoElectronico)
                    .HasName("PK_CorreoElectronico");

                entity.Property(e => e.IdcorreoElectronico).HasColumnName("IDCorreoElectronico");

                entity.Property(e => e.Correo)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.IdtipoCorreoElectronico).HasColumnName("IDTipoCorreoElectronico");

                entity.HasOne(d => d.IdtipoCorreoElectronicoNavigation)
                    .WithMany(p => p.CorreoElectronico)
                    .HasForeignKey(d => d.IdtipoCorreoElectronico)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_CorreoElectronico_TipoCorreoElectronico");
            });

            modelBuilder.Entity<CuadreCaja>(entity =>
            {
                entity.HasKey(e => e.IdcuadreCaja)
                    .HasName("PK_CuadreCaja");

                entity.Property(e => e.IdcuadreCaja).HasColumnName("IDCuadreCaja");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.Idempresa).HasColumnName("IDEmpresa");

                entity.Property(e => e.Idmoneda)
                    .IsRequired()
                    .HasColumnName("IDMoneda")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Idsucursal).HasColumnName("IDSucursal");

                entity.Property(e => e.Idusuario).HasColumnName("IDUsuario");

                entity.Property(e => e.MontoCalculadoBoucher).HasColumnType("decimal");

                entity.Property(e => e.MontoCalculadoCheque).HasColumnType("decimal");

                entity.Property(e => e.MontoCalculadoEfectivo).HasColumnType("decimal");

                entity.Property(e => e.MontoContadoBoucher).HasColumnType("decimal");

                entity.Property(e => e.MontoContadoCheque).HasColumnType("decimal");

                entity.Property(e => e.MontoContadoEfectivo).HasColumnType("decimal");

                entity.HasOne(d => d.IdempresaNavigation)
                    .WithMany(p => p.CuadreCaja)
                    .HasForeignKey(d => d.Idempresa)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_CuadreCaja_Empresa");

                entity.HasOne(d => d.IdmonedaNavigation)
                    .WithMany(p => p.CuadreCaja)
                    .HasForeignKey(d => d.Idmoneda)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_CuadreCaja_Moneda");

                entity.HasOne(d => d.IdsucursalNavigation)
                    .WithMany(p => p.CuadreCaja)
                    .HasForeignKey(d => d.Idsucursal)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_CuadreCaja_Sucursal");

                entity.HasOne(d => d.IdusuarioNavigation)
                    .WithMany(p => p.CuadreCaja)
                    .HasForeignKey(d => d.Idusuario)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_CuadreCaja_Usuario");
            });

            modelBuilder.Entity<Devolucion>(entity =>
            {
                entity.HasKey(e => e.Iddevolucion)
                    .HasName("PK_Devolucion");

                entity.Property(e => e.Iddevolucion)
                    .HasColumnName("IDDevolucion")
                    .ValueGeneratedNever();

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.Idcliente).HasColumnName("IDCliente");

                entity.Property(e => e.Idempresa).HasColumnName("IDEmpresa");

                entity.Property(e => e.Idfactura).HasColumnName("IDFactura");

                entity.Property(e => e.Ncf)
                    .IsRequired()
                    .HasColumnName("NCF")
                    .HasColumnType("varchar(50)");

                entity.HasOne(d => d.IdclienteNavigation)
                    .WithMany(p => p.Devolucion)
                    .HasForeignKey(d => d.Idcliente)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Devolucion_Cliente");

                entity.HasOne(d => d.IdempresaNavigation)
                    .WithMany(p => p.Devolucion)
                    .HasForeignKey(d => d.Idempresa)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Devolucion_Empresa");

                entity.HasOne(d => d.IdfacturaNavigation)
                    .WithMany(p => p.Devolucion)
                    .HasForeignKey(d => d.Idfactura)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Devolucion_Factura");
            });

            modelBuilder.Entity<DevolucionProducto>(entity =>
            {
                entity.HasKey(e => new { e.Iddevolucion, e.Idproducto, e.Idalmacen })
                    .HasName("PK_DevolucionProducto");

                entity.Property(e => e.Iddevolucion).HasColumnName("IDDevolucion");

                entity.Property(e => e.Idproducto).HasColumnName("IDProducto");

                entity.Property(e => e.Idalmacen).HasColumnName("IDAlmacen");

                entity.Property(e => e.Descuento).HasColumnType("decimal");

                entity.Property(e => e.Impuesto).HasColumnType("decimal");

                entity.Property(e => e.Precio).HasColumnType("decimal");

                entity.HasOne(d => d.IdalmacenNavigation)
                    .WithMany(p => p.DevolucionProducto)
                    .HasForeignKey(d => d.Idalmacen)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_DevolucionProducto_Almacen");

                entity.HasOne(d => d.IddevolucionNavigation)
                    .WithMany(p => p.DevolucionProducto)
                    .HasForeignKey(d => d.Iddevolucion)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_DevolucionProducto_Devolucion");

                entity.HasOne(d => d.IdproductoNavigation)
                    .WithMany(p => p.DevolucionProducto)
                    .HasForeignKey(d => d.Idproducto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_DevolucionProducto_Producto");
            });

            modelBuilder.Entity<Direccion>(entity =>
            {
                entity.HasKey(e => e.Iddireccion)
                    .HasName("PK__Direccio__2BA9D3845F4B9F88");

                entity.Property(e => e.Iddireccion).HasColumnName("IDDireccion");

                entity.Property(e => e.Ciudad)
                    .IsRequired()
                    .HasColumnType("varchar(60)");

                entity.Property(e => e.CodigoPostal).HasColumnType("varchar(15)");

                entity.Property(e => e.DireccionLinea1)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.DireccionLinea2).HasColumnType("varchar(100)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.IdtipoDireccion).HasColumnName("IDTipoDireccion");

                entity.HasOne(d => d.IdtipoDireccionNavigation)
                    .WithMany(p => p.Direccion)
                    .HasForeignKey(d => d.IdtipoDireccion)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Direccion_TipoDireccion");
            });

            modelBuilder.Entity<Empleado>(entity =>
            {
                entity.HasKey(e => e.Idempleado)
                    .HasName("PK_Empleado");

                entity.Property(e => e.Idempleado).HasColumnName("IDEmpleado");

                entity.Property(e => e.Apellido)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Cargo)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaIngreso).HasColumnType("datetime");

                entity.Property(e => e.Idempresa).HasColumnName("IDEmpresa");

                entity.Property(e => e.Identificacion)
                    .IsRequired()
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.Idsucursal).HasColumnName("IDSucursal");

                entity.Property(e => e.IdtipoIdentificacion).HasColumnName("IDTipoIdentificacion");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.HasOne(d => d.IdempresaNavigation)
                    .WithMany(p => p.Empleado)
                    .HasForeignKey(d => d.Idempresa)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Empleado_Empresa");

                entity.HasOne(d => d.IdsucursalNavigation)
                    .WithMany(p => p.Empleado)
                    .HasForeignKey(d => d.Idsucursal)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Empleado_Sucursal");

                entity.HasOne(d => d.IdtipoIdentificacionNavigation)
                    .WithMany(p => p.Empleado)
                    .HasForeignKey(d => d.IdtipoIdentificacion)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Empleado_TipoIdentificacion");
            });

            modelBuilder.Entity<EmpleadoCorreoElectronico>(entity =>
            {
                entity.HasKey(e => new { e.IdcorreoElectronico, e.Idempleado })
                    .HasName("PK_EmpleadoCorreoElectronico");

                entity.Property(e => e.IdcorreoElectronico).HasColumnName("IDCorreoElectronico");

                entity.Property(e => e.Idempleado).HasColumnName("IDEmpleado");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IdcorreoElectronicoNavigation)
                    .WithMany(p => p.EmpleadoCorreoElectronico)
                    .HasForeignKey(d => d.IdcorreoElectronico)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_EmpleadoCorreoElectronico_CorreoElectronico");

                entity.HasOne(d => d.IdempleadoNavigation)
                    .WithMany(p => p.EmpleadoCorreoElectronico)
                    .HasForeignKey(d => d.Idempleado)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_EmpleadoCorreoElectronico_Empleado");
            });

            modelBuilder.Entity<EmpleadoDireccion>(entity =>
            {
                entity.HasKey(e => new { e.Idempleado, e.Iddireccion })
                    .HasName("PK_EmpleadoDireccion");

                entity.Property(e => e.Idempleado).HasColumnName("IDEmpleado");

                entity.Property(e => e.Iddireccion).HasColumnName("IDDireccion");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IddireccionNavigation)
                    .WithMany(p => p.EmpleadoDireccion)
                    .HasForeignKey(d => d.Iddireccion)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_EmpleadoDireccion_Direccion");

                entity.HasOne(d => d.IdempleadoNavigation)
                    .WithMany(p => p.EmpleadoDireccion)
                    .HasForeignKey(d => d.Idempleado)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_EmpleadoDireccion_Empleado");
            });

            modelBuilder.Entity<EmpleadoTelefono>(entity =>
            {
                entity.HasKey(e => new { e.Idempleado, e.Idtelefono })
                    .HasName("PK_EmpleadoTelefono");

                entity.Property(e => e.Idempleado).HasColumnName("IDEmpleado");

                entity.Property(e => e.Idtelefono).HasColumnName("IDTelefono");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IdempleadoNavigation)
                    .WithMany(p => p.EmpleadoTelefono)
                    .HasForeignKey(d => d.Idempleado)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_EmpleadoTelefono_Empleado");

                entity.HasOne(d => d.IdtelefonoNavigation)
                    .WithMany(p => p.EmpleadoTelefono)
                    .HasForeignKey(d => d.Idtelefono)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_EmpleadoTelefono_Telefono");
            });

            modelBuilder.Entity<Empresa>(entity =>
            {
                entity.HasKey(e => e.Idempresa)
                    .HasName("PK_Empresa");

                entity.Property(e => e.Idempresa).HasColumnName("IDEmpresa");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnType("varchar(500)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(120)");

                entity.Property(e => e.Rnc)
                    .IsRequired()
                    .HasColumnName("RNC")
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<Entrada>(entity =>
            {
                entity.HasKey(e => e.Identrada)
                    .HasName("PK_Entrada");

                entity.Property(e => e.Identrada).HasColumnName("IDEntrada");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.Idalmacen).HasColumnName("IDAlmacen");

                entity.Property(e => e.Idempresa).HasColumnName("IDEmpresa");

                entity.Property(e => e.Idsuplidor).HasColumnName("IDSuplidor");

                entity.Property(e => e.Idusuario).HasColumnName("IDUsuario");

                entity.HasOne(d => d.IdalmacenNavigation)
                    .WithMany(p => p.Entrada)
                    .HasForeignKey(d => d.Idalmacen)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Entrada_Almacen");

                entity.HasOne(d => d.IdempresaNavigation)
                    .WithMany(p => p.Entrada)
                    .HasForeignKey(d => d.Idempresa)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Entrada_Empresa");

                entity.HasOne(d => d.IdsuplidorNavigation)
                    .WithMany(p => p.Entrada)
                    .HasForeignKey(d => d.Idsuplidor)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Entrada_Suplidor");

                entity.HasOne(d => d.IdusuarioNavigation)
                    .WithMany(p => p.Entrada)
                    .HasForeignKey(d => d.Idusuario)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Entrada_Usuario");
            });

            modelBuilder.Entity<EntradaProducto>(entity =>
            {
                entity.HasKey(e => new { e.IdentradaProducto, e.Identrada, e.Idproducto })
                    .HasName("PK_EntradaProducto");

                entity.Property(e => e.IdentradaProducto)
                    .HasColumnName("IDEntradaProducto")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Identrada).HasColumnName("IDEntrada");

                entity.Property(e => e.Idproducto).HasColumnName("IDProducto");

                entity.Property(e => e.Compartimiento).HasColumnType("varchar(20)");

                entity.Property(e => e.Estante).HasColumnType("varchar(20)");

                entity.HasOne(d => d.IdentradaNavigation)
                    .WithMany(p => p.EntradaProducto)
                    .HasForeignKey(d => d.Identrada)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_EntradaProducto_Entrada");

                entity.HasOne(d => d.IdproductoNavigation)
                    .WithMany(p => p.EntradaProducto)
                    .HasForeignKey(d => d.Idproducto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_EntradaProducto_Producto");
            });

            modelBuilder.Entity<Factura>(entity =>
            {
                entity.HasKey(e => e.Idfactura)
                    .HasName("PK_Factura");

                entity.Property(e => e.Idfactura).HasColumnName("IDFactura");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.EstatusPago)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.Idcaja).HasColumnName("IDCaja");

                entity.Property(e => e.Idcliente).HasColumnName("IDCliente");

                entity.Property(e => e.Idempresa).HasColumnName("IDEmpresa");

                entity.Property(e => e.IdmodoPago)
                    .IsRequired()
                    .HasColumnName("IDModoPago")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Idsucursal).HasColumnName("IDSucursal");

                entity.Property(e => e.IdtipoPago)
                    .IsRequired()
                    .HasColumnName("IDTipoPago")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Idusuario).HasColumnName("IDUsuario");

                entity.Property(e => e.Itbis).HasColumnType("decimal");

                entity.Property(e => e.MontoBruto).HasColumnType("decimal");

                entity.Property(e => e.MontoDevuelta).HasColumnType("decimal");

                entity.Property(e => e.MontoNeto).HasColumnType("decimal");

                entity.Property(e => e.MontoRecibido).HasColumnType("decimal");

                entity.Property(e => e.MontoRestante).HasColumnType("decimal");

                entity.Property(e => e.MontoTarjeta).HasColumnType("decimal");

                entity.Property(e => e.Ncf)
                    .IsRequired()
                    .HasColumnName("NCF")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.NumeroAprobacionTarjeta).HasColumnType("varchar(25)");

                entity.HasOne(d => d.IdcajaNavigation)
                    .WithMany(p => p.Factura)
                    .HasForeignKey(d => d.Idcaja)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Factura_Caja");

                entity.HasOne(d => d.IdclienteNavigation)
                    .WithMany(p => p.Factura)
                    .HasForeignKey(d => d.Idcliente)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Factura_Cliente");

                entity.HasOne(d => d.IdempresaNavigation)
                    .WithMany(p => p.Factura)
                    .HasForeignKey(d => d.Idempresa)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Factura_Empresa");

                entity.HasOne(d => d.IdmodoPagoNavigation)
                    .WithMany(p => p.Factura)
                    .HasForeignKey(d => d.IdmodoPago)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Factura_ModoPago");

                entity.HasOne(d => d.IdsucursalNavigation)
                    .WithMany(p => p.Factura)
                    .HasForeignKey(d => d.Idsucursal)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Factura_Sucursal");

                entity.HasOne(d => d.IdtipoPagoNavigation)
                    .WithMany(p => p.Factura)
                    .HasForeignKey(d => d.IdtipoPago)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Factura_TipoPago");

                entity.HasOne(d => d.IdusuarioNavigation)
                    .WithMany(p => p.Factura)
                    .HasForeignKey(d => d.Idusuario)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Factura_Usuario");
            });

            modelBuilder.Entity<FacturaProducto>(entity =>
            {
                entity.HasKey(e => new { e.Idfactura, e.Idproducto })
                    .HasName("PK_FacturaProducto");

                entity.Property(e => e.Idfactura).HasColumnName("IDFactura");

                entity.Property(e => e.Idproducto).HasColumnName("IDProducto");

                entity.Property(e => e.Descuentos).HasColumnType("decimal");

                entity.Property(e => e.Itbis).HasColumnType("decimal");

                entity.Property(e => e.Precio).HasColumnType("decimal");

                entity.HasOne(d => d.IdfacturaNavigation)
                    .WithMany(p => p.FacturaProducto)
                    .HasForeignKey(d => d.Idfactura)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_FacturaProducto_Factura");

                entity.HasOne(d => d.IdproductoNavigation)
                    .WithMany(p => p.FacturaProducto)
                    .HasForeignKey(d => d.Idproducto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_FacturaProducto_Producto");
            });

            modelBuilder.Entity<HistorialCostoProducto>(entity =>
            {
                entity.HasKey(e => new { e.Idproducto, e.FechaInicio })
                    .HasName("PK_HistorialCostoProducto");

                entity.Property(e => e.Idproducto).HasColumnName("IDProducto");

                entity.Property(e => e.FechaInicio).HasColumnType("datetime");

                entity.Property(e => e.Costo).HasColumnType("decimal");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaFin).HasColumnType("datetime");

                entity.HasOne(d => d.IdproductoNavigation)
                    .WithMany(p => p.HistorialCostoProducto)
                    .HasForeignKey(d => d.Idproducto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_HistorialCostoProducto_Producto");
            });

            modelBuilder.Entity<HistorialPrecioProducto>(entity =>
            {
                entity.HasKey(e => new { e.Idproducto, e.FechaInicio })
                    .HasName("PK_HistorialPrecioProducto");

                entity.Property(e => e.Idproducto).HasColumnName("IDProducto");

                entity.Property(e => e.FechaInicio).HasColumnType("datetime");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaFin).HasColumnType("datetime");

                entity.Property(e => e.Precio).HasColumnType("decimal");

                entity.HasOne(d => d.IdproductoNavigation)
                    .WithMany(p => p.HistorialPrecioProducto)
                    .HasForeignKey(d => d.Idproducto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_HistorialPrecioProducto_Producto");
            });

            modelBuilder.Entity<Log>(entity =>
            {
                entity.HasKey(e => e.Idlog)
                    .HasName("PK_Log");

                entity.Property(e => e.Idlog).HasColumnName("IDLog");

                entity.Property(e => e.Comentario).HasColumnType("text");

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.Property(e => e.Idobjeto).HasColumnName("IDObjeto");

                entity.Property(e => e.Idrol).HasColumnName("IDRol");

                entity.Property(e => e.Idusuario).HasColumnName("IDUsuario");

                entity.HasOne(d => d.IdobjetoNavigation)
                    .WithMany(p => p.Log)
                    .HasForeignKey(d => d.Idobjeto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Log_Objeto");

                entity.HasOne(d => d.IdrolNavigation)
                    .WithMany(p => p.Log)
                    .HasForeignKey(d => d.Idrol)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Log_Rol");

                entity.HasOne(d => d.IdusuarioNavigation)
                    .WithMany(p => p.Log)
                    .HasForeignKey(d => d.Idusuario)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Log_Usuario");
            });

            modelBuilder.Entity<ModoPago>(entity =>
            {
                entity.HasKey(e => e.IdmodoPago)
                    .HasName("PK_ModoPago");

                entity.Property(e => e.IdmodoPago)
                    .HasColumnName("IDModoPago")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Moneda>(entity =>
            {
                entity.HasKey(e => e.Idmoneda)
                    .HasName("PK_Moneda");

                entity.Property(e => e.Idmoneda)
                    .HasColumnName("IDMoneda")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<NotaCredito>(entity =>
            {
                entity.HasKey(e => e.IdnotaCredito)
                    .HasName("PK_NotaCredito");

                entity.Property(e => e.IdnotaCredito).HasColumnName("IDNotaCredito");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.Idcliente).HasColumnName("IDCliente");

                entity.Property(e => e.Idsucursal).HasColumnName("IDSucursal");

                entity.Property(e => e.Idusuario).HasColumnName("IDUsuario");

                entity.Property(e => e.Ncf)
                    .IsRequired()
                    .HasColumnName("NCF")
                    .HasColumnType("varchar(40)");

                entity.HasOne(d => d.IdclienteNavigation)
                    .WithMany(p => p.NotaCredito)
                    .HasForeignKey(d => d.Idcliente)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_NotaCredito_Cliente");

                entity.HasOne(d => d.IdsucursalNavigation)
                    .WithMany(p => p.NotaCredito)
                    .HasForeignKey(d => d.Idsucursal)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_NotaCredito_Sucursal");

                entity.HasOne(d => d.IdusuarioNavigation)
                    .WithMany(p => p.NotaCredito)
                    .HasForeignKey(d => d.Idusuario)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_NotaCredito_Usuario");
            });

            modelBuilder.Entity<NotaCreditoFactura>(entity =>
            {
                entity.HasKey(e => new { e.IdnotaCredito, e.Idfactura })
                    .HasName("PK_NotaCreditoFactura");

                entity.Property(e => e.IdnotaCredito).HasColumnName("IDNotaCredito");

                entity.Property(e => e.Idfactura).HasColumnName("IDFactura");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Monto).HasColumnType("decimal");

                entity.Property(e => e.MontoRestante).HasColumnType("decimal");
            });

            modelBuilder.Entity<Objeto>(entity =>
            {
                entity.HasKey(e => e.Idobjeto)
                    .HasName("PK_Objetos");

                entity.Property(e => e.Idobjeto).HasColumnName("IDObjeto");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.IdobjetoRelacionado).HasColumnName("IDObjetoRelacionado");

                entity.Property(e => e.NombreFisico)
                    .IsRequired()
                    .HasColumnType("varchar(300)");

                entity.Property(e => e.NombreLogico)
                    .IsRequired()
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.TipoObjeto)
                    .IsRequired()
                    .HasColumnType("varchar(10)");

                entity.HasOne(d => d.IdobjetoRelacionadoNavigation)
                    .WithMany(p => p.InverseIdobjetoRelacionadoNavigation)
                    .HasForeignKey(d => d.IdobjetoRelacionado)
                    .HasConstraintName("FK_Objetos_Objetos");
            });

            modelBuilder.Entity<PoliticaSeguridad>(entity =>
            {
                entity.HasKey(e => e.Idpolitica)
                    .HasName("PK_PoliticaSeguridad");

                entity.Property(e => e.Idpolitica).HasColumnName("IDPolitica");
            });

            modelBuilder.Entity<Producto>(entity =>
            {
                entity.HasKey(e => e.Idproducto)
                    .HasName("PK_Producto");

                entity.Property(e => e.Idproducto).HasColumnName("IDProducto");

                entity.Property(e => e.CodigoProducto)
                    .IsRequired()
                    .HasColumnType("varchar(30)");

                entity.Property(e => e.CodigoUnidadMedidaPeso).HasColumnType("varchar(3)");

                entity.Property(e => e.CodigoUnidadMedidaSize).HasColumnType("varchar(3)");

                entity.Property(e => e.Color).HasColumnType("varchar(15)");

                entity.Property(e => e.Costo).HasColumnType("decimal");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaFinVenta).HasColumnType("datetime");

                entity.Property(e => e.FechaInicioVenta).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.Idempresa).HasColumnName("IDEmpresa");

                entity.Property(e => e.IdsubcategoriaProducto).HasColumnName("IDSubcategoriaProducto");

                entity.Property(e => e.IdtipoProducto).HasColumnName("IDTipoProducto");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Peso).HasColumnType("decimal");

                entity.Property(e => e.Precio).HasColumnType("decimal");

                entity.Property(e => e.Size).HasColumnType("varchar(5)");

                entity.HasOne(d => d.CodigoUnidadMedidaPesoNavigation)
                    .WithMany(p => p.ProductoCodigoUnidadMedidaPesoNavigation)
                    .HasForeignKey(d => d.CodigoUnidadMedidaPeso)
                    .HasConstraintName("FK_Producto_UnidadMedida1");

                entity.HasOne(d => d.CodigoUnidadMedidaSizeNavigation)
                    .WithMany(p => p.ProductoCodigoUnidadMedidaSizeNavigation)
                    .HasForeignKey(d => d.CodigoUnidadMedidaSize)
                    .HasConstraintName("FK_Producto_UnidadMedida");

                entity.HasOne(d => d.IdempresaNavigation)
                    .WithMany(p => p.Producto)
                    .HasForeignKey(d => d.Idempresa)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Producto_Empresa");

                entity.HasOne(d => d.IdsubcategoriaProductoNavigation)
                    .WithMany(p => p.Producto)
                    .HasForeignKey(d => d.IdsubcategoriaProducto)
                    .HasConstraintName("FK_Producto_SubcategoriaProducto");

                entity.HasOne(d => d.IdtipoProductoNavigation)
                    .WithMany(p => p.Producto)
                    .HasForeignKey(d => d.IdtipoProducto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Producto_TipoProducto");
            });

            modelBuilder.Entity<ProductoExistencia>(entity =>
            {
                entity.HasKey(e => new { e.Idproducto, e.AlmacenId })
                    .HasName("PK_ProductoExistencia");

                entity.Property(e => e.Idproducto).HasColumnName("IDProducto");

                entity.Property(e => e.AlmacenId).HasColumnName("AlmacenID");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.HasOne(d => d.Almacen)
                    .WithMany(p => p.ProductoExistencia)
                    .HasForeignKey(d => d.AlmacenId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ProductoExistencia_Almacen");

                entity.HasOne(d => d.IdproductoNavigation)
                    .WithMany(p => p.ProductoExistencia)
                    .HasForeignKey(d => d.Idproducto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ProductoExistencia_Producto");
            });

            modelBuilder.Entity<Recibo>(entity =>
            {
                entity.HasKey(e => e.Idrecibo)
                    .HasName("PK_Recibo");

                entity.Property(e => e.Idrecibo).HasColumnName("IDRecibo");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.Idcaja).HasColumnName("IDCaja");

                entity.Property(e => e.Idcliente).HasColumnName("IDCliente");

                entity.Property(e => e.Idsucursal).HasColumnName("IDSucursal");

                entity.HasOne(d => d.IdcajaNavigation)
                    .WithMany(p => p.Recibo)
                    .HasForeignKey(d => d.Idcaja)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Recibo_Caja");

                entity.HasOne(d => d.IdclienteNavigation)
                    .WithMany(p => p.Recibo)
                    .HasForeignKey(d => d.Idcliente)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Recibo_Cliente");

                entity.HasOne(d => d.IdsucursalNavigation)
                    .WithMany(p => p.Recibo)
                    .HasForeignKey(d => d.Idsucursal)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Recibo_Sucursal");
            });

            modelBuilder.Entity<ReciboFactura>(entity =>
            {
                entity.HasKey(e => new { e.Idrecibo, e.Idfactura })
                    .HasName("PK_ReciboFactura");

                entity.Property(e => e.Idrecibo).HasColumnName("IDRecibo");

                entity.Property(e => e.Idfactura).HasColumnName("IDFactura");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Monto).HasColumnType("decimal");

                entity.Property(e => e.MontoRestante).HasColumnType("decimal");

                entity.HasOne(d => d.IdfacturaNavigation)
                    .WithMany(p => p.ReciboFactura)
                    .HasForeignKey(d => d.Idfactura)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ReciboFactura_Factura");

                entity.HasOne(d => d.IdreciboNavigation)
                    .WithMany(p => p.ReciboFactura)
                    .HasForeignKey(d => d.Idrecibo)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ReciboFactura_Recibo");
            });

            modelBuilder.Entity<ReciboPago>(entity =>
            {
                entity.HasKey(e => e.IdreciboPago)
                    .HasName("PK_ReciboPago");

                entity.Property(e => e.IdreciboPago).HasColumnName("IDReciboPago");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Idrecibo).HasColumnName("IDRecibo");

                entity.Property(e => e.IdtipoPago)
                    .IsRequired()
                    .HasColumnName("IDTipoPago")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Monto).HasColumnType("decimal");

                entity.HasOne(d => d.IdreciboNavigation)
                    .WithMany(p => p.ReciboPago)
                    .HasForeignKey(d => d.Idrecibo)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ReciboPago_Recibo");

                entity.HasOne(d => d.IdtipoPagoNavigation)
                    .WithMany(p => p.ReciboPago)
                    .HasForeignKey(d => d.IdtipoPago)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_ReciboPago_TipoPago");
            });

            modelBuilder.Entity<Rol>(entity =>
            {
                entity.HasKey(e => e.Idrol)
                    .HasName("PK_Rol");

                entity.Property(e => e.Idrol).HasColumnName("IDRol");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnType("varchar(500)");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.RutaPrincipal)
                    .IsRequired()
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<RolObjeto>(entity =>
            {
                entity.HasKey(e => new { e.Idrol, e.Idobjeto })
                    .HasName("PK_Rol_Objeto");

                entity.Property(e => e.Idrol).HasColumnName("IDRol");

                entity.Property(e => e.Idobjeto).HasColumnName("IDObjeto");

                entity.HasOne(d => d.IdobjetoNavigation)
                    .WithMany(p => p.RolObjeto)
                    .HasForeignKey(d => d.Idobjeto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Rol_Objeto_Objeto");

                entity.HasOne(d => d.IdrolNavigation)
                    .WithMany(p => p.RolObjeto)
                    .HasForeignKey(d => d.Idrol)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_RolObjeto_Rol");
            });

            modelBuilder.Entity<SubcategoriaProducto>(entity =>
            {
                entity.HasKey(e => e.IdsubcategoriaProducto)
                    .HasName("PK_SubcategoriaProducto");

                entity.Property(e => e.IdsubcategoriaProducto).HasColumnName("IDSubcategoriaProducto");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.IdcategoriaProducto).HasColumnName("IDCategoriaProducto");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.HasOne(d => d.IdcategoriaProductoNavigation)
                    .WithMany(p => p.SubcategoriaProducto)
                    .HasForeignKey(d => d.IdcategoriaProducto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SubcategoriaProducto_CategoriaProducto");
            });

            modelBuilder.Entity<Sucursal>(entity =>
            {
                entity.HasKey(e => e.Idsucursal)
                    .HasName("PK_Sucursal");

                entity.Property(e => e.Idsucursal).HasColumnName("IDSucursal");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.Idempresa).HasColumnName("IDEmpresa");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                entity.HasOne(d => d.IdempresaNavigation)
                    .WithMany(p => p.Sucursal)
                    .HasForeignKey(d => d.Idempresa)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Sucursal_Empresa");
            });

            modelBuilder.Entity<SucursalContacto>(entity =>
            {
                entity.HasKey(e => new { e.Idsucursal, e.Idcontacto })
                    .HasName("PK_SucursalContacto");

                entity.Property(e => e.Idsucursal).HasColumnName("IDSucursal");

                entity.Property(e => e.Idcontacto).HasColumnName("IDContacto");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IdcontactoNavigation)
                    .WithMany(p => p.SucursalContacto)
                    .HasForeignKey(d => d.Idcontacto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SucursalContacto_Contacto");

                entity.HasOne(d => d.IdsucursalNavigation)
                    .WithMany(p => p.SucursalContacto)
                    .HasForeignKey(d => d.Idsucursal)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SucursalContacto_Sucursal");
            });

            modelBuilder.Entity<SucursalDireccion>(entity =>
            {
                entity.HasKey(e => new { e.Idsucursal, e.Iddireccion })
                    .HasName("PK_SucursalDireccion");

                entity.Property(e => e.Idsucursal).HasColumnName("IDSucursal");

                entity.Property(e => e.Iddireccion).HasColumnName("IDDireccion");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IddireccionNavigation)
                    .WithMany(p => p.SucursalDireccion)
                    .HasForeignKey(d => d.Iddireccion)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SucursalDireccion_Direccion");

                entity.HasOne(d => d.IdsucursalNavigation)
                    .WithMany(p => p.SucursalDireccion)
                    .HasForeignKey(d => d.Idsucursal)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SucursalDireccion_Sucursal");
            });

            modelBuilder.Entity<SucursalTelefono>(entity =>
            {
                entity.HasKey(e => new { e.Idsucursal, e.Idtelefono })
                    .HasName("PK_SucursalTelefono");

                entity.Property(e => e.Idsucursal).HasColumnName("IDSucursal");

                entity.Property(e => e.Idtelefono).HasColumnName("IDTelefono");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IdsucursalNavigation)
                    .WithMany(p => p.SucursalTelefono)
                    .HasForeignKey(d => d.Idsucursal)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SucursalTelefono_Sucursal");

                entity.HasOne(d => d.IdtelefonoNavigation)
                    .WithMany(p => p.SucursalTelefono)
                    .HasForeignKey(d => d.Idtelefono)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SucursalTelefono_Telefono");
            });

            modelBuilder.Entity<Suplidor>(entity =>
            {
                entity.HasKey(e => e.Idsuplidor)
                    .HasName("PK_Suplidor");

                entity.Property(e => e.Idsuplidor).HasColumnName("IDSuplidor");

                entity.Property(e => e.Comentario).HasColumnType("varchar(200)");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Idempresa).HasColumnName("IDEmpresa");

                entity.Property(e => e.Identificacion)
                    .IsRequired()
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.IdtipoIdentificacion).HasColumnName("IDTipoIdentificacion");

                entity.Property(e => e.IdtipoSuplidor).HasColumnName("IDTipoSuplidor");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                entity.HasOne(d => d.IdempresaNavigation)
                    .WithMany(p => p.Suplidor)
                    .HasForeignKey(d => d.Idempresa)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Suplidor_Empresa");

                entity.HasOne(d => d.IdtipoIdentificacionNavigation)
                    .WithMany(p => p.Suplidor)
                    .HasForeignKey(d => d.IdtipoIdentificacion)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Suplidor_TipoIdentificacion");

                entity.HasOne(d => d.IdtipoSuplidorNavigation)
                    .WithMany(p => p.Suplidor)
                    .HasForeignKey(d => d.IdtipoSuplidor)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Suplidor_TipoSuplidor");
            });

            modelBuilder.Entity<SuplidorContacto>(entity =>
            {
                entity.HasKey(e => new { e.Idsuplidor, e.Idcontacto })
                    .HasName("PK_SuplidorContacto");

                entity.Property(e => e.Idsuplidor).HasColumnName("IDSuplidor");

                entity.Property(e => e.Idcontacto).HasColumnName("IDContacto");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IdcontactoNavigation)
                    .WithMany(p => p.SuplidorContacto)
                    .HasForeignKey(d => d.Idcontacto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SuplidorContacto_Contacto");

                entity.HasOne(d => d.IdsuplidorNavigation)
                    .WithMany(p => p.SuplidorContacto)
                    .HasForeignKey(d => d.Idsuplidor)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SuplidorContacto_Suplidor");
            });

            modelBuilder.Entity<SuplidorCorreoElectronico>(entity =>
            {
                entity.HasKey(e => new { e.Idsuplidor, e.IdcorreoElectronico })
                    .HasName("PK_SuplidorCorreoElectronico");

                entity.Property(e => e.Idsuplidor).HasColumnName("IDSuplidor");

                entity.Property(e => e.IdcorreoElectronico).HasColumnName("IDCorreoElectronico");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IdcorreoElectronicoNavigation)
                    .WithMany(p => p.SuplidorCorreoElectronico)
                    .HasForeignKey(d => d.IdcorreoElectronico)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SuplidorCorreoElectronico_CorreoElectronico");

                entity.HasOne(d => d.IdsuplidorNavigation)
                    .WithMany(p => p.SuplidorCorreoElectronico)
                    .HasForeignKey(d => d.Idsuplidor)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SuplidorCorreoElectronico_Suplidor");
            });

            modelBuilder.Entity<SuplidorDireccion>(entity =>
            {
                entity.HasKey(e => new { e.Idsuplidor, e.Iddireccion })
                    .HasName("PK_SuplidorDireccion");

                entity.Property(e => e.Idsuplidor).HasColumnName("IDSuplidor");

                entity.Property(e => e.Iddireccion).HasColumnName("IDDireccion");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IddireccionNavigation)
                    .WithMany(p => p.SuplidorDireccion)
                    .HasForeignKey(d => d.Iddireccion)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SuplidorDireccion_Direccion");

                entity.HasOne(d => d.IdsuplidorNavigation)
                    .WithMany(p => p.SuplidorDireccion)
                    .HasForeignKey(d => d.Idsuplidor)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SuplidorDireccion_Suplidor");
            });

            modelBuilder.Entity<SuplidorTelefono>(entity =>
            {
                entity.HasKey(e => new { e.Idsuplidor, e.Idtelefono })
                    .HasName("PK_SuplidorTelefono");

                entity.Property(e => e.Idsuplidor).HasColumnName("IDSuplidor");

                entity.Property(e => e.Idtelefono).HasColumnName("IDTelefono");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IdsuplidorNavigation)
                    .WithMany(p => p.SuplidorTelefono)
                    .HasForeignKey(d => d.Idsuplidor)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SuplidorTelefono_Suplidor");

                entity.HasOne(d => d.IdtelefonoNavigation)
                    .WithMany(p => p.SuplidorTelefono)
                    .HasForeignKey(d => d.Idtelefono)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_SuplidorTelefono_Telefono");
            });

            modelBuilder.Entity<Tanda>(entity =>
            {
                entity.HasKey(e => e.Idtanda)
                    .HasName("PK_Tanda");

                entity.Property(e => e.Idtanda)
                    .HasColumnName("IDTanda")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Estatus).HasColumnType("varchar(3)");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<Telefono>(entity =>
            {
                entity.HasKey(e => e.Idtelefono)
                    .HasName("PK_Telefono");

                entity.Property(e => e.Idtelefono).HasColumnName("IDTelefono");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.IdtipoTelefono).HasColumnName("IDTipoTelefono");

                entity.Property(e => e.Numero)
                    .IsRequired()
                    .HasColumnType("varchar(20)");

                entity.HasOne(d => d.IdtipoTelefonoNavigation)
                    .WithMany(p => p.Telefono)
                    .HasForeignKey(d => d.IdtipoTelefono)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Telefono_TipoTelefono");
            });

            modelBuilder.Entity<TipoAjuste>(entity =>
            {
                entity.HasKey(e => e.IdtipoAjuste)
                    .HasName("PK_TipoAjuste");

                entity.Property(e => e.IdtipoAjuste).HasColumnName("IDTipoAjuste");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");
            });

            modelBuilder.Entity<TipoCliente>(entity =>
            {
                entity.HasKey(e => e.IdtipoCliente)
                    .HasName("PK_TipoCliente");

                entity.Property(e => e.IdtipoCliente).HasColumnName("IDTipoCliente");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<TipoComprobante>(entity =>
            {
                entity.HasKey(e => e.IdtipoComprobante)
                    .HasName("PK_TipoComprobante");

                entity.Property(e => e.IdtipoComprobante)
                    .HasColumnName("IDTipoComprobante")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<TipoContacto>(entity =>
            {
                entity.HasKey(e => e.IdtipoContacto)
                    .HasName("PK_TipoContacto");

                entity.Property(e => e.IdtipoContacto)
                    .HasColumnName("IDTipoContacto")
                    .ValueGeneratedNever();

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<TipoCorreoElectronico>(entity =>
            {
                entity.HasKey(e => e.IdtipoCorreoElectronico)
                    .HasName("PK_TipoCorreoElectronico");

                entity.Property(e => e.IdtipoCorreoElectronico).HasColumnName("IDTipoCorreoElectronico");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.Nombre).HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<TipoDireccion>(entity =>
            {
                entity.HasKey(e => e.IdtipoDireccion)
                    .HasName("PK_TipoDireccion");

                entity.Property(e => e.IdtipoDireccion).HasColumnName("IDTipoDireccion");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(60)");
            });

            modelBuilder.Entity<TipoIdentificacion>(entity =>
            {
                entity.HasKey(e => e.IdtipoIdentificacion)
                    .HasName("PK_TipoIdentificacion");

                entity.Property(e => e.IdtipoIdentificacion).HasColumnName("IDTipoIdentificacion");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<TipoPago>(entity =>
            {
                entity.HasKey(e => e.IdtipoPago)
                    .HasName("PK_TipoPago");

                entity.Property(e => e.IdtipoPago)
                    .HasColumnName("IDTipoPago")
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<TipoProducto>(entity =>
            {
                entity.HasKey(e => e.IdtipoProducto)
                    .HasName("PK_TipoProducto");

                entity.Property(e => e.IdtipoProducto).HasColumnName("IDTipoProducto");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<TipoSuplidor>(entity =>
            {
                entity.HasKey(e => e.IdtipoSuplidor)
                    .HasName("PK_TipoSuplidor");

                entity.Property(e => e.IdtipoSuplidor).HasColumnName("IDTipoSuplidor");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<TipoTelefono>(entity =>
            {
                entity.HasKey(e => e.IdtipoTelefono)
                    .HasName("PK_TipoTelefono");

                entity.Property(e => e.IdtipoTelefono).HasColumnName("IDTipoTelefono");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<Transferencia>(entity =>
            {
                entity.HasKey(e => e.Idtransferencia)
                    .HasName("PK_Transferencia");

                entity.Property(e => e.Idtransferencia).HasColumnName("IDTransferencia");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaRecibe).HasColumnType("datetime");

                entity.Property(e => e.FechaSolicita).HasColumnType("datetime");

                entity.Property(e => e.IdalmacenDestino).HasColumnName("IDAlmacenDestino");

                entity.Property(e => e.IdalmacenOrigen).HasColumnName("IDAlmacenOrigen");

                entity.Property(e => e.IdusuarioRecibe).HasColumnName("IDUsuarioRecibe");

                entity.Property(e => e.IdusuarioSolicita).HasColumnName("IDUsuarioSolicita");

                entity.HasOne(d => d.IdalmacenDestinoNavigation)
                    .WithMany(p => p.TransferenciaIdalmacenDestinoNavigation)
                    .HasForeignKey(d => d.IdalmacenDestino)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Transferencia_AlmacenDestino");

                entity.HasOne(d => d.IdalmacenOrigenNavigation)
                    .WithMany(p => p.TransferenciaIdalmacenOrigenNavigation)
                    .HasForeignKey(d => d.IdalmacenOrigen)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Transferencia_AlmacenOrigen");

                entity.HasOne(d => d.IdusuarioRecibeNavigation)
                    .WithMany(p => p.TransferenciaIdusuarioRecibeNavigation)
                    .HasForeignKey(d => d.IdusuarioRecibe)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Transferencia_UsuarioRecibe");

                entity.HasOne(d => d.IdusuarioSolicitaNavigation)
                    .WithMany(p => p.TransferenciaIdusuarioSolicitaNavigation)
                    .HasForeignKey(d => d.IdusuarioSolicita)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Transferencia_UsuarioSolicita");
            });

            modelBuilder.Entity<TransferenciaProducto>(entity =>
            {
                entity.HasKey(e => e.IdtransferenciaProducto)
                    .HasName("PK_TransferenciaProducto");

                entity.Property(e => e.IdtransferenciaProducto).HasColumnName("IDTransferenciaProducto");

                entity.Property(e => e.Comentario)
                    .IsRequired()
                    .HasColumnType("varchar(500)");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.Idproducto).HasColumnName("IDProducto");

                entity.Property(e => e.Idtransferencia).HasColumnName("IDTransferencia");

                entity.HasOne(d => d.IdproductoNavigation)
                    .WithMany(p => p.TransferenciaProducto)
                    .HasForeignKey(d => d.Idproducto)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_TransferenciaProducto_Producto");

                entity.HasOne(d => d.IdtransferenciaNavigation)
                    .WithMany(p => p.TransferenciaProducto)
                    .HasForeignKey(d => d.Idtransferencia)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_TransferenciaProducto_Transferencia");
            });

            modelBuilder.Entity<UnidadMedida>(entity =>
            {
                entity.HasKey(e => e.CodigoUnidad)
                    .HasName("PK_UnidadMedida");

                entity.Property(e => e.CodigoUnidad).HasColumnType("varchar(3)");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.FechaCreacion).HasColumnType("datetime");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(60)");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(e => e.Idusuario)
                    .HasName("PK_Usuario");

                entity.Property(e => e.Idusuario).HasColumnName("IDUsuario");

                entity.Property(e => e.Correo).HasColumnType("varchar(150)");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaBloqueo).HasColumnType("date");

                entity.Property(e => e.FechaCambioClave).HasColumnType("datetime");

                entity.Property(e => e.Idioma).HasColumnType("varchar(10)");

                entity.Property(e => e.Nombre).HasColumnType("varchar(100)");

                entity.Property(e => e.PasswordHash)
                    .IsRequired()
                    .HasColumnType("varchar(max)");

                entity.Property(e => e.Salt)
                    .IsRequired()
                    .HasColumnType("varchar(max)");

                entity.Property(e => e.Telefono).HasColumnType("varchar(50)");

                entity.Property(e => e.Token)
                    .IsRequired()
                    .HasColumnType("varchar(max)");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnType("varchar(150)");
            });

            modelBuilder.Entity<UsuarioEmpleado>(entity =>
            {
                entity.HasKey(e => new { e.Idusuario, e.Idempleado })
                    .HasName("PK_UsuarioEmpleado");

                entity.Property(e => e.Idusuario).HasColumnName("IDUsuario");

                entity.Property(e => e.Idempleado).HasColumnName("IDEmpleado");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IdempleadoNavigation)
                    .WithMany(p => p.UsuarioEmpleado)
                    .HasForeignKey(d => d.Idempleado)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UsuarioEmpleado_Empleado");

                entity.HasOne(d => d.IdusuarioNavigation)
                    .WithMany(p => p.UsuarioEmpleado)
                    .HasForeignKey(d => d.Idusuario)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UsuarioEmpleado_Usuario");
            });

            modelBuilder.Entity<UsuarioRol>(entity =>
            {
                entity.HasKey(e => new { e.Idusuario, e.Idrol })
                    .HasName("PK_UsuarioRol");

                entity.Property(e => e.Idusuario).HasColumnName("IDUsuario");

                entity.Property(e => e.Idrol).HasColumnName("IDRol");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.HasOne(d => d.IdrolNavigation)
                    .WithMany(p => p.UsuarioRol)
                    .HasForeignKey(d => d.Idrol)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Usuario_Rol_Rol");

                entity.HasOne(d => d.IdusuarioNavigation)
                    .WithMany(p => p.UsuarioRol)
                    .HasForeignKey(d => d.Idusuario)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Usuario_Rol_Usuario");
            });

            modelBuilder.Entity<UsuarioToken>(entity =>
            {
                entity.HasKey(e => e.IdusuarioToken)
                    .HasName("PK_UsuarioToken");

                entity.Property(e => e.IdusuarioToken).HasColumnName("IDUsuarioToken");

                entity.Property(e => e.Canal)
                    .IsRequired()
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasColumnType("varchar(3)");

                entity.Property(e => e.FechaExpiracion).HasColumnType("datetime");

                entity.Property(e => e.FechaInicio).HasColumnType("datetime");

                entity.Property(e => e.FechaSalida).HasColumnType("datetime");

                entity.Property(e => e.Idusuario).HasColumnName("IDUsuario");

                entity.Property(e => e.Token)
                    .IsRequired()
                    .HasColumnType("text");

                entity.HasOne(d => d.IdusuarioNavigation)
                    .WithMany(p => p.UsuarioToken)
                    .HasForeignKey(d => d.Idusuario)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UsuarioToken_Usuario");
            });
        }
    }
}